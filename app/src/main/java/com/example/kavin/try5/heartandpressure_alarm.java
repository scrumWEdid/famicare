package com.example.kavin.try5;

import android.database.Cursor;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class heartandpressure_alarm extends AppCompatActivity {
    private DrawerLayout mDrawerLayout;
    private localDatabase localDB = null;
    private Cursor cursor = null;
    private final static  String RECORD_TYPE = "bloodsugar";
    private EditText heartbeat_top, heartbeat_bottom;
    private EditText tightvalue, relaxvalue;
    private Button submitBtn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_heartandpressure_alarm);

        BottomNavigationView bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottom_navigation_view);
        bottomNavigationView.setSelectedItemId(R.id.nav_description);
        bottomNavigationView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        //local dasabase
        localDB = new localDatabase(this);
        localDB.open();

        //----介面部分
        toolbar mytoolbar = new toolbar(this);
        mDrawerLayout = findViewById(R.id.drawer_layout);
        //介面部分----

        tightvalue = findViewById(R.id.tight_value);
        relaxvalue = findViewById(R.id.relax_value);
        heartbeat_top = findViewById(R.id.heartbeat_top);
        heartbeat_bottom = findViewById(R.id.heartbeat_bottom);

        submitBtn = findViewById(R.id.submitBtn);
        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String tight_pressure = tightvalue.getText().toString();
                String relax_pressure = relaxvalue.getText().toString();
                String heart_top = heartbeat_top.getText().toString();
                String heart_bottom = heartbeat_bottom.getText().toString();
                if(tight_pressure.equals("")) {
                    tight_pressure = "140";
                }
                if(relax_pressure.equals("")){
                    relax_pressure = "90";
                }
                if(heart_top.equals(""))
                    heart_top = "100";
                if(heart_bottom.equals(""))
                    heart_bottom = "60";

                localDB.update_alert(heart_top, heart_bottom, "heartbeat");
                localDB.update_alert(tight_pressure, "0", "tight_pressure");
                localDB.update_alert(relax_pressure, "0", "relax_pressure");
                setResult(RESULT_OK);
                finish();
            }
        });

        cursor = localDB.get_alert("heartbeat");
        if(cursor.getCount() > 0) {
            heartbeat_top.setText(String.valueOf(cursor.getDouble(1)));
            heartbeat_bottom.setText(String.valueOf(cursor.getDouble(2)));
        }

        cursor = localDB.get_alert("tight_pressure");
        if(cursor.getCount() > 0) {
            tightvalue.setText(String.valueOf(cursor.getDouble(1)));
        }
        cursor = localDB.get_alert("relax_pressure");
        if(cursor.getCount() > 0) {
            relaxvalue.setText(String.valueOf(cursor.getDouble(1)));
        }
    }

    //close DB
    @Override
    protected void onDestroy() {
        super.onDestroy();
        localDB.close();
    }
    //--介面部分
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
//                mDrawerLayout.openDrawer(Gravity.RIGHT);
                heartandpressure_alarm.this.finish();
                return true;
            case R.id.user_info:
                mDrawerLayout.openDrawer(Gravity.RIGHT);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.game_menu, menu);
        //set pattent name of menu
        MenuItem patientname = menu.findItem(R.id.patient_name);
        patientname.setTitle(localDB.getpatientname());
        return true;
    }
//    介面部分--

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected( MenuItem item) {
            BottomNavigation bn = new BottomNavigation(heartandpressure_alarm.this, item.getItemId());
            return true;
        }

    };
}
