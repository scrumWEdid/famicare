package com.example.kavin.try5;

public class Type {
    private String item_date;
    private String item_time;
    private String item_data1;
    private String item_data2;
    private String item_data3;
    private String flag;
    public String toString(){
        return "item_date : " + item_date + "\nitem_time : " + item_time + "\nitem_data1 : " + item_data1 +
                "\nitem_data2 : " + item_data2 + "\nitem_data3 : " + item_data3 ;
    }
    public Type(String item_date, String item_time, String item_data1, String item_data2, String item_data3,String flag) {
        this.item_date = item_date;
        this.item_time = item_time;
        this.item_data1 = item_data1;
        this.item_data2 = item_data2;
        this.item_data3 = item_data3;
        this.flag = flag;


    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getItem_date() {
        return item_date;
    }

    public void setItem_date(String item_date) {
        this.item_date = item_date;
    }

    public String getItem_time() {
        return item_time;
    }

    public void setItem_time(String item_time) {
        this.item_time = item_time;
    }

    public String getItem_data1() {
        return item_data1;
    }

    public void setItem_data1(String item_data1) {
        this.item_data1 = item_data1;
    }

    public String getItem_data2() {
        return item_data2;
    }

    public void setItem_data2(String item_data2) {
        this.item_data2 = item_data2;
    }

    public String getItem_data3() {
        return item_data3;
    }

    public void setItem_data3(String item_data3) {
        this.item_data3 = item_data3;
    }



}
