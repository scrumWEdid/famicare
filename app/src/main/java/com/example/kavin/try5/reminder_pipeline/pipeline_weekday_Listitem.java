package com.example.kavin.try5.reminder_pipeline;

import org.json.JSONException;
import org.json.JSONObject;

public class pipeline_weekday_Listitem {

    private String reminder_pipeline_time;
    private String reminder_pipeline_name;
    private JSONObject reminder_pipeline_info;
    public pipeline_weekday_Listitem(String reminder_pipeline_time, String reminder_pipeline_name, JSONObject reminder_pipeline_info) {
        this.reminder_pipeline_time = reminder_pipeline_time;
        this.reminder_pipeline_name = reminder_pipeline_name;
        this.reminder_pipeline_info = reminder_pipeline_info;
    }

    public String getReminder_pipeline_time() {
        return reminder_pipeline_time;
    }

    public String getReminder_pipeline_name() {
        return reminder_pipeline_name;
    }

    public JSONObject getReminder_pipeline_info() {
        return reminder_pipeline_info;
    }

    public String getReminder_pipeline_id() throws JSONException {return reminder_pipeline_info.getString("pipelineReminderID");}

}