package com.example.kavin.try5;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.kavin.try5.reminder_measure.reminder_measure_weekday;
import com.example.kavin.try5.reminder_medicine.reminder_medicine_weekday;
import com.example.kavin.try5.reminder_pipeline.reminder_pipeline_weekday;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import static com.facebook.accountkit.internal.AccountKitController.getApplicationContext;

public class reminder_mainPage2 extends Fragment {
    private localDatabase localDB = null;
    //slide
    ViewPager viewPager;
    LinearLayout sliderDotspanel;
    private int dotscount;
    private ImageView[] dots;
    private static String  URL_MEDICINE_SLIDE = "http://140.117.71.74/graduation/fetch_allreminder.php";
    private JSONArray record = new JSONArray();
    private JSONObject record_all = new JSONObject();
    private String day[] = {"","",""};
    private String hour[] = {"","",""};
    private String minute[] = {"","",""};
    private String name[] = {"","",""};
    public static String time [] ={"","",""};
    public View onCreateView(LayoutInflater inflater , ViewGroup container , Bundle savedInstanceState ){
        localDB = new localDatabase(getContext());
        localDB.open();

        View rootView = inflater.inflate(R.layout.activity_reminder_main_page2,container,false);
        TextView medicineTxt = rootView.findViewById(R.id.medicine);
        medicineTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setClass(getActivity(), reminder_medicine_weekday.class);
                startActivity(intent);
            }
        });
        TextView measureTxt = rootView.findViewById(R.id.record);
        measureTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setClass(getActivity(), reminder_measure_weekday.class);
                startActivity(intent);
            }
        });
        TextView pipelineTxt = rootView.findViewById(R.id.pipeline);
        pipelineTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setClass(getActivity(), reminder_pipeline_weekday.class);
                startActivity(intent);
            }
        });

        ///slide
        fetch_medicine(localDB.getpatientinfo());
        viewPager = (ViewPager) rootView.findViewById(R.id.viewPager);

        sliderDotspanel = (LinearLayout) rootView.findViewById(R.id.SliderDots);

        final ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(getActivity());

        viewPager.setAdapter(viewPagerAdapter);

        dotscount = viewPagerAdapter.getCount();
        dots = new ImageView[dotscount];


        for(int i = 0; i < dotscount; i++){

            dots[i] = new ImageView(getActivity());
            dots[i].setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.nonactive_dot));

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);

            params.setMargins(8, 0, 8, 0);

            sliderDotspanel.addView(dots[i], params);
        }

        dots[0].setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.active_dot));

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                for(int i = 0; i< dotscount; i++){
                    dots[i].setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.nonactive_dot));
                }
                viewPagerAdapter.notifyDataSetChanged();
                dots[position].setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.active_dot));

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        Timer timer = new Timer();
        timer.scheduleAtFixedRate(new MyTimerTask(), 2000, 10000);

        return rootView;
    }

    public class MyTimerTask extends TimerTask {

        @Override
        public void run() {
            if(getActivity() == null)
                return;
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    if(viewPager.getCurrentItem() == 0){
                        viewPager.setCurrentItem(1);
                    } else if(viewPager.getCurrentItem() == 1){
                        viewPager.setCurrentItem(2);
                    } else {
                        viewPager.setCurrentItem(0);
                    }

                }
            });

        }
    }
    private void fetch_medicine( final String patientID) {

        record = new JSONArray();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL_MEDICINE_SLIDE,
                new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            String success =jsonObject.getString("success");
                            String message = jsonObject.getString("message");
                            if (success.equals("1")){
                                record = jsonObject.getJSONArray("record");
//                                Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                                try {
                                    for (int i = 0 ; i <3 ; i ++){
                                        record_all = record.getJSONObject(i);
                                        day[i] = record_all.getString("day");
                                        hour[i] = record_all.getString("hour");
                                        minute[i] = record_all.getString("minute");
                                        name[i] = record_all.getString("name");
                                        if (name[i].equals("no reminder")){
                                            time[i]="目前沒有提醒";
                                        }
                                        else{
                                            time[i] = name[i]+"\n"+hour[i]+"小時"+minute[i]+"分鐘";
                                        }
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.d("asd", e.toString());
                        }
                    }
                },
                new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("asd", error.toString());
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("patientID", patientID);
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        requestQueue.add(stringRequest);
    }

}
