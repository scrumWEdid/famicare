package com.example.kavin.try5;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.github.florent37.singledateandtimepicker.SingleDateAndTimePicker;
import com.github.florent37.singledateandtimepicker.dialog.SingleDateAndTimePickerDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class pain_add extends AppCompatActivity {
    private DrawerLayout mDrawerLayout;
    private localDatabase localDB = null;
    private Cursor cursor = null;
    private String user_id, patient_id;
    private static final String RECORD_TYPE = "pain";
    private EditText selecttime;
    private Spinner part, locate, locate2, level;
    private EditText hour, minute;
    private CheckBox[] painstatusCheckbox = new CheckBox[6];
    private Button submitBtn;
    private Date save_date;
    private SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
    private SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy/MM/dd a hh:mm:ss");
    private String[][] pain_locate_TWN = {
            {},
            {"位置", "額頭到頭頂之間(額葉)", "頭頂到後腦勺之間(頂葉)", "後腦勺(枕葉)", "臉頰後之腦部(顳葉)", "後腦勺到脖子之間(腦幹)"},
            {"位置", "耳朵", "鼻子", "牙齒", "眼睛", "臉頰", "額頭"},
            {"位置", "手指", "手掌", "手腕", "小手臂(前臂)", "大手臂(上臂)"},
            {"位置", "肩膀", "頸部"},
            {"位置", "大腿", "膝蓋", "小腿", "腳踝", "腳掌", "腳趾"},
            {"位置", "胸部", "肺", "心臟", "腹部", "大小腸", "胃", "肝"},
            {"位置", "上背部", "下背部", "脊椎", "腎臟"},
            {"位置", "子宮", "卵巢", "膀胱", "尿道"}
    };
    private String[] pain_part_TWN = {"部位", "頭部", "臉部", "手", "肩頸", "腳", "胸腹", "背部", "其他"};
    private String[] pain_locate2_TWN = {"左右邊", "左邊", "右邊"};
    private String[] pain_level_TWN = {"選取等級", "1","2", "3", "4", "5"};
    private String[] painCheckboxText_TWN = {"紅", "腫", "癢", "神經痛", "肌肉痛", "表皮挫傷"};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pain_add);
        BottomNavigationView bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottom_navigation_view);
        bottomNavigationView.setSelectedItemId(R.id.nav_description);
        bottomNavigationView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);


        localDB = new localDatabase(this);
        localDB.open();
        //----介面部分
        toolbar mytoolbar = new toolbar(this);
        mDrawerLayout = findViewById(R.id.drawer_layout);
// 介面部分-----

        hour = findViewById(R.id.hourEdt);
        minute = findViewById(R.id.minuteEdt);

        part = findViewById(R.id.spinner_part);
        String[] pain_part =  getResources().getStringArray(R.array.painpart);
        part.setAdapter(setupadapter(pain_part));

        locate = findViewById(R.id.spinner_locate);
        final String[][] pain_locate = {
                {},
                getResources().getStringArray(R.array.painlocatearray1),
                getResources().getStringArray(R.array.painlocatearray2),
                getResources().getStringArray(R.array.painlocatearray3),
                getResources().getStringArray(R.array.painlocatearray4),
                getResources().getStringArray(R.array.painlocatearray5),
                getResources().getStringArray(R.array.painlocatearray6),
                getResources().getStringArray(R.array.painlocatearray7),
                getResources().getStringArray(R.array.painlocatearray8)
        };

        locate2 = findViewById(R.id.spinner_locate2);
        String[] pain_locate2 =  getResources().getStringArray(R.array.painlocate2);
        locate2.setAdapter(setupadapter(pain_locate2));

        level = findViewById(R.id.spinner_level);
        String[] pain_level = getResources().getStringArray(R.array.painlevel);
        level.setAdapter(setupadapter(pain_level));

        part.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if(i == 0){
                    locate.setVisibility(View.INVISIBLE);
                    locate2.setVisibility(View.INVISIBLE);
                }
                else{
                    locate.setVisibility(View.VISIBLE);
                    locate2.setVisibility(View.VISIBLE);
                    locate.setAdapter(setupadapter(pain_locate[i]));
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        painstatusCheckbox[0] = findViewById(R.id.checkBox1);
        painstatusCheckbox[1] = findViewById(R.id.checkBox2);
        painstatusCheckbox[2] = findViewById(R.id.checkBox3);
        painstatusCheckbox[3] = findViewById(R.id.checkBox4);
        painstatusCheckbox[4] = findViewById(R.id.checkBox5);
        painstatusCheckbox[5] = findViewById(R.id.checkBox6);


        submitBtn = findViewById(R.id.submitBtn);
        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                add_record(RECORD_TYPE);
            }
        });


        cursor = localDB.getinfo();
        if(cursor.getCount() >= 0){
            user_id = String.valueOf(cursor.getInt(0));
        }
        patient_id = localDB.getpatientinfo();

        selecttime = findViewById(R.id.datetime);
        save_date = new Date();
        selecttime.setText(sdf2.format(save_date));
        //datetimepicker
        selecttime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                new SingleDateAndTimePickerDialog.Builder(pain_add.this)

                        .minutesStep(1)
                        .bottomSheet()
//                        .displayAmPm(false)
                        .setDayFormatter(new SimpleDateFormat("M月d日 E"))
                        .displayListener(new SingleDateAndTimePickerDialog.DisplayListener() {
                            @Override
                            public void onDisplayed(SingleDateAndTimePicker picker) {
                                picker.setDefaultDate(save_date);
                            }
                        })
                        .title(getString(R.string.selecttime))
                        .listener(new SingleDateAndTimePickerDialog.Listener() {
                            @Override
                            public void onDateSelected(Date date) {
                                save_date = date;
                                String select_date = sdf2.format(date);
                                selecttime.setText(select_date);
                            }
                        }).display();
            }
        });
    }
    @Override
    protected void onDestroy(){
        super.onDestroy();
        localDB.close(); // 關閉資料庫
    }

    //spinner adapter
    ArrayAdapter<String> setupadapter(String[] item){
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, item){
            @Override
            public boolean isEnabled(int position){
                if(position == 0)
                    return false;
                else
                    return true;
            }
            @Override
            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView tv = (TextView) view;
                if(position == 0){
                    // Set the hint text color gray
                    tv.setTextColor(Color.GRAY);
                }
                else {
                    tv.setTextColor(Color.BLACK);
                }
                return view;
            }
        };
        adapter.setDropDownViewResource(R.layout.spinner_item);
        return adapter;
    }

    private void add_record(final String record_type){
        //更新mySQL permission資料
        String URL = "http://140.117.71.74/graduation/add_record.php";
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            Log.d("書出訊息:", String.valueOf(jsonObject));
                            String success =jsonObject.getString("success");
                            String message = jsonObject.getString("message");
                            if (success.equals("1")){
                                Toast.makeText(pain_add.this, message, Toast.LENGTH_SHORT).show();
                                setResult(RESULT_OK);
                                finish();
                            }
                            else{
                                Toast.makeText(pain_add.this, message, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.d("錯誤訊息",e.toString());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(pain_add.this,getString(R.string.searchfail)+error.toString(),Toast.LENGTH_SHORT).show();
                    }
                })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String>params = new HashMap<>();
                params.put("record_type", record_type);
                params.put("patientid", patient_id);
                params.put("caretakerid", user_id);
                params.put("datetime", sdf.format(save_date));
                params.put("painpart", pain_part_TWN[part.getSelectedItemPosition()]);
                params.put("painlocate", pain_locate_TWN[part.getSelectedItemPosition()][locate.getSelectedItemPosition()]);
                params.put("painlocate2", pain_locate2_TWN[locate2.getSelectedItemPosition()]);
                params.put("painlevel", pain_level_TWN[level.getSelectedItemPosition()]);
                int duration = Integer.valueOf(hour.getText().toString()) * 60;
                duration = duration + Integer.valueOf(minute.getText().toString());
                params.put("duration", String.valueOf(duration));

                String painstatus = "";
                for(int i = 0; i < 6; i++) {
                    if (painstatusCheckbox[i].isChecked())
                        painstatus += painCheckboxText_TWN[i] + ",";
                }
                params.put("painstatus", painstatus);
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    //--介面部分
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
//                mDrawerLayout.openDrawer(Gravity.RIGHT);
                pain_add.this.finish();
                return true;
            case R.id.user_info:
                mDrawerLayout.openDrawer(Gravity.RIGHT);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.game_menu, menu);
        //set pattent name of menu
        MenuItem patientname = menu.findItem(R.id.patient_name);
        patientname.setTitle(localDB.getpatientname());
        return true;
    }
//    介面部分--

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected( MenuItem item) {
            BottomNavigation bn = new BottomNavigation(pain_add.this, item.getItemId());
            return true;
        }

    };

}
