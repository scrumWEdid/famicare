package com.example.kavin.try5;

import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

import com.example.kavin.try5.Volley.Volleyconnect;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

public class pain_view extends AppCompatActivity {
    private DrawerLayout mDrawerLayout;
    private localDatabase localDB = null;
    private Cursor cursor = null;
    private String user_id, user_acc, patient_id;
    private static final String RECORD_TYPE = "pain";
    private SimpleDateFormat sdf = new SimpleDateFormat("yyyy/M/dd HH:mm");
    private SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy/MM/dd a hh:mm");
    private String recordID;
    private TextView part, locate, level;
    private TextView hour, minute;
    private TextView datetime;
    private TextView editBtn, delBtn;
    private CheckBox[] painstatusCheckbox = new CheckBox[6];
    private Button submitBtn;
    private String[] data;

    private String[][] pain_locate_TWN = {
            {},
            {"位置", "額頭到頭頂之間(額葉)", "頭頂到後腦勺之間(頂葉)", "後腦勺(枕葉)", "臉頰後之腦部(顳葉)", "後腦勺到脖子之間(腦幹)"},
            {"位置", "耳朵", "鼻子", "牙齒", "眼睛", "臉頰", "額頭"},
            {"位置", "手指", "手掌", "手腕", "小手臂(前臂)", "大手臂(上臂)"},
            {"位置", "肩膀", "頸部"},
            {"位置", "大腿", "膝蓋", "小腿", "腳踝", "腳掌", "腳趾"},
            {"位置", "胸部", "肺", "心臟", "腹部", "大小腸", "胃", "肝"},
            {"位置", "上背部", "下背部", "脊椎", "腎臟"},
            {"位置", "子宮", "卵巢", "膀胱", "尿道"}
    };
    private String[] pain_part_TWN = {"部位", "頭部", "臉部", "手", "肩頸", "腳", "胸腹", "背部", "其他"};
    private String[] pain_locate2_TWN = {"左右邊", "左邊", "右邊"};
    private String[] pain_level_TWN = {"選取等級", "1","2", "3", "4", "5"};
    private String[] painCheckboxText_TWN = {"紅", "腫", "癢", "神經痛", "肌肉痛", "表皮挫傷"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pain_view);
        BottomNavigationView bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottom_navigation_view);
        bottomNavigationView.setSelectedItemId(R.id.nav_description);
        bottomNavigationView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        //local dasabase
        localDB = new localDatabase(this);
        localDB.open();
        cursor = localDB.getinfo();
        if(cursor.getCount() >= 0){
            user_id = String.valueOf(cursor.getInt(0));
            user_acc = cursor.getString(1);
        }
        patient_id = localDB.getpatientinfo();

//----介面部分
        toolbar mytoolbar = new toolbar(this);
        mDrawerLayout = findViewById(R.id.drawer_layout);
// 介面部分-----


        datetime = findViewById(R.id.datetime);
        hour = findViewById(R.id.hourEdt);
        minute = findViewById(R.id.minuteEdt);
        part = findViewById(R.id.spinner_part);
        locate = findViewById(R.id.spinner_locate);
        level = findViewById(R.id.spinner_level);
        painstatusCheckbox[0] = findViewById(R.id.checkBox1);
        painstatusCheckbox[1] = findViewById(R.id.checkBox2);
        painstatusCheckbox[2] = findViewById(R.id.checkBox3);
        painstatusCheckbox[3] = findViewById(R.id.checkBox4);
        painstatusCheckbox[4] = findViewById(R.id.checkBox5);
        painstatusCheckbox[5] = findViewById(R.id.checkBox6);

        Bundle bundle = getIntent().getExtras();
        data = bundle.getStringArray("data");
        set_data(data);

        editBtn = findViewById(R.id.editimgBtn);
        editBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setClass(pain_view.this, pain_edit.class);
                Bundle bundle = new Bundle();
                bundle.putStringArray("data", data);
                intent.putExtras(bundle);
                startActivityForResult(intent, 1);
            }
        });

        delBtn = findViewById(R.id.clearBtn);
        delBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new AlertDialog.Builder(pain_view.this)
                        .setTitle(getString(R.string.suredelete))
                        .setMessage(getString(R.string.deletenorecovery))
                        .setNegativeButton(getString(R.string.sure), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialoginterface, int i)
                            {
                                String recordid = data[8];
                                //delete record
                                Volleyconnect volleyconnect = new Volleyconnect(getApplicationContext());
                                volleyconnect.delete_record(RECORD_TYPE, recordid, new Volleyconnect.VolleyCallback() {
                                    @Override
                                    public void onSuccess(String result) {
                                        if(result.equals("1")){
                                            setResult(RESULT_OK);
                                            finish();
                                        }
                                    }
                                });
                            }
                        })
                        .setPositiveButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialoginterface, int i)
                            {

                            }
                        })
                        .show();
            }
        });



    }

    private void set_data(String[] data){
        String format_time = data[0] + " " + data[1];
        try {
            Date dt = sdf.parse(format_time);
            format_time = sdf2.format(dt);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        datetime.setText(format_time);

        int index = Arrays.asList(pain_part_TWN).indexOf(data[2]);
        String convertedString = getResources().getStringArray(R.array.painpart)[index];
        part.setText(convertedString);

        int id = getResources().getIdentifier("painlocatearray"+index, "array", getPackageName());
        index = Arrays.asList(pain_locate_TWN[index]).indexOf(data[3]);
        convertedString = getResources().getStringArray(id)[index] + "  ";
        index = Arrays.asList(pain_locate2_TWN).indexOf(data[4]);
        convertedString += getResources().getStringArray(R.array.painlocate2)[index];
        locate.setText(convertedString);

        index = Arrays.asList(pain_level_TWN).indexOf(data[6]);
        convertedString = getResources().getStringArray(R.array.painlevel)[index];
        level.setText(convertedString);

        hour.setText(String.valueOf( Integer.valueOf(data[5])/60) );
        minute.setText(String.valueOf( Integer.valueOf(data[5])%60) );

        String[] painstatus_data = data[7].split(",");
        for(int i = 0; i < 6; i++) //reset checkbox
            painstatusCheckbox[i].setChecked(false);
        for(int i = 0, j = 0; i < 6; i++){
            if(painCheckboxText_TWN[i].equals(painstatus_data[j])){
                painstatusCheckbox[i].setChecked(true);
                j++;
                if(j == painstatus_data.length)
                    break;
            }
        }
    }
    @Override
    protected void onDestroy(){
        super.onDestroy();
        localDB.close(); // 關閉資料庫
    }

    //編輯頁面回傳
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        if(requestCode == 1){
            if(resultCode == RESULT_OK){
                Bundle bundle = intent.getExtras();
                data = bundle.getStringArray("data");
                set_data(data);
                setResult(RESULT_OK);
            }
        }
    }

    //--介面部分
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
//                mDrawerLayout.openDrawer(Gravity.RIGHT);
                pain_view.this.finish();
                return true;
            case R.id.user_info:
                mDrawerLayout.openDrawer(Gravity.RIGHT);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.game_menu, menu);
        //set pattent name of menu
        MenuItem patientname = menu.findItem(R.id.patient_name);
        patientname.setTitle(localDB.getpatientname());
        return true;
    }
//    介面部分--

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected( MenuItem item) {
            BottomNavigation bn = new BottomNavigation(pain_view.this, item.getItemId());
            return true;
        }

    };
}
