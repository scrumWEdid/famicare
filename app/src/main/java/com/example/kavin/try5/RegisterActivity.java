package com.example.kavin.try5;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class RegisterActivity extends AppCompatActivity {
    private EditText Account ,Password, Name, Phone,Email;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        Account = findViewById(R.id.registerAccount);
        Password = findViewById(R.id.registerPassword);
        Name = findViewById(R.id.registerName);
        Phone = findViewById(R.id.registerPhone);
        Email = findViewById(R.id.registerEmail);
        final Button finishRegister = (Button) findViewById(R.id.registerButton);
        finishRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(Account.getText().toString().isEmpty()) {
                    Toast toast = Toast.makeText(RegisterActivity.this, getString(R.string.pleaseenteraccount), Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER, 0, -100);
                    toast.show();
                }
                else if(Password.getText().toString().isEmpty()){
                    Toast toast = Toast.makeText(RegisterActivity.this, getString(R.string.pleaseenterpassword), Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER, 0, -100);
                    toast.show();
                }
                else if(Name.getText().toString().isEmpty()){
                    Toast toast = Toast.makeText(RegisterActivity.this, getString(R.string.entername), Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER, 0, -100);
                    toast.show();
                }
                else if(Phone.getText().toString().length() != 10){
                    Toast toast = Toast.makeText(RegisterActivity.this, getString(R.string.numberlength), Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER, 0, -100);
                    toast.show();
                }
                else if(Email.getText().toString().isEmpty()){
                    Toast toast = Toast.makeText(RegisterActivity.this, getString(R.string.enteremail), Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER, 0, -100);
                    toast.show();
                }
                else if(!android.util.Patterns.EMAIL_ADDRESS.matcher(Email.getText().toString()).matches()){
                    Toast toast = Toast.makeText(RegisterActivity.this, getString(R.string.emailformat), Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER, 0, -100);
                    toast.show();
                }
                else
                    Register();
            }
        });
    }
    private  void Register(){
        final String URL_REGISTER ="http://140.117.71.74/graduation/register.php";
        final String Account =this.Account.getText().toString().trim();
        final String Password =this.Password.getText().toString().trim();
        final String Name =this.Name.getText().toString().trim();
        final String Phone =this.Phone.getText().toString().trim();
        final String Email =this.Email.getText().toString().trim();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL_REGISTER,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            String success = jsonObject.getString("success");
                            String message = jsonObject.getString("message");
//                            Log.d("檢查訊息", String.valueOf(jsonObject));

                            if (success.equals("1")) {
                                Toast.makeText(RegisterActivity.this, message, Toast.LENGTH_SHORT).show();
                                Intent finishRegisterIntent = new Intent(RegisterActivity.this,LoginActivity.class);
                                RegisterActivity.this.startActivity(finishRegisterIntent);
                                finish();
                            }
                            else{
                                Toast toast = Toast.makeText(RegisterActivity.this, message, Toast.LENGTH_SHORT);
                                toast.setGravity(Gravity.CENTER, 0, -100);
                                toast.show();
                                EditText Account = (EditText)findViewById(R.id.registerAccount);
                                Account.setText("");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(RegisterActivity.this, "Register error!" + e.toString(), Toast.LENGTH_SHORT).show();


                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(RegisterActivity.this, "Register error!"+ error.toString(), Toast.LENGTH_SHORT).show();

                    }
                })
        {
            @Override
            protected Map<String, String> getParams() {

                Map<String,String> params =new HashMap<>();
                params.put("account",Account);
                params.put("password",Password);
                params.put("name",Name);
                params.put("phone",Phone);
                params.put("email",Email);
                return params;

            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }
}
