package com.example.kavin.try5;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.kavin.try5.Volley.Volleyconnect;

import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

public class patientInfoActivity extends AppCompatActivity {
    private DrawerLayout mDrawerLayout;
    private TextView mDisplayDate;
    private DatePickerDialog.OnDateSetListener mDataSetListener;
    private localDatabase localDB = null;
    private Cursor cursor = null;
    private String patientID;
    private RadioButton boy, girl;
    private CheckBox smoke, drunk, eat;
    private EditText name, birthday, height, weight;
    private EditText personal_disease, family_disease;
    private Button submitBtn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_patient_info);
        boy = findViewById(R.id.patientInfoMaleRadioButton);
        girl = findViewById(R.id.patientInfoFeMaleRadioButton);
        smoke = findViewById(R.id.patientInfoSmokeCheckBox);
        drunk = findViewById(R.id.patientInfoDrinkCheckBox);
        eat = findViewById(R.id.patientInfoArecaCheckBox);
        name = findViewById(R.id.patientInfoNameEditText);
        birthday = findViewById(R.id.patientInfoBirthEditText);
        height = findViewById(R.id.patientInfoHeightEditText);
        weight = findViewById(R.id.patientInfoWeightEditText);
        personal_disease = findViewById(R.id.patientInfoHistoryEditText);
        family_disease = findViewById(R.id.patientInfoHistory2EditText);
        submitBtn = findViewById(R.id.patientInfoSubmitButton);
        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                update_patient();
                /*hide keyboard*/
                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        });


        mDisplayDate = (EditText)findViewById(R.id.patientInfoBirthEditText);

        mDisplayDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int year, month, day;
                if(birthday.getText().toString().equals("")) {
                    Calendar cal = Calendar.getInstance();
                    year = cal.get(Calendar.YEAR);
                    month = cal.get(Calendar.MONTH);
                    day = cal.get(Calendar.DAY_OF_MONTH);
                }
                else{
                    String[] date = birthday.getText().toString().split("/");
                    year = Integer.valueOf(date[0]);
                    month = Integer.valueOf(date[1])-1;
                    day = Integer.valueOf(date[2]);
                }

                DatePickerDialog dialog = new DatePickerDialog(
                        patientInfoActivity.this,
                        android.R.style.Theme_Holo_Light_Dialog_MinWidth,
                        mDataSetListener,
                        year,month,day);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();
            }
        });

        mDataSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                month=month+1;
                String date = year+"/"+month+"/"+dayOfMonth;
                mDisplayDate.setText(date);
            }
        };

        //localdatebase
        localDB = new localDatabase(this);
        localDB.open();
        patientID = localDB.getpatientinfo();
        get_patient();

        //----------------------------------------------
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp);

        //----讀取權限 設定是否要顯示的新增,編輯按鈕
        final Volleyconnect volleyconnect = new Volleyconnect(this);
        volleyconnect.fetch_permission(localDB.getuserid(), localDB.getpatientinfo(),
                new Volleyconnect.VolleyCallback() {
                    @Override
                    public void onSuccess(String result) {
                        if(result.equals("查看者")) {
                            name.setEnabled(false);
                            name.setBackground(null);
                            boy.setEnabled(false);
                            girl.setEnabled(false);
                            birthday.setEnabled(false);
                            birthday.setBackground(null);
                            height.setEnabled(false);
                            height.setBackground(null);
                            weight.setEnabled(false);
                            weight.setBackground(null);
                            smoke.setEnabled(false);
                            drunk.setEnabled(false);
                            eat.setEnabled(false);
                            personal_disease.setEnabled(false);
                            personal_disease.setBackground(null);
                            family_disease.setEnabled(false);
                            family_disease.setBackground(null);
                            submitBtn.setVisibility(View.INVISIBLE);
                        }
                    }
                });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                patientInfoActivity.this.finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.game_menu, menu);
        //hide icon
        MenuItem userinfo = menu.findItem(R.id.user_info);
        userinfo.setIcon(android.R.color.transparent);
        return true;
    }

    //close localDB
    @Override
    protected void onDestroy() {
        super.onDestroy();
        localDB.close();
    }

    //----------------connect to mySQL----------------
    private void get_patient(){
        //抓取被照顧者資料
        String URL = "http://140.117.71.74/graduation/fetch_patient.php";
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            Log.d("書出訊息:", String.valueOf(jsonObject));
                            String success =jsonObject.getString("success");
                            if (success.equals("1")){
                                JSONObject jsonData = jsonObject.getJSONObject("0");
//                                Log.d("測試訊息", String.valueOf(jsonData));
                                name.setText(jsonData.getString("name"));
                                if(jsonData.getString("gender").equals("男"))
                                    boy.setChecked(true);
                                else
                                    girl.setChecked(true);
                                birthday.setText(jsonData.getString("birthday"));
                                height.setText(jsonData.getString("height"));
                                weight.setText(jsonData.getString("weight"));
                                if(jsonData.getString("hassmoke").equals("0"))
                                    smoke.setChecked(false);
                                else
                                    smoke.setChecked(true);
                                if(jsonData.getString("hasdrunk").equals("0"))
                                    drunk.setChecked(false);
                                else
                                    drunk.setChecked(true);
                                if(jsonData.getString("haseat").equals("0"))
                                    eat.setChecked(false);
                                else
                                    eat.setChecked(true);
                                personal_disease.setText(jsonData.getString("personal_disease"));
                                family_disease.setText(jsonData.getString("family_disease"));
                            }
                            else{
                                Toast.makeText(patientInfoActivity.this, getString(R.string.wronghappen), Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.d("錯誤訊息",e.toString());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
//                        Toast.makeText(addPantient.this,"搜尋失敗"+error.toString(),Toast.LENGTH_SHORT).show();
                    }
                })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String>params = new HashMap<>();
                params.put("id", patientID);
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }


    private void update_patient(){
        //更新被照顧者MySQL
        String URL = "http://140.117.71.74/graduation/update_patient.php";
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            Log.d("書出訊息:", String.valueOf(jsonObject));
                            String success =jsonObject.getString("success");
                            if (success.equals("1")){
                                Toast.makeText(patientInfoActivity.this, getString(R.string.updatesuccess), Toast.LENGTH_SHORT).show();
                                localDB.update_patient(patientID, name.getText().toString());
                                finish();
                            }
                            else{
                                Toast.makeText(patientInfoActivity.this, getString(R.string.wronghappen), Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.d("錯誤訊息",e.toString());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
//                        Toast.makeText(addPantient.this,"搜尋失敗"+error.toString(),Toast.LENGTH_SHORT).show();
                    }
                })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String>params = new HashMap<>();
                params.put("id", patientID);
                params.put("name", name.getText().toString());
                params.put("birthday", birthday.getText().toString());
                params.put("height", height.getText().toString());
                params.put("weight", weight.getText().toString());
                params.put("personal_disease", personal_disease.getText().toString());
                params.put("family_disease", family_disease.getText().toString());
                if(boy.isChecked())
                    params.put("gender", "男");
                else
                    params.put("gender", "女");
                if(smoke.isChecked())
                    params.put("smoke", "1");
                else
                    params.put("smoke", "0");
                if(drunk.isChecked())
                    params.put("drunk", "1");
                else
                    params.put("drunk", "0");
                if(eat.isChecked())
                    params.put("eat", "1");
                else
                    params.put("eat", "0");
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }
}
