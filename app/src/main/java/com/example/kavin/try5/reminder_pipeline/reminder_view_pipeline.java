package com.example.kavin.try5.reminder_pipeline;

import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.kavin.try5.R;
import com.example.kavin.try5.localDatabase;
import com.example.kavin.try5.toolbar;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class reminder_view_pipeline extends AppCompatActivity {
    private DrawerLayout mDrawerLayout;
    private localDatabase localDB = null;
    private Cursor cursor = null;
    private toolbar mytoolbar;
    private String RECORD_TYPE = "pipeline";
    private JSONObject reminder;

    private TextView typeTxt;
    private TextView usehourTxt, useminTxt;
    private TextView startdatetime, enddatetime;
    private Date start_date;
    private SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm");
    private SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private TextView editBtn, delBtn;
    private Button confirmBtn;

    private String[] typeitem_TWN = {"管路種類", "尿管", "B管", "C管"};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reminder_view_pipeline);
        //----open localDB
        localDB = new localDatabase(this);
        localDB.open();
//----介面部分
        mytoolbar = new toolbar(this);
        mDrawerLayout = findViewById(R.id.drawer_layout);
// 介面部分-----
        typeTxt = findViewById(R.id.typeTxt);
        usehourTxt = findViewById(R.id.usehourTxt);
        useminTxt = findViewById(R.id.useminTxt);
        startdatetime = findViewById(R.id.startdatetime);
        enddatetime = findViewById(R.id.enddatetime);
        
        fetch_data(getIntent()); //顯示資料

        editBtn = findViewById(R.id.editimgBtn);
        editBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent();
                intent.setClass(reminder_view_pipeline.this, reminder_edit_pipeline.class);
                Bundle bundle = new Bundle();
                bundle.putString("reminder", reminder.toString());
                intent.putExtras(bundle);
                startActivityForResult(intent, 1);

            }
        });
        delBtn = findViewById(R.id.clearBtn);
        delBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new AlertDialog.Builder(reminder_view_pipeline.this)
                        .setTitle(getString(R.string.suredelete))
                        .setMessage(getString(R.string.deletenorecovery))
                        .setNegativeButton(getString(R.string.recovery), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialoginterface, int i)
                            {
                                delete_reminder(RECORD_TYPE, reminder);
                            }
                        })
                        .setPositiveButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialoginterface, int i)
                            {

                            }
                        })
                        .show();
            }
        });
        confirmBtn = findViewById(R.id.confirmBtn);
        confirmBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setResult(RESULT_OK);
                finish();
            }
        });

    }
    @Override
    protected void onDestroy(){
        super.onDestroy();
        localDB.close(); // 關閉資料庫
    }

    //設定顯示資料
    private void fetch_data(Intent intent){
        try {
            reminder = new JSONObject(intent.getStringExtra("reminder"));

            int index = Arrays.asList(typeitem_TWN).indexOf(reminder.getString("type"));
            String s = getResources().getStringArray(R.array.pipetypeitem)[index];
            typeTxt.setText(s);

            startdatetime.setText(sdf.format(sdf2.parse(reminder.getString("startdatetime"))));
            int duration = Integer.valueOf(reminder.getString("duration"));
            usehourTxt.setText(String.valueOf(duration / 60));
            useminTxt.setText(String.valueOf(duration % 60));
            enddatetime.setText(sdf.format(sdf2.parse(reminder.getString("enddatetime"))));

        } catch (JSONException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    /*下一頁修改完畢回傳更新資料*/
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == 1){
            if(resultCode == RESULT_OK){
                fetch_data(data);
                setResult(RESULT_OK);
            }
        }
    }

    //-----------connect to MYSQL--------------
    private void delete_reminder(final String RECORD_TYPE, final JSONObject record){
        String URL = "http://140.117.71.74/graduation/delete_reminder.php";
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            Log.d("書出訊息:", String.valueOf(jsonObject));
                            String success =jsonObject.getString("success");
                            String message = jsonObject.getString("message");
                            if (success.equals("1")){
                                Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                                setResult(RESULT_OK);
                                finish();
                            }
                            else{
                                Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.d("錯誤訊息",e.toString());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(),getString(R.string.searchfail)+error.toString(),Toast.LENGTH_SHORT).show();
                    }
                })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String>params = new HashMap<>();
                params.put("record_type", RECORD_TYPE);
                try {
                    params.put("pipelineReminderID", record.getString("pipelineReminderID"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }
    //--介面部分
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
//                mDrawerLayout.openDrawer(Gravity.RIGHT);
                finish();
                return true;
            case R.id.user_info:
                mDrawerLayout.openDrawer(Gravity.RIGHT);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.game_menu, menu);
        //set pattent name of menu
        MenuItem patientname = menu.findItem(R.id.patient_name);
        patientname.setTitle(localDB.getpatientname());
        return true;
    }
//    介面部分--
}
