package com.example.kavin.try5.reminder_measure;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.kavin.try5.BottomNavigation;
import com.example.kavin.try5.CareTakerManagePermissionActivity;
import com.example.kavin.try5.ChoosePatientActivity;
import com.example.kavin.try5.MemberInfoActivity;
import com.example.kavin.try5.R;
import com.example.kavin.try5.localDatabase;
import com.example.kavin.try5.pain_add;
import com.example.kavin.try5.patientInfoActivity;
import com.example.kavin.try5.reminder_notification.PlayReceiver;
import com.example.kavin.try5.toolbar;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class reminder_add_measure extends AppCompatActivity {
    private DrawerLayout mDrawerLayout;
    private localDatabase localDB = null;
    private Cursor cursor = null;
    private String REMINDER_TYPE = "measure";
    private String user_id, patient_id;
    private TextView[] taketime = new TextView[4];
    private ImageView clockimg;
    private Spinner type_spinner;
    private String[] typeitem_TWN = {"測量項目", "體溫", "心跳血壓", "血糖"};

    private Spinner repeat_spinner, repeat_spinner2;
    private String[] repeatitem_TWN = {"每日", "每隔", "每週"};
    private String[] repeat2item_TWN = {"0","1天","2天","3天","4天","5天"};

    private Spinner taketime_spinner;
    private String[] taketimeitem_TWN = {"0", "1次", "2次", "3次", "4次"};

    private Button continueBtn, confirmBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reminder_add_measure);
        //----open localDB
        localDB = new localDatabase(this);
        localDB.open();

        BottomNavigationView bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottom_navigation_view);
        bottomNavigationView.setSelectedItemId(R.id.alarm);
        bottomNavigationView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

//----介面部分
        toolbar mytoolbar = new toolbar(this);
        mDrawerLayout = findViewById(R.id.drawer_layout);
// 介面部分-----
//----get string array from strings.xml
        String[] typeitem = getResources().getStringArray(R.array.typeitem);
        String[] repeatitem = getResources().getStringArray(R.array.time);
        String[] repeat2item = getResources().getStringArray(R.array.days);
        String[] taketimeitem = getResources().getStringArray(R.array.times);
//get string array from strings.xml----
        cursor = localDB.getinfo();
        if(cursor.getCount() >= 0){
            user_id = String.valueOf(cursor.getInt(0));
        }
        patient_id = localDB.getpatientinfo();

        clockimg = findViewById(R.id.clockimage);
        taketime[0] = findViewById(R.id.taketimetxt1);
        taketime[1] = findViewById(R.id.taketimetxt2);
        taketime[2] = findViewById(R.id.taketimetxt3);
        taketime[3] = findViewById(R.id.taketimetxt4);
        for(int i = 0; i < 4; i++){ //set timepicker when onclick
            final int finalI = i;
            taketime[i].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    opentimepicker(finalI);
                }
            });
        }

        type_spinner = findViewById(R.id.type_spinner);
        type_spinner.setAdapter(setupadapter(typeitem));

        taketime_spinner = findViewById(R.id.taketime_spinner);
        taketime_spinner.setAdapter(setupadapter(taketimeitem));
        taketime_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                if(position > 0){
                    clockimg.setVisibility(View.VISIBLE);
                    int i = 0;
                    for(i = 0; i < position; i++)
                        taketime[i].setVisibility(View.VISIBLE);
                    for(; i < 4; i++)
                        taketime[i].setVisibility(View.GONE);
                }
                else{
                    clockimg.setVisibility(View.GONE);
                    for(int i = 0; i < 4; i++)
                        taketime[i].setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        repeat_spinner = findViewById(R.id.repeat_spinner);
        repeat_spinner2 = findViewById(R.id.repeat_spinner2);

        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, repeatitem);
        adapter.setDropDownViewResource(R.layout.spinner_item);
        repeat_spinner.setAdapter(adapter);

        repeat_spinner2.setAdapter(setupadapter(repeat2item));

        repeat_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if(i == 1)
                    repeat_spinner2.setVisibility(View.VISIBLE);
                else
                    repeat_spinner2.setVisibility(View.GONE);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        continueBtn = findViewById(R.id.continueBtn);
        continueBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                add_reminder(REMINDER_TYPE, new VolleyCallback() {
                    @Override
                    public void onSuccess(String result) {
                        if(result.equals("1")){
                            try {
                                add_reminder_notification(REMINDER_TYPE);
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                            setResult(RESULT_OK);
                            taketime_spinner.setSelection(0, true);
                            type_spinner.setSelection(0, true);
                            repeat_spinner.setSelection(0, true);
                            repeat_spinner2.setSelection(0, true);
                            clockimg.setVisibility(View.GONE);
                            for(int i = 0; i < 4; i++)
                                taketime[i].setVisibility(View.GONE);

                        }
                    }
                });
            }
        });

        confirmBtn = findViewById(R.id.confirmBtn);
        confirmBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                add_reminder(REMINDER_TYPE, new VolleyCallback() {
                    @Override
                    public void onSuccess(String result) {
                        if(result.equals("1")){
                            try {
                                add_reminder_notification(REMINDER_TYPE);
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                            setResult(RESULT_OK);
                            finish();
                        }
                    }
                });
            }
        });
    }
    @Override
    protected void onDestroy(){
        super.onDestroy();
        localDB.close(); // 關閉資料庫
    }

    //spinner adapter
    ArrayAdapter<String> setupadapter(String[] item){
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, item){
            @Override
            public boolean isEnabled(int position){
                if(position == 0)
                    return false;
                else
                    return true;
            }
            @Override
            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView tv = (TextView) view;
                if(position == 0){
                    // Set the hint text color gray
                    tv.setTextColor(Color.GRAY);
                }
                else {
                    tv.setTextColor(Color.BLACK);
                }
                return view;
            }
        };
        adapter.setDropDownViewResource(R.layout.spinner_item);
        return adapter;
    }

    //open timepickerdialog
    private void opentimepicker(final int position){
        int hour, minute;
        String[] time= taketime[position].getText().toString().split(":");
        hour = Integer.valueOf(time[0]);
        minute = Integer.valueOf(time[1]);
//        Calendar cal = Calendar.getInstance();
//        int hour = cal.get(Calendar.HOUR_OF_DAY);
//        int minute = cal.get(Calendar.MINUTE);
        new TimePickerDialog(reminder_add_measure.this, new TimePickerDialog.OnTimeSetListener(){
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                String h = String.valueOf(hourOfDay);
                String m = String.valueOf(minute);
                if(hourOfDay < 10)
                    h = "0" + h;
                if(minute < 10)
                    m = "0" + m;
                taketime[position].setText(h + ":" + m);
            }
        }, hour, minute, false).show();
    }

    //--------connect to MySQL----------
    public interface VolleyCallback{
        void onSuccess(String result);
    }
    private void add_reminder(final String record_type, final VolleyCallback callback){
        String URL = "http://140.117.71.74/graduation/add_reminder.php";
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            Log.d("書出訊息:", String.valueOf(jsonObject));
                            String success =jsonObject.getString("success");
                            String message = jsonObject.getString("message");
                            if (success.equals("1")){
                                Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                                callback.onSuccess(success);
                            }
                            else{
                                Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.d("錯誤訊息",e.toString());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(),getResources().getString(R.string.searchfail)+error.toString(),Toast.LENGTH_SHORT).show();
                    }
                })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String>params = new HashMap<>();
                params.put("patientid", patient_id);
                params.put("caretakerid", user_id);
                params.put("record_type", record_type);
                params.put("type", typeitem_TWN[type_spinner.getSelectedItemPosition()]);
                params.put("recordtimes", taketimeitem_TWN[taketime_spinner.getSelectedItemPosition()].substring(0,1));
                String repeat = "";
                if(repeat_spinner.getSelectedItemPosition() == 0)
                    repeat = "1";
                else if(repeat_spinner.getSelectedItemPosition() == 1)
                    repeat = repeat2item_TWN[repeat_spinner2.getSelectedItemPosition()].substring(0,1);
                else
                    repeat = "7";
                params.put("repeatday", repeat);
                int times = Integer.valueOf(taketimeitem_TWN[taketime_spinner.getSelectedItemPosition()].substring(0,1));
                for(int i = 0; i < times; i++){
                    params.put("taketime"+i, taketime[i].getText().toString());
                }
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    //--介面部分
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
//                mDrawerLayout.openDrawer(Gravity.RIGHT);
                finish();
                return true;
            case R.id.user_info:
                mDrawerLayout.openDrawer(Gravity.RIGHT);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.game_menu, menu);
        //set pattent name of menu
        MenuItem patientname = menu.findItem(R.id.patient_name);
        patientname.setTitle(localDB.getpatientname());
        return true;
    }

    private void add_reminder_notification(String reminder_type) throws ParseException {
        String[] reminder_time = new String[4]; //每天服用的時間
        String type;
        type = typeitem_TWN[type_spinner.getSelectedItemPosition()];
        String repeat = "";
        if(repeat_spinner.getSelectedItemPosition() == 0)
            repeat = "1";
        else if(repeat_spinner.getSelectedItemPosition() == 1)
            repeat = repeat2item_TWN[repeat_spinner2.getSelectedItemPosition()].substring(0,1);
        else
            repeat = "7";
        int times = Integer.valueOf(taketimeitem_TWN[taketime_spinner.getSelectedItemPosition()].substring(0,1));
        for(int i = 0; i < times; i++){
            reminder_time[i] = taketime[i].getText().toString();
        }
        SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        Date currenttime = sdf.parse(sdf.format(new Date())); //當前時間
        Calendar cal = Calendar.getInstance(); //get current datetime

//        for(int i = 0; i < 3; i++) { //重複三次的天數 等通知OK後反助解掉
        int add_day = Integer.valueOf(repeat); //提醒日期
        for(int j = 0; j < Integer.valueOf(times); j++){ //提醒時間
            String remindtime = taketime[j].getText().toString() + ":00";

            Date add_time = sdf.parse(remindtime);
            if(add_time.before(currenttime)){ //開始日 過去的時間不用提醒

            }
//                if(add_time.before(currenttime) && i == 0){ //開始日 過去的時間不用提醒
//
//                }
            else{
                long difference = add_time.getTime() - currenttime.getTime();
                cal.add(Calendar.MILLISECOND, (int) difference);

//-----------下面這行要反註解後才有通知 傳的cal就是推播的日期時間-----------
                set_reminder(cal,type); //設定通知
                Log.d("datetime", sdf2.format(cal.getTime())); //推播的日期時間

                cal.add(Calendar.MILLISECOND, (int) difference*(-1));
            }

        }
        cal.add(Calendar.DATE, add_day);
//        }

        /*
        //notification test 10/22
        Calendar cal = Calendar.getInstance();
        // 設定於 3 分鐘後執行
        cal.add(Calendar.SECOND, 3);

        Intent intent = new Intent(this, PlayReceiver.class);
        intent.putExtra("msg", "play_hskay");
        intent.putExtra("title", "好吃的藥");
        intent.putExtra("message", "該吃藥囉");

        PendingIntent pi = PendingIntent.getBroadcast(this, 1, intent, PendingIntent.FLAG_ONE_SHOT);
        AlarmManager am = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
//        am.setRepeating(AlarmManager.RTC, time, AlarmManager.INTERVAL_DAY, pi);
        am.set(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), pi);
        */
    }

    //設定通知
    private void set_reminder(Calendar cal,String type){
        Intent intent = new Intent(this, PlayReceiver.class);
        intent.putExtra("msg", "measure");
        intent.putExtra("title", "FamiCare");
        intent.putExtra("type", type);
        SimpleDateFormat sdf2 = new SimpleDateFormat("HH:mm");
        String date = sdf2.format(cal.getTime()); //明明就是time呀XD
        Log.d("setreminderdate",date+ "");
        Log.d("setreminderdate", String.valueOf(cal));
        intent.putExtra("Sdate",date);
        intent.putExtra("message", date + " 該測量 " + type);

        //要設不同的id才能設置多個推播 不然會被蓋掉
        int id = Integer.parseInt(date.replace(":", ""));
        PendingIntent pi = PendingIntent.getBroadcast(this, id, intent, PendingIntent.FLAG_ONE_SHOT);
        AlarmManager am = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
//        am.setRepeating(AlarmManager.RTC, time, AlarmManager.INTERVAL_DAY, pi);
        am.set(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), pi);

        //測試用 這樣不用一直等時間到才推通知
//        Calendar cal_test = Calendar.getInstance();
//        am.set(AlarmManager.RTC_WAKEUP, cal_test.getTimeInMillis(), pi);
    }







//    介面部分--


    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected( MenuItem item) {
            BottomNavigation bn = new BottomNavigation(reminder_add_measure.this, item.getItemId());
            return true;
        }

    };


}
