package com.example.kavin.try5;

import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewFlipper;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.kavin.try5.Volley.Volleyconnect;
import com.example.kavin.try5.mpchartCustom.drawLineChart;
import com.github.florent37.singledateandtimepicker.SingleDateAndTimePicker;
import com.github.florent37.singledateandtimepicker.dialog.SingleDateAndTimePickerDialog;
import com.github.mikephil.charting.charts.LineChart;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import jp.wasabeef.recyclerview.adapters.AlphaInAnimationAdapter;
import jp.wasabeef.recyclerview.animators.SlideInUpAnimator;

public class temperature_main extends AppCompatActivity {
    private DrawerLayout mDrawerLayout;
    private localDatabase localDB = null;
    private Cursor cursor = null;
    private String user_id, user_acc, patient_id;
    private final static String RECORD_TYPE = "temperature";
    private double top_alarm = 0, bottom_alarm = 0;
    private TextView startdate_txt, enddate_txt;
    private TextView startdate_txt2, enddate_txt2;
    private Date start_date, end_date;
    private SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
    private SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy/M/dd HH:mm");
    private SimpleDateFormat sdf3 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private RecyclerView mRecyclerView;
    private MyAdapter mAdapter;
    private ArrayList<String> myDataset = new ArrayList<>();
    private LineChart mChart;
    private drawLineChart chart;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_temperature_main);

        BottomNavigationView bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottom_navigation_view);
        bottomNavigationView.setSelectedItemId(R.id.nav_description);
        bottomNavigationView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
//////----open localDB
        localDB = new localDatabase(this);
        localDB.open();
//----介面部分
        toolbar mytoolbar = new toolbar(this);
        mDrawerLayout = findViewById(R.id.drawer_layout);
// 介面部分-----
//----讀取權限 設定是否要顯示的新增,編輯按鈕
        final Volleyconnect volleyconnect = new Volleyconnect(this);
        volleyconnect.fetch_permission(localDB.getuserid(), localDB.getpatientinfo(),
                new Volleyconnect.VolleyCallback() {
                    @Override
                    public void onSuccess(String result) {
                        if(result.equals("查看者")) {
                            volleyconnect.hideTextViewicon("addBtn", "temperature_main");
                            volleyconnect.hideTextViewicon("editBtn", "temperature_main");
                            volleyconnect.hideTextViewicon("addBtn", "temperature_main2");
                            volleyconnect.hideTextViewicon("editBtn", "temperature_main2");
                        }
                    }
                });
//讀取權限----

        View main = findViewById(R.id.temperature_main);
        View main2 = findViewById(R.id.temperature_main2);

        TextView addBtn = main.findViewById(R.id.addimgBtn);
        TextView addBtn2 = main2.findViewById(R.id.addimgBtn);

        TextView editBtn = main.findViewById(R.id.editimgBtn);
        TextView editBtn2 = main2.findViewById(R.id.editimgBtn);

        TextView settingBtn = main.findViewById(R.id.settingBtn);
        TextView settingBtn2 = main2.findViewById(R.id.settingBtn);

        startdate_txt = main.findViewById(R.id.start_date);
        enddate_txt = main.findViewById(R.id.end_date);

        startdate_txt2 = main2.findViewById(R.id.start_date);
        enddate_txt2 = main2.findViewById(R.id.end_date);

        addBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setClass(temperature_main.this, temperature_add.class);
                startActivity(intent);
            }
        });
        addBtn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setClass(temperature_main.this, temperature_add.class);
                startActivity(intent);
            }
        });
        editBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LinearLayout layout = findViewById(R.id.recyclerview_header);
                if(layout.getChildAt(3).getVisibility() == View.GONE){
                    layout.getChildAt(3).setVisibility(View.VISIBLE);
                    layout.getChildAt(4).setVisibility(View.VISIBLE);
                    mAdapter.updatevisibility(true);
                }
                else{
                    layout.getChildAt(3).setVisibility(View.GONE);
                    layout.getChildAt(4).setVisibility(View.GONE);
                    mAdapter.updatevisibility(false);
                }
            }
        });
        editBtn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ViewFlipper vf = findViewById(R.id.view_flipper);
                vf.showPrevious();

                LinearLayout layout = findViewById(R.id.recyclerview_header);
                if(layout.getChildAt(3).getVisibility() == View.GONE){
                    layout.getChildAt(3).setVisibility(View.VISIBLE);
                    layout.getChildAt(4).setVisibility(View.VISIBLE);
                    mAdapter.updatevisibility(true);
                }
            }
        });
        settingBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setClass(temperature_main.this, temperature_alarm.class);
                startActivityForResult(intent, 1);
            }
        });
        settingBtn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setClass(temperature_main.this, temperature_alarm.class);
                startActivityForResult(intent, 1);
            }
        });

        start_date = new Date();
        end_date = new Date();
        startdate_txt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                open_datetimepicker(1);
            }
        });
        startdate_txt2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                open_datetimepicker(1);
            }
        });
        enddate_txt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                open_datetimepicker(2);
            }
        });
        enddate_txt2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                open_datetimepicker(2);
            }
        });

        //local dasabase
        cursor = localDB.getinfo();
        if(cursor.getCount() > 0){
            user_id = String.valueOf(cursor.getInt(0));
            user_acc = cursor.getString(1);
        }
        patient_id = localDB.getpatientinfo();
        cursor = localDB.get_alert(RECORD_TYPE);
        if(cursor.getCount() > 0) {
            top_alarm = cursor.getDouble(1);
            bottom_alarm = cursor.getDouble(2);
        }
        //初始化recycler view
        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerview);
        final LinearLayoutManager layoutManager = new LinearLayoutManager(temperature_main.this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mRecyclerView.setLayoutManager(layoutManager);
        //myDataset放道adapter中去建立recycler view
//        mAdapter = new MyAdapter(myDataset);
//        mRecyclerView.setAdapter(mAdapter);
        //初始化圖表
        mChart = findViewById(R.id.chart1);
        chart = new drawLineChart(this, mChart);
        chart.setValueindex(2);

        //取得體溫資料
        fetch_record(RECORD_TYPE);

        //設定切換數據/圖表
        final ViewFlipper vf = findViewById(R.id.view_flipper);
        final TextView graphBtn = findViewById(R.id.graphBtn);
        final TextView dataBtn = findViewById(R.id.dataBtn2);

        graphBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(vf.getDisplayedChild() == 0) {
                    vf.showNext();
                }
            }
        });
        dataBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(vf.getDisplayedChild() == 1) {
                    vf.showPrevious();
                }
            }
        });
    }

    @Override
    protected void onDestroy(){
        super.onDestroy();
        localDB.close(); // 關閉資料庫
    }

    private void open_datetimepicker(int id){
        if(id == 1) { //選開始時間
            new SingleDateAndTimePickerDialog.Builder(temperature_main.this)
                    .bottomSheet()
                    .displayMinutes(false)
                    .displayHours(false)
                    .displayDays(false)
                    .displayMonth(true)
                    .displayYears(true)
                    .displayDaysOfMonth(true)
                    .maxDateRange(end_date)
                    .displayListener(new SingleDateAndTimePickerDialog.DisplayListener() {
                        @Override
                        public void onDisplayed(SingleDateAndTimePicker picker) {
                            picker.setDefaultDate(start_date);
                        }
                    })
                    .title(getString(R.string.startdate))
                    .listener(new SingleDateAndTimePickerDialog.Listener() {
                        @Override
                        public void onDateSelected(Date date) {
                            start_date = date;
                            end_date = date;
                            String select_date = sdf.format(date);
                            startdate_txt.setText(select_date);
                            startdate_txt2.setText(select_date);

                            if(myDataset.size() > 0) //無資料clear會閃退
                                mChart.clearValues();
                            fetch_record(RECORD_TYPE);
                        }
                    }).display();
        }
        else{ //選截止時間
            new SingleDateAndTimePickerDialog.Builder(temperature_main.this)
                    .bottomSheet()
                    .displayMinutes(false)
                    .displayHours(false)
                    .displayDays(false)
                    .displayMonth(true)
                    .displayYears(true)
                    .displayDaysOfMonth(true)
                    .minDateRange(start_date)
                    .maxDateRange(new Date())
                    .displayListener(new SingleDateAndTimePickerDialog.DisplayListener() {
                        @Override
                        public void onDisplayed(SingleDateAndTimePicker picker) {
                            picker.setDefaultDate(end_date);
                        }
                    })
                    .title(getString(R.string.stopdate))
                    .listener(new SingleDateAndTimePickerDialog.Listener() {
                        @Override
                        public void onDateSelected(Date date) {
                            end_date = date;
                            start_date = date;
                            String select_date = sdf.format(date);
                            enddate_txt.setText(select_date);
                            enddate_txt2.setText(select_date);

                            if(myDataset.size() > 0) //無資料clear會閃退
                                mChart.clearValues();
                            fetch_record(RECORD_TYPE);
                        }
                    }).display();
        }
    }

    //取得體溫資料
    private void fetch_record(final String record_type){
//        String URL = "http://140.117.71.74/graduation/fetch_record.php";
        String URL = "http://140.117.71.74/graduation/fetch_record.php";
        myDataset.clear();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            Log.d("書出訊息:", String.valueOf(jsonObject));
                            String success =jsonObject.getString("success");
                            String message = jsonObject.getString("message");
                            JSONArray record = jsonObject.getJSONArray("record");
                            if (success.equals("1")){
                                //放進datalist中
                                for(int i = 0; i < record.length(); i++){
                                    JSONObject single_record = record.getJSONObject(i);
                                    String item = "";
                                    //處理輸出日期時間的格式
                                    String format_datetime = single_record.getString("datetime");
                                    try {
                                        Date dt = sdf3.parse(format_datetime);
                                        format_datetime = sdf2.format(dt);
                                    } catch (ParseException e) {
                                        e.printStackTrace();
                                    }
                                    item = format_datetime
                                            + " " + single_record.getString("temperaturevalue")
                                            + " " + single_record.getString("temperatureID");
                                    myDataset.add(item);
                                }
                                //myDataset放道adapter中去建立recycler view
                                mAdapter = new MyAdapter(myDataset);
                                mRecyclerView.setItemAnimator(new SlideInUpAnimator());
                                mRecyclerView.getItemAnimator().setRemoveDuration(0);
                                mRecyclerView.getItemAnimator().setChangeDuration(500);
                                mRecyclerView.setAdapter(new AlphaInAnimationAdapter(mAdapter));


                                //myDataset放道圖表中建立圖表
                                if(myDataset.size() > 0) {
                                    chart.addDataSet(myDataset, "#59c7fa", getString(R.string.temperature));
                                    chart.setupChart(myDataset);
                                }
                                //Toast.makeText(temperature_main.this, message, Toast.LENGTH_SHORT).show();
                            }
                            else{
                                Toast.makeText(temperature_main.this, message, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.d("錯誤訊息",e.toString());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(temperature_main.this,getString(R.string.searchfail)+error.toString(),Toast.LENGTH_SHORT).show();
                    }
                })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String>params = new HashMap<>();
                params.put("record_type", record_type);
                params.put("patientid", patient_id);
                if(!startdate_txt.getText().toString().equals(getString(R.string.startdate)))
                    params.put("startdate", startdate_txt.getText().toString());
                if(!enddate_txt.getText().toString().equals(getString(R.string.enddate)))
                    params.put("enddate", enddate_txt.getText().toString());
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    //以下皆為recyclerview有關的function
    public class MyAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
        private List<String> mData;
        private boolean isvisible = false;
        //要求權現的viewholder
        public class ViewHolder1 extends RecyclerView.ViewHolder {
            public TextView datetxt, timetxt;
            public TextView valuetxt;
            public ImageView editBtn, delBtn;
            public ViewHolder1(View v) {
                super(v);
                datetxt = (TextView) v.findViewById(R.id.recycler_date);
                timetxt = v.findViewById(R.id.recycler_time);
                valuetxt = v.findViewById(R.id.recycler_value);
                editBtn = v.findViewById(R.id.recycler_editBtn);
                delBtn = v.findViewById(R.id.recycler_delBtn);
            }

        }

        public MyAdapter(List<String> data) {
            mData = data;
        }
        @Override
        public int getItemViewType(int position) {
           return 0;
        }


        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v;
            v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycleview_temperature, parent, false);
            return new ViewHolder1(v);
        }

        @Override
        public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
            final ViewHolder1 viewholder1 = (ViewHolder1)holder;
            final String[] data = mData.get(position).split(" ");
            viewholder1.datetxt.setText(data[0].substring(5));
            viewholder1.timetxt.setText(data[1]);
            viewholder1.valuetxt.setText(data[2]);
            Double value = Double.parseDouble(data[2]);
            if((top_alarm != 0 && value > top_alarm) || (bottom_alarm != 0 && value < bottom_alarm))
                viewholder1.valuetxt.setTextColor(Color.RED);

            if(isvisible){
                viewholder1.editBtn.setVisibility(View.VISIBLE);
                viewholder1.delBtn.setVisibility(View.VISIBLE);
            }
            else{
                viewholder1.editBtn.setVisibility(View.GONE);
                viewholder1.delBtn.setVisibility(View.GONE);
            }

            viewholder1.editBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent();
                    intent.setClass(getApplication(), temperature_edit.class);
                    Bundle bundle = new Bundle();
                    bundle.putString("datetime", data[0] + " " + data[1]);
                    bundle.putString("temperaturevalue", data[2]);
                    bundle.putString("temperatureid", data[3]);
                    intent.putExtras(bundle);
                    startActivityForResult(intent, 1);
                }
            });
            viewholder1.delBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    new AlertDialog.Builder(temperature_main.this)
                            .setTitle(getString(R.string.suredelete))
                            .setMessage(getString(R.string.deletenorecovery))
                            .setNegativeButton(getString(R.string.sure), new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialoginterface, int i)
                                {
                                    String recordid = data[3];
                                    //delete record
                                    Volleyconnect volleyconnect = new Volleyconnect(getApplicationContext());
                                    volleyconnect.delete_record(RECORD_TYPE, recordid, new Volleyconnect.VolleyCallback() {
                                        @Override
                                        public void onSuccess(String result) {
                                            if(result.equals("1")){
                                                myDataset.remove(position);
                                                notifyItemRemoved(position);
                                                notifyItemRangeChanged(position, myDataset.size());

                                                //刷新linechart
                                                chart.resetchart();
                                                if(myDataset.size() > 0) {
                                                    chart.addDataSet(myDataset, "#59c7fa", getString(R.string.temperature));
                                                    chart.setupChart(myDataset);
                                                }
                                            }
                                        }
                                    });
                                }
                            })
                            .setPositiveButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialoginterface, int i)
                                {

                                }
                            })
                            .show();
                }
            });
        }

        @Override
        public int getItemCount() {
            return mData.size();
        }

        public void updatevisibility(boolean value){
            isvisible = value;
            if(value == true) //有動畫
                notifyItemRangeChanged(0, mData.size());
            else //無動畫
                notifyDataSetChanged();
        }
    }


    /*下一頁修改完畢回傳時刷新recyclerview*/
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == 1){
            if(resultCode == RESULT_OK){
                cursor = localDB.get_alert(RECORD_TYPE);
                if(cursor.getCount() > 0) {
                    top_alarm = cursor.getDouble(1);
                    bottom_alarm = cursor.getDouble(2);
                }
                fetch_record(RECORD_TYPE);
            }
        }
    }
//--介面部分
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
//                mDrawerLayout.openDrawer(Gravity.RIGHT);
                temperature_main.this.finish();
                return true;
            case R.id.user_info:
                mDrawerLayout.openDrawer(Gravity.RIGHT);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.game_menu, menu);
        //set pattent name of menu
        MenuItem patientname = menu.findItem(R.id.patient_name);
        patientname.setTitle(localDB.getpatientname());
        return true;
    }
//    介面部分--

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected( MenuItem item) {
            BottomNavigation bn = new BottomNavigation(temperature_main.this, item.getItemId());
            return true;
        }

    };
}
