package com.example.kavin.try5;

import android.content.Intent;
import android.content.res.Resources;
import android.database.Cursor;
import android.os.Bundle;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class bathroom_edit extends AppCompatActivity {
    private DrawerLayout mDrawerLayout;
    private localDatabase localDB = null;
    private Cursor cursor = null;
    private String user_id, user_acc, patient_id;
    private static final String RECORD_TYPE = "bathroom";
    private SimpleDateFormat sdf = new SimpleDateFormat("yyyy/M/dd HH:mm");
    private SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy/MM/dd a hh:mm");
    private String recordID;
    private RadioButton wantbathroom, hasbathroom; //想尿, 排尿
    private TextView datetime;
    private Button submitBtn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bathroom_edit);
        BottomNavigationView bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottom_navigation_view);
        bottomNavigationView.setSelectedItemId(R.id.nav_description);
        bottomNavigationView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        //local dasabase
        localDB = new localDatabase(this);
        localDB.open();
        cursor = localDB.getinfo();
        if(cursor.getCount() >= 0){
            user_id = String.valueOf(cursor.getInt(0));
            user_acc = cursor.getString(1);
        }
        patient_id = localDB.getpatientinfo();

//----介面部分
        toolbar mytoolbar = new toolbar(this);
        mDrawerLayout = findViewById(R.id.drawer_layout);
// 介面部分-----

        wantbathroom = findViewById(R.id.radioButton4);
        hasbathroom = findViewById(R.id.radioButton5);
        datetime = findViewById(R.id.datetime);
        Bundle bundle = this.getIntent().getExtras();
        String[] data = bundle.getStringArray("data");

        String format_time = data[0] + " " + data[1];
        try {
            Date dt = sdf.parse(format_time);
            format_time = sdf2.format(dt);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        datetime.setText(format_time);

        if(data[2].equals("想尿"))
            wantbathroom.setChecked(true);
        else
            hasbathroom.setChecked(true);
        recordID = data[3];
        submitBtn = findViewById(R.id.submitBtn);
        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                update_record(RECORD_TYPE);
            }
        });




    }

    @Override
    protected void onDestroy(){
        super.onDestroy();
        localDB.close(); // 關閉資料庫
    }

    private void update_record(final String record_type){
//        String URL = "http://140.117.71.74/graduation/update_record.php";
        String URL = "http://140.117.71.74/graduation/update_record.php";
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            Log.d("書出訊息:", String.valueOf(jsonObject));
                            String success =jsonObject.getString("success");
                            String message = jsonObject.getString("message");
                            if (success.equals("1")){
                                Toast.makeText(bathroom_edit.this, message, Toast.LENGTH_SHORT).show();
                                setResult(RESULT_OK);
                                finish();
                            }
                            else{
                                Toast.makeText(bathroom_edit.this, message, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.d("錯誤訊息",e.toString());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        String searchfail = Resources.getSystem().getString(R.string.searchfail);
                        Toast.makeText(bathroom_edit.this,searchfail+error.toString(),Toast.LENGTH_SHORT).show();
                    }
                })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String>params = new HashMap<>();
                params.put("record_type", record_type);
                params.put("caretakerID", user_id);
                String status = "";
                if(wantbathroom.isChecked())
                    status = "想尿";
                else
                    status = "排尿";
                params.put("status", status);
                params.put("recordID", recordID);
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    //--介面部分
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
//                mDrawerLayout.openDrawer(Gravity.RIGHT);
                bathroom_edit.this.finish();
                return true;
            case R.id.user_info:
                mDrawerLayout.openDrawer(Gravity.RIGHT);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.game_menu, menu);
        //set pattent name of menu
        MenuItem patientname = menu.findItem(R.id.patient_name);
        patientname.setTitle(localDB.getpatientname());
        return true;
    }
//    介面部分--

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected( MenuItem item) {
            BottomNavigation bn = new BottomNavigation(bathroom_edit.this, item.getItemId());
            return true;
        }

    };
}
