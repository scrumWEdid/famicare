package com.example.kavin.try5.fetch_remindertime;

import android.content.Context;
import android.content.res.Resources;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.kavin.try5.R;
import com.example.kavin.try5.localDatabase;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class MyReminder2 {
    private Context context;
    private localDatabase localDB = null;
    private String RECORD_TYPE = "";
    private String patientID = "";

    private JSONArray record = new JSONArray();
    private JSONArray donerecord = new JSONArray(); //存已完成的

    public MyReminder2(Context ctx, String record_type){
        this.context = ctx;
        RECORD_TYPE = record_type;
        //----open localDB
        localDB = new localDatabase(context);
        localDB.open();
        patientID = localDB.getpatientinfo();
    }

    //--------connect to MySQL----------
    public interface VolleyCallback{
        void onSuccess(String result);
    }
    public void fetch_data(final VolleyCallback callback){
        record = new JSONArray();
        String URL = "http://140.117.71.74/graduation/fetch_reminder.php";
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            String success =jsonObject.getString("success");
                            String message = jsonObject.getString("message");
                            if (success.equals("1")){
                                record = jsonObject.getJSONArray("record");
//                                Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                                callback.onSuccess(success);
                            }
                            else{
                                Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.d("錯誤訊息",e.toString());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(context, Resources.getSystem().getString(R.string.searchfail)+error.toString(),Toast.LENGTH_SHORT).show();
                    }
                })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String>params = new HashMap<>();
                params.put("patientID", patientID);
                params.put("record_type", RECORD_TYPE);
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        requestQueue.add(stringRequest);
    }

    //抓取已完成的
    public void fetchdonedata(){
        donerecord = new JSONArray();
        String URL = "http://140.117.71.74/graduation/fetch_donereminder.php";
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            String success =jsonObject.getString("success");
                            String message = jsonObject.getString("message");
                            if (success.equals("1")){
//                                Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                                donerecord = jsonObject.getJSONArray("record");
                            }
                            else{
                                Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.d("錯誤訊息",e.toString());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(context,Resources.getSystem().getString(R.string.searchfail)+error.toString(),Toast.LENGTH_SHORT).show();
                    }
                })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String>params = new HashMap<>();
                params.put("patientID", patientID);
                params.put("record_type", RECORD_TYPE);
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        requestQueue.add(stringRequest);
    }

    //寫入已完成的提醒
    public void done_reminder(final String id, final String donedate, final String donetime){
        String URL = "http://140.117.71.74/graduation/add_donereminder.php";
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            String success =jsonObject.getString("success");
                            String message = jsonObject.getString("message");
                            if (success.equals("1")){
                                Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                            }
                            else{
                                Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.d("錯誤訊息",e.toString());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(context,Resources.getSystem().getString(R.string.searchfail)+error.toString(),Toast.LENGTH_SHORT).show();
                    }
                })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String>params = new HashMap<>();
                params.put("id", id);
                params.put("donedate", donedate);
                params.put("donetime", donetime);
                params.put("record_type", RECORD_TYPE);
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        requestQueue.add(stringRequest);
    }
    //--------connect to MySQL----------//

    //測量提醒
    public ArrayList<JSONObject> dayreminder(String date) throws ParseException, JSONException {

        ArrayList<JSONObject> myDataset = new ArrayList<>();

        //算選取日期與開始時間差幾天
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date selectdate = sdf.parse(date);
//        Toast.makeText(context, record.toString(), Toast.LENGTH_SHORT).show();
        for(int i = 0; i < record.length(); i++) {
            JSONObject data = new JSONObject(record.getJSONObject(i).toString()); //pass by value
            Date startdate = sdf.parse(data.getString("startdate"));

            long diff = selectdate.getTime() - startdate.getTime();
            int day_diff = (int) (diff / (24 * 60 * 60 * 1000));
            int repeatday = Integer.valueOf(data.getString("repeatday"));

            if(day_diff % repeatday == 0 && day_diff >= 0) { //這天要提醒
                String id = data.getString("recordReminderID");
                String donedate = date;
                String donetime = "";
                JSONArray untaketimeArray = new JSONArray();

                JSONArray remindtimeArray = data.getJSONArray("remindertime");
                for (int j = 0; j < remindtimeArray.length(); j++) {
                    donetime = remindtimeArray.getString(j);

                    //判斷有沒用藥
                    int k;
                    for(k = 0; k < donerecord.length(); k++){
                        JSONObject jsonObject = donerecord.getJSONObject(k);
                        if(jsonObject.getString("recordReminderID").equals(id) //已服用這筆藥
                            && jsonObject.getString("donedate").equals(donedate)
                            && jsonObject.getString("donetime").equals(donetime))
                            break;
                    }
                    if(k == donerecord.length()) //未完成該提醒!!
                        untaketimeArray.put(donetime);
                }
                data.put("undoneremindertime", untaketimeArray); //這行反註解掉就能過濾
                myDataset.add(data);
            }
        }
        return myDataset;
    }

    //管路提醒
    public ArrayList<JSONObject> dayreminder2(String date) throws ParseException, JSONException {

        ArrayList<JSONObject> myDataset = new ArrayList<>();

        //算選取日期與開始時間差幾天
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date selectdate = sdf.parse(date);
//        Toast.makeText(context, record.toString(), Toast.LENGTH_SHORT).show();
        for(int i = 0; i < record.length(); i++) {
            JSONObject data = new JSONObject(record.getJSONObject(i).toString()); //pass by value
            String enddate = data.getString("enddatetime").split(" ")[0];

            if(enddate.equals(date)) { //要提醒
                String id = data.getString("pipelineReminderID");
                String endtime = data.getString("enddatetime").split(" ")[1];
                //判斷有完成
                int k;
                for (k = 0; k < donerecord.length(); k++) {
                    JSONObject jsonObject = donerecord.getJSONObject(k);
                    if (jsonObject.getString("pipelineReminderID").equals(id) //已服用這筆藥
                            && jsonObject.getString("donedate").equals(enddate)
                            && jsonObject.getString("donetime").equals(endtime))
                        break;
                }
                if (k == donerecord.length()) //未完成該提醒!!
                    myDataset.add(data);
            }
        }
        return myDataset;
    }
}
