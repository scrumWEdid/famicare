package com.example.kavin.try5;

import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewFlipper;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import jp.wasabeef.recyclerview.adapters.AlphaInAnimationAdapter;
import jp.wasabeef.recyclerview.animators.SlideInUpAnimator;

public class videoMainPage extends AppCompatActivity {
    private DrawerLayout mDrawerLayout;
    private localDatabase localDB = null;
    private Cursor cursor = null;
    private String user_id, patient_id;
    private RecyclerView mRecyclerView,mRecyclerView2;
    private MyAdapter mAdapter;
    private final static String RECORD_TYPE = "video";
    private ArrayList<String> myDataset = new ArrayList<>();
    private ArrayList<String> myDataset2 = new ArrayList<>();
    public static final int ALL_TYPE =1;
    public static final int SELECTED_TYPE =2;
    public int flag =0;
    public boolean edit =false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_main_page);

        //設定切換數據/圖表
        final ViewFlipper vf = findViewById(R.id.view_flipper);
        final TextView dataBtn = findViewById(R.id.textView114);
        final TextView graphBtn = findViewById(R.id.textView115);
        final TextView dataBtn2 = findViewById(R.id.textView1142);
        final TextView graphBtn2 = findViewById(R.id.textView1152);
        final FloatingActionButton addBtn =findViewById(R.id.addimgBtn);
        final FloatingActionButton editBtn =findViewById(R.id.editimgBtn);
        addBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(vf.getDisplayedChild() == 0) {
                    vf.showNext();
                    Log.d("tab test","2");
                    //all
                    flag =1;
                    edit =false;
                    fetch_record(RECORD_TYPE);
                }
            }
        });
        editBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edit ==false){
                    edit = true;
                }
                else{
                    edit =false;
                }
            fetch_record(RECORD_TYPE);
            }
        });
//        final FloatingActionButton addBtn = findViewById(R.id.fab);
//        addBtn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if(vf.getDisplayedChild() == 0) {
//                    vf.showNext();
//                }
//            }
//        });

//        dataBtn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if(vf.getDisplayedChild() == 1) {
//                    vf.showPrevious();
//                    Log.d("tab test","1");
//                }
//            }
//        });
        graphBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(vf.getDisplayedChild() == 0) {
                    vf.showNext();
                    Log.d("tab test","2");
                    //all
                    flag =1;
                    edit =false;
                    fetch_record(RECORD_TYPE);
                }
            }
        });

//        dataBtn2.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if(vf.getDisplayedChild() == 0) {
//                    vf.showPrevious();
//                    Log.d("tab test","3");
//                }
//            }
//        });
        graphBtn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(vf.getDisplayedChild() == 1) {
                    vf.showNext();
                    Log.d("tab test","4");
                    //selected
                    flag=0;
                    fetch_record(RECORD_TYPE);
                }
            }
        });

        BottomNavigationView bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottom_navigation_view);
        bottomNavigationView.setSelectedItemId(R.id.videopage);
        bottomNavigationView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        //取得資料


        //----介面部分
        toolbar mytoolbar = new toolbar(this);
        mDrawerLayout = findViewById(R.id.drawer_layout);
// 介面部分-----
        localDB = new localDatabase(this);
        localDB.open();
        cursor = localDB.getinfo();
        if(cursor.getCount() >= 0){
            user_id = String.valueOf(cursor.getInt(0));
        }
        patient_id = localDB.getpatientinfo();

        //初始化recycler view
        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerview);
        final LinearLayoutManager layoutManager = new LinearLayoutManager(videoMainPage.this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mRecyclerView.setLayoutManager(layoutManager);

        mRecyclerView2 = (RecyclerView) findViewById(R.id.recyclerview2);
        final LinearLayoutManager layoutManager2 = new LinearLayoutManager(videoMainPage.this);
        layoutManager2.setOrientation(LinearLayoutManager.VERTICAL);
        mRecyclerView2.setLayoutManager(layoutManager2);

        fetch_record(RECORD_TYPE);
        //建個十筆測試資料
//        for(int i = 0; i < 10; i++)
//            myDataset.add("" + i);
        //myDataset放道adapter中去建立recycler view
//        mAdapter = new MyAdapter(myDataset);
//        mRecyclerView.setAdapter(mAdapter);
    }
    //--介面部分
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
//                mDrawerLayout.openDrawer(Gravity.RIGHT);
                videoMainPage.this.finish();
                return true;
            case R.id.user_info:
                mDrawerLayout.openDrawer(Gravity.RIGHT);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.game_menu, menu);
        //set pattent name of menu
        MenuItem patientname = menu.findItem(R.id.patient_name);
        patientname.setTitle(localDB.getpatientname());
        return true;
    }
    //    介面部分--
    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected( MenuItem item) {
            BottomNavigation bn = new BottomNavigation(videoMainPage.this, item.getItemId());
            return true;
        }

    };
    //----------------Connect to mySQL----------------------
    //add or delete video
    private void select_video(final String record_type,final String name,final String video_ID){
//        String URL = "http://140.117.71.74/graduation/fetch_record.php";
        String URL = "http://140.117.71.74/graduation/select_video.php";

        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            Log.d("書出訊息:", String.valueOf(jsonObject));
                            String success =jsonObject.getString("success");
                            String message = jsonObject.getString("message");
                            JSONArray record = jsonObject.getJSONArray("record");
                            if (success.equals("1")){
                            }
                            else{
                                Toast.makeText(videoMainPage.this, message, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.d("錯誤訊息",e.toString());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(videoMainPage.this,getString(R.string.searchfail)+error.toString(),Toast.LENGTH_SHORT).show();
                    }
                })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String>params = new HashMap<>();
                params.put("record_type", record_type);
                params.put("patientid", patient_id);
                params.put("name", name);
                params.put("video_ID", video_ID);
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    private void fetch_record(final String record_type){
//        String URL = "http://140.117.71.74/graduation/fetch_record.php";
        String URL = "http://140.117.71.74/graduation/fetch_record.php";
        myDataset.clear();
        myDataset2.clear();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            Log.d("書出訊息:", String.valueOf(jsonObject));
                            String success =jsonObject.getString("success");
                            String message = jsonObject.getString("message");
                            JSONArray record = jsonObject.getJSONArray("record");
                            if (success.equals("1")){
                                //放進datalist中
                                for(int i = 0; i < record.length(); i++){
                                    JSONObject single_record = record.getJSONObject(i);
                                    String item = "";
                                    //處理輸出日期時間的格式
                                    String selected = "0";
                                    if(single_record.getString("patientID")!="null"){
                                        Log.d("asd",single_record.getString("name"));
                                        selected ="1";
                                        String item2 = "";
                                        String video_name = single_record.getString("name");
                                        String video_Id = single_record.getString("video_ID");
                                        String video_url = single_record.getString("url");
                                        item2 = video_name
                                                +" " + video_Id
                                                +" " + video_url
                                                +" " + selected;
                                        myDataset2.add(item2);
                                        if(myDataset2.size() > 0) {
                                            mAdapter = new videoMainPage.MyAdapter(myDataset2);
//                                        mRecyclerView2.setItemAnimator(new SlideInUpAnimator());
//                                        mRecyclerView2.getItemAnimator().setRemoveDuration(0);
//                                        mRecyclerView2.getItemAnimator().setChangeDuration(500);
//                                        mRecyclerView2.setAdapter(new AlphaInAnimationAdapter(mAdapter));
                                            mRecyclerView2.setAdapter(mAdapter);
                                        }
                                    }


                                    String video_name = single_record.getString("name");
                                    String video_Id = single_record.getString("video_ID");
                                    String video_url = single_record.getString("url");
                                    item = video_name
                                            +" " + video_Id
                                            +" " + video_url
                                            +" " + selected;
                                    myDataset.add(item);
                                }
                                //myDataset放道adapter中去建立recycler view
                                mAdapter = new videoMainPage.MyAdapter(myDataset);
//                                mRecyclerView.setItemAnimator(new SlideInUpAnimator());
//                                mRecyclerView.getItemAnimator().setRemoveDuration(0);
//                                mRecyclerView.getItemAnimator().setChangeDuration(500);
//                                mRecyclerView.setAdapter(new AlphaInAnimationAdapter(mAdapter));
                                mRecyclerView.setAdapter(mAdapter);


                                //myDataset放道圖表中建立圖表
//                                if(myDataset.size() > 0) {
//                                    classifydataset();
//                                }
                            }
                            else{
                                Toast.makeText(videoMainPage.this, message, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.d("錯誤訊息",e.toString());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(videoMainPage.this,getString(R.string.searchfail)+error.toString(),Toast.LENGTH_SHORT).show();
                    }
                })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String>params = new HashMap<>();
                params.put("record_type", record_type);
                params.put("patientid", patient_id);

                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }


    //以下皆為recyclerview有關的function
    public class MyAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

        private List<String> mData;
        private boolean isvisible = false;
        //要求權現的viewholder
        public class ViewHolder1 extends RecyclerView.ViewHolder {
            public TextView descriptionTxt;
            public ImageView videoImg, modImg;
            public ViewHolder1(View v) {
                super(v);
                descriptionTxt = v.findViewById(R.id.descriptionTxt);
                videoImg = v.findViewById(R.id.videoImg);
                modImg = v.findViewById(R.id.modImg);
            }

        }
        public class ViewHolder2 extends RecyclerView.ViewHolder {
            public TextView descriptionTxt2;
            public ImageView videoImg2, modImg2;
            public ViewHolder2(View v) {
                super(v);
                descriptionTxt2 = v.findViewById(R.id.descriptionTxt2);
                videoImg2 = v.findViewById(R.id.videoImg2);
                modImg2 = v.findViewById(R.id.modImg2);
            }

        }
        public MyAdapter(List<String> data) {
            mData = data;
        }
        @Override
        public int getItemViewType(int position) {
            if(flag==1){
                return ALL_TYPE ;
            }
            else return SELECTED_TYPE;
        }


        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
           if (viewType==ALL_TYPE) {
               View v;
               v = LayoutInflater.from(parent.getContext())
                       .inflate(R.layout.recyclerview_videopage, parent, false);
               return new ViewHolder1(v);
           }
           else {
                    View v2;
                    v2 = LayoutInflater.from(parent.getContext())
                            .inflate(R.layout.recyclerview_videopage2, parent, false);
                    return new ViewHolder2(v2);
            }
        }

        @Override
        public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
            if (holder.getItemViewType()==ALL_TYPE) {

                final ViewHolder1 viewholder1 = (ViewHolder1) holder;
                final String[] data = mData.get(position).split(" ");
                viewholder1.descriptionTxt.setText(data[0]);
                String imgURL = "http://140.117.71.74/graduation/videoimage/" + data[1] + ".png";
                Log.d("imgURL", imgURL);
                Picasso.get()
                        .load(imgURL)
                        .error(R.drawable.ic_drugs) //movie icon
                        .into(viewholder1.videoImg);
                String url = data[2];
                final Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                viewholder1.videoImg.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        startActivity(i);
                    }
                });
                viewholder1.descriptionTxt.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        startActivity(i);
                    }
                });
                if (data[3].equals("1")) {
                    viewholder1.modImg.setImageResource(R.drawable.ic_star_black_24dp);
                    viewholder1.modImg.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            select_video("delete",data[0],data[1]);

                            fetch_record(RECORD_TYPE);//poor way to refresh
                        }
                    });
                } else {
                    viewholder1.modImg.setImageResource(R.drawable.ic_star_border_blank_24dp);
                    viewholder1.modImg.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            select_video("add",data[0],data[1]);

                            fetch_record(RECORD_TYPE);//poor way to refresh
                        }
                    });
                }
            }
              else{
                    final ViewHolder2 viewholder2 = (ViewHolder2)holder;
                    final String[] data2 = mData.get(position).split(" ");
                    viewholder2.descriptionTxt2.setText(data2[0]);
                    String imgURL2 = "http://140.117.71.74/graduation/videoimage/"+data2[1]+ ".png";
                    Log.d("imgURL",imgURL2);
                    Picasso.get()
                            .load(imgURL2)
                            .error(R.drawable.ic_drugs) //movie icon
                            .into(viewholder2.videoImg2);
                    String url2 = data2[2];
                    final Intent i2 = new Intent(Intent.ACTION_VIEW);
                    i2.setData(Uri.parse(url2));
                    viewholder2.videoImg2.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            startActivity(i2);
                        }
                    });
                    viewholder2.descriptionTxt2.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            startActivity(i2);
                        }
                    });
                if(edit==true){
                    viewholder2.modImg2.setVisibility(View.VISIBLE);
                }else {
                    viewholder2.modImg2.setVisibility(View.GONE);
                }
                viewholder2.modImg2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        select_video("delete",data2[0],data2[1]);
                        fetch_record(RECORD_TYPE);//poor way to refresh
                    }
                });
            }

        }

        @Override
        public int getItemCount() {
            return mData.size();
        }

        public void updatevisibility(boolean value){
            isvisible = value;
            if(value == true) //有動畫
                notifyItemRangeChanged(0, mData.size());
            else //無動畫
                notifyDataSetChanged();
        }
    }
}
