package com.example.kavin.try5.fetch_remindertime;

import android.content.Context;
import android.content.res.Resources;
import android.text.format.DateUtils;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.kavin.try5.R;
import com.example.kavin.try5.localDatabase;
import com.google.gson.JsonArray;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class MyReminder {
    private Context context;
    private localDatabase localDB = null;
    private String RECORD_TYPE = "";
    private String patientID = "";

    private JSONArray record = new JSONArray();
    private JSONArray donerecord = new JSONArray(); //存服用完的藥品

    public MyReminder(Context ctx, String record_type){
        this.context = ctx;
        RECORD_TYPE = record_type;
        //----open localDB
        localDB = new localDatabase(context);
        localDB.open();
        patientID = localDB.getpatientinfo();
    }
    //--------connect to MySQL----------
    public interface VolleyCallback{
        void onSuccess(String result);
    }
    public void fetch_data(final String method, final String id, final VolleyCallback callback){
        record = new JSONArray();
        String URL = "http://140.117.71.74/graduation/fetch_reminder.php";
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            String success =jsonObject.getString("success");
                            String message = jsonObject.getString("message");
                            if (success.equals("1")){
                                record = jsonObject.getJSONArray("record");
//                                Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                                callback.onSuccess(success);
                            }
                            else{
                                Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.d("錯誤訊息",e.toString());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(context,"搜尋失敗"+error.toString(),Toast.LENGTH_SHORT).show();
                    }
                })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String>params = new HashMap<>();
                if(RECORD_TYPE.equals("medicine")) {
                    if (method.equals("view_all")) //用要提醒
                        params.put("patientID", id);
                    else //個人藥盒
                        params.put("personalMedicineID", id);
                }
                params.put("record_type", RECORD_TYPE);
                params.put("method", method);
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        requestQueue.add(stringRequest);
    }

    //抓取已用藥
    public void fetchdoneMedicine(){
        donerecord = new JSONArray();
        String URL = "http://140.117.71.74/graduation/fetch_taken_medicine.php";
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            Log.d("書出訊息:", String.valueOf(jsonObject));
                            String success =jsonObject.getString("success");
                            String message = jsonObject.getString("message");
                            if (success.equals("1")){
//                                Toast.makeText(context, message + "123", Toast.LENGTH_SHORT).show();
                                donerecord = jsonObject.getJSONArray("record");
                            }
                            else{
                                Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.d("錯誤訊息",e.toString());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(context, Resources.getSystem().getString(R.string.searchfail)+error.toString(),Toast.LENGTH_SHORT).show();
                    }
                })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String>params = new HashMap<>();
                params.put("patientID", patientID);
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        requestQueue.add(stringRequest);
    }

    public void done_reminder(final String medName, final String reminddate, final String remindtime,
                              final VolleyCallback callback){
        String URL = "http://140.117.71.74/graduation/add_donereminder.php";
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            String success =jsonObject.getString("success");
                            String message = jsonObject.getString("message");
                            if (success.equals("1")){
//                                Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                                callback.onSuccess(success);
                            }
                            else{
                                Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.d("錯誤訊息",e.toString());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(context,Resources.getSystem().getString(R.string.searchfail)+error.toString(),Toast.LENGTH_SHORT).show();
                    }
                })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String>params = new HashMap<>();
                params.put("patientID", patientID);
                params.put("name", medName);
                params.put("reminddate", reminddate);
                params.put("remindtime", remindtime);
                params.put("record_type", RECORD_TYPE);
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        requestQueue.add(stringRequest);
    }
    //--------connect to MySQL----------


    public String[] single_pill_time(){

        String[] output = new String[2];

        try {
            JSONObject data = record.getJSONObject(0);

            String[] outputdate = new String[2];
            String[] outputtime = new String[2];

            //算今天與開始時間差幾天
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Date startdate = sdf.parse(data.getString("startdate"));
            Date currentdate = new Date();
            long diff = currentdate.getTime() - startdate.getTime();
            int day_diff = (int)(float)diff /  (24 * 60 * 60 * 1000);
            int repeatday = Integer.valueOf(data.getString("repeatday"));

            //算最近的每日用藥時間
            SimpleDateFormat sdf2 = new SimpleDateFormat("HH:mm:ss");
            String currenttime = sdf2.format(new Date());



            if(day_diff % repeatday == 0 && day_diff >= 0){ //這天要吃藥
                JSONArray time = data.getJSONArray("remindertime");
                int i;
                for(i = 0; i < time.length(); i++){ //紀錄現在時間在哪個兩個用藥時間中間
                    String t = time.getString(i);
                    if(currenttime.compareTo(t) < 0) {
                        break;
                    }
                }

                if(i == 0) { //上次用藥時間不在今天
                    Calendar c = Calendar.getInstance();

                    outputdate[1] = sdf.format(c.getTime());
                    c.add(Calendar.DATE, repeatday*(-1));  // number of days to add
                    outputdate[0] = sdf.format(c.getTime());  // dt is now the new date

                    outputtime[0] = time.getString(time.length()-1).substring(0, 5);
                    outputtime[1] = time.getString(i).substring(0, 5);



                }
                else if(i == time.length()){ //下次用藥時間不在今天
                    Calendar c = Calendar.getInstance();
                    outputdate[0] = sdf.format(c.getTime());

                    c.add(Calendar.DATE, repeatday);  // number of days to add
                    outputdate[1] = sdf.format(c.getTime());  // dt is now the new date

                    outputtime[0] = time.getString(i - 1).substring(0, 5);
                    outputtime[1] = time.getString(0).substring(0, 5);

                }
                else {
                    Calendar c = Calendar.getInstance();
                    outputdate[0] = sdf.format(c.getTime());
                    outputdate[1] = sdf.format(c.getTime());
                    outputtime[0] = time.getString(i - 1).substring(0, 5);
                    outputtime[1] = time.getString(i).substring(0, 5);

                }

            }
            else{ //不是這天吃藥
                int d = day_diff % repeatday;
                JSONArray time = data.getJSONArray("remindertime");

                Calendar c = Calendar.getInstance();;
                c.add(Calendar.DATE, d*(-1));  // number of days to add
                outputdate[0] = sdf.format(c.getTime());   // dt is now the new date

                c = Calendar.getInstance();
                c.add(Calendar.DATE, repeatday-d);
                outputdate[1] = sdf.format(c.getTime());

                outputtime[0] = time.getString(time.length()-1).substring(0, 5);
                outputtime[1] = time.getString(0).substring(0, 5);

            }

            output[0] = outputdate[0] + " " + outputtime[0];
            output[1] = outputdate[1] + " " + outputtime[1];

        } catch (JSONException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return output;
    }

    public ArrayList<JSONObject> dayreminder(String date) throws ParseException, JSONException {

        ArrayList<JSONObject> myDataset = new ArrayList<>();

        //算選取日期與開始時間差幾天
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date selectdate = sdf.parse(date);
//        Toast.makeText(context, record.toString(), Toast.LENGTH_SHORT).show();
        for(int i = 0; i < record.length(); i++) {
            JSONObject data = new JSONObject(record.getJSONObject(i).toString()); //pass by value
            Date startdate = sdf.parse(data.getString("startdate"));

            long diff = selectdate.getTime() - startdate.getTime();
            int day_diff = (int) (diff / (24 * 60 * 60 * 1000));
            int repeatday = Integer.valueOf(data.getString("repeatday"));

            if(day_diff % repeatday == 0 && day_diff >= 0) { //這天要用藥
                String name = data.getString("name");
                String donedate = date;
                String donetime = "";
                JSONArray untaketimeArray = new JSONArray();

                JSONArray remindtimeArray = data.getJSONArray("remindertime");
                for (int j = 0; j < remindtimeArray.length(); j++) {
                    donetime = remindtimeArray.getString(j);

                    //判斷有沒用藥
                    int k;
                    for(k = 0; k < donerecord.length(); k++){
                        JSONObject jsonObject = donerecord.getJSONObject(k);
                        if(jsonObject.getString("name").equals(name) //已服用這筆藥
                            && jsonObject.getString("reminddate").equals(donedate)
                            && jsonObject.getString("remindtime").equals(donetime))
                            break;
                    }
                    if(k == donerecord.length()) //未服用這筆藥!!
                        untaketimeArray.put(donetime);
                }
                data.put("undoneremindertime", untaketimeArray); //這行反註解掉就能過濾
                myDataset.add(data);
            }
        }
        return myDataset;
    }


}
