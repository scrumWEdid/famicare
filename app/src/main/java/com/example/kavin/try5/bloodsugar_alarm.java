package com.example.kavin.try5;

import android.content.Intent;
import android.database.Cursor;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class bloodsugar_alarm extends AppCompatActivity {
    private DrawerLayout mDrawerLayout;
    private localDatabase localDB = null;
    private Cursor cursor = null;
    private final static  String RECORD_TYPE = "bloodsugar";
    private EditText topvalue, bottomvalue;
    private Button submitBtn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bloodsugar_alarm);
        BottomNavigationView bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottom_navigation_view);
        bottomNavigationView.setSelectedItemId(R.id.nav_description);
        bottomNavigationView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        //local dasabase
        localDB = new localDatabase(this);
        localDB.open();

        //----介面部分
        toolbar mytoolbar = new toolbar(this);
        mDrawerLayout = findViewById(R.id.drawer_layout);
// 介面部分-----

        topvalue = findViewById(R.id.alarm_top);
        bottomvalue = findViewById(R.id.alarm_bottom);
        submitBtn = findViewById(R.id.submitBtn);
        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String beforevalue = topvalue.getText().toString();// 飯前
                String aftervalue = bottomvalue.getText().toString(); //飯後
                if(beforevalue.equals("")) {
                    beforevalue = "126";
                }
                if(aftervalue.equals("")){
                    aftervalue = "180";
                }
                localDB.update_alert(beforevalue, "0", "bloodsugar_before");
                localDB.update_alert(aftervalue, "0", "bloodsugar_after");
                setResult(RESULT_OK);
                finish();
            }
        });

        cursor = localDB.get_alert("bloodsugar_before");
        if(cursor.getCount() > 0) {
            topvalue.setText(String.valueOf(cursor.getDouble(1)));
        }
        cursor = localDB.get_alert("bloodsugar_after");
        if(cursor.getCount() > 0) {
            bottomvalue.setText(String.valueOf(cursor.getDouble(1)));
        }
    }
    //close DB
    @Override
    protected void onDestroy() {
        super.onDestroy();
        localDB.close();
    }
    //--介面部分
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
//                mDrawerLayout.openDrawer(Gravity.RIGHT);
                bloodsugar_alarm.this.finish();
                return true;
            case R.id.user_info:
                mDrawerLayout.openDrawer(Gravity.RIGHT);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.game_menu, menu);
        //set pattent name of menu
        MenuItem patientname = menu.findItem(R.id.patient_name);
        patientname.setTitle(localDB.getpatientname());
        return true;
    }
//    介面部分--

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected( MenuItem item) {
            BottomNavigation bn = new BottomNavigation(bloodsugar_alarm.this, item.getItemId());
            return true;
        }

    };
}
