package com.example.kavin.try5;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.kavin.try5.Volley.Volleyconnect;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

public class myPillbox_add extends AppCompatActivity {

    private DrawerLayout mDrawerLayout;
    private localDatabase localDB = null;
    private Cursor cursor = null;
    private toolbar mytoolbar;
    private String patient_id, user_id;
    private PopupWindow popup;
    private RecyclerView medicineList;
    private MyAdapter mAdapter;

    private ArrayList<JSONObject> myDataset = new ArrayList<>();
    private ImageView medicineImg;
    private TextView medicine_name, startdatetv;

    private Spinner methodSpn;
    private String[] methoditem_TWN = {"用法", "口服", "注射", "外用", "其他"};

    private Spinner takeday1Spn, takeday2Spn, taketimingSpn;

    private String[] takeday2item_TWN = {"0", "1天","2天", "3天", "4天", "5天"}; //選每隔X天的話
    private String[] takeday2item2_TWN = {"0", "1次", "2次", "3次", "4次"}; //選每日的話
    private String[] taketimingitem_TWN = { "飯前","飯後","不限"};

    private Spinner takeunitSpn;
    private String[] takeunititem_TWN = {"片", "粒", "cc", "mg", "個", "滴", "劑", "單位"};

    private EditText takeamountEdt, stuckEdt;
    private EditText effect, sideeffect;
    private CheckBox checkBoxYes, checkBoxNo;
    private Button submitBtn;

    private int flag  = 0; //判斷有沒有開啟popup

    private DatePickerDialog.OnDateSetListener mDateSetListener;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_pillbox_add);

        localDB = new localDatabase(this);
        localDB.open();
        BottomNavigationView bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottom_navigation_view);
        bottomNavigationView.setSelectedItemId(R.id.drug);
        bottomNavigationView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        //--介面部分
        //setup toolbar
        mytoolbar = new toolbar(this);
        mDrawerLayout = findViewById(R.id.drawer_layout);
        //介面部分--
//----get string array from strings.xml
        String[] methoditem = getResources().getStringArray(R.array.usemethods);
        String[] takeday1item = getResources().getStringArray(R.array.time);
        final String[] takeday2item = getResources().getStringArray(R.array.days); //選每隔X天的話
        final String[] takeday2item2 = getResources().getStringArray(R.array.times); //選每日的話
        String[] taketimingitem = getResources().getStringArray(R.array.eattime);
        String[] takeunititem = getResources().getStringArray(R.array.units);
//get string array from strings.xml----

        //hide keyboard when launch activity
//        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        cursor = localDB.getinfo();
        if(cursor.getCount() >= 0){
            user_id = String.valueOf(cursor.getInt(0));
        }
        patient_id = localDB.getpatientinfo();


        medicineImg = findViewById(R.id.medicineImg);
        medicine_name = findViewById(R.id.medicine_name);
        medicine_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showpopup(myPillbox_add.this);
            }
        });

        methodSpn = findViewById(R.id.methodSpn);
        methodSpn.setAdapter(setupadapter(methoditem));

        takeday1Spn = findViewById(R.id.takeday1Spn);
        takeday2Spn = findViewById(R.id.takeday2Spn);

        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, takeday1item);
        adapter.setDropDownViewResource(R.layout.spinner_item);
        takeday1Spn.setAdapter(adapter);
        takeday1Spn.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                switch (i){
                    case 0: //每日
                        takeday2Spn.setVisibility(View.VISIBLE);
                        takeday2Spn.setAdapter(setupadapter(takeday2item2));
                        break;
                    case 1: //每隔X天
                        takeday2Spn.setVisibility(View.VISIBLE);
                        takeday2Spn.setAdapter(setupadapter(takeday2item));
                        break;
                    case 2: //每周
                        takeday2Spn.setVisibility(View.GONE);
                        break;

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        taketimingSpn = findViewById(R.id.taketimingSpn);
        adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, taketimingitem);
        adapter.setDropDownViewResource(R.layout.spinner_item);
        taketimingSpn.setAdapter(adapter);

        takeunitSpn = findViewById(R.id.takeunitSpn);
        adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, takeunititem);
        adapter.setDropDownViewResource(R.layout.spinner_item);
        takeunitSpn.setAdapter(adapter);

        takeamountEdt = findViewById(R.id.takeamountEdt);
        stuckEdt = findViewById(R.id.stuckEdt);

        effect = findViewById(R.id.effectEdt);
        sideeffect = findViewById(R.id.sideeffectEdt);

        checkBoxYes = findViewById(R.id.checkBoxYes);
        checkBoxNo = findViewById(R.id.checkBoxNo);
        //一次只能選其中一個
        checkBoxYes.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b == true)
                    checkBoxNo.setChecked(false);
            }
        });
        checkBoxNo.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b == true)
                    checkBoxYes.setChecked(false);
            }
        });
        submitBtn = findViewById(R.id.submitBtn);
        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //查看是否有互斥 有的話會跳警告
                Volleyconnect volleyconnect= new Volleyconnect(myPillbox_add.this);
                volleyconnect.checkeffect(patient_id, medicine_name.getText().toString(), new Volleyconnect.VolleyCallback() {
                    @Override
                    public void onSuccess(String result) {
                        try {
                            JSONObject jsonObject = new JSONObject(result);
                            if(jsonObject.getString("success").equals("1") //有產生互斥
                                    && jsonObject.getString("flag").equals("1")){
                                String alertmessage = "";
                                JSONArray data = jsonObject.getJSONArray("data");
                                for(int i = 0; i < data.length(); i++){
                                    alertmessage = alertmessage + data.getJSONObject(i).getString("name") + "\n";
                                }
                                new AlertDialog.Builder(myPillbox_add.this)
                                        .setTitle("互斥警告")
                                        .setMessage("與\n" + alertmessage + "會有互斥")
                                        .setIcon(R.drawable.ic_report_problem_black_24dp)
                                        .setNegativeButton(getString(R.string.sure), new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialoginterface, int i)
                                            {
                                                add_medicine();
                                            }
                                        })
                                        .setPositiveButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialoginterface, int i)
                                            {

                                            }
                                        })
                                        .show();
                            }
                            else{ //沒產生互斥
                                add_medicine();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                });
            }
        });
        medicineImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                IntentIntegrator intentIntegrator = new IntentIntegrator(myPillbox_add.this);
                intentIntegrator.setDesiredBarcodeFormats(intentIntegrator.QR_CODE_TYPES);
                intentIntegrator.setCameraId(0);
                intentIntegrator.setOrientationLocked(false);
                intentIntegrator.setPrompt("掃描");
                intentIntegrator.setBeepEnabled(true);
                intentIntegrator.setBarcodeImageEnabled(true);
                intentIntegrator.initiateScan();
            }
        });



    }

    public String getcurrentdate(){
        Calendar cal = Calendar.getInstance();
        int year = cal.get(Calendar.YEAR);
        int month = cal.get(Calendar.MONTH) + 1;
        int day = cal.get(Calendar.DAY_OF_MONTH);

        String todaydate = "" + year;
        if(month < 10)
            todaydate = todaydate + "/0" + month;
        else
            todaydate = todaydate + "/" + month;
        if(day < 10)
            todaydate = todaydate + "/0" + day;
        else
            todaydate = todaydate + "/" + day;
        return todaydate;
    }

    //open datepickerDialog
    private void opendatepicker(){
        int year, month, day;

        if(startdatetv.getText().toString().equals(getResources().getString(R.string.selectstartdate))) {
            Calendar cal = Calendar.getInstance();
            year = cal.get(Calendar.YEAR);
            month = cal.get(Calendar.MONTH);
            day = cal.get(Calendar.DAY_OF_MONTH);
        }
        else{
            String[] date = startdatetv.getText().toString().split("/");
            year = Integer.valueOf(date[0]);
            month = Integer.valueOf(date[1])-1;
            day = Integer.valueOf(date[2]);
        }
        DatePickerDialog dialog = new DatePickerDialog(
                myPillbox_add.this,
                android.R.style.Theme_Holo_Light_Dialog_MinWidth,
                mDateSetListener,
                year, month, day);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();

    }

    //spinner adapter
    ArrayAdapter<String> setupadapter(String[] item){
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, item){
            @Override
            public boolean isEnabled(int position){
                if(position == 0)
                    return false;
                else
                    return true;
            }
            @Override
            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView tv = (TextView) view;
                if(position == 0){
                    // Set the hint text color gray
                    tv.setTextColor(Color.GRAY);
                }
                else {
                    tv.setTextColor(Color.BLACK);
                }
                return view;
            }
        };
        adapter.setDropDownViewResource(R.layout.spinner_item);
        return adapter;
    }

    //select medicine popup window
    private void showpopup(final Activity context)
    {
        flag  = 1; //判斷有沒有開啟popup

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        //get 選取藥名textview的位置
        int[] location = new int[2];
        medicine_name.getLocationOnScreen(location);
        //get 送出button的位置
        int[] location2 = new int[2];
        submitBtn.getLocationOnScreen(location2);

        // Inflate the popup_layout.xml
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = layoutInflater.inflate(R.layout.popup_selectmedicine, null);

        // Creating the PopupWindow
        popup = new PopupWindow(context);
        popup.setContentView(layout);
        popup.setWidth(size.x-48);
        popup.setHeight(location2[1] - location[1] + submitBtn.getHeight());
        popup.setFocusable(true);
        popup.update();


        //// Displaying the popup at the specified location, + offsets.
        popup.showAtLocation(layout, Gravity.TOP, 0, location[1] + medicine_name.getHeight() + 10);


        // Getting a reference to Close button, and close the popup when clicked.
        ImageView close = layout.findViewById(R.id.closeBtn);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popup.dismiss();
            }
        });

        final EditText medicinename = layout.findViewById(R.id.searchMedicine);
        medicinename.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
                if(actionId == EditorInfo.IME_ACTION_DONE){
                    if(!medicinename.getText().toString().equals(""))
                        search_medicine(medicinename.getText().toString());
                    // hide virtual keyboard
                    InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(medicinename.getWindowToken(), 0);
                    return true;
                }
                return false;
            }
        });

        //初始化recycler view
        medicineList = layout.findViewById(R.id.searchMedicine_recyclerview);
        final LinearLayoutManager layoutManager = new LinearLayoutManager(myPillbox_add.this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        medicineList.setLayoutManager(layoutManager);
        search_medicine("");

    }

    //recyclerview有關的function
    public class MyAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
        private List<JSONObject> mData;
        //要求權現的viewholder
        public class ViewHolder1 extends RecyclerView.ViewHolder {
            public TextView medicineChinese, medicineEng;
            public TextView medicineFrom;
            public ConstraintLayout medicineBlock;
            public ViewHolder1(View v) {
                super(v);
                medicineChinese = v.findViewById(R.id.medicinechinese);
                medicineEng = v.findViewById(R.id.medicineeng);
                medicineFrom = v.findViewById(R.id.medicinefrom);
                medicineBlock = v.findViewById(R.id.medicineBlock);
            }

        }

        public MyAdapter(List<JSONObject> data) {
            mData = data;
        }
        @Override
        public int getItemViewType(int position) {
            return 0;
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v;
            v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.recyclerview_searchmedicine, parent, false);
            return new ViewHolder1(v);
        }

        @Override
        public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
            final ViewHolder1 viewholder1 = (ViewHolder1)holder;
            JSONObject record = mData.get(position);
            try {
                viewholder1.medicineChinese.setText(record.getString("name"));
                viewholder1.medicineEng.setText(record.getString("engname"));
                int length = record.length();
                if(length > 7) {
                    viewholder1.medicineFrom.setText(getResources().getString(R.string.drugbox2));
                    viewholder1.medicineFrom.setTextColor(Color.BLUE);
                }
                else {
                    viewholder1.medicineFrom.setText(getResources().getString(R.string.drugwarehouse));
                    viewholder1.medicineFrom.setTextColor(Color.RED);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            viewholder1.medicineBlock.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    JSONObject record = mData.get(position);
                    fetch_data(record);
                    popup.dismiss();
                }
            });
        }

        @Override
        public int getItemCount() {
            return mData.size();
        }
    }

    //選取用藥>更新頁面(自動填入的概念)
    private void fetch_data(JSONObject medicineInfo){
        try {
            medicine_name.setText(medicineInfo.getString("name"));
            int position = Arrays.asList(methoditem_TWN).indexOf(medicineInfo.getString("method"));
            methodSpn.setSelection(position);
            if(medicineInfo.getString("effect") != null)
                effect.setText(medicineInfo.getString("effect").replace(" ", "\n"));
            if(medicineInfo.getString("sideeffect") != null)
            sideeffect.setText(medicineInfo.getString("sideeffect").replace(" ", "\n"));

            String imgURL = "http://140.117.71.74/graduation/medicineimage/"+medicineInfo.getString("engname") + ".jpg";
            Picasso.get()
                    .load(imgURL)
                    .error(R.drawable.ic_add_a_photo_black_24dp)
                    .into(medicineImg);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    //--------connect to MySQL----------
    private void search_medicine(final String medicinename){
        myDataset.clear();
        String URL = "http://140.117.71.74/graduation/fetch_medicine.php";
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            Log.d("書出訊息:", String.valueOf(jsonObject));
                            String success =jsonObject.getString("success");
                            String message = jsonObject.getString("message");
                            if (success.equals("1")){
//                                Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                                JSONArray record = jsonObject.getJSONArray("record");
                                //放進datalist中
                                for(int i = 0; i < record.length(); i++) {
                                    JSONObject single_record = record.getJSONObject(i);
                                    myDataset.add(single_record);
                                }

                                //判斷是popup執行search_medicine還是QRcode掃描後執行
                                if(flag == 1) {
                                    //myDataset放道adapter中去建立recycler view
                                    mAdapter = new MyAdapter(myDataset);
                                    medicineList.setAdapter(mAdapter);
                                    flag = 0;
                                }
                                else{
                                    fetch_data(myDataset.get(0));
                                }
                            }
                            else{
                                Toast.makeText(myPillbox_add.this, message, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.d("錯誤訊息",e.toString());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(myPillbox_add.this,getResources().getString(R.string.searchfail)+error.toString(),Toast.LENGTH_SHORT).show();
                    }
                })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String>params = new HashMap<>();
                params.put("patientid", patient_id);
                params.put("caretakerid", user_id);
                params.put("medicinename", medicinename);
                params.put("method", "pillbox_add");
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    private void add_medicine(){
        String URL = "http://140.117.71.74/graduation/add_medicine.php";
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            Log.d("書出訊息:", String.valueOf(jsonObject));
                            String success =jsonObject.getString("success");
                            String message = jsonObject.getString("message");
                            if (success.equals("1")){
                                Toast.makeText(myPillbox_add.this, message, Toast.LENGTH_SHORT).show();
                                setResult(RESULT_OK);
                                finish();
                            }
                            else{
                                Toast.makeText(myPillbox_add.this, message, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.d("錯誤訊息",e.toString());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(myPillbox_add.this,getResources().getString(R.string.searchfail)+error.toString(),Toast.LENGTH_SHORT).show();
                    }
                })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String>params = new HashMap<>();
                params.put("patientid", patient_id);
                params.put("caretakerid", user_id);
                params.put("name", medicine_name.getText().toString());
                params.put("method", methoditem_TWN[methodSpn.getSelectedItemPosition()]);

                params.put("takedatetype", "");
                params.put("takedatevalue", "");

                switch(takeday1Spn.getSelectedItemPosition()){
                    case 0: //每日
                        params.put("taketime", takeday2item2_TWN[takeday2Spn.getSelectedItemPosition()].substring(0,1));
                        params.put("repeatday", "1");
                        break;
                    case 1: //每隔X天
                        params.put("taketime", "1");
                        params.put("repeatday", takeday2item_TWN[takeday2Spn.getSelectedItemPosition()].substring(0,1));
                        break;
                    case 2: //每周
                        params.put("taketime", "1");
                        params.put("repeatday", "7");
                        break;
                }

                params.put("taketiming", taketimingitem_TWN[taketimingSpn.getSelectedItemPosition()]);
                params.put("takeamount", takeamountEdt.getText().toString());
                params.put("takeunit", takeunititem_TWN[takeunitSpn.getSelectedItemPosition()]);
                params.put("remark", "");
                params.put("startdate", getcurrentdate());
                if(checkBoxYes.isChecked())
                    params.put("prescription", "1");
                else
                    params.put("prescription", "0");
                params.put("stuck", stuckEdt.getText().toString());
                params.put("hasreminder", "0");
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    //這邊是QRCODE接收的資料
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        final IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);

        String nametotal = "";
        String name = "";
//        date = new Date();
//        sdf = new SimpleDateFormat("yyyy/-MM-dd ");
        if (result != null && result.getContents() != null) {
//            "寶齡" 美骨健錠15毫克"#外用#0#2#5#飯後#5#粒#1#5
            JSONObject medicineInfo = new JSONObject();
            String medicinedata = result.getContents(); //掃完QRcode後獨到的字串
            StringTokenizer st1 = new StringTokenizer(medicinedata, "_");
            int i = 0;
            while (st1.hasMoreTokens()) {
                String[] singledata = st1.nextToken().split("#");
                name = singledata[0];
                try {
                    medicineInfo.put("name", singledata[0]);
                    medicineInfo.put("method", singledata[1]);
                    medicineInfo.put("takedatevalue", singledata[2]);
                    medicineInfo.put("taketime", singledata[3]);
                    medicineInfo.put("repeatday", singledata[4]);
                    medicineInfo.put("taketiming", singledata[5]);
                    medicineInfo.put("takeamount", singledata[6]);
                    medicineInfo.put("takeunit", singledata[7]);
                    medicineInfo.put("prescription", singledata[8]);
                    medicineInfo.put("stuck", singledata[9]);
                    medicineInfo.put("engname", singledata[10]);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
//                fetch_data(medicineInfo);
                search_medicine(singledata[0]);

                nametotal = nametotal + "\n" + name;
                Log.d("nametotal", nametotal);
//                add_medicine();
            }
        }
    }

    @Override
    protected void onDestroy(){
        super.onDestroy();
        localDB.close(); // 關閉資料庫
    }
    //--介面部分
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.user_info:
                mDrawerLayout.openDrawer(Gravity.RIGHT);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.game_menu, menu);
        //set pattent name of menu
        MenuItem patientname = menu.findItem(R.id.patient_name);
        patientname.setTitle(localDB.getpatientname());
        return true;
    }
//    介面部分--

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected( MenuItem item) {
            BottomNavigation bn = new BottomNavigation(myPillbox_add.this, item.getItemId());
            return true;
        }

    };

}
