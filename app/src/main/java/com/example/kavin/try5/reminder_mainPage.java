package com.example.kavin.try5;

import android.content.Intent;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.NavigationView;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.kavin.try5.reminder_measure.reminder_mainedit_measure;
import com.example.kavin.try5.reminder_medicine.reminder_mainedit_medicine;
import com.example.kavin.try5.reminder_pipeline.reminder_mainedit_pipeline;

import java.util.Timer;
import java.util.TimerTask;

public class reminder_mainPage extends AppCompatActivity {
    private localDatabase localDB = null;
    private DrawerLayout mDrawerLayout;

    //slide
    ViewPager viewPager;
    LinearLayout sliderDotspanel;
    private int dotscount;
    private ImageView[] dots;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reminder_main_page);//xml檔案記得要改

        TextView medicineTxt = findViewById(R.id.medicine);
        medicineTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setClass(reminder_mainPage.this, reminder_mainedit_medicine.class);
                startActivity(intent);
            }
        });
        TextView measureTxt = findViewById(R.id.record);
        measureTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setClass(reminder_mainPage.this, reminder_mainedit_measure.class);
                startActivity(intent);
            }
        });
        TextView pipelineTxt = findViewById(R.id.pipeline);
        pipelineTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setClass(reminder_mainPage.this, reminder_mainedit_pipeline.class);
                startActivity(intent);
            }
        });

//        ----可能會重複用到的

        BottomNavigationView bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottom_navigation_view);
        bottomNavigationView.setSelectedItemId(R.id.alarm);
        bottomNavigationView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);




//        可能會重複用到的-----
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp);

        mDrawerLayout = findViewById(R.id.drawer_layout);

        NavigationView navigationView = findViewById(R.id.nav_view);

        //drawer nav header on click的部分
        View headerview = navigationView.getHeaderView(0);
        /* Update Navheader PatientInfo */
        localDB = new localDatabase(this);
        localDB.open();
        TextView header_PatientID = headerview.findViewById(R.id.nav_headerPatientIDTextView);
        header_PatientID.setText("ID : " + localDB.getpatientinfo());
        TextView header_PatientName = headerview.findViewById(R.id.nav_headerPatientNameTextView);
        header_PatientName.setText(localDB.getpatientname());

        LinearLayout header = (LinearLayout) headerview.findViewById(R.id.nav_view_layout);
        header.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent patientInfoIntent = new Intent(reminder_mainPage.this,patientInfoActivity.class);
                reminder_mainPage.this.startActivity(patientInfoIntent);
                mDrawerLayout.closeDrawers();
            }
        });


        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        // set item as selected to persist highlight
                        menuItem.setChecked(true);
                        // close drawer when item is tapped
                        mDrawerLayout.closeDrawers();

                        // Add code here to update the UI based on the item selected
                        // For example, swap UI fragments here

                        switch(menuItem.getItemId()){
                            case R.id.nav_switch_patient:
                                Intent switchIntent = new Intent(reminder_mainPage.this,ChoosePatientActivity.class);
                                reminder_mainPage.this.startActivity(switchIntent);
                                mDrawerLayout.closeDrawers();
                                return true;
                            case R.id.nav_manage_patient:
                                Intent managePatientIntent = new Intent(reminder_mainPage.this,CareTakerManagePermissionActivity.class);
                                reminder_mainPage.this.startActivity(managePatientIntent);
                                mDrawerLayout.closeDrawers();
                                return true;
                            case R.id.nav_member_info:
                                Intent MemberInfoIntent = new Intent(reminder_mainPage.this,MemberInfoActivity.class);
                                reminder_mainPage.this.startActivity(MemberInfoIntent);
                                mDrawerLayout.closeDrawers();
                                return true;
                            case R.id.nav_setting:
                                return true;
                        }

                        return true;
                    }
                });


        ///slide
        viewPager = (ViewPager) findViewById(R.id.viewPager);

        sliderDotspanel = (LinearLayout) findViewById(R.id.SliderDots);

        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(this);

        viewPager.setAdapter(viewPagerAdapter);
        dotscount = viewPagerAdapter.getCount();
        dots = new ImageView[dotscount];
        for(int i = 0; i < dotscount; i++){

            dots[i] = new ImageView(this);
            dots[i].setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.nonactive_dot));

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);

            params.setMargins(8, 0, 8, 0);

            sliderDotspanel.addView(dots[i], params);

        }

        dots[0].setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.active_dot));

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                for(int i = 0; i< dotscount; i++){
                    dots[i].setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.nonactive_dot));
                }

                dots[position].setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.active_dot));

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        Timer timer = new Timer();
        timer.scheduleAtFixedRate(new MyTimerTask(), 20000, 100000);


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
//                mDrawerLayout.openDrawer(Gravity.RIGHT);
                reminder_mainPage.this.finish();
                return true;
            case R.id.user_info:
                mDrawerLayout.openDrawer(Gravity.RIGHT);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.game_menu, menu);
        //set pattent name of menu
        MenuItem patientname = menu.findItem(R.id.patient_name);
        patientname.setTitle(localDB.getpatientname());
        return true;
    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected( MenuItem item) {
            switch (item.getItemId()){
                case R.id.nav_home:
                    reminder_mainPage.this.finish();
                    return true;
            }
            return true;
        }

    };

    public class MyTimerTask extends TimerTask {

        @Override
        public void run() {

            reminder_mainPage.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    if(viewPager.getCurrentItem() == 0){
                        viewPager.setCurrentItem(1);
                    } else if(viewPager.getCurrentItem() == 1){
                        viewPager.setCurrentItem(2);
                    } else {
                        viewPager.setCurrentItem(0);
                    }

                }
            });

        }
    }

}
