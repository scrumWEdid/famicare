package com.example.kavin.try5;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.database.Cursor;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;


import java.util.Locale;

public class setting_language extends AppCompatActivity {
    private DrawerLayout mDrawerLayout;
    private localDatabase localDB = null;
    private Cursor cursor = null;
    private int flag = 0; //判斷是否是首次載入畫面
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting_language);
        setting_language.this.setTitle(R.string.settinglanguage);
        final Spinner spinner = (Spinner)findViewById(R.id.spinner);
        ArrayAdapter<CharSequence> languagelist = ArrayAdapter.createFromResource(setting_language.this,R.array.languages,android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(languagelist);

        Resources res = getResources();
        Configuration conf = res.getConfiguration();

        if(  conf.locale.getLanguage().equals("en") ||   conf.locale.getLanguage().equals("zh")){

            spinner.setSelection(1);
        }
        else if(  conf.locale.getLanguage().equals("in")){
            spinner.setSelection(2);
        }
        else{

        }

        BottomNavigationView bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottom_navigation_view);
        bottomNavigationView.setSelectedItemId(R.id.nav_description);
        bottomNavigationView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if(flag++ > 0) {
                    if (i == 0) {
                        String defalut = Locale.getDefault().getLanguage();
                        Toast.makeText(setting_language.this, defalut, Toast.LENGTH_SHORT).show();
                    } else if (i == 1) {
                        //                   Toast.makeText(setting_language.this,getString(R.string.plsrestart),Toast.LENGTH_SHORT).show();
                        Locale myLocale = new Locale("en");
                        Resources res = getResources();
                        DisplayMetrics dm = res.getDisplayMetrics();
                        Configuration conf = res.getConfiguration();
                        conf.locale = myLocale;
                        res.updateConfiguration(conf, dm);
                        Intent intent = new Intent();
                        intent.setClass(setting_language.this, indexActivity.class);
                        startActivity(intent);
                        setting_language.this.finishAffinity();
                    } else if (i == 2) {
                        //                   Toast.makeText(setting_language.this,getString(R.string.plsrestart),Toast.LENGTH_SHORT).show();
                        Locale myLocale2 = new Locale("in");
                        Resources res = getResources();
                        DisplayMetrics dm = res.getDisplayMetrics();
                        Configuration conf = res.getConfiguration();
                        conf.locale = myLocale2;
                        res.updateConfiguration(conf, dm);

                        Intent intent = new Intent();
                        intent.setClass(setting_language.this, indexActivity.class);
                        startActivity(intent);
                        setting_language.this.finishAffinity();
                    } else {
                        Toast.makeText(setting_language.this, getString(R.string.plsrestart), Toast.LENGTH_SHORT).show();
                        setting_language.this.finishAffinity();
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        localDB = new localDatabase(this);
        localDB.open();
        //----介面部分
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp);

        mDrawerLayout = findViewById(R.id.drawer_layout);

        NavigationView navigationView = findViewById(R.id.nav_view);

        //drawer nav header on click的部分
        View headerview = navigationView.getHeaderView(0);

        TextView header_PatientID = headerview.findViewById(R.id.nav_headerPatientIDTextView);
        header_PatientID.setText("ID : " + localDB.getpatientinfo());
        TextView header_PatientName = headerview.findViewById(R.id.nav_headerPatientNameTextView);
        header_PatientName.setText(localDB.getpatientname());

        LinearLayout header = (LinearLayout) headerview.findViewById(R.id.nav_view_layout);
        header.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent patientInfoIntent = new Intent(setting_language.this,patientInfoActivity.class);
                setting_language.this.startActivity(patientInfoIntent);
                mDrawerLayout.closeDrawers();
            }
        });

        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        // set item as selected to persist highlight
                        menuItem.setChecked(true);
                        // close drawer when item is tapped
                        mDrawerLayout.closeDrawers();

                        // Add code here to update the UI based on the item selected
                        // For example, swap UI fragments here

                        switch(menuItem.getItemId()){
                            case R.id.nav_switch_patient:
                                Intent switchIntent = new Intent(setting_language.this,ChoosePatientActivity.class);
                                setting_language.this.startActivityForResult(switchIntent, 1);
                                mDrawerLayout.closeDrawers();
                                return true;
                            case R.id.nav_manage_patient:
                                Intent managePatientIntent = new Intent(setting_language.this,CareTakerManagePermissionActivity.class);
                                setting_language.this.startActivity(managePatientIntent);
                                mDrawerLayout.closeDrawers();
                                return true;
                            case R.id.nav_member_info:
                                Intent MemberInfoIntent = new Intent(setting_language.this,MemberInfoActivity.class);
                                setting_language.this.startActivity(MemberInfoIntent);
                                mDrawerLayout.closeDrawers();
                                return true;
                            case R.id.nav_setting:
                                return true;
                        }

                        return true;
                    }
                });
// 介面部分-----


    }

    //--介面部分
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
//                mDrawerLayout.openDrawer(Gravity.RIGHT);
                setting_language.this.finish();
                return true;
            case R.id.user_info:
                mDrawerLayout.openDrawer(Gravity.RIGHT);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.game_menu, menu);
        //set pattent name of menu
        MenuItem patientname = menu.findItem(R.id.patient_name);
        patientname.setTitle(localDB.getpatientname());
        return true;
    }
    //    介面部分--
    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected( MenuItem item) {
            BottomNavigation bn = new BottomNavigation(setting_language.this, item.getItemId());
            return true;
        }

    };
}
