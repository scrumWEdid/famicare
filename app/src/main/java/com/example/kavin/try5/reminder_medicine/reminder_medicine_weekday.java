package com.example.kavin.try5.reminder_medicine;
import com.example.kavin.try5.BottomNavigation;
import com.example.kavin.try5.R;
import com.example.kavin.try5.Volley.Volleyconnect;
import com.example.kavin.try5.WeekDaysCalender.*;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.kavin.try5.fetch_remindertime.MyReminder;
import com.example.kavin.try5.indexActivity;
import com.example.kavin.try5.localDatabase;
import com.example.kavin.try5.toolbar;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

public class reminder_medicine_weekday extends AppCompatActivity implements CLCalendarItemView.OnItemSelectListener, medicine_weekday_RecyclerItemTouchHelper.RecyclerItemTouchHelperListener{
    private DrawerLayout mDrawerLayout;
    private localDatabase localDB = null;
    private toolbar mytoolbar;
    private Cursor cursor = null;
    private String patient_id;

    private static String  URL_REMAIN = "http://140.117.71.74/graduation/fetch_remain.php";
    private TextView current,select_today,colume_time,colume_information;
    private Button viewBtn,doneBtn,cancelBtn;
    private RecyclerView recyclerView;
//    private RecyclerView.Adapter adapter;
    private medicine_weekday_adapter mAdapter;
    private List<medicine_weekday_Listitem> listItems = new ArrayList<>();
    private CoordinatorLayout coordinatorLayout;
    private CLCalendarView clCalendarView;
    private ImageView add,edit,view;
    private MyReminder myreminder;
    private ArrayList<JSONObject> myDataset = new ArrayList<>();
    private boolean flag = false; //判斷提醒資料讀取完成沒
    private String select_date = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_medicine_reminder_weekday);

        //----open localDB
        localDB = new localDatabase(this);
        localDB.open();
        BottomNavigationView bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottom_navigation_view);
        bottomNavigationView.setSelectedItemId(R.id.alarm);
        bottomNavigationView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
//----介面部分
        mytoolbar = new toolbar(this);
        mDrawerLayout = findViewById(R.id.drawer_layout);
// 介面部分-----
//----讀取權限 設定是否要顯示的新增,編輯按鈕
        final Volleyconnect volleyconnect = new Volleyconnect(this);
        volleyconnect.fetch_permission(localDB.getuserid(), localDB.getpatientinfo(),
                new Volleyconnect.VolleyCallback() {
                    @Override
                    public void onSuccess(String result) {
                        if(result.equals("查看者")) {
                            volleyconnect.hideImageViewicon("addImg");
                            volleyconnect.hideImageViewicon("editImg");
                        }
                    }
                });
//讀取權限----
        coordinatorLayout = findViewById(R.id.coordinator_layout);
//        getMainlinearLayout();

       add = (ImageView)findViewById(R.id.addImg);
        edit = (ImageView)findViewById(R.id.editImg);
          view = (ImageView)findViewById(R.id.viewImg);
         current = (TextView)findViewById(R.id.current);
        clCalendarView =(CLCalendarView)findViewById(R.id.clCalendarView);
         recyclerView = (RecyclerView)findViewById(R.id.weekdayrecycleriew);
        select_today  = (TextView)findViewById(R.id.select_todday);
        viewBtn = (Button)findViewById(R.id.viewBtn);
        doneBtn =(Button)findViewById(R.id.doneBtn);
        cancelBtn=(Button)findViewById(R.id.cancelBtn);
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(reminder_medicine_weekday.this, reminder_add_medicine.class);
                startActivityForResult(intent, 1);
            }
        });
        edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(reminder_medicine_weekday.this, reminder_mainedit_medicine.class);
                startActivityForResult(intent, 1);
            }
        });
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(reminder_medicine_weekday.this, reminder_history_medicine.class);
                startActivity(intent);
            }
        });

        viewBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            if (medicine_weekday_common.position!=-1){
                Intent intent = new Intent();
                intent.setClass(reminder_medicine_weekday.this, reminder_view_medicine.class);
                Bundle bundle = new Bundle();
                bundle.putString("medicine", medicine_weekday_common.currentItem.getReminder_medicine_info().toString());
                intent.putExtras(bundle);
                startActivityForResult(intent, 1);
                ShowDone(false);
                ShowView(false);
                medicine_weekday_common.position=-1;
                mAdapter.row_index=-1;
                mAdapter.notifyDataSetChanged();
            }

            }
        });
        doneBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(medicine_weekday_common.position!=-1&&mAdapter.flag==false) {
                    medicine_weekday_Listitem selectitem = medicine_weekday_common.currentItem;
                    myreminder.done_reminder(selectitem.getReminder_medicine_name(),
                            select_date,
                            selectitem.getReminder_medicine_time(),
                            new MyReminder.VolleyCallback() {
                                @Override
                                public void onSuccess(String result) {
                                    if(result.equals("1")){
                                        Toast.makeText(reminder_medicine_weekday.this, getString(R.string.havefinishmedicine), Toast.LENGTH_SHORT).show();
                                    }
                                }
                            });
                    mAdapter.removeItem(medicine_weekday_common.position);
                    medicine_weekday_common.position=-1;
                    mAdapter.row_index=-1;
                    medicine_weekday_common.Sdate="";
                    ShowView(false);
                    ShowDone(false);
                    ShowCancel(false);
                    mAdapter.notifyDataSetChanged();
                }
                if (mAdapter.flag==true){
                    ShowDone(false);
                    ShowCancel(false);
                    for (int x = mAdapter.checked.length-1;x>=0;x--){

                       if (mAdapter.checked[x]!=null){
                            // Log.d("volly",mAdapter.checkedname[x]);
//                           Log.d("volly",select_date);
//                           Log.d("volly",mAdapter.checkedtime[x]);
                           myreminder.done_reminder(mAdapter.checkedname[x],
                                   select_date,
                                   mAdapter.checkedtime[x],
                                   new MyReminder.VolleyCallback() {
                                       @Override
                                       public void onSuccess(String result) {
                                            if(result.equals("1")){

                                            }
                                       }
                                   });
                           mAdapter.removeItem(x);
                           mAdapter.checked[x]=null;
                           mAdapter.notifyDataSetChanged();

                       }
                    }
                    myreminder.fetchdoneMedicine();
                    Toast.makeText(reminder_medicine_weekday.this, getString(R.string.havefinishmedicine), Toast.LENGTH_SHORT).show();


                }

            }
        });
        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mAdapter.flag=false;
                medicine_weekday_common.position=-1;
                mAdapter.row_index=-1;
                mAdapter.notifyDataSetChanged();
                medicine_weekday_common.Sdate="";
                ShowCancel(false);
                ShowDone(false);
                ShowView(false);
            }
        });

        clCalendarView.setOnItemSelectListener(this);



        select_today.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Date today = new Date();
//                onSelectDate(null, new Date());
//                final Calendar calendar = Calendar.getInstance();
//                calendar.setTime(today);
//
//                if(calendar.get(Calendar.DAY_OF_WEEK)==1){
//                    clCalendarView.setSelectDate( CLCalendarWeekUtil.getWeekUtilCalender(today,CLCalendarWeekUtil.getWeek(today)));
//                    clCalendarView.setCurrentCalender( CLCalendarWeekUtil.getWeekUtilCalender(today,CLCalendarWeekUtil.getWeek(today)));
//                }
//                else{
//                    Log.d("asd", String.valueOf(calendar.get(Calendar.DAY_OF_WEEK)));
//                    calendar.setTime(today);
//                    for (int x = calendar.get(Calendar.DAY_OF_WEEK);x>1;x--){
//                        calendar.add(Calendar.DATE,-1);
//                        Log.d("asd", String.valueOf(calendar));
//                    }
//                    Log.d("date", String.valueOf(calendar));
//                    Date setweeksun ;
//                    setweeksun =today;
//                    setweeksun = calendar.getTime();
//                    Log.d("date", String.valueOf(setweeksun));
//
//                    clCalendarView.setCurrentCalender( CLCalendarWeekUtil.getWeekUtilCalender(setweeksun,CLCalendarWeekUtil.getWeek(setweeksun)));
//                    clCalendarView.setSelectDate( CLCalendarWeekUtil.getWeekUtilCalender(today,CLCalendarWeekUtil.getWeek(today)));

                Intent refresh = new Intent(getApplicationContext(), reminder_medicine_weekday.class);
                startActivity(refresh);//Start the same Activity
                finish(); //finish Activity.
            }
        });
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));

        ItemTouchHelper.SimpleCallback itemTouchHelperCallback = new medicine_weekday_RecyclerItemTouchHelper(0, ItemTouchHelper.LEFT, this);
        new ItemTouchHelper(itemTouchHelperCallback).attachToRecyclerView(recyclerView);


        onSelectDate(null, new Date());
        Calendar calendar = Calendar.getInstance();
        String day= String.valueOf(calendar.get(Calendar.DATE));
        String month = String.valueOf(calendar.get(Calendar.MONTH)+1);
        String year = String.valueOf(calendar.get(Calendar.YEAR));
        if( day.length()==1){
            String addzero = "0";
            day = addzero + day ;
        }
        if (month.length()==1){
            String addzero = "0";
            month= addzero +month;
        }
        select_date = year+"-"+month+"-"+day;

        //讀取提醒資料
        myreminder = new MyReminder(this, "medicine");
        myreminder.fetchdoneMedicine();
        myreminder.fetch_data("view_all", localDB.getpatientinfo(), new MyReminder.VolleyCallback() {
            @Override
            public void onSuccess(String result) {
                if(result.equals("1")){
                    flag = true;
                    loadRecyclerViewData(select_date);
                }
            }
        });

        //點提醒進來的
        Bundle bData = getIntent().getExtras();
        if (bData != null) {
            if (bData.get("flag").equals("notification")) {
                Log.d("asd", (String) bData.get("flag"));
                String result = bData.getString("Sdate");
                Log.d("weekdayintentget", result);
                medicine_weekday_common.Sdate =  bData.getString("Sdate");
            }
        }

    }

    //點提醒進來時如果已經有啟動此activity的話
    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        Bundle bData = intent.getExtras();
        if (bData != null) {
            if (bData.get("flag").equals("notification")) {
                Log.d("asd", (String) bData.get("flag"));
                String result = bData.getString("Sdate");
                Log.d("weekdayintentget", result);
                medicine_weekday_common.Sdate =  bData.getString("Sdate");
                mAdapter.notifyDataSetChanged();
            }
        }
//        Toast.makeText(getApplicationContext(), "fromnotification", Toast.LENGTH_SHORT).show();
    }

    private void loadRecyclerViewData(final String date){
        listItems = new ArrayList<>();
        if(flag) {
            try {
                myDataset = myreminder.dayreminder(date);
                for (JSONObject item : myDataset) {
                    String name = item.getString("name");
                    JSONArray remindtimeArray = item.getJSONArray("undoneremindertime");
                    for (int i = 0; i < remindtimeArray.length(); i++) {
                        String remindtime = remindtimeArray.getString(i).substring(0, 5);
                        listItems.add(new medicine_weekday_Listitem(remindtime, name, item));
                    }
                }
                //照時間排序
                Collections.sort(listItems, new Comparator<medicine_weekday_Listitem>() {
                    @Override
                    public int compare(medicine_weekday_Listitem item1, medicine_weekday_Listitem item2) {
                        return item1.getReminder_medicine_time().compareTo(item2.getReminder_medicine_time());
                    }
                });
                //更新recyclerview
                mAdapter = new medicine_weekday_adapter(listItems, reminder_medicine_weekday.this,this);
                recyclerView.setAdapter(mAdapter);

            } catch (ParseException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
    public void ShowView(Boolean showView){
        if(showView==false){
            viewBtn.setVisibility(View.INVISIBLE);
        }
        if(showView==true){
            viewBtn.setVisibility(View.VISIBLE);
        }
    }
    public void ShowCancel(Boolean ShowCancel){
        if(ShowCancel==false){
            cancelBtn.setVisibility(View.INVISIBLE);
        }
        if(ShowCancel==true){
            cancelBtn.setVisibility(View.VISIBLE);

        }
    }
    public void ShowDone(boolean showDone){
        if(showDone==false){
            doneBtn.setVisibility(View.INVISIBLE);
        }
        if(showDone==true){
            doneBtn.setVisibility(View.VISIBLE);

        }
    }


    /*下一頁修改完畢回傳更新資料*/
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == 1){
            if(resultCode == RESULT_OK){
                //讀取提醒資料
                myreminder.fetch_data("view_all", localDB.getpatientinfo(), new MyReminder.VolleyCallback() {
                    @Override
                    public void onSuccess(String result) {
                        if(result.equals("1")){
                            flag = true;
                            loadRecyclerViewData(select_date);
                        }
                    }
                });
            }
        }
    }

    @Override
    public void onSelectDate(CLCalendarWeekDTO calenderBean, Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        medicine_weekday_common.Sdate="";
        ShowDone(false);
        ShowView(false);
        ShowCancel(false);
       String day= String.valueOf(calendar.get(Calendar.DATE));
       String month = String.valueOf(calendar.get(Calendar.MONTH)+1);
       String year = String.valueOf(calendar.get(Calendar.YEAR));
        if( day.length()==1){
            String addzero = "0";
            day = addzero + day ;
        }
        if (month.length()==1){
            String addzero = "0";
            month= addzero +month;
        }
        select_date = year+"-"+month+"-"+day;
        Log.d("asd",select_date);
        loadRecyclerViewData(select_date);
        current.setText( calendar.get(Calendar.YEAR) + "/" +(calendar.get(Calendar.MONTH) + 1)+ "/" +calendar.get(Calendar.DATE));
    }

    @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction, int position) {
        if (viewHolder instanceof medicine_weekday_adapter.ViewHolder) {
            // get the removed item name to display it in snack bar
            String name = listItems.get(viewHolder.getAdapterPosition()).getReminder_medicine_name();

            // backup of removed item for undo purpose
            final medicine_weekday_Listitem deletedItem = listItems.get(viewHolder.getAdapterPosition());
            final int deletedIndex = viewHolder.getAdapterPosition();

            // remove the item from recycler view
                mAdapter.removeItem(viewHolder.getAdapterPosition());

            // showing snack bar with Undo option
            Snackbar snackbar = Snackbar
                    .make(coordinatorLayout, name + getString(R.string.finishuse), Snackbar.LENGTH_LONG);
            snackbar.setAction(getString(R.string.recovery), new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    // undo is selected, restore the deleted item
                    mAdapter.restoreItem(deletedItem, deletedIndex);
                }
            });
            snackbar.setActionTextColor(Color.YELLOW);
            snackbar.show();
        }
    }


    //--介面部分
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
//                mDrawerLayout.openDrawer(Gravity.RIGHT);
                if(isTaskRoot()) { //是最後一個activity(從外頭推播點進來的話)
                    Intent intent = new Intent();
                    intent.setClass(getApplicationContext(), indexActivity.class);
                    startActivity(intent);
                    finish();
                }
                else
                    finish();
                return true;
            case R.id.user_info:
                mDrawerLayout.openDrawer(Gravity.RIGHT);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.game_menu, menu);
        //set pattent name of menu
        MenuItem patientname = menu.findItem(R.id.patient_name);
        patientname.setTitle(localDB.getpatientname());
        return true;
    }
//    介面部分--

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected( MenuItem item) {
            BottomNavigation bn = new BottomNavigation(reminder_medicine_weekday.this, item.getItemId());
            return true;
        }

    };


}

