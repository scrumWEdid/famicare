package com.example.kavin.try5;

import android.content.Intent;
import android.content.res.Resources;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class agreementActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agreement);


        final Button agreeButton = (Button) findViewById(R.id.agreeButton);
        agreeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent agreeIntent = new Intent(agreementActivity.this,RegisterActivity.class);
                agreementActivity.this.startActivity(agreeIntent);
            }
        });

        final Button DagreeButton = (Button) findViewById(R.id.dontAgreeButton);
        DagreeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String clickagree = Resources.getSystem().getString(R.string.clickagree);
                Toast.makeText(agreementActivity.this, clickagree, Toast.LENGTH_SHORT).show();
                finish();
            }
        });
    }
}
