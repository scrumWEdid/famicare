package com.example.kavin.try5.reminder_medicine;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.DatePickerDialog;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.NavigationView;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.kavin.try5.BottomNavigation;
import com.example.kavin.try5.CareTakerManagePermissionActivity;
import com.example.kavin.try5.ChoosePatientActivity;
import com.example.kavin.try5.MainActivity;
import com.example.kavin.try5.MemberInfoActivity;
import com.example.kavin.try5.R;
import com.example.kavin.try5.Volley.Volleyconnect;
import com.example.kavin.try5.localDatabase;
import com.example.kavin.try5.patientInfoActivity;
import com.example.kavin.try5.reminder_notification.PlayReceiver;
import com.example.kavin.try5.toolbar;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

public class reminder_add_medicine extends AppCompatActivity {
    private DrawerLayout mDrawerLayout;
    private localDatabase localDB = null;
    private Cursor cursor = null;
    private PopupWindow popup;
    private RecyclerView medicineList;
    private MyAdapter mAdapter;
    private ArrayList<JSONObject> myDataset = new ArrayList<>();

    private String REMINDER_TYPE = "medicine";
    private String user_id, patient_id;
    private TextView medicine_name;
    private ImageView medicineImg;
    private TextView[] taketime = new TextView[4];
    private ImageView clockimg;
    private Spinner takemethod_spinner;
    private String[] takemethoditem_TWN = {"用法","口服","注射","外用","其他"};

    private Spinner takeday1Spn, takeday2Spn, taketimingSpn;
    private String[] takeday1item_TWN ={"每日","每隔","每周"};
    private String[] takeday2item_TWN =  {"0","1天","2天","3天","4天","5天"}; //選每隔X天的話
    private String[] takeday2item2_TWN =  {"0","1次","2次","3次","4次"};//選每日的話
    private int takeday2pos = 0; //存takeday2位置 之後takeday2spn setadapter後再設定位置
    private String[] taketimingitem_TWN = {"飯前","飯後","不限"};

    private Spinner taketiming_spinner;
    private Spinner takeunit_spinner;
    private EditText takeamountEdt, takedayEdt;
    private String[] takeunititem_TWN = {"片","粒","cc","mg","個","滴","劑","單位"};
    private EditText remark;
    private Button submitBtn;
    private DatePickerDialog.OnDateSetListener mDateSetListener;

    //notificaiton
    private NotificationManagerCompat notificationManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reminder_add_medicine);

        //----open localDB
        localDB = new localDatabase(this);
        localDB.open();

        BottomNavigationView bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottom_navigation_view);
        bottomNavigationView.setSelectedItemId(R.id.alarm);
        bottomNavigationView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
//----介面部分
        toolbar mytoolbar = new toolbar(this);
        mDrawerLayout = findViewById(R.id.drawer_layout);
//----介面部分
//----get string array from strings.xml
        String[] takemethoditem=getResources().getStringArray(R.array.usemethods);
        String[] takeday1item=getResources().getStringArray(R.array.time);
        final String[] takeday2item= getResources().getStringArray(R.array.days);
        final String[] takeday2item2=getResources().getStringArray(R.array.times);
        String[] taketimingitem=getResources().getStringArray(R.array.eattime);
        String[] takeunititem=getResources().getStringArray(R.array.units);
//get string array from strings.xml----

        cursor = localDB.getinfo();
        if(cursor.getCount() >= 0){
            user_id = String.valueOf(cursor.getInt(0));
        }
        patient_id = localDB.getpatientinfo();

        medicineImg = findViewById(R.id.medicineImg);
        medicine_name = findViewById(R.id.medicine_name);
        medicine_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showpopup(reminder_add_medicine.this);
            }
        });
        remark = findViewById(R.id.remark);
        clockimg = findViewById(R.id.clockimage);
        taketime[0] = findViewById(R.id.taketimetxt1);
        taketime[1] = findViewById(R.id.taketimetxt2);
        taketime[2] = findViewById(R.id.taketimetxt3);
        taketime[3] = findViewById(R.id.taketimetxt4);
        for(int i = 0; i < 4; i++){ //set timepicker when onclick
            final int finalI = i;
            taketime[i].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    opentimepicker(finalI);
                }
            });
        }

        takemethod_spinner = findViewById(R.id.takemethod_spinner);
        takemethod_spinner.setAdapter(setupadapter(takemethoditem));

        takeday1Spn = findViewById(R.id.takeday1Spn);
        takeday2Spn = findViewById(R.id.takeday2Spn);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, takeday1item);
        adapter.setDropDownViewResource(R.layout.spinner_item);
        takeday1Spn.setAdapter(adapter);
        takeday1Spn.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                switch (position){
                    case 0: //每日
                        takeday2Spn.setVisibility(View.VISIBLE);
                        takeday2Spn.setAdapter(setupadapter(takeday2item2));
                        takeday2Spn.setSelection(takeday2pos);
                        break;
                    case 1: //每隔X天
                        takeday2Spn.setVisibility(View.VISIBLE);
                        takeday2Spn.setAdapter(setupadapter(takeday2item));
                        takeday2Spn.setSelection(takeday2pos);
                        break;
                    case 2: //每周
                        takeday2Spn.setVisibility(View.GONE);

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        takeday2Spn.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                if(takeday1Spn.getSelectedItemPosition() == 0) { //服用時間選每日
                    if (position > 0) {
                        clockimg.setVisibility(View.VISIBLE);
                        int i = 0;
                        for (i = 0; i < position; i++)
                            taketime[i].setVisibility(View.VISIBLE);
                        for (; i < 4; i++)
                            taketime[i].setVisibility(View.GONE);
                    } else {
                        clockimg.setVisibility(View.GONE);
                        for (int i = 0; i < 4; i++)
                            taketime[i].setVisibility(View.GONE);
                    }
                }
                else{ //服用時間選每周或每隔幾天 提醒一定只有一次
                    clockimg.setVisibility(View.VISIBLE);
                    taketime[0].setVisibility(View.VISIBLE);
                    for (int i = 1; i < 4; i++)
                        taketime[i].setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        taketiming_spinner = findViewById(R.id.taketiming_spinner);
        adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, taketimingitem);
        adapter.setDropDownViewResource(R.layout.spinner_item);
        taketiming_spinner.setAdapter(adapter);

        takeunit_spinner = findViewById(R.id.takeunit_spinner);
        adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, takeunititem);
        adapter.setDropDownViewResource(R.layout.spinner_item);
        takeunit_spinner.setAdapter(adapter);

        takeamountEdt = findViewById(R.id.takeamountEdt);
        takedayEdt = findViewById(R.id.takedayEdt);

        submitBtn = findViewById(R.id.submitBtn);
        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //查看是否有互斥 有的話會跳警告
                Volleyconnect volleyconnect= new Volleyconnect(reminder_add_medicine.this);
                volleyconnect.checkeffect(patient_id, medicine_name.getText().toString(), new Volleyconnect.VolleyCallback() {
                    @Override
                    public void onSuccess(String result) {
                        try {
                            JSONObject jsonObject = new JSONObject(result);
                            if(jsonObject.getString("success").equals("1") //有產生互斥
                                    && jsonObject.getString("flag").equals("1")){
                                String alertmessage = "";
                                JSONArray data = jsonObject.getJSONArray("data");
                                for(int i = 0; i < data.length(); i++){
                                    alertmessage = alertmessage + data.getJSONObject(i).getString("name") + "\n";
                                }
                                new AlertDialog.Builder(reminder_add_medicine.this)
                                        .setTitle("互斥警告")
                                        .setMessage("與\n" + alertmessage + "會有互斥")
                                        .setIcon(R.drawable.ic_report_problem_black_24dp)
                                        .setNegativeButton(getString(R.string.sure), new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialoginterface, int i)
                                            {
                                                add_medicine();
                                                try {
                                                    add_reminder(REMINDER_TYPE); //Undone
                                                } catch (ParseException e) {
                                                    e.printStackTrace();
                                                }
                                            }
                                        })
                                        .setPositiveButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialoginterface, int i)
                                            {

                                            }
                                        })
                                        .show();
                            }
                            else{ //沒產生互斥
                                add_medicine();
                                try {
                                    add_reminder(REMINDER_TYPE); //Undone
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                });

            }
        });

        notificationManager = NotificationManagerCompat.from(this);  //notification
        //如果還沒選取藥的話 可以點圖片進行掃描
            medicineImg.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    IntentIntegrator intentIntegrator = new IntentIntegrator(reminder_add_medicine.this);
                    intentIntegrator.setDesiredBarcodeFormats(intentIntegrator.QR_CODE_TYPES);
                    intentIntegrator.setCameraId(0);
                    intentIntegrator.setOrientationLocked(false);
                    intentIntegrator.setPrompt("掃描");
                    intentIntegrator.setBeepEnabled(true);
                    intentIntegrator.setBarcodeImageEnabled(true);
                    intentIntegrator.initiateScan();
                }
            });
    }
    @Override
    protected void onDestroy(){
        super.onDestroy();
        localDB.close(); // 關閉資料庫
    }

    //spinner adapter
    ArrayAdapter<String> setupadapter(String[] item){
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, item){
            @Override
            public boolean isEnabled(int position){
                if(position == 0)
                    return false;
                else
                    return true;
            }
            @Override
            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView tv = (TextView) view;
                if(position == 0){
                    // Set the hint text color gray
                    tv.setTextColor(Color.GRAY);
                }
                else {
                    tv.setTextColor(Color.BLACK);
                }
                return view;
            }
        };
        adapter.setDropDownViewResource(R.layout.spinner_item);
        return adapter;
    }
    public String getcurrentdate(){
        Calendar cal = Calendar.getInstance();
        int year = cal.get(Calendar.YEAR);
        int month = cal.get(Calendar.MONTH) + 1;
        int day = cal.get(Calendar.DAY_OF_MONTH);

        String todaydate = "" + year;
        if(month < 10)
            todaydate = todaydate + "/0" + month;
        else
            todaydate = todaydate + "/" + month;
        if(day < 10)
            todaydate = todaydate + "/0" + day;
        else
            todaydate = todaydate + "/" + day;
        return todaydate;
    }


    //open timepickerdialog
    private void opentimepicker(final int position){
        int hour, minute;
        String[] time= taketime[position].getText().toString().split(":");
        hour = Integer.valueOf(time[0]);
        minute = Integer.valueOf(time[1]);
//        Calendar cal = Calendar.getInstance();
//        int hour = cal.get(Calendar.HOUR_OF_DAY);
//        int minute = cal.get(Calendar.MINUTE);
        new TimePickerDialog(reminder_add_medicine.this, new TimePickerDialog.OnTimeSetListener(){
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                String h = String.valueOf(hourOfDay);
                String m = String.valueOf(minute);
                if(hourOfDay < 10)
                    h = "0" + h;
                if(minute < 10)
                    m = "0" + m;
                taketime[position].setText(h + ":" + m);
            }
        }, hour, minute, false).show();
    }

    //select medicine popup window
    private void showpopup(final Activity context)
    {


        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        //get 選取藥名textview的位置
        int[] location = new int[2];
        medicine_name.getLocationOnScreen(location);
        //get 送出button的位置
        int[] location2 = new int[2];
        submitBtn.getLocationOnScreen(location2);

        // Inflate the popup_layout.xml
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = layoutInflater.inflate(R.layout.popup_selectmedicine, null);

        // Creating the PopupWindow
        popup = new PopupWindow(context);
        popup.setContentView(layout);
        popup.setWidth(size.x-48);
        popup.setHeight(location2[1] - location[1] + submitBtn.getHeight());
        popup.setFocusable(true);
        popup.update();


        //// Displaying the popup at the specified location, + offsets.
        popup.showAtLocation(layout, Gravity.TOP, 0, location[1] + medicine_name.getHeight() + 10);


        // Getting a reference to Close button, and close the popup when clicked.
        ImageView close = layout.findViewById(R.id.closeBtn);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popup.dismiss();
            }
        });

        final EditText medicinename = layout.findViewById(R.id.searchMedicine);
        medicinename.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
                if(actionId == EditorInfo.IME_ACTION_DONE){
                    search_medicine(medicinename.getText().toString());
                    // hide virtual keyboard
                    InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(medicinename.getWindowToken(), 0);
                    return true;
                }
                return false;
            }
        });

        //初始化recycler view
        medicineList = layout.findViewById(R.id.searchMedicine_recyclerview);
        final LinearLayoutManager layoutManager = new LinearLayoutManager(reminder_add_medicine.this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        medicineList.setLayoutManager(layoutManager);
        search_medicine("");
        //myDataset放道adapter中去建立recycler view
//        mAdapter = new MyAdapter(myDataset);
//        mRecyclerView.setAdapter(mAdapter);
    }

    //recyclerview有關的function
    public class MyAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
        private List<JSONObject> mData;
        //要求權現的viewholder
        public class ViewHolder1 extends RecyclerView.ViewHolder {
            public TextView medicineChinese, medicineEng;
            public TextView medicineFrom;
            public ConstraintLayout medicineBlock;
            public ViewHolder1(View v) {
                super(v);
                medicineChinese = v.findViewById(R.id.medicinechinese);
                medicineEng = v.findViewById(R.id.medicineeng);
                medicineFrom = v.findViewById(R.id.medicinefrom);
                medicineBlock = v.findViewById(R.id.medicineBlock);
            }

        }

        public MyAdapter(List<JSONObject> data) {
            mData = data;
        }
        @Override
        public int getItemViewType(int position) {
            return 0;
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v;
            v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.recyclerview_searchmedicine, parent, false);
            return new ViewHolder1(v);
        }

        @Override
        public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
            final ViewHolder1 viewholder1 = (ViewHolder1)holder;
            JSONObject record = mData.get(position);
            try {
                viewholder1.medicineChinese.setText(record.getString("name"));
                viewholder1.medicineEng.setText(record.getString("engname"));
                int length = record.length();
                if(length > 7) {
                    viewholder1.medicineFrom.setText(getResources().getString(R.string.drugBox));
                    viewholder1.medicineFrom.setTextColor(Color.BLUE);
                }
                else {
                    viewholder1.medicineFrom.setText(getString(R.string.drugwarehouse));
                    viewholder1.medicineFrom.setTextColor(Color.RED);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            viewholder1.medicineBlock.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    JSONObject record = mData.get(position);
                    fetch_data(record);
                    popup.dismiss();
                }
            });
        }

        @Override
        public int getItemCount() {
            return mData.size();
        }
    }
    //選取用藥>更新頁面(自動填入的概念)
    private void fetch_data(JSONObject medicineInfo){
        try {
            medicine_name.setText(medicineInfo.getString("name"));
            String imgURL = "http://140.117.71.74/graduation/medicineimage/"+medicineInfo.getString("engname") + ".jpg";
            Picasso.get()
                    .load(imgURL)
                    .error(R.drawable.ic_add_a_photo_black_24dp)
                    .into(medicineImg);

            int position = Arrays.asList(takemethoditem_TWN).indexOf(medicineInfo.getString("method"));
            takemethod_spinner.setSelection(position);
            int length = medicineInfo.length();
            if(length > 7) {
                takedayEdt.setText(medicineInfo.getString("takedatevalue"));

                //下面是從個人藥盒抓的
                int repeatday = Integer.valueOf(medicineInfo.getString("repeatday"));
                switch (repeatday){
                    case 1: { //每日
                        takeday1Spn.setSelection(0);
                        position = Arrays.asList(takeday2item2_TWN).indexOf(medicineInfo.getString("taketime") + "次");
                        takeday2pos = position;
                        takeday2Spn.setSelection(position);
                        break;
                    }
                    case 7: { //每周
                        takeday1Spn.setSelection(2);
                        break;
                    }
                    default: { //每隔X天
                        takeday1Spn.setSelection(1);
                        position = Arrays.asList(takeday2item_TWN).indexOf(medicineInfo.getString("repeatday") + "天");
                        takeday2pos = position;
                    }
                }
                position = Arrays.asList(taketimingitem_TWN).indexOf(medicineInfo.getString("taketiming"));
                taketiming_spinner.setSelection(position);


                takeamountEdt.setText(medicineInfo.getString("takeamount"));
                position = Arrays.asList(takeunititem_TWN).indexOf(medicineInfo.getString("takeunit"));
                takeunit_spinner.setSelection(position);
                remark.setText(medicineInfo.getString("remark"));

                JSONArray remindertime = medicineInfo.getJSONArray("remindertime");
                for(int i = 0; i < remindertime.length(); i++)
                    taketime[i].setText(remindertime.getString(i).substring(0, 5));

            }
            else{ //從藥庫選的 重智選單
                takeday1Spn.setSelection(0);
                takeday2Spn.setSelection(0);
                taketiming_spinner.setSelection(0);
                takeunit_spinner.setSelection(0);
                takeamountEdt.setText("");
                takedayEdt.setText("");
                remark.setText("");
                clockimg.setVisibility(View.GONE);
                for(int i = 0; i < 4; i++)
                    taketime[i].setVisibility(View.GONE);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    //--------connect to MySQL----------
    private void search_medicine(final String medicinename){
        myDataset.clear();
        String URL = "http://140.117.71.74/graduation/fetch_medicine.php";
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            Log.d("書出訊息:", String.valueOf(jsonObject));
                            String success =jsonObject.getString("success");
                            String message = jsonObject.getString("message");
                            if (success.equals("1")){
//                                Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                                JSONArray record = jsonObject.getJSONArray("record");
                                //放進datalist中
                                for(int i = 0; i < record.length(); i++) {
                                    JSONObject single_record = record.getJSONObject(i);
                                    myDataset.add(single_record);
                                }


                                //myDataset放道adapter中去建立recycler view
                                mAdapter = new MyAdapter(myDataset);
                                medicineList.setAdapter(mAdapter);
                            }
                            else{
                                Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.d("錯誤訊息",e.toString());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(),getString(R.string.searchfail)+error.toString(),Toast.LENGTH_SHORT).show();
                    }
                })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String>params = new HashMap<>();
                params.put("patientid", patient_id);
                params.put("caretakerid", user_id);
                params.put("medicinename", medicinename);

                /*本來只顯示藥庫的code
                if(medicinename.equals("")){ //只抓藥盒
                    params.put("medicinename", medicinename);
                }
                else{ //抓藥盒跟藥庫

                }
                */


                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    private void add_medicine(){
        String URL = "http://140.117.71.74/graduation/add_medicine.php";
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            Log.d("書出訊息:", String.valueOf(jsonObject));
                            String success =jsonObject.getString("success");
                            String message = jsonObject.getString("message");
                            if (success.equals("1")){
                                Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                                setResult(RESULT_OK);
                                finish();
                            }
                            else{
                                Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.d("錯誤訊息",e.toString());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(),getString(R.string.searchfail)+error.toString(),Toast.LENGTH_SHORT).show();
                    }
                })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String>params = new HashMap<>();
                params.put("patientid", patient_id);
                params.put("caretakerid", user_id);
                params.put("name", medicine_name.getText().toString());
                params.put("method", takemethoditem_TWN[takemethod_spinner.getSelectedItemPosition()]);
                params.put("takedatetype", "");
                params.put("takedatevalue", takeamountEdt.getText().toString());

                String times;
                if(takeday1Spn.getSelectedItemPosition() == 0) //每日
                    times = takeday2item2_TWN[takeday2Spn.getSelectedItemPosition()].substring(0,1);
                else
                    times = "1";
                params.put("taketime", times);
                for(int i = 0; i < Integer.valueOf(times); i++){
                    params.put("taketime"+i, taketime[i].getText().toString());
                }
                params.put("taketiming", taketimingitem_TWN[taketiming_spinner.getSelectedItemPosition()]);
                params.put("takeamount", takeamountEdt.getText().toString());
                params.put("takeunit", takeunititem_TWN[takeunit_spinner.getSelectedItemPosition()]);
                params.put("remark", remark.getText().toString());
                params.put("startdate", getcurrentdate());

                String repeat = "";
                if(takeday1Spn.getSelectedItemPosition() == 0) //每日
                    repeat = "1";
                else if(takeday1Spn.getSelectedItemPosition() == 1) { //每隔
                    repeat = takeday2item_TWN[takeday2Spn.getSelectedItemPosition()].substring(0,1);
                }
                else   //每周
                    repeat = "7";
                params.put("repeatday", repeat);

                params.put("prescription", "0");
                int stuck = Integer.valueOf(times) * Integer.valueOf(takedayEdt.getText().toString())
                        * Integer.valueOf(takeamountEdt.getText().toString());
                params.put("stuck", "0");
                params.put("hasreminder", "1");
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    private void add_reminder(String reminder_type) throws ParseException {
        String times; //每日服用次數
        String[] reminder_time = new String[4]; //每天服用的時間
        if(takeday1Spn.getSelectedItemPosition() == 0) //每日
            times = takeday2item2_TWN[takeday2Spn.getSelectedItemPosition()].substring(0,1);
        else
            times = "1";
        for(int i = 0; i < Integer.valueOf(times); i++){
            reminder_time[i] = taketime[i].getText().toString();
        }
        String repeat = ""; //每隔幾天提醒
        if(takeday1Spn.getSelectedItemPosition() == 0) //每日
            repeat = "1";
        else if(takeday1Spn.getSelectedItemPosition() == 1) { //每隔
            repeat = takeday2item_TWN[takeday2Spn.getSelectedItemPosition()].substring(0,1);
        }
        else   //每周
            repeat = "7";
        //傳日期跟時間給伯倫 重複三次
        SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        Date currenttime = sdf.parse(sdf.format(new Date())); //當前時間
        Calendar cal = Calendar.getInstance(); //get current datetime

//        for(int i = 0; i < 3; i++) { //重複三次的天數 等通知OK後反助解掉
            int add_day = Integer.valueOf(repeat); //提醒日期
            for(int j = 0; j < Integer.valueOf(times); j++){ //提醒時間
                String remindtime = taketime[j].getText().toString() + ":00";

                Date add_time = sdf.parse(remindtime);
                if(add_time.before(currenttime)){ //開始日 過去的時間不用提醒

                }
//                if(add_time.before(currenttime) && i == 0){ //開始日 過去的時間不用提醒
//
//                }
                else{
                    long difference = add_time.getTime() - currenttime.getTime();
                    cal.add(Calendar.MILLISECOND, (int) difference);

//-----------下面這行要反註解後才有通知 傳的cal就是推播的日期時間-----------
                    set_reminder(cal); //設定通知
                    Log.d("datetime", sdf2.format(cal.getTime())); //推播的日期時間

                    cal.add(Calendar.MILLISECOND, (int) difference*(-1));
                }

            }
            cal.add(Calendar.DATE, add_day);
//        }

        /*
        //notification test 10/22
        Calendar cal = Calendar.getInstance();
        // 設定於 3 分鐘後執行
        cal.add(Calendar.SECOND, 3);

        Intent intent = new Intent(this, PlayReceiver.class);
        intent.putExtra("msg", "play_hskay");
        intent.putExtra("title", "好吃的藥");
        intent.putExtra("message", "該吃藥囉");

        PendingIntent pi = PendingIntent.getBroadcast(this, 1, intent, PendingIntent.FLAG_ONE_SHOT);
        AlarmManager am = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
//        am.setRepeating(AlarmManager.RTC, time, AlarmManager.INTERVAL_DAY, pi);
        am.set(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), pi);
        */
    }

    //設定通知
    private void set_reminder(Calendar cal){
        Intent intent = new Intent(this, PlayReceiver.class);
        intent.putExtra("msg", "play_hskay");
        intent.putExtra("title", "FamiCare");
        SimpleDateFormat sdf2 = new SimpleDateFormat("HH:mm");
        String date = sdf2.format(cal.getTime()); //明明就是time呀XD
        Log.d("setreminderdate",date+ "");
        Log.d("setreminderdate", String.valueOf(cal));
        intent.putExtra("Sdate",date);
        intent.putExtra("message", date + " 該吃藥囉");

        //要設不同的id才能設置多個推播 不然會被蓋掉
        int id = Integer.parseInt(date.replace(":", ""));
        PendingIntent pi = PendingIntent.getBroadcast(this, id, intent, PendingIntent.FLAG_ONE_SHOT);
        AlarmManager am = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
//        am.setRepeating(AlarmManager.RTC, time, AlarmManager.INTERVAL_DAY, pi);
        am.set(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), pi);

        //測試用 這樣不用一直等時間到才推通知
//        Calendar cal_test = Calendar.getInstance();
//        am.set(AlarmManager.RTC_WAKEUP, cal_test.getTimeInMillis(), pi);
    }
    //--介面部分
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
//                mDrawerLayout.openDrawer(Gravity.RIGHT);
                finish();
                return true;
            case R.id.user_info:
                mDrawerLayout.openDrawer(Gravity.RIGHT);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


    //這邊是QRCODE接收的資料
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        final IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);

        String nametotal ="";
        String name="";
//        date = new Date();
//        sdf = new SimpleDateFormat("yyyy/-MM-dd ");
        if (result!=null &&result.getContents()!=null) {
//            "寶齡" 美骨健錠15毫克"#外用#0#2#5#飯後#5#粒#1#5
            JSONObject medicineInfo = new JSONObject();
            String medicinedata = result.getContents(); //掃完QRcode後獨到的字串
            StringTokenizer st1 = new StringTokenizer(medicinedata, "_");
            int i = 0;
            while (st1.hasMoreTokens()) {
                String[] singledata = st1.nextToken().split("#");
                name = singledata[0];
                try {
                    medicineInfo.put("name", singledata[0]);
                    medicineInfo.put("method", singledata[1]);
                    medicineInfo.put("takedatevalue", singledata[2]);
                    medicineInfo.put("taketime", singledata[3]);
                    medicineInfo.put("repeatday", singledata[4]);
                    medicineInfo.put("taketiming", singledata[5]);
                    medicineInfo.put("takeamount", singledata[6]);
                    medicineInfo.put("takeunit", singledata[7]);
                    medicineInfo.put("prescription", singledata[8]);
                    medicineInfo.put("stuck", singledata[9]);
                    medicineInfo.put("engname", singledata[10]);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                fetch_data(medicineInfo);

                nametotal= nametotal +"\n"+ name;
                Log.d("nametotal",nametotal);
//                add_medicine();
            }
        }

//       ***** AlertDialog******
        if (result!=null &&result.getContents()!=null){
            final String finalNametotal = nametotal;
            new AlertDialog.Builder(reminder_add_medicine.this)
                    .setTitle("新增藥品")
                    .setMessage(finalNametotal)
                    .setPositiveButton("確定", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
//                    ClipboardManager manager = (ClipboardManager)getSystemService(CLIPBOARD_SERVICE);
//                    ClipData data = ClipData.newPlainText("result", finalNametotal);
//                    manager.setPrimaryClip(data);
                        }
//            }).setNegativeButton("cancel", new DialogInterface.OnClickListener() {
//                @Override
//                public void onClick(DialogInterface dialog, int which) {
//                    dialog.dismiss();
//                }
                    }).create().show();

        }
        super.onActivityResult(requestCode, resultCode, data);

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.game_menu, menu);
        //set pattent name of menu
        MenuItem patientname = menu.findItem(R.id.patient_name);
        patientname.setTitle(localDB.getpatientname());
        return true;
    }
//    介面部分--

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected( MenuItem item) {
            BottomNavigation bn = new BottomNavigation(reminder_add_medicine.this, item.getItemId());
            return true;
        }

    };

}
