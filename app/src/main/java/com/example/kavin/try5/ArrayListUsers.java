package com.example.kavin.try5;

import com.google.gson.annotations.SerializedName;

public class ArrayListUsers {

    @SerializedName("permission")
    private String permission;
    @SerializedName("name")
    private String name;
    @SerializedName("patientID")
    private String patientID;

    public String getPermission() {
        return "("+ permission + ")";
    }

    public String getPatientname() {
        return name;
    }

    public String getPatientID() {
        return patientID;
    }
}
