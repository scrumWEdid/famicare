package com.example.kavin.try5.mpchartCustom;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;

import java.util.ArrayList;
import java.util.Collections;

import com.example.kavin.try5.R;

public class drawLineChart {
    private Context context = null;
    private int valueindex = 0;
    private ArrayList<String> inputdata = null;
    private LineChart chart;
    private LineData linedata = new LineData();
    public drawLineChart(Context ctx, LineChart mChart){
        this.context = ctx;
        this.chart = mChart;
    }
    public void setupChart(ArrayList<String> allinputdata) {

        this.inputdata = new ArrayList(allinputdata); //pass by value
        Collections.reverse(inputdata); //reverse data

        ArrayList<String> xlabels = setXlabels(inputdata);
        ArrayList<String> markerstxt = setMarkerText(inputdata);
        // no description text
        chart.getDescription().setEnabled(false);

        // enable / disable grid background
        chart.setDrawGridBackground(false);

        // enable touch gestures
        chart.setTouchEnabled(true);

        // enable scaling and dragging
        chart.setDragEnabled(true);
        chart.setScaleEnabled(true);

        // if disabled, scaling can be done on x- and y-axis separately
        chart.setPinchZoom(false);

        // create a custom MarkerView (extend MarkerView) and specify the layout
        // to use for it

        MyMarkerView mv = new MyMarkerView(context, R.layout.linechart_marker_view, markerstxt);
        mv.setChartView(chart); // For bounds control
        chart.setMarker(mv); // Set the marker to the chart

        chart.setBackgroundColor(Color.WHITE);

        // set custom chart offsets (automatic offset calculation is hereby disabled)
//        chart.setViewPortOffsets(10, 0, 10, 0); //這個會讓xy軸出不來= =

        // add data
        chart.setData(linedata);
        chart.invalidate();// refresh after setting data

        // get the legend (only possible after setting data)
        Legend l = chart.getLegend();
        l.setTextSize(16f);
        l.setWordWrapEnabled(true);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.CENTER);
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.TOP);
        //x軸
        XAxis xAxis = chart.getXAxis();
        xAxis.setDrawGridLines(false);
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setValueFormatter(new MyXAxisValueFormatter(xlabels));
        xAxis.setLabelRotationAngle(-45);
        xAxis.setGranularity(1f);
        chart.setXAxisRenderer(new CustomXAxisRenderer(chart.getViewPortHandler(), chart.getXAxis(), chart.getTransformer(YAxis.AxisDependency.LEFT)));
        chart.setExtraTopOffset(-25);
        chart.setExtraRightOffset(20);
        //y軸
        YAxis leftAxis = chart.getAxisLeft();
        leftAxis.removeAllLimitLines(); // reset all limit lines to avoid overlapping lines
//        leftAxis.setAxisMaximum(40f);
//        leftAxis.setAxisMinimum(30f);
        leftAxis.enableGridDashedLine(10f, 10f, 0f);
        leftAxis.setDrawAxisLine(true);
        leftAxis.setDrawGridLines(true);
        leftAxis.setDrawZeroLine(false);
        leftAxis.setPosition(YAxis.YAxisLabelPosition.OUTSIDE_CHART);
        chart.getAxisRight().setEnabled(false);

        // animate calls invalidate()...
//        chart.animateX(2500);
    }
    public void setValueindex(int value){
        valueindex = value;
    }
    public void addDataSet(ArrayList<String> inputdata, String linecolor, String typename) {

        inputdata = new ArrayList(inputdata); //pass by value
        Collections.reverse(inputdata); //reverse data

        ArrayList<Entry> yVals = new ArrayList<Entry>();

        String[] single_data;
        for(int i = 0; i < inputdata.size(); i++){
            if(!inputdata.get(i).isEmpty()) {
                single_data = inputdata.get(i).split(" ");
                yVals.add(new Entry(i, Float.parseFloat(single_data[valueindex])));
            }
        }

        // create a dataset and give it a type
        LineDataSet set1 = new LineDataSet(yVals, typename);

        set1.setLineWidth(1.75f);
        set1.setCircleRadius(5f);
        set1.setCircleHoleRadius(2.5f);
        set1.setColor(Color.parseColor(linecolor));
        set1.setCircleColor(Color.parseColor(linecolor));
//        set1.setHighLightColor(Color.rgb(89, 199, 250));
        set1.setDrawHighlightIndicators(false);
        set1.setDrawValues(false);


        // create a data object with the datasets
//        LineData data = new LineData(set1);
        if(!yVals.isEmpty())
            linedata.addDataSet(set1);
    }
    public ArrayList<String> setXlabels(ArrayList<String> inputdata){
        ArrayList<String> xlabels = new ArrayList<String>();
        for(int i = 0; i < inputdata.size(); i++) {
            String[] s = inputdata.get(i).split(" ");
            String xlabeltext = s[0].substring(5) + "\n" + s[1];
            xlabels.add(xlabeltext);
        }
        return xlabels;
    }
    public ArrayList<String> setMarkerText(ArrayList<String> inputdata){
        ArrayList<String> markerstxt = new ArrayList<String>();
        for(int i = 0; i < inputdata.size(); i++) {
            String[] s = inputdata.get(i).split(" ");
            String xlabeltext = s[0] + " " + s[1] + "\n";
            markerstxt.add(xlabeltext);
        }
        return markerstxt;
    }
    public void resetchart(){
        linedata.clearValues();
        chart.notifyDataSetChanged();
        chart.clear();
        chart.invalidate();
    }
}
