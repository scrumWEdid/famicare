package com.example.kavin.try5;

import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;

public class BottomNavigation {
    private Context context;
    private int viewID;
    public BottomNavigation(Context ctx, int viewId){
        this.context = ctx;
        this.viewID = viewId;
        ActivityOptions options =
                ActivityOptions.makeCustomAnimation(context, R.anim.slide_in_right, R.anim.slide_out_left);

        switch (viewID){
            case R.id.nav_home:
                Intent homeIntent = new Intent(context,indexActivity.class);
                homeIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                homeIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                context.startActivity(homeIntent);
                break;
            case R.id.alarm:
                Intent alarmIntent = new Intent(context,intentbundletestActivity.class);
                alarmIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                alarmIntent.putExtra("type","reminder");
                context.startActivity(alarmIntent, options.toBundle());
                break;
            case R.id.drug:
                Intent drugIntent = new Intent(context,intentbundletestActivity.class);
                drugIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                drugIntent.putExtra("type","mypillbox");
                context.startActivity(drugIntent, options.toBundle());
                break;
            case R.id.nav_description:
                Intent recordIntent = new Intent(context,intentbundletestActivity.class);
                recordIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                recordIntent.putExtra("type","record");
                context.startActivity(recordIntent, options.toBundle());
                break;
            case R.id.videoPage:
                Intent videoIntent = new Intent(context,intentbundletestActivity.class);
                videoIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                videoIntent.putExtra("type","video");
                context.startActivity(videoIntent, options.toBundle());
                break;

        }
    }
}
