package com.example.kavin.try5;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.CountDownTimer;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.kavin.try5.reminder_medicine.reminder_medicine_weekday;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;

public class ViewPagerAdapter extends PagerAdapter {

    private Context context;
    private LayoutInflater layoutInflater;
    private Integer [] images = {R.drawable.medicine_slide,R.drawable.measure_slide,R.drawable.pipeline_slide};
    private String [] title = {"用藥提醒","管路提醒","測量提醒"};
    private TextView textView ;


    public ViewPagerAdapter(Context context) {
        this.context = context;
    }

    @Override
    public int getCount() {
        return images.length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }
    @Override
    public int getItemPosition(Object object){
        return POSITION_NONE;
    }
    @Override
    public Object instantiateItem(ViewGroup container, final int position) {

        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.glide_layout, null);
        ImageView imageView = (ImageView) view.findViewById(R.id.imageView);
        final TextView counttimetv = (TextView)view.findViewById(R.id.tv_slide);
        final TextView counttitletv = (TextView)view.findViewById(R.id.tv_top);
        imageView.setImageResource(images[position]);
        counttitletv.setText(title[position]);
        counttimetv.setText(reminder_mainPage2.time[position]);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(position == 0){
                    Intent intent = new Intent();
                    intent.setClass(context, reminder_medicine_weekday.class);
                    context.startActivity(intent);
                } else if(position == 1){
                    Intent intent = new Intent();
                    intent.setClass(context, reminder_medicine_weekday.class);
                    context.startActivity(intent);
                } else {
                    Intent intent = new Intent();
                    intent.setClass(context, reminder_medicine_weekday.class);
                    context.startActivity(intent);
                }

            }
        });

        ViewPager vp = (ViewPager) container;
        vp.addView(view, 0);
        return view;

    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {

        ViewPager vp = (ViewPager) container;
        View view = (View) object;
        vp.removeView(view);

    }}
