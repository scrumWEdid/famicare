package com.example.kavin.try5.reminder_notification;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.widget.Toast;

import com.example.kavin.try5.R;
import com.example.kavin.try5.bloodsugar_add;
import com.example.kavin.try5.heartandpressure_add;
import com.example.kavin.try5.reminder_medicine.reminder_medicine_weekday;
import com.example.kavin.try5.temperature_add;

import static com.example.kavin.try5.reminder_notification.channels.CHANNEL_1_ID;

public class PlayReceiver extends BroadcastReceiver {
    private NotificationManagerCompat notificationManager;
    @Override
    public void onReceive(Context context, Intent intent) {

        notificationManager = NotificationManagerCompat.from(context);
        Bundle bData = intent.getExtras();
        if(bData.get("msg").equals("play_hskay"))
        {
            sendOnChannel1(context,bData.getString("title"), bData.getString("message"),bData.getString("Sdate"));

        }
        if(bData.get("msg").equals("measure"))
        {
            sendOnChannel2(context,bData.getString("title"), bData.getString("message"),bData.getString("Sdate"),bData.getString("type"));

        }
    }

    //推撥通知的
    public void sendOnChannel1(Context context,String title, String message,String date) {


        Intent activityIntent = new Intent(context,reminder_medicine_weekday.class); //intent會傳到reminder_medicine_weekday
        activityIntent.putExtra("Sdate",date);
        Log.d("Sdateset",date);
        activityIntent.putExtra("flag", "notification");

        PendingIntent contentIntent = PendingIntent.getActivity(context,
                0, activityIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        Intent broadcastIntent = new Intent(context, NotificationReceiver.class);
        broadcastIntent.putExtra("toastMessage", message);
        PendingIntent actionIntent = PendingIntent.getBroadcast(context, //設toast
                0, broadcastIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        Bitmap largeIcon = BitmapFactory.decodeResource(context.getResources(), R.mipmap.logo_withbackground);
//        largeIcon.eraseColor(Color.parseColor("#FF0000"));//填充颜色
        Notification notification = new NotificationCompat.Builder(context, CHANNEL_1_ID) //sent to channel1
                .setSmallIcon(R.mipmap.logo_withbackground)
                .setContentTitle(title)
                .setContentText(message)
                .setLargeIcon(largeIcon)
                .setStyle(new NotificationCompat.BigTextStyle()
                        .bigText(message)
                        .setBigContentTitle(title))
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setCategory(NotificationCompat.CATEGORY_MESSAGE)
//                .setColor(Color.WHITE)
                .setContentIntent(contentIntent)
                .setAutoCancel(true)
                .setOnlyAlertOnce(true)
                .setVibrate(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400})
                .setDefaults(Notification.DEFAULT_SOUND | Notification.DEFAULT_VIBRATE)

                .build();

        notificationManager.notify(1, notification);
    }
    //推撥通知的
    public void sendOnChannel2(Context context,String title, String message,String date,String type) {
        Intent activityIntent =null;
        if (type.equals("體溫")){
            Log.d("type","tem");
           activityIntent = new Intent(context,temperature_add.class); //intent會傳到reminder_medicine_weekday
        }
        else if (type.equals("心跳血壓")){
            Log.d("type","hear");
            activityIntent = new Intent(context,heartandpressure_add.class); //intent會傳到reminder_medicine_weekday
        }
        else if(type.equals("血糖")){
            Log.d("type","sugar");
            activityIntent = new Intent(context,bloodsugar_add.class); //intent會傳到reminder_medicine_weekday
        }
        else{
            Log.d("type","weekday");
            activityIntent = new Intent(context,reminder_medicine_weekday.class); //intent會傳到reminder_medicine_weekday
        }
        activityIntent.putExtra("Sdate",date);
        Log.d("Sdateset",date);
        activityIntent.putExtra("flag", "notification");

        PendingIntent contentIntent = PendingIntent.getActivity(context,
                0, activityIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        Intent broadcastIntent = new Intent(context, NotificationReceiver.class);
        broadcastIntent.putExtra("toastMessage", message);
        PendingIntent actionIntent = PendingIntent.getBroadcast(context, //設toast
                0, broadcastIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        Bitmap largeIcon = BitmapFactory.decodeResource(context.getResources(), R.mipmap.logo_withbackground);
//        largeIcon.eraseColor(Color.parseColor("#FF0000"));//填充颜色
        Notification notification = new NotificationCompat.Builder(context, CHANNEL_1_ID) //sent to channel1
                .setSmallIcon(R.mipmap.logo_withbackground)
                .setContentTitle(title)
                .setContentText(message)
                .setLargeIcon(largeIcon)
                .setStyle(new NotificationCompat.BigTextStyle()
                        .bigText(message)
                        .setBigContentTitle(title))
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setCategory(NotificationCompat.CATEGORY_MESSAGE)
//                .setColor(Color.WHITE)
                .setContentIntent(contentIntent)
                .setAutoCancel(true)
                .setOnlyAlertOnce(true)
                .setVibrate(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400})
                .setDefaults(Notification.DEFAULT_SOUND | Notification.DEFAULT_VIBRATE)

                .build();

        notificationManager.notify(1, notification);
    }
}
