package com.example.kavin.try5;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewFlipper;

import org.w3c.dom.Text;

public class indexActivity extends AppCompatActivity {

    private DrawerLayout mDrawerLayout;
    private localDatabase localDB = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_index);
        indexActivity.this.setTitle(R.string.index);

        ImageView teach1 = (ImageView) findViewById(R.id.teach1);
        teach1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                watchYoutubeVideo(indexActivity.this,"AdOL6Z3ejAQ");
            }
        });

        ImageView teach2= (ImageView) findViewById(R.id.teach2);
        teach2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                watchYoutubeVideo(indexActivity.this,"6vMeuPT5n7U");
            }
        });

        ImageView teach3= (ImageView) findViewById(R.id.teach3);
        teach3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                watchYoutubeVideo(indexActivity.this,"wTSLjQM4ccA");
            }
        });

        ViewFlipper vf =findViewById(R.id.view_flipper);
        vf.setFlipInterval(5000);
        vf.startFlipping();

        final TextView recordEverydayLink = (TextView) findViewById(R.id.recordEverydayTextView);
        recordEverydayLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent registerIntent = new Intent(indexActivity.this,intentbundletestActivity.class);
                registerIntent.putExtra("type","record");
                indexActivity.this.startActivity(registerIntent);
            }
        });
        final TextView reminderLink = findViewById(R.id.reminderTextView);
        reminderLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent registerIntent = new Intent(indexActivity.this,intentbundletestActivity.class);
                registerIntent.putExtra("type","reminder");
                indexActivity.this.startActivity(registerIntent);
            }
        });
        final TextView mypillbox = findViewById(R.id.mypillboxTextview);
        mypillbox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent registerIntent = new Intent(indexActivity.this,intentbundletestActivity.class);
                registerIntent.putExtra("type","mypillbox");
                indexActivity.this.startActivity(registerIntent);
            }
        });

        final TextView videoPage = findViewById(R.id.videopage);
        videoPage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(indexActivity.this, intentbundletestActivity.class);
                intent.putExtra("type","video");
                startActivity(intent);
            }
        });


        //localDatebase
        localDB = new localDatabase(this);
        localDB.open();

        //介面部分
        toolbar mytoolbar = new toolbar(this);
        mytoolbar.setbackbtn(false);
        mDrawerLayout = findViewById(R.id.drawer_layout);
        //介面部分


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                indexActivity.this.finish();
                return true;
            case R.id.user_info:
                mDrawerLayout.openDrawer(Gravity.RIGHT);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.game_menu, menu);
        //set pattent name of menu
        MenuItem patientname = menu.findItem(R.id.patient_name);
        patientname.setTitle(localDB.getpatientname());
        return true;
    }


    //close localDB
    @Override
    protected void onDestroy() {
        super.onDestroy();
        localDB.close();
    }

    public static void watchYoutubeVideo(Context context, String id){
        Intent appIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("vnd.youtube:" + id));
        Intent webIntent = new Intent(Intent.ACTION_VIEW,
                Uri.parse("http://www.youtube.com/watch?v=" + id));
        try {
            context.startActivity(appIntent);
        } catch (ActivityNotFoundException ex) {
            context.startActivity(webIntent);
        }
    }

}
