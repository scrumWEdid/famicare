package com.example.kavin.try5;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.github.florent37.singledateandtimepicker.SingleDateAndTimePicker;
import com.github.florent37.singledateandtimepicker.dialog.SingleDateAndTimePickerDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class SelectPreviewActivity extends AppCompatActivity {
    private DrawerLayout mDrawerLayout;
    private CheckBox select_all_checkbox,temperature_checkbox,hearthbeatpressure_checkbox,bloodsugar_checkbox,pain_checkbox;
    private CheckBox other_checkbox,bathroom_checkbox;
    private TextView startdate_txt, enddate_txt;
    private Date start_date, end_date;
    private static String URL_SELECTPREVIEW = "http://140.117.71.74/graduation/create_record.php";
    private String check_temperature,check_thearthpressure,check_bloodsugar,check_pain,check_other,check_bathroom,patientID;
    private SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
    private localDatabase localDB = null;
    boolean flag = false, flag1 =false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_preview);
        BottomNavigationView bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottom_navigation_view);
        bottomNavigationView.setSelectedItemId(R.id.nav_description);
        bottomNavigationView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        //----open localDB
        localDB = new localDatabase(this);
        localDB.open();
        //----介面部分
        toolbar mytoolbar = new toolbar(this);
        mDrawerLayout = findViewById(R.id.drawer_layout);
// 介面部分-----

        Button preview = findViewById(R.id.preivew_button);
        select_all_checkbox = findViewById(R.id.selectall_checkbox);
        temperature_checkbox = findViewById(R.id.temperature_checkbox);
        hearthbeatpressure_checkbox =findViewById(R.id.hearthbeatpressure_checkbox);
        bloodsugar_checkbox =findViewById(R.id.bloodsugar_checkbox);
        bathroom_checkbox =findViewById(R.id.bathroom_checkbox);
        other_checkbox=findViewById(R.id.other_checkbox);
        pain_checkbox = findViewById(R.id.pain_checkbox);
        patientID = localDB.getpatientinfo();

        startdate_txt =(TextView) findViewById(R.id.start_date);
        enddate_txt = (TextView) findViewById(R.id.end_date);

        start_date = new Date();
        end_date = new Date();
        startdate_txt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                open_datetimepicker(1);
            }
        });
        enddate_txt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                open_datetimepicker(2);
            }
        });

        select_all_checkbox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(select_all_checkbox.isChecked()){
                    temperature_checkbox.setChecked(true);
                    hearthbeatpressure_checkbox.setChecked(true);
                    bloodsugar_checkbox.setChecked(true);
                    pain_checkbox.setChecked(true);
                    bathroom_checkbox.setChecked(true);
                    other_checkbox.setChecked(true);
                }
                if(!select_all_checkbox.isChecked()){
                    temperature_checkbox.setChecked(false);
                    hearthbeatpressure_checkbox.setChecked(false);
                    bloodsugar_checkbox.setChecked(false);
                    pain_checkbox.setChecked(false);
                    bathroom_checkbox.setChecked(false);
                    other_checkbox.setChecked(false);
                }
            }
        });

        preview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                check_temperature="2";
                check_thearthpressure = "2";
                check_bloodsugar = "2";
                check_pain = "2";
                check_bathroom="2";
                check_other="2";
                if(temperature_checkbox.isChecked()){
                    check_temperature ="1";
                }
                if(hearthbeatpressure_checkbox.isChecked()){
                    check_thearthpressure = "1";
                }
                if(bloodsugar_checkbox.isChecked()){
                    check_bloodsugar = "1";
                }
                if (pain_checkbox.isChecked()){
                    check_pain = "1";
                }
                if (other_checkbox.isChecked()){
                    check_other="1";
                }
                if(bathroom_checkbox.isChecked()){
                    check_bathroom="1";
                }

                if(check_temperature!="1"&&check_thearthpressure!="1"&&check_bloodsugar!="1"&check_pain!="1"&&check_other!="1"&&check_bathroom!="1"){
                    Toast.makeText(SelectPreviewActivity.this,
                            getString(R.string.atleasechooseone),Toast.LENGTH_SHORT).show();
                }
                else if(flag ==false||flag1==false){
                    Toast.makeText(SelectPreviewActivity.this,getString(R.string.pleasechoosedate),Toast.LENGTH_SHORT).show();
                }
                else{
                    String startdate =startdate_txt.getText().toString();
                    String enddate = enddate_txt.getText().toString();
                    Fetch(check_temperature,check_thearthpressure,check_bloodsugar,check_pain,check_bathroom,check_other,startdate,enddate,patientID);

                }
            }

        });

    }

    private void Fetch(final String check_temperature,final String check_thearthpressure,final String check_bloodsugar,final String check_pain,final String check_bathroom,final String check_other,final String startdate,final String enddate, final String patientID) {


        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL_SELECTPREVIEW,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            String success =jsonObject.getString("success");
                            if (success.equals("1")){

                                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://140.117.71.74/graduation/record/"+ patientID+"_my_record.html"));
                                startActivity(browserIntent);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.d("錯誤訊息",e.toString());

                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("錯誤訊息",error.toString());
                    }
                })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String>params = new HashMap<>();
                params.put("check_temperature",check_temperature);
                params.put("check_thearthpressure",check_thearthpressure);
                params.put("check_bloodsugar",check_bloodsugar);
                params.put("check_pain",check_pain);
                params.put("check_bathroom",check_bathroom);
                params.put("check_other",check_other);
                params.put("patientID",patientID);
                params.put("startdate",startdate);
                params.put("enddate",enddate);
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    private void open_datetimepicker(int id){
        if(id == 1) { //選開始時間
            new SingleDateAndTimePickerDialog.Builder(SelectPreviewActivity.this)
                    .bottomSheet()
                    .displayMinutes(false)
                    .displayHours(false)
                    .displayDays(false)
                    .displayMonth(true)
                    .displayYears(true)
                    .displayDaysOfMonth(true)
                    .maxDateRange(end_date)
                    .displayListener(new SingleDateAndTimePickerDialog.DisplayListener() {
                        @Override
                        public void onDisplayed(SingleDateAndTimePicker picker) {
                            picker.setDefaultDate(start_date);
                        }
                    })
                    .title(getString(R.string.startdate))
                    .listener(new SingleDateAndTimePickerDialog.Listener() {
                        @Override
                        public void onDateSelected(Date date) {
                            start_date = date;
                            String select_date = sdf.format(date);
                            startdate_txt.setText(select_date);
                            flag = true;
                            //fetch_record(RECORD_TYPE);
                        }
                    }).display();
        }
        else{ //選截止時間
            new SingleDateAndTimePickerDialog.Builder(SelectPreviewActivity.this)
                    .bottomSheet()
                    .displayMinutes(false)
                    .displayHours(false)
                    .displayDays(false)
                    .displayMonth(true)
                    .displayYears(true)
                    .displayDaysOfMonth(true)
                    .minDateRange(start_date)
                    .maxDateRange(new Date())
                    .displayListener(new SingleDateAndTimePickerDialog.DisplayListener() {
                        @Override
                        public void onDisplayed(SingleDateAndTimePicker picker) {
                            picker.setDefaultDate(end_date);
                        }
                    })
                    .title(getString(R.string.stopdate))
                    .listener(new SingleDateAndTimePickerDialog.Listener() {
                        @Override
                        public void onDateSelected(Date date) {
                            end_date = date;
                            String select_date = sdf.format(date);
                            enddate_txt.setText(select_date);
                            flag1 = true;
                            //fetch_record(RECORD_TYPE);
                        }
                    }).display();
        }
    }

    //--介面部分
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
//                mDrawerLayout.openDrawer(Gravity.RIGHT);
                finish();
                return true;
            case R.id.user_info:
                mDrawerLayout.openDrawer(Gravity.RIGHT);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.game_menu, menu);
        //set pattent name of menu
        MenuItem patientname = menu.findItem(R.id.patient_name);
        patientname.setTitle(localDB.getpatientname());
        return true;
    }
//    介面部分--
private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
        = new BottomNavigationView.OnNavigationItemSelectedListener() {

    @Override
    public boolean onNavigationItemSelected( MenuItem item) {
        BottomNavigation bn = new BottomNavigation(SelectPreviewActivity.this, item.getItemId());
        return true;
    }

};
}
