package com.example.kavin.try5;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.ViewFlipper;

import org.w3c.dom.Text;

import java.util.Calendar;

public class CreatePatientActivity extends AppCompatActivity {

    private DrawerLayout mDrawerLayout;
    private TextView mDisplayDate;
    private DatePickerDialog.OnDateSetListener mDataSetListener;
    private localDatabase localDB = null;
    private Cursor cursor = null;
    private String user_id, user_acc;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_patient);

        //local Database
        localDB = new localDatabase(this);
        if(localDB.open()) {
            cursor = localDB.getinfo();
            if(cursor.getCount() >= 0){
                user_id = String.valueOf(cursor.getInt(0));
                user_acc = String.valueOf(cursor.getString(1));
            }
        }
        else {
            user_id = getIntent().getStringExtra("userid");
            user_acc = getIntent().getStringExtra("useracc");
        }

        //去讀取寫好的兩個java(一個是ID搜尋的 一個是建立新被照顧者
        addPatient_IDsearch instance = new addPatient_IDsearch(this, this, user_id);
        addPatient_create instance2 = new addPatient_create(this, this, user_id, user_acc);

//        mDisplayDate = (EditText)findViewById(R.id.birthdayEditText);
//
//        mDisplayDate.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Calendar cal = Calendar.getInstance();
//                int year = cal.get(Calendar.YEAR);
//                int month = cal.get(Calendar.MONTH);
//                int day = cal.get(Calendar.DAY_OF_MONTH);
//
//                DatePickerDialog dialog = new DatePickerDialog(
//                        CreatePatientActivity.this,
//                        android.R.style.Theme_Holo_Light_Dialog_MinWidth,
//                        mDataSetListener,
//                        year,month,day);
//                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//                dialog.show();
//            }
//        });
//
//        mDataSetListener = new DatePickerDialog.OnDateSetListener() {
//            @Override
//            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
//                month=month+1;
//
//
//                String date = year+"/"+month+"/"+dayOfMonth;
//                mDisplayDate.setText(date);
//            }
//        };

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp);

        mDrawerLayout = findViewById(R.id.drawer_layout);


        final ViewFlipper vf = (ViewFlipper)findViewById(R.id.vf);
        TextView vfLink = (TextView)findViewById(R.id.idSearchPatient);
        TextView vfLink2 = (TextView)findViewById(R.id.createNewPatient);
        TextView vfLink3 = (TextView)findViewById(R.id.idSearchPatient2);
        TextView vfLink4 = (TextView)findViewById(R.id.createNewPatient2);

        vfLink.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                vf.setDisplayedChild(0);
            }
        });
        vfLink2.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                vf.setDisplayedChild(1);
            }
        });
        vfLink3.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                vf.setDisplayedChild(0);
            }
        });
        vfLink4.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                vf.setDisplayedChild(1);
            }
        });



        NavigationView navigationView = findViewById(R.id.nav_view);

        //drawer nav header on click的部分
        View headerview = navigationView.getHeaderView(0);
        LinearLayout header = (LinearLayout) headerview.findViewById(R.id.nav_view_layout);
        header.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent patientInfoIntent = new Intent(CreatePatientActivity.this,patientInfoActivity.class);
                CreatePatientActivity.this.startActivity(patientInfoIntent);
                mDrawerLayout.closeDrawers();
            }
        });

        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        // set item as selected to persist highlight
                        menuItem.setChecked(true);
                        // close drawer when item is tapped
                        mDrawerLayout.closeDrawers();

                        // Add code here to update the UI based on the item selected
                        // For example, swap UI fragments here
                        switch(menuItem.getItemId()){
                            case R.id.nav_switch_patient:
                                Intent switchIntent = new Intent(CreatePatientActivity.this,ChoosePatientActivity.class);
                                CreatePatientActivity.this.startActivity(switchIntent);
                                mDrawerLayout.closeDrawers();
                                return true;
                            case R.id.nav_manage_patient:
                                Intent managePatientIntent = new Intent(CreatePatientActivity.this,CareTakerManagePermissionActivity.class);
                                CreatePatientActivity.this.startActivity(managePatientIntent);
                                mDrawerLayout.closeDrawers();
                                return true;
                            case R.id.nav_member_info:
                                Intent MemberInfoIntent = new Intent(CreatePatientActivity.this,MemberInfoActivity.class);
                                CreatePatientActivity.this.startActivity(MemberInfoIntent);
                                mDrawerLayout.closeDrawers();
                                return true;
                            case R.id.nav_setting:
                                return true;
                        }

                        return true;
                    }
                });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
//                mDrawerLayout.openDrawer(Gravity.RIGHT);
                CreatePatientActivity.this.finish();
                return true;
            case R.id.user_info:
//                mDrawerLayout.openDrawer(Gravity.RIGHT);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.game_menu, menu);
        return true;
    }
}
