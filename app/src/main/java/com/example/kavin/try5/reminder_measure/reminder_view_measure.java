package com.example.kavin.try5.reminder_measure;

import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.kavin.try5.R;
import com.example.kavin.try5.localDatabase;
import com.example.kavin.try5.toolbar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class reminder_view_measure extends AppCompatActivity {
    private DrawerLayout mDrawerLayout;
    private localDatabase localDB = null;
    private toolbar mytoolbar;
    private Cursor cursor = null;
    private String REMINDER_TYPE = "measure";
    private JSONObject reminder;

    private TextView typeTxt;
    private TextView[] taketime = new TextView[4];
    private ImageView clockimg;
    private TextView repeatTxt;
    private TextView taketimeTxt;

    private TextView editBtn, delBtn;
    private Button confirmBtn;

    private String[] typeitem_TWN = {"測量項目", "體溫", "心跳血壓", "血糖"};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reminder_view_measure);
        //----open localDB
        localDB = new localDatabase(this);
        localDB.open();
//----介面部分
        mytoolbar = new toolbar(this);
        mDrawerLayout = findViewById(R.id.drawer_layout);
// 介面部分-----

        typeTxt = findViewById(R.id.typeTxt);
        repeatTxt = findViewById(R.id.repeatTxt);
        taketimeTxt = findViewById(R.id.taketimeTxt);

        clockimg = findViewById(R.id.clockimage);
        taketime[0] = findViewById(R.id.taketimetxt1);
        taketime[1] = findViewById(R.id.taketimetxt2);
        taketime[2] = findViewById(R.id.taketimetxt3);
        taketime[3] = findViewById(R.id.taketimetxt4);


        fetch_data(getIntent()); //顯示資料


        editBtn = findViewById(R.id.editimgBtn);
        editBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent();
                intent.setClass(reminder_view_measure.this, reminder_edit_measure.class);
                Bundle bundle = new Bundle();
                bundle.putString("reminder", reminder.toString());
                intent.putExtras(bundle);
                startActivityForResult(intent, 1);

            }
        });
        delBtn = findViewById(R.id.clearBtn);
        delBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new AlertDialog.Builder(reminder_view_measure.this)
                        .setTitle(getString(R.string.suredelete))
                        .setMessage(getString(R.string.deletenorecovery))
                        .setNegativeButton(getString(R.string.sure), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialoginterface, int i)
                            {
                                delete_reminder(REMINDER_TYPE, reminder);
                            }
                        })
                        .setPositiveButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialoginterface, int i)
                            {

                            }
                        })
                        .show();
            }
        });
        confirmBtn = findViewById(R.id.confirmBtn);
        confirmBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setResult(RESULT_OK);
                finish();
            }
        });

    }
    @Override
    protected void onDestroy(){
        super.onDestroy();
        localDB.close(); // 關閉資料庫
    }


    //設定編輯畫面
    private void fetch_data(Intent intent){
        try {
            int position;
            String s = "";
            reminder =  new JSONObject(intent.getStringExtra("reminder"));

            position = Arrays.asList(typeitem_TWN).indexOf(reminder.getString("type"));
            s = getResources().getStringArray(R.array.typeitem)[position];
            typeTxt.setText(s);

            s = reminder.getString("recordtimes") + getString(R.string.times);
            taketimeTxt.setText(s);

            JSONArray remindertime = reminder.getJSONArray("remindertime");
            for(int i = 0; i < 4; i++){ //因為可能會從編輯畫面回來 所以先重置成都沒顯示時間
                taketime[i].setVisibility(View.INVISIBLE);
            }
            for(int i = 0; i < Integer.valueOf(reminder.getString("recordtimes")); i++) {
                taketime[i].setText(remindertime.getString(i).substring(0, 5));
                taketime[i].setVisibility(View.VISIBLE);
            }

            if(reminder.getString("repeatday").equals("1"))
                s = getString(R.string.everyday);
            else if(reminder.getString("repeatday").equals("7"))
                s = getString(R.string.everyweek);
            else
                s = getString(R.string.every) + reminder.getString("repeatday") + getString(R.string.aday);
            repeatTxt.setText(s);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /*下一頁修改完畢回傳更新資料*/
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == 1){
            if(resultCode == RESULT_OK){
                fetch_data(data);
                setResult(RESULT_OK);
            }
        }
    }

    //--------connect to MySQL----------
    public interface VolleyCallback{
        void onSuccess(String result);
    }
    private void delete_reminder(final String RECORD_TYPE, final JSONObject record){
        String URL = "http://140.117.71.74/graduation/delete_reminder.php";
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            Log.d("書出訊息:", String.valueOf(jsonObject));
                            String success =jsonObject.getString("success");
                            String message = jsonObject.getString("message");
                            if (success.equals("1")){
                                Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                            }
                            else{
                                Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.d("錯誤訊息",e.toString());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(),getString(R.string.searchfail)+error.toString(),Toast.LENGTH_SHORT).show();
                    }
                })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String>params = new HashMap<>();
                params.put("record_type", RECORD_TYPE);
                try {
                    params.put("recordReminderID", record.getString("recordReminderID"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }
    //--介面部分
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
//                mDrawerLayout.openDrawer(Gravity.RIGHT);
                finish();
                return true;
            case R.id.user_info:
                mDrawerLayout.openDrawer(Gravity.RIGHT);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.game_menu, menu);
        //set pattent name of menu
        MenuItem patientname = menu.findItem(R.id.patient_name);
        patientname.setTitle(localDB.getpatientname());
        return true;
    }
//    介面部分--
}
