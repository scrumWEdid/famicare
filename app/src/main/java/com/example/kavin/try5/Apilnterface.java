package com.example.kavin.try5;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface Apilnterface {
    @POST("select_patient.php")
    @FormUrlEncoded
    Call<List<ArrayListUsers>> savePost(@Field("key") String key);

    }

