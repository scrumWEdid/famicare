package com.example.kavin.try5;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.database.Cursor;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class addPatient_IDsearch {
    public Activity activity;
    private Context mCtx = null;
    private localDatabase localDB = null;
    private Cursor cursor = null;
    private String user_id;
    private EditText patientIDedt;
    private TextView patientNametxt, patientIDtxt;
    private Button searchBtn, submitBtn;
    private ImageView patientimage;
    private View straightline;

    public addPatient_IDsearch(Activity _activity, Context ctx, String userid){
        this.activity = _activity;
        this.mCtx = ctx;
        this.user_id = userid;
        patientIDedt = this.activity.findViewById(R.id.addPatient_edtxt);
        patientNametxt = this.activity.findViewById(R.id.addPatient_patientname);
        patientIDtxt = this.activity.findViewById(R.id.addPatient_patientID);
        patientimage = this.activity.findViewById(R.id.addPatient_image);
        searchBtn = this.activity.findViewById(R.id.addPatient_searchBtn);
        submitBtn = this.activity.findViewById(R.id.addPatient_submitBtn);
        straightline = this.activity.findViewById(R.id.straightline);
        straightline.setVisibility(View.INVISIBLE);
        searchBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                searchpatient(patientIDedt.getText().toString());
                /*hide keyboard*/
                InputMethodManager imm = (InputMethodManager)mCtx.getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        });
        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addpatient();
            }
        });
//        localDB = new localDatabase(mCtx);
//        localDB.open();
//        cursor =  localDB.getinfo();
//        if(cursor.getCount() >= 0){
//            user_id = String.valueOf(cursor.getInt(0));
//        }

    }

    private void searchpatient(final String patientid){
        //進mySQL抓資料
        String URL = "http://140.117.71.74/graduation/search_patient.php";
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            Log.d("書出訊息:", String.valueOf(jsonObject));
                            String success =jsonObject.getString("success");
                            if (success.equals("1")){
                                patientIDtxt.setText(jsonObject.getString("patientID"));
                                patientIDedt.setText("");
                                patientNametxt.setText(jsonObject.getString("name"));
                                patientNametxt.setVisibility(View.VISIBLE);
                                patientimage.setVisibility(View.VISIBLE);
                                submitBtn.setVisibility(View.VISIBLE);
                                straightline.setVisibility(View.VISIBLE);
                            }
                            else{
                                patientNametxt.setVisibility(View.INVISIBLE);
                                patientimage.setVisibility(View.INVISIBLE);
                                submitBtn.setVisibility(View.INVISIBLE);
                                straightline.setVisibility(View.INVISIBLE);
                                String nothisone = Resources.getSystem().getString(R.string.nothisone);
                                Toast.makeText(mCtx, nothisone, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.d("錯誤訊息",e.toString());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
//                        Toast.makeText(addPantient.this,"搜尋失敗"+error.toString(),Toast.LENGTH_SHORT).show();
                    }
                })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String>params = new HashMap<>();
                params.put("id", patientid);
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(mCtx);
        requestQueue.add(stringRequest);
    }

    private void addpatient(){
        //進SQL新增資料
        String URL = "http://140.117.71.74/graduation/add_patient.php";
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            Log.d("書出訊息:", String.valueOf(jsonObject));
                            String success =jsonObject.getString("success");
                            if (success.equals("1")){
                                String addsuccesswait=Resources.getSystem().getString(R.string.addsuccessandwait);
                                Toast.makeText(mCtx, addsuccesswait, Toast.LENGTH_SHORT).show();
                                patientNametxt.setVisibility(View.INVISIBLE);
                                patientimage.setVisibility(View.INVISIBLE);
                                submitBtn.setVisibility(View.INVISIBLE);
                            }
                            else{
                                String message = jsonObject.getString("message");
                                Toast.makeText(mCtx, message, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.d("錯誤訊息",e.toString());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
//                        Toast.makeText(addPantient.this,"搜尋失敗"+error.toString(),Toast.LENGTH_SHORT).show();
                    }
                })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String>params = new HashMap<>();
                params.put("patient_id", patientIDtxt.getText().toString());
                params.put("caretaker_id", user_id);
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(mCtx);
        requestQueue.add(stringRequest);
    }
}
