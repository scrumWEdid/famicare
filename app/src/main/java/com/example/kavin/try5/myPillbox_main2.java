package com.example.kavin.try5;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.kavin.try5.Volley.Volleyconnect;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static android.app.Activity.RESULT_OK;

public class myPillbox_main2 extends Fragment {

    private DrawerLayout mDrawerLayout;
    private localDatabase localDB = null;
    private Cursor cursor = null;
    private toolbar mytoolbar;
    private String patient_id, user_id;

    private RecyclerView recyclerView;
    private MyAdapter mAdapter;
    private ArrayList<JSONObject> myDataset = new ArrayList<>();

    private String[] filtermethod ={"全部","現在","過去"};
    private Spinner filterspinner;
    @Override
    public View onCreateView(LayoutInflater inflater , ViewGroup container , Bundle savedInstanceState ){
        View rootView = inflater.inflate(R.layout.my_pillbox_main,container,false);

        localDB = new localDatabase(getActivity());
        localDB.open();

        cursor = localDB.getinfo();
        if(cursor.getCount() >= 0){
            user_id = String.valueOf(cursor.getInt(0));
        }
        patient_id = localDB.getpatientinfo();


        //初始化recycler view
        recyclerView = rootView.findViewById(R.id.recyclerview);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);

        filterspinner = rootView.findViewById(R.id.filterspinner);
        filtermethod = getResources().getStringArray(R.array.period);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, filtermethod);
        adapter.setDropDownViewResource(R.layout.spinner_item);
        filterspinner.setAdapter(adapter);
        filterspinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if(i == 0 && myDataset.isEmpty()) //第一次載入時
                    fetch_medicine();
                else{
                    try {
                        filter(i);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        final TextView addBtn = rootView.findViewById(R.id.addimgBtn);
        addBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setClass(getActivity(), myPillbox_add.class);
                startActivityForResult(intent, 1);
            }
        });

//----讀取權限 設定是否要顯示的新增,編輯按鈕
        final Volleyconnect volleyconnect = new Volleyconnect(getActivity());
        volleyconnect.fetch_permission(localDB.getuserid(), localDB.getpatientinfo(),
                new Volleyconnect.VolleyCallback() {
                    @Override
                    public void onSuccess(String result) {
                        if(result.equals("查看者")) {
                            addBtn.setVisibility(View.INVISIBLE);
                        }
                    }
                });
//讀取權限----

        return rootView;
    }


    private void filter(int position) throws JSONException {
        ArrayList<JSONObject> filteredList = new ArrayList<>();

        if(position == 0) //全部
            filteredList = myDataset;
        else if(position == 1) { //現在啦~
            for (JSONObject item : myDataset) {
                if (Integer.valueOf(item.getString("stuck")) > 0) {
                    filteredList.add(item);
                }
            }
        }
        else{
            for (JSONObject item : myDataset) { //過去啦~
                if (Integer.valueOf(item.getString("stuck")) <= 0) {
                    filteredList.add(item);
                }
            }
        }

        mAdapter.filterList(filteredList);
    }

    //recyclerview有關的function
    public class MyAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
        private List<JSONObject> mData;
        //要求權現的viewholder
        public class ViewHolder1 extends RecyclerView.ViewHolder {
            public TextView name;
            public ConstraintLayout medicineBlock;
            public ViewHolder1(View v) {
                super(v);
                name = v.findViewById(R.id.name);
                medicineBlock = v.findViewById(R.id.medicineBlock);
            }

        }

        public MyAdapter(List<JSONObject> data) {
            mData = data;
        }
        @Override
        public int getItemViewType(int position) {
            return 0;
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v;
            v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.recyclerview_pillbox, parent, false);
            return new ViewHolder1(v);
        }

        @Override
        public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
            final ViewHolder1 viewholder1 = (ViewHolder1)holder;
            JSONObject record = mData.get(position);
            try {
                viewholder1.name.setText(record.getString("name"));
            } catch (JSONException e) {
                e.printStackTrace();
            }

            viewholder1.medicineBlock.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    JSONObject record = mData.get(position);
                    Intent intent = new Intent();
                    intent.setClass(getActivity(), myPillbox_view.class);
                    intent.putExtra("medicineInfo", record.toString());
                    startActivityForResult(intent, 1);
                }
            });
        }

        @Override
        public int getItemCount() {
            return mData.size();
        }

        public void filterList(ArrayList<JSONObject> filteredList) {
            mData = filteredList;
            notifyDataSetChanged();
        }
    }

    /*下一頁修改完畢回傳時刷新recyclerview*/
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == 1){
            if(resultCode == RESULT_OK){
                fetch_medicine();
            }
        }
    }

    //--------connect to MySQL----------
    private void fetch_medicine(){
        myDataset.clear();
        String URL = "http://140.117.71.74/graduation/fetch_medicine.php";
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
//                            Log.d("書出訊息:", String.valueOf(jsonObject));
                            String success =jsonObject.getString("success");
                            String message = jsonObject.getString("message");
                            if (success.equals("1")){
//                                Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                                JSONArray record = jsonObject.getJSONArray("record");
                                //放進datalist中
                                for(int i = 0; i < record.length(); i++) {
                                    JSONObject single_record = record.getJSONObject(i);
                                    myDataset.add(single_record);
                                }


                                //myDataset放道adapter中去建立recycler view
                                mAdapter = new MyAdapter(myDataset);
                                recyclerView.setAdapter(mAdapter);
                            }
                            else{
                                Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.d("錯誤訊息",e.toString());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getActivity(),getResources().getString(R.string.searchfail)+error.toString(),Toast.LENGTH_SHORT).show();
                    }
                })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String>params = new HashMap<>();
                params.put("patientid", patient_id);
                params.put("caretakerid", user_id);
                params.put("method", "main");
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(stringRequest);
    }


}
