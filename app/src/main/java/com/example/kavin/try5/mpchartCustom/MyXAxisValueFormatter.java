package com.example.kavin.try5.mpchartCustom;

import android.util.Log;

import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;

import java.util.ArrayList;

public class MyXAxisValueFormatter implements IAxisValueFormatter {
    private ArrayList<String> data = new ArrayList<String>();

    public MyXAxisValueFormatter(ArrayList<String> inputdata) {
        this.data = inputdata;
    }

    @Override
    public String getFormattedValue(float value, AxisBase axis) {
        // "value" represents the position of the label on the axis (x or y)
        if(((int)value) >= data.size() || ((int)value) < 0) //防止超去data.get(value)範圍而造成APP crash
            return "";
        else
            return data.get((int) value);
    }

    /** this is only needed if numbers are returned, else return 0 */
//    @Override
//    public int getDecimalDigits() { return 0; }
}
