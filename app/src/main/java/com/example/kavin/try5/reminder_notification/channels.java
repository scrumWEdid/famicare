package com.example.kavin.try5.reminder_notification;

import android.app.Application;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.os.Build;
import android.support.v4.app.NotificationCompat;

public class channels extends Application {
    public static final String CHANNEL_1_ID = "channel1";
    public static final String CHANNEL_2_ID = "channel2";

    @Override
    public void onCreate() {
        super.onCreate();

        createNotificationChannels();
    }

    private void createNotificationChannels() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel1 = new NotificationChannel(
                    CHANNEL_1_ID,
                    "用藥提醒",
                    NotificationManager.IMPORTANCE_HIGH
            );
            channel1.setDescription("This is Channel 1");
            channel1.setSound(null, null);
            channel1.setLockscreenVisibility(NotificationCompat.PRIORITY_HIGH);
            channel1.setVibrationPattern(new long[] {2000});
            channel1.enableVibration(true);
            NotificationChannel channel2 = new NotificationChannel(
                    CHANNEL_2_ID,
                    "測量提醒",
                    NotificationManager.IMPORTANCE_LOW
            );
            channel2.setDescription("This is Channel 2");
            channel2.setSound(null, null);
            channel2.setLockscreenVisibility(NotificationCompat.PRIORITY_HIGH);
            channel2.setVibrationPattern(new long[] {2000});
            channel2.enableVibration(true);

            NotificationManager manager = getSystemService(NotificationManager.class);
            manager.createNotificationChannel(channel1);
            manager.createNotificationChannel(channel2);
        }
    }

}
