package com.example.kavin.try5;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class recordPageOtherActivity2 extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.record_page_other, container, false);

        String title = getResources().getString(R.string.recordeverydayother);
        getActivity().setTitle(title);

        TextView bathroomTxt = rootView.findViewById(R.id.bathroom);
        bathroomTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setClass(getActivity(), bathroom_main.class);
                startActivity(intent);
            }
        });

        TextView painTxt = rootView.findViewById(R.id.pain);
        painTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setClass(getActivity(), pain_main.class);
                startActivity(intent);
            }
        });

        TextView otherTxt = rootView.findViewById(R.id.other_disease);
        otherTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setClass(getActivity(), other_main.class);
                startActivity(intent);
            }
        });

//        ImageView selectpreviewTxt =(ImageView) rootView.findViewById(R.id.historyview);
//        selectpreviewTxt.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent intent = new Intent();
//                intent.setClass(getActivity(), recordHistoryActivity.class);
//                startActivity(intent);
//            }
//        });



        return rootView;
    }

}
