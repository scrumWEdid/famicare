package com.example.kavin.try5;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import static android.app.Activity.RESULT_OK;

public class addPatient_create {
    public Activity activity;
    private Context mCtx = null;
    private localDatabase localDB = null;
    private Cursor cursor = null;
    private EditText name, birthday;
    private TextView displayDatePicker;
    private RadioButton boy, girl;
    private Button submitBtn;
    private String user_acc, user_id;
    private DatePickerDialog.OnDateSetListener mDateSetListener;


    public addPatient_create(Activity _activity, Context ctx, String userid, String useracc){
        this.activity = _activity;
        this.mCtx = ctx;
        this.user_id = userid;
        this.user_acc = useracc;
        name = this.activity.findViewById(R.id.addPatient2_name);
        birthday = this.activity.findViewById(R.id.addPatient2_birthday);
        /*點生日欄位叫出datepicker*/
        birthday.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int year, month, day;
                if(birthday.getText().toString().equals("")) {
                    Calendar cal = Calendar.getInstance();
                    year = cal.get(Calendar.YEAR);
                    month = cal.get(Calendar.MONTH);
                    day = cal.get(Calendar.DAY_OF_MONTH);
                }
                else{
                    String[] date = birthday.getText().toString().split("/");
                    year = Integer.valueOf(date[0]);
                    month = Integer.valueOf(date[1])-1;
                    day = Integer.valueOf(date[2]);
                }
                DatePickerDialog dialog = new DatePickerDialog(
                        mCtx,
                        android.R.style.Theme_Holo_Light_Dialog_MinWidth,
                        mDateSetListener,
                        year, month, day);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();
            }
        });
        mDateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                month = month + 1;
                String date = "" + year;
                if(month < 10)
                    date = date + "/0" + month;
                else
                    date = date + "/" + month;
                date = date + "/" + day;
                birthday.setText(date);
            }
        };

        boy = this.activity.findViewById(R.id.addPatient2_boy);
        girl = this.activity.findViewById(R.id.addPatient2_girl);
        submitBtn = this.activity.findViewById(R.id.addPatient2_submitBtn);
        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                create_patient();
            }
        });
//        localDB = new localDatabase(mCtx);
//        localDB.open();
//        cursor =  localDB.getinfo();
//        if(cursor.getCount() >= 0){
//            user_id = String.valueOf(cursor.getInt(0));
//            user_acc = cursor.getString(1);
//        }
    }
    private void create_patient(){

        final String gender;
        if(boy.isChecked())
            gender="男";
        else
            gender="女";
        String URL = "http://140.117.71.74/graduation/create_patient.php";
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            Log.d("書出訊息:", String.valueOf(jsonObject));
                            String success =jsonObject.getString("success");
                            if (success.equals("1")){
                                String patient_id = jsonObject.getString("patientid");
                                String addSuccess = mCtx.getString(R.string.addsuccess);
                                Toast.makeText(mCtx, addSuccess, Toast.LENGTH_SHORT).show();
                                activity.setResult(RESULT_OK);
                                activity.finish(); //跳回選擇被照顧者畫面
                            }
                            else{
                                String addfail = mCtx.getString(R.string.addfail);
                                Toast.makeText(mCtx, addfail, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.d("錯誤訊息",e.toString());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        String addfail = Resources.getSystem().getString(R.string.addfail);
                        Toast.makeText(mCtx,addfail+error.toString(),Toast.LENGTH_SHORT).show();
                    }
                })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String>params = new HashMap<>();
                params.put("id", user_id);
                params.put("account",user_acc);
                params.put("name", name.getText().toString());
                params.put("birthday", birthday.getText().toString());
                params.put("gender", gender);
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(mCtx);
        requestQueue.add(stringRequest);
    }
}
