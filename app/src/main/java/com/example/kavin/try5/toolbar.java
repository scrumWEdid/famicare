package com.example.kavin.try5;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.kavin.try5.Volley.Volleyconnect;

public class toolbar extends Activity{
    private Context context;
    private Activity activity;
    private DrawerLayout mDrawerLayout;
    private localDatabase localDB = null;
    private ActionBar actionbar;
    public toolbar(Context ctx){
        this.context = ctx;
        this.activity = (Activity) context;
        //----open localDB
        localDB = new localDatabase(context);
        localDB.open();
//----介面部分
        Toolbar toolbar = activity.findViewById(R.id.toolbar);
        ((AppCompatActivity)activity).setSupportActionBar(toolbar);

        actionbar = ((AppCompatActivity)activity).getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp);

        mDrawerLayout = activity.findViewById(R.id.drawer_layout);

        final NavigationView navigationView = activity.findViewById(R.id.nav_view);

        //drawer nav header on click的部分
        View headerview = navigationView.getHeaderView(0);

        TextView header_PatientID = headerview.findViewById(R.id.nav_headerPatientIDTextView);
        header_PatientID.setText("ID : " + localDB.getpatientinfo());
        TextView header_PatientName = headerview.findViewById(R.id.nav_headerPatientNameTextView);
        header_PatientName.setText(localDB.getpatientname());

        LinearLayout header = (LinearLayout) headerview.findViewById(R.id.nav_view_layout);
        header.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent patientInfoIntent = new Intent(context,patientInfoActivity.class);
                activity.startActivity(patientInfoIntent);
                mDrawerLayout.closeDrawers();
            }
        });
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        // set item as selected to persist highlight
                        menuItem.setChecked(true);
                        // close drawer when item is tapped after 0.5second
                        new android.os.Handler().postDelayed(
                                new Runnable() {
                                    public void run() {
                                        mDrawerLayout.closeDrawers();
                                    }
                                },
                                500);
                        // Add code here to update the UI based on the item selected
                        // For example, swap UI fragments here

                        switch(menuItem.getItemId()){
                            case R.id.nav_switch_patient:
                                Intent switchIntent = new Intent(context,ChoosePatientActivity.class);
                                switchIntent.putExtra("fromWhere", "toolbar");
                                activity.startActivityForResult(switchIntent, 1);
                                return true;
                            case R.id.nav_manage_patient:
                                Intent managePatientIntent = new Intent(context,CareTakerManagePermissionActivity.class);
                                activity.startActivity(managePatientIntent);
                                return true;
                            case R.id.nav_member_info:
                                Intent MemberInfoIntent = new Intent(context,MemberInfoActivity.class);
                                activity.startActivity(MemberInfoIntent);
                                return true;
                            case R.id.nav_setting:
                                Intent settingIntent = new Intent(context,setting.class);
                                activity.startActivity(settingIntent);
                                return true;
                            case R.id.nav_logout:
                                context.deleteDatabase("local.db");
                                Intent intent = new Intent();
                                intent.setClass(context, LoginActivity.class);
                                activity.startActivity(intent);
                                activity.finishAffinity();
                                return true;
                        }

                        return true;
                    }
                });

        //----讀取權限 設定是否要顯示被照顧者權限管理(只有擁有者會顯示)
        final Volleyconnect volleyconnect = new Volleyconnect(activity);
        volleyconnect.fetch_permission(localDB.getuserid(), localDB.getpatientinfo(),
                new Volleyconnect.VolleyCallback() {
                    @Override
                    public void onSuccess(String result) {
                        Menu menuNav = navigationView.getMenu();
                        MenuItem nav_manage_patient = menuNav.findItem(R.id.nav_manage_patient);
                        if(result.equals("擁有者")) {
                            nav_manage_patient.setVisible(true);
                        }
                        else{
                            nav_manage_patient.setVisible(false);
                        }
                    }
                });
    }

    public void setbackbtn(boolean flag){ //設置是否顯示返回
        actionbar.setDisplayHomeAsUpEnabled(flag);
    }

}
