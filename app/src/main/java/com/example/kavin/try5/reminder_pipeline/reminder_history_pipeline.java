package com.example.kavin.try5.reminder_pipeline;

import android.database.Cursor;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.kavin.try5.BottomNavigation;
import com.example.kavin.try5.R;
import com.example.kavin.try5.localDatabase;
import com.example.kavin.try5.toolbar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import jp.wasabeef.recyclerview.adapters.AlphaInAnimationAdapter;

public class reminder_history_pipeline extends AppCompatActivity {

    private DrawerLayout mDrawerLayout;
    private localDatabase localDB = null;
    private toolbar mytoolbar;
    private Cursor cursor = null;
    private String patient_id;

    private TextView filterdateTv;
    private Spinner filterMedSpn;
    private boolean flag = false;

    private RecyclerView mRecyclerView;
    private MyAdapter mAdapter;
    private ArrayList<JSONObject> myDataset = new ArrayList<>(); //歷史用藥項目
    private ArrayList<String> myDataset2 = new ArrayList<>(); //藥品項目

    private String[] typeitem_TWN = {"管路種類", "尿管", "B管", "C管"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reminder_history_pipeline);
        //----open localDB
        localDB = new localDatabase(this);
        localDB.open();
        BottomNavigationView bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottom_navigation_view);
        bottomNavigationView.setSelectedItemId(R.id.alarm);
        bottomNavigationView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

//----介面部分
        mytoolbar = new toolbar(this);
        mDrawerLayout = findViewById(R.id.drawer_layout);
// 介面部分-----
        patient_id = localDB.getpatientinfo();
        filterMedSpn = findViewById(R.id.filterMedSpn);
        filterdateTv = findViewById(R.id.filterdateTv);

        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerview);
        final LinearLayoutManager layoutManager = new LinearLayoutManager(reminder_history_pipeline.this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mRecyclerView.setLayoutManager(layoutManager);

        filterMedSpn.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if(flag) {
                    String medName = myDataset2.get(i);
                    filter(medName);
                }
                flag = true;
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        fetch_medicine();
    }

    //--------connect to MySQL----------
    private void fetch_medicine(){
        myDataset.clear();
        myDataset2.clear();
        myDataset2.add(getString(R.string.allpipeline));
        String URL = "http://140.117.71.74/graduation/fetch_donereminder.php";
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            String success =jsonObject.getString("success");
                            String message = jsonObject.getString("message");
                            if (success.equals("1")){
//                                Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                                JSONArray record = jsonObject.getJSONArray("record");

                                //放進datalist中
                                String devider_month = "";
                                for(int i = 0; i < record.length(); i++) {
                                    JSONObject single_record = record.getJSONObject(i);

                                    //建立每月分隔
                                    if(!devider_month.equals(single_record.getString("donedate").substring(0, 7))) {
                                        devider_month = single_record.getString("donedate").substring(0, 7);
                                        JSONObject month_title = new JSONObject();
                                        month_title.put("month", devider_month);
                                        myDataset.add(month_title);
                                    }
                                    //建立藥品項目
                                    int index = Arrays.asList(typeitem_TWN).indexOf(single_record.getString("type"));
                                    String s = getResources().getStringArray(R.array.pipetypeitem)[index];
                                    single_record.put("type", s);
                                    if(!myDataset2.contains(single_record.getString("type"))) {
                                        myDataset2.add(single_record.getString("type"));
                                    }

                                    myDataset.add(single_record);
                                }


                                //myDataset放道adapter中去建立recycler view
                                mAdapter = new MyAdapter(myDataset);
                                mRecyclerView.setAdapter(new AlphaInAnimationAdapter(mAdapter));

                                //建立spinner
                                String[] medicineArray = new String[myDataset2.size()];
                                medicineArray = myDataset2.toArray(medicineArray);
                                ArrayAdapter<String> adapter = new ArrayAdapter<String>(reminder_history_pipeline.this, android.R.layout.simple_spinner_dropdown_item, medicineArray);
                                filterMedSpn.setAdapter(adapter);

                            }
                            else{
                                Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.d("錯誤訊息",e.toString());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(),getString(R.string.searchfail)+error.toString(),Toast.LENGTH_SHORT).show();
                    }
                })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String>params = new HashMap<>();
                params.put("patientID", patient_id);
                params.put("record_type", "pipeline");
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }


    //----------------------------以下皆為recyclerview有關的function
    public class MyAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
        private List<JSONObject> mData;
        //XXXX年X月
        public class ViewHolder1 extends RecyclerView.ViewHolder {
            public TextView row1;
            public ViewHolder1(View v) {
                super(v);
                row1 = v.findViewById(R.id.row1);
            }

        }
        //02周四 8:07 XX藥
        public class ViewHolder2 extends RecyclerView.ViewHolder {
            public TextView row1, row2;
            public ViewHolder2(View v) {
                super(v);
                row1 = v.findViewById(R.id.row1);
                row2 = v.findViewById(R.id.row2);
            }
        }

        public MyAdapter(List<JSONObject> data) {
            mData = data;
        }
        @Override
        public int getItemViewType(int position) {
            if(mData.get(position).length() == 1) //歷史用藥 - 日期分隔row
                return 0;
            else  //歷史用藥 - 每日的row
                return 1;
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v;
            if(viewType == 0 || viewType == 2) {
                v = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.recyclerview_histroy_layout1, parent, false);
                return new ViewHolder1(v);
            }
            else{
                v = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.recyclerview_histroy_layout2, parent, false);
                return new ViewHolder2(v);
            }
        }

        @Override
        public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
            if(holder.getItemViewType() == 0){ //歷史用藥 - 日期分隔row
                ViewHolder1 viewholder1 = (ViewHolder1)holder;
                JSONObject data = mData.get(position);
                try {
                    viewholder1.row1.setText(data.getString("month").replace("-", "年") + "月");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            else if(holder.getItemViewType() == 1){ //歷史用藥 - 每日的row
                ViewHolder2 viewholder2 = (ViewHolder2)holder;
                if(position % 2 == 1) {
                    viewholder2.row1.setBackgroundColor(0xFFE1E1E1);
                    viewholder2.row2.setBackgroundColor(0xFFE1E1E1);
                }

                JSONObject data = mData.get(position);
                try {
                    if(mData.get(position-1).length() == 1) { //上一個是每月分隔
                        String nowdate = mData.get(position).getString("donedate");
                        String weekofday = "";
                        Date date = new SimpleDateFormat("yyyy-MM-dd").parse(nowdate);
                        weekofday = new SimpleDateFormat("EE").format(date);
                        viewholder2.row1.setText(data.getString("donedate").substring(8) + "\n" + weekofday);
                    }
                    else if(position > 1) {
                        String prevdate = mData.get(position - 1).getString("donedate");
                        String nowdate = mData.get(position).getString("donedate");

                        String weekofday = "";
                        Date date = new SimpleDateFormat("yyyy-MM-dd").parse(nowdate);
                        weekofday = new SimpleDateFormat("EE").format(date);

                        if(!prevdate.equals(nowdate))
                            viewholder2.row1.setText(data.getString("donedate").substring(8) + "\n" + weekofday);
                        else
                            viewholder2.row1.setText("");
                    }
                    viewholder2.row2.setText(data.getString("donetime").substring(0, 5) + " " + data.getString("type"));
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public int getItemCount() {
            return mData.size();
        }

        public void changelayout(ArrayList<JSONObject> changedList){
            mData = changedList;
            notifyDataSetChanged();
        }
    }

    private void filter(String text){
        ArrayList<JSONObject> filteredList = new ArrayList<>();
        JSONObject month = null;

        if(text.equals(getString(R.string.allpipeline))) //所有
            filteredList = myDataset;
        else {
            for (JSONObject item : myDataset) {
                try {
                    if (item.length() == 1)
                        month = item;
                    else if (item.getString("type").contains(text)) {
                        if (month != null) { //如果還沒有每月分隔的話
                            filteredList.add(month);
                            month = null;
                        }
                        filteredList.add(item);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }
        mAdapter.changelayout(filteredList);
    }

    //--介面部分
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
//                mDrawerLayout.openDrawer(Gravity.RIGHT);
                finish();
                return true;
            case R.id.user_info:
                mDrawerLayout.openDrawer(Gravity.RIGHT);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.game_menu, menu);
        //set pattent name of menu
        MenuItem patientname = menu.findItem(R.id.patient_name);
        patientname.setTitle(localDB.getpatientname());
        return true;
    }
//    介面部分--

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected( MenuItem item) {
            BottomNavigation bn = new BottomNavigation(reminder_history_pipeline.this, item.getItemId());
            return true;
        }

    };
}
