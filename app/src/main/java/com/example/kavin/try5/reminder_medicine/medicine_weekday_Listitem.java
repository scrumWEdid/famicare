package com.example.kavin.try5.reminder_medicine;

import org.json.JSONObject;

public class medicine_weekday_Listitem {

    private String reminder_medicine_time;
    private String reminder_medicine_name;
    private JSONObject reminder_medicine_info;
    public medicine_weekday_Listitem(String reminder_medicine_time, String reminder_medicine_name, JSONObject reminder_medicine_info) {
        this.reminder_medicine_time = reminder_medicine_time;
        this.reminder_medicine_name = reminder_medicine_name;
        this.reminder_medicine_info = reminder_medicine_info;
    }

    public String getReminder_medicine_time() {
        return reminder_medicine_time;
    }

    public String getReminder_medicine_name() {
        return reminder_medicine_name;
    }

    public JSONObject getReminder_medicine_info() {
        return reminder_medicine_info;
    }

}