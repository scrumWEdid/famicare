package com.example.kavin.try5;


import android.view.View;
import android.widget.TextView;

import com.thoughtbot.expandablerecyclerview.viewholders.ChildViewHolder;

public class TypeViewHolder extends ChildViewHolder {

    private TextView item_date;
    private TextView item_time;
    private TextView item_data1;
    private TextView item_data2;
    private TextView item_data3;



    public TypeViewHolder(View itemView) {

        super(itemView);
        item_date = (TextView)itemView.findViewById(R.id.item_date);
        item_time = (TextView)itemView.findViewById(R.id.item_time);
        item_data1 = (TextView)itemView.findViewById(R.id.item_data1);
        item_data2 = (TextView)itemView.findViewById(R.id.item_data2);
        item_data3 = (TextView)itemView.findViewById(R.id.item_data3);

    }

    public void setItem_data1(String data1) {
        item_data1.setText(data1);
    }

    public void setItem_data2(String data2) {
        item_data2.setText(data2);
    }

    public void setItem_data3(String data3) {
        item_data3.setText(data3);
    }

    public void setItem_date(String date) {
        item_date.setText(date);
    }

    public void setItem_time(String time) {
        item_time.setText(time);
    }

}