package com.example.kavin.try5.reminder_pipeline;
import com.example.kavin.try5.BottomNavigation;
import com.example.kavin.try5.R;
import com.example.kavin.try5.Volley.Volleyconnect;
import com.example.kavin.try5.WeekDaysCalender.*;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.kavin.try5.fetch_remindertime.MyReminder;
import com.example.kavin.try5.fetch_remindertime.MyReminder2;
import com.example.kavin.try5.localDatabase;
import com.example.kavin.try5.toolbar;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

public class reminder_pipeline_weekday extends AppCompatActivity implements CLCalendarItemView.OnItemSelectListener, pipeline_weekday_RecyclerItemTouchHelper.RecyclerItemTouchHelperListener{
    private DrawerLayout mDrawerLayout;
    private localDatabase localDB = null;
    private toolbar mytoolbar;
    private Cursor cursor = null;
    private String patient_id;

    private static String  URL_REMAIN = "http://140.117.71.74/graduation/fetch_remain.php";
    private TextView current,select_today,colume_time,colume_information;
    private Button viewBtn,doneBtn,cancelBtn;
    private RecyclerView recyclerView;
    //    private RecyclerView.Adapter adapter;
    private pipeline_weekday_adapter mAdapter;
    private List<pipeline_weekday_Listitem> listItems = new ArrayList<>();
    private CoordinatorLayout coordinatorLayout;
    private CLCalendarView clCalendarView;
    private ImageView add,edit,view;
    private MyReminder2 myreminder;
    private ArrayList<JSONObject> myDataset = new ArrayList<>();
    private boolean flag = false; //判斷提醒資料讀取完成沒
    private String select_date = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pipeline_reminder_weekday);

        //----open localDB
        localDB = new localDatabase(this);
        localDB.open();
        BottomNavigationView bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottom_navigation_view);
        bottomNavigationView.setSelectedItemId(R.id.alarm);
        bottomNavigationView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

//----介面部分
        mytoolbar = new toolbar(this);
        mDrawerLayout = findViewById(R.id.drawer_layout);
// 介面部分-----
//----讀取權限 設定是否要顯示的新增,編輯按鈕
        final Volleyconnect volleyconnect = new Volleyconnect(this);
        volleyconnect.fetch_permission(localDB.getuserid(), localDB.getpatientinfo(),
                new Volleyconnect.VolleyCallback() {
                    @Override
                    public void onSuccess(String result) {
                        if(result.equals("查看者")) {
                            volleyconnect.hideImageViewicon("addImg");
                            volleyconnect.hideImageViewicon("editImg");
                        }
                    }
                });
//讀取權限----
        coordinatorLayout = findViewById(R.id.coordinator_layout);
//        getMainlinearLayout();

        add = (ImageView)findViewById(R.id.addImg);
        edit = (ImageView)findViewById(R.id.editImg);
        view = (ImageView)findViewById(R.id.viewImg);
        current = (TextView)findViewById(R.id.current);
        clCalendarView =(CLCalendarView)findViewById(R.id.clCalendarView);
        recyclerView = (RecyclerView)findViewById(R.id.weekdayrecycleriew);
        select_today  = (TextView)findViewById(R.id.select_todday);
        viewBtn = (Button)findViewById(R.id.viewBtn);
        doneBtn =(Button)findViewById(R.id.doneBtn);
        cancelBtn=(Button)findViewById(R.id.cancelBtn);
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(reminder_pipeline_weekday.this, reminder_add_pipeline.class);
                startActivityForResult(intent, 1);
            }
        });
        edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(reminder_pipeline_weekday.this, reminder_mainedit_pipeline.class);
                startActivityForResult(intent, 1);
            }
        });
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(reminder_pipeline_weekday.this, reminder_history_pipeline.class);
                startActivity(intent);
            }
        });

        viewBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (pipeline_weekday_common.position!=-1){
                    Intent intent = new Intent();
                    intent.setClass(reminder_pipeline_weekday.this, reminder_view_pipeline.class);
                    Bundle bundle = new Bundle();
//                    bundle.putString("reminder"
                    bundle.putString("reminder", pipeline_weekday_common.currentItem.getReminder_pipeline_info().toString());
                    intent.putExtras(bundle);
                    startActivityForResult(intent, 1);
                    ShowDone(false);
                    ShowView(false);
                    pipeline_weekday_common.position=-1;
                    mAdapter.row_index=-1;
                    mAdapter.notifyDataSetChanged();
                }

            }
        });
        doneBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                To:伯倫:
//                myreminder.done_reminder(mAdapter.checkedid[x], donedate, mAdapter.checkedtime[x]);
//                P.S. 測量跟管路提醒的第一個值改抓recordReminderID 不是name!!!
                if(pipeline_weekday_common.position!=-1&&mAdapter.flag==false) {
                    pipeline_weekday_Listitem selectitem = pipeline_weekday_common.currentItem;
                    try {
                        myreminder.done_reminder(selectitem.getReminder_pipeline_id(),
                                select_date, selectitem.getReminder_pipeline_time());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    mAdapter.removeItem(pipeline_weekday_common.position);
                    pipeline_weekday_common.position=-1;
                    mAdapter.row_index=-1;
                    ShowView(false);
                    ShowDone(false);
                    ShowCancel(false);
                    mAdapter.notifyDataSetChanged();
                }
                if (mAdapter.flag==true){
                    ShowDone(false);
                    ShowCancel(false);
                    for (int x = mAdapter.checked.length-1;x>=0;x--){

                        if (mAdapter.checked[x]!=null){
                            // Log.d("volly",mAdapter.checkedname[x]);
//                           Log.d("volly",select_date);
//                           Log.d("volly",mAdapter.checkedtime[x]);
                            myreminder.done_reminder(mAdapter.checkedid[x],
                                    select_date,
                                    mAdapter.checkedtime[x]);
                            mAdapter.removeItem(x);
                            mAdapter.checked[x]=null;
                            mAdapter.notifyDataSetChanged();

                        }
                    }
                    myreminder.fetchdonedata();
                    Toast.makeText(reminder_pipeline_weekday.this, getString(R.string.havefinishmedicine), Toast.LENGTH_SHORT).show();


                }

            }
        });
        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mAdapter.flag=false;
                pipeline_weekday_common.position=-1;
                mAdapter.row_index=-1;
                mAdapter.notifyDataSetChanged();
                ShowCancel(false);
                ShowDone(false);
                ShowView(false);
            }
        });

        clCalendarView.setOnItemSelectListener(this);



        select_today.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Date today = new Date();
//                onSelectDate(null, new Date());
//                final Calendar calendar = Calendar.getInstance();
//                calendar.setTime(today);
//
//                if(calendar.get(Calendar.DAY_OF_WEEK)==1){
//                    clCalendarView.setSelectDate( CLCalendarWeekUtil.getWeekUtilCalender(today,CLCalendarWeekUtil.getWeek(today)));
//                    clCalendarView.setCurrentCalender( CLCalendarWeekUtil.getWeekUtilCalender(today,CLCalendarWeekUtil.getWeek(today)));
//                }
//                else{
//                    Log.d("asd", String.valueOf(calendar.get(Calendar.DAY_OF_WEEK)));
//                    calendar.setTime(today);
//                    for (int x = calendar.get(Calendar.DAY_OF_WEEK);x>1;x--){
//                        calendar.add(Calendar.DATE,-1);
//                        Log.d("asd", String.valueOf(calendar));
//                    }
//                    Log.d("date", String.valueOf(calendar));
//                    Date setweeksun ;
//                    setweeksun =today;
//                    setweeksun = calendar.getTime();
//                    Log.d("date", String.valueOf(setweeksun));
//
//                    clCalendarView.setCurrentCalender( CLCalendarWeekUtil.getWeekUtilCalender(setweeksun,CLCalendarWeekUtil.getWeek(setweeksun)));
//                    clCalendarView.setSelectDate( CLCalendarWeekUtil.getWeekUtilCalender(today,CLCalendarWeekUtil.getWeek(today)));

                Intent refresh = new Intent(getApplicationContext(), reminder_pipeline_weekday.class);
                startActivity(refresh);//Start the same Activity
                finish(); //finish Activity.
            }
        });
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));

        ItemTouchHelper.SimpleCallback itemTouchHelperCallback = new pipeline_weekday_RecyclerItemTouchHelper(0, ItemTouchHelper.LEFT, this);
        new ItemTouchHelper(itemTouchHelperCallback).attachToRecyclerView(recyclerView);


        onSelectDate(null, new Date());
        Calendar calendar = Calendar.getInstance();
        String day= String.valueOf(calendar.get(Calendar.DATE));
        String month = String.valueOf(calendar.get(Calendar.MONTH)+1);
        String year = String.valueOf(calendar.get(Calendar.YEAR));
        if( day.length()==1){
            String addzero = "0";
            day = addzero + day ;
        }
        if (month.length()==1){
            String addzero = "0";
            month= addzero +month;
        }
        select_date = year+"-"+month+"-"+day;

        //讀取提醒資料
        myreminder = new MyReminder2(this, "pipeline");
        myreminder.fetchdonedata();
        myreminder.fetch_data(new MyReminder2.VolleyCallback() {
            @Override
            public void onSuccess(String result) {
                if(result.equals("1")){
                    flag = true;
                    loadRecyclerViewData(select_date);
                }
            }
        });


    }
    private void loadRecyclerViewData(final String date){
        listItems = new ArrayList<>();
        if(flag) {
            try {
                myDataset = myreminder.dayreminder2(date);
                for (JSONObject item : myDataset) {
                    String name = item.getString("type");
                    String remindtime = item.getString("enddatetime").split(" ")[1].substring(0, 5);
                    listItems.add(new pipeline_weekday_Listitem(remindtime, name, item));
                }
                //照時間排序
                Collections.sort(listItems, new Comparator<pipeline_weekday_Listitem>() {
                    @Override
                    public int compare(pipeline_weekday_Listitem item1, pipeline_weekday_Listitem item2) {
                        return item1.getReminder_pipeline_time().compareTo(item2.getReminder_pipeline_time());
                    }
                });
                //更新recyclerview
                mAdapter = new pipeline_weekday_adapter(listItems, reminder_pipeline_weekday.this,this);
                recyclerView.setAdapter(mAdapter);

            } catch (ParseException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
    public void ShowView(Boolean showView){
        if(showView==false){
            viewBtn.setVisibility(View.INVISIBLE);
        }
        if(showView==true){
            viewBtn.setVisibility(View.VISIBLE);
        }
    }
    public void ShowCancel(Boolean ShowCancel){
        if(ShowCancel==false){
            cancelBtn.setVisibility(View.INVISIBLE);
        }
        if(ShowCancel==true){
            cancelBtn.setVisibility(View.VISIBLE);

        }
    }
    public void ShowDone(boolean showDone){
        if(showDone==false){
            doneBtn.setVisibility(View.INVISIBLE);
        }
        if(showDone==true){
            doneBtn.setVisibility(View.VISIBLE);

        }
    }


    /*下一頁修改完畢回傳更新資料*/
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == 1){
            if(resultCode == RESULT_OK){
                //讀取提醒資料
                myreminder.fetch_data(new MyReminder2.VolleyCallback() {
                    @Override
                    public void onSuccess(String result) {
                        if(result.equals("1")){
                            flag = true;
                            loadRecyclerViewData(select_date);
                        }
                    }
                });
            }
        }
    }

    @Override
    public void onSelectDate(CLCalendarWeekDTO calenderBean, Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        ShowDone(false);
        ShowView(false);
        ShowCancel(false);
        String day= String.valueOf(calendar.get(Calendar.DATE));
        String month = String.valueOf(calendar.get(Calendar.MONTH)+1);
        String year = String.valueOf(calendar.get(Calendar.YEAR));
        if( day.length()==1){
            String addzero = "0";
            day = addzero + day ;
        }
        if (month.length()==1){
            String addzero = "0";
            month= addzero +month;
        }
        select_date = year+"-"+month+"-"+day;
        Log.d("asd",select_date);
        loadRecyclerViewData(select_date);
        current.setText( calendar.get(Calendar.YEAR) + "/" +(calendar.get(Calendar.MONTH) + 1)+ "/" +calendar.get(Calendar.DATE));
    }

    @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction, int position) {
        if (viewHolder instanceof pipeline_weekday_adapter.ViewHolder) {
            // get the removed item name to display it in snack bar
            String name = listItems.get(viewHolder.getAdapterPosition()).getReminder_pipeline_name();

            // backup of removed item for undo purpose
            final pipeline_weekday_Listitem deletedItem = listItems.get(viewHolder.getAdapterPosition());
            final int deletedIndex = viewHolder.getAdapterPosition();

            // remove the item from recycler view
            mAdapter.removeItem(viewHolder.getAdapterPosition());

            // showing snack bar with Undo option
            Snackbar snackbar = Snackbar
                    .make(coordinatorLayout, name + getString(R.string.finishuse), Snackbar.LENGTH_LONG);
            snackbar.setAction(getString(R.string.recovery), new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    // undo is selected, restore the deleted item
                    mAdapter.restoreItem(deletedItem, deletedIndex);
                }
            });
            snackbar.setActionTextColor(Color.YELLOW);
            snackbar.show();
        }
    }

    //--介面部分
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
//                mDrawerLayout.openDrawer(Gravity.RIGHT);
                finish();
                return true;
            case R.id.user_info:
                mDrawerLayout.openDrawer(Gravity.RIGHT);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.game_menu, menu);
        //set pattent name of menu
        MenuItem patientname = menu.findItem(R.id.patient_name);
        patientname.setTitle(localDB.getpatientname());
        return true;
    }
//    介面部分--
private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
        = new BottomNavigationView.OnNavigationItemSelectedListener() {

    @Override
    public boolean onNavigationItemSelected( MenuItem item) {
        BottomNavigation bn = new BottomNavigation(reminder_pipeline_weekday.this, item.getItemId());
        return true;
    }

};


}

