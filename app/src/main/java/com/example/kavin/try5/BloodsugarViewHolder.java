package com.example.kavin.try5;

import android.view.View;
import android.widget.TextView;

import com.thoughtbot.expandablerecyclerview.viewholders.ChildViewHolder;

public class BloodsugarViewHolder extends ChildViewHolder {
    private TextView b_item_date;
    private TextView b_item_time;
    private TextView b_item_bloodsugar;
    private TextView b_item_timing;

    public BloodsugarViewHolder(View itemView) {

        super(itemView);
        b_item_date = (TextView)itemView.findViewById(R.id.b_item_date);
        b_item_time = (TextView)itemView.findViewById(R.id.b_item_time);
        b_item_bloodsugar = (TextView)itemView.findViewById(R.id.b_item_bloodsugar);
        b_item_timing = (TextView)itemView.findViewById(R.id.b_item_timing);

    }

    public void setB_item_date(String date) {
        b_item_date.setText(date);
    }

    public void setB_item_time(String time) {
        b_item_date.setText(time);
    }

    public void setB_item_bloodsugar(String bloodsugar) {
        b_item_bloodsugar.setText(bloodsugar);
    }
    public void setB_item_timing(String timing) {
        b_item_timing.setText(timing);
    }
}



