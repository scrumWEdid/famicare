package com.example.kavin.try5;

import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.kavin.try5.fetch_remindertime.MyReminder;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class myPillbox_view extends AppCompatActivity {

    private DrawerLayout mDrawerLayout;
    private localDatabase localDB = null;
    private Cursor cursor = null;
    private toolbar mytoolbar;

    private JSONObject medicineInfo;

    private ImageView medicineimg;
    private TextView name, stuck;
    private TextView pretaketime, futuretaketime;
    private TextView startdate; //開始日期
    private TextView method, taketiming, takeamount, taketime; //用法那行
    private TextView effect, sideeffect, prescription;
    private TextView[] timetv = new TextView[4];

    private MyReminder myreminder;

    private String[] methoditem_TWN = {"用法", "口服", "注射", "外用", "其他"};
    private String[] taketimingitem_TWN = { "飯前","飯後","不限"};
    private String[] takeunititem_TWN = {"片", "粒", "cc", "mg", "個", "滴", "劑", "單位"};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_pillbox_view);

        localDB = new localDatabase(this);
        localDB.open();
        BottomNavigationView bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottom_navigation_view);
        bottomNavigationView.setSelectedItemId(R.id.drug);
        bottomNavigationView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        myreminder = new MyReminder(this, "medicine");

        //--介面部分
        //setup toolbar
        mytoolbar = new toolbar(this);
        mDrawerLayout = findViewById(R.id.drawer_layout);
        //介面部分--

        medicineimg = findViewById(R.id.medicineimg);
        name = findViewById(R.id.name);
        stuck = findViewById(R.id.stuck);
        pretaketime = findViewById(R.id.prevtaketime);
        futuretaketime = findViewById(R.id.futuretaketime);
        startdate = findViewById(R.id.startdate);
        method = findViewById(R.id.method);
        taketiming = findViewById(R.id.taketiming);
        takeamount = findViewById(R.id.takeamount);
        taketime = findViewById(R.id.taketime);
        effect = findViewById(R.id.effect);
        sideeffect = findViewById(R.id.sideeffect);
        prescription = findViewById(R.id.prescription);

        timetv[0] = findViewById(R.id.timeTv1);
        timetv[1] = findViewById(R.id.timeTv2);
        timetv[2] = findViewById(R.id.timeTv3);
        timetv[3] = findViewById(R.id.timeTv4);

        try {
            JSONObject inputmedicine = new JSONObject(getIntent().getStringExtra("medicineInfo"));
            String medicineid = inputmedicine.getString("personalMedicineID");
            myreminder.fetch_data("view_single", medicineid, new MyReminder.VolleyCallback() {
                @Override
                public void onSuccess(String result) {

                }
            });
            fetch_data(medicineid); //讀取資料

        } catch (JSONException e) {
            e.printStackTrace();
        }


        TextView recordBtn = findViewById(R.id.recordBtn); //另一個樣子
        recordBtn.setVisibility(View.INVISIBLE);
        recordBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });


        TextView editBtn = findViewById(R.id.editimgBtn);
        editBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setClass(myPillbox_view.this, myPillbox_edit.class);
                intent.putExtra("medicineInfo", medicineInfo.toString());
                startActivityForResult(intent, 1);
            }
        });

        TextView delBtn = findViewById(R.id.delBtn);
        delBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new AlertDialog.Builder(myPillbox_view.this)
                        .setTitle(getResources().getString(R.string.suredelete))
                        .setMessage(getResources().getString(R.string.deletenorecovery))
                        .setNegativeButton(getResources().getString(R.string.sure), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialoginterface, int i)
                            {
                                delete_data(medicineInfo);
                            }
                        })
                        .setPositiveButton(getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialoginterface, int i)
                            {

                            }
                        })
                        .show();
            }
        });
    }

    private void setup_view() throws JSONException {
        String imgURL = "http://140.117.71.74/graduation/medicineimage/" + medicineInfo.getString("engname") + ".jpg";
        Picasso.get()
                .load(imgURL)
                .error(R.drawable.ic_drugs)
                .into(medicineimg);

        String s="";
        int pos = 0;
        name.setText(medicineInfo.getString("name"));

        String time[] = myreminder.single_pill_time(); //讀取上下一筆用藥時間
        pretaketime.setText(time[0]);
        futuretaketime.setText(time[1]);

        pos = Arrays.asList(takeunititem_TWN).indexOf(medicineInfo.getString("takeunit"));
        s = getString(R.string.respository) + ":" + medicineInfo.getString("stuck")
                + getResources().getStringArray(R.array.units)[pos];
        stuck.setText(s);
        s = medicineInfo.getString("startdate") + "  "+ getString(R.string.onlystart);
        startdate.setText(s);

        pos = Arrays.asList(methoditem_TWN).indexOf(medicineInfo.getString("method"));
        method.setText(getResources().getStringArray(R.array.usemethods)[pos]);


        pos = Arrays.asList(taketimingitem_TWN).indexOf(medicineInfo.getString("taketiming"));
        String temp = getResources().getStringArray(R.array.eattime)[pos];
        if(medicineInfo.getString("repeatday").equals("7")) //每周
            s = getString(R.string.everyweek) + temp;
        else if(medicineInfo.getString("repeatday").equals("1"))
            s = getString(R.string.everyday) + temp;
        else //每隔
            s = getString(R.string.every) + medicineInfo.getString("repeatday") +
                    getString(R.string.aday) + " " + temp;
        taketiming.setText(s);

        pos = Arrays.asList(takeunititem_TWN).indexOf(medicineInfo.getString("takeunit"));
        s = getString(R.string.onetime) + medicineInfo.getString("takeamount") + getResources().getStringArray(R.array.units)[pos];
        takeamount.setText(s);

        effect.setText(medicineInfo.getString("effect").replace(" ", "\n"));
        sideeffect.setText(medicineInfo.getString("sideeffect"));

        s = (medicineInfo.getString("prescription").equals("1")) ? getString(R.string.yes): getString(R.string.nope);
        prescription.setText(s);

        s = getString(R.string.everyday) + medicineInfo.getString("taketime") + getString(R.string.times);
        taketime.setText(s);

        JSONArray remindertime = medicineInfo.getJSONArray("remindertime");
        for(int i = 0; i < 4; i++){ //因為可能會從編輯畫面回來 所以先重置成都沒顯示時間
            timetv[i].setVisibility(View.INVISIBLE);
        }
        for(int i = 0; i < Integer.valueOf(medicineInfo.getString("taketime")); i++) {
            timetv[i].setText(remindertime.getString(i).substring(0, 5));
            timetv[i].setVisibility(View.VISIBLE);
        }

    }


    //-----------connect to MYSQL--------------
    private void fetch_data(final String id){
        String URL = "http://140.117.71.74/graduation/fetch_medicine.php";
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
//                            Log.d("書出訊息:", String.valueOf(jsonObject));
                            String success =jsonObject.getString("success");
                            String message = jsonObject.getString("message");
                            if (success.equals("1")){
//                                Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                                JSONArray record = jsonObject.getJSONArray("record");
                                //放進datalist中
                                medicineInfo = record.getJSONObject(0);
                                setup_view(); //顯示資料

                            }
                            else{
                                Toast.makeText(myPillbox_view.this, message, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.d("錯誤訊息",e.toString());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(myPillbox_view.this,getResources().getString(R.string.searchfail)+error.toString(),Toast.LENGTH_SHORT).show();
                    }
                })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String>params = new HashMap<>();
                params.put("patientid", "");
                params.put("caretakerid", "0");
                params.put("personalMedicineID", id);
                params.put("method", "view");
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    private void delete_data(final JSONObject record){
        String URL = "http://140.117.71.74/graduation/delete_medicine.php";
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            Log.d("書出訊息:", String.valueOf(jsonObject));
                            String success =jsonObject.getString("success");
                            String message = jsonObject.getString("message");
                            if (success.equals("1")){
                                Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                                setResult(RESULT_OK);
                                finish();
                            }
                            else{
                                Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.d("錯誤訊息",e.toString());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(),getResources().getString(R.string.searchfail)+error.toString(),Toast.LENGTH_SHORT).show();
                    }
                })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String>params = new HashMap<>();
                try {
                    params.put("personalMedicineID", record.getString("personalMedicineID"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }
    //編輯頁面回傳
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == 1){
            if(resultCode == RESULT_OK){
                try {
                    JSONObject inputmedicine = new JSONObject(getIntent().getStringExtra("medicineInfo"));
                    String medicineid = inputmedicine.getString("personalMedicineID");
                    fetch_data(medicineid); //讀取資料
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }
    //--介面部分
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.user_info:
                mDrawerLayout.openDrawer(Gravity.RIGHT);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.game_menu, menu);
        //set pattent name of menu
        MenuItem patientname = menu.findItem(R.id.patient_name);
        patientname.setTitle(localDB.getpatientname());
        return true;
    }
//    介面部分--

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected( MenuItem item) {
            BottomNavigation bn = new BottomNavigation(myPillbox_view.this, item.getItemId());
            return true;
        }

    };


}
