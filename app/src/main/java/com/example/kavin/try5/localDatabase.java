package com.example.kavin.try5;

import android.content.Context;
import android.content.res.Resources;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import android.widget.Toast;

import java.io.File;

public class localDatabase {

    public SQLiteDatabase db = null; //資料庫類別
    private final static String DATABASE_NAME="local.db"; //資料庫名稱
    /* 建立資料表的欄位 */
    private final static String	CREATE_USER_TABLE = "CREATE TABLE 'user' ( 'id' INTEGER PRIMARY KEY, 'account' TEXT)";
    private final static String CREATE_PATIENT_TABLE = "CREATE TABLE 'patient' ('id' TEXT PRIMARY KEY, 'name' TEXT)";
    private final static String CREATE_ALARM_TABLE = "CREATE TABLE 'alarm' ('type' TEXT PRIMARY KEY, 'top' DOUBLE, 'bottom' DOUBLE)";
    private Context mCtx = null;
    public localDatabase(Context ctx){ // 建構式
        this.mCtx = ctx;      // 傳入 建立物件的 ＭainActivity
    }

    public boolean open() throws SQLException { // 開啟已經存在的資料庫

        File dbfile = mCtx.getDatabasePath("local.db");
        if(!dbfile.exists()){ //資料庫不存在
            return false;
        }
        else {
            db = mCtx.openOrCreateDatabase(DATABASE_NAME, 0, null);
            return true;
        }

    }
    public void close(){
        db.close();
    }
    public void login_success(String id, String account){
        try {
            db = mCtx.openOrCreateDatabase(DATABASE_NAME, 0, null);
            create_table();
            db.execSQL("INSERT INTO patient (id, name) VALUES ('0', '')");
            adduser(id, account);
            add_default_alarm();
        }catch (Exception e){
            Log.d("資料庫訊息", e.toString());
        }
    }

    public void create_table(){
        db.execSQL(CREATE_USER_TABLE);// 建立照顧者資料表
        db.execSQL(CREATE_PATIENT_TABLE);// 建立被照顧者資料表
        db.execSQL(CREATE_ALARM_TABLE);
    }

    public void adduser(String id, String account){ //登入後新增使用者資訊至DB中
        String insertSql = "INSERT INTO user (id, account) VALUES (" + id + ", '" + account + "')";
        try{
            db.execSQL(insertSql);
        }catch(Exception e){
            Toast.makeText(mCtx, e.toString(), Toast.LENGTH_SHORT).show();
        }
    }

    public void add_default_alarm(){ //0代表沒有警示直
        String sql = "";
        sql = "INSERT INTO alarm (type, bottom, top) VALUES ( 'temperature', 35.7, 37.9 )"; //體溫
        db.execSQL(sql);
        sql = "INSERT INTO alarm (type, bottom, top) VALUES ( 'bloodsugar_before', 0, 126 )"; //血糖(飯前)
        db.execSQL(sql);
        sql = "INSERT INTO alarm (type, bottom, top) VALUES ( 'bloodsugar_after', 0, 180 )";//血糖(飯後)
        db.execSQL(sql);
        sql = "INSERT INTO alarm (type, bottom, top) VALUES ( 'tight_pressure', 0, 140 )";//收縮壓
        db.execSQL(sql);
        sql = "INSERT INTO alarm (type, bottom, top) VALUES ( 'relax_pressure', 0, 90 )"; //舒張壓
        db.execSQL(sql);
        sql = "INSERT INTO alarm (type, bottom, top) VALUES ( 'heartbeat', 60, 100 )"; //心跳
        db.execSQL(sql);
    }

    public int update_patient(String patient_id, String patient_name){
        String updateSql = "UPDATE patient SET id='" + patient_id + "', name='" + patient_name + "'";
        try{
            db.execSQL(updateSql);
            String test_output = getpatientinfo();
            Log.d("更新訊息:", "success id = " + test_output);
            return 1;
        }catch(Exception e){
            Log.d("更新訊息:", e.toString());
            return 0;
        }
    }

    /*取得照顧者ID*/
    public Cursor getinfo(){
        Cursor cursor = db.rawQuery("SELECT * FROM 'user'", null);
        if(cursor.getCount() > 0){
            cursor.moveToFirst();
        }
        else {
            Toast.makeText(mCtx, "no user!", Toast.LENGTH_SHORT).show();
        }
        return cursor;
    }
    public String getuserid(){
        Cursor cursor = db.rawQuery("SELECT id FROM 'user'", null);
        if(cursor.getCount() > 0){
            cursor.moveToFirst();
        }
        else {
            Toast.makeText(mCtx, "no user!", Toast.LENGTH_SHORT).show();
        }
        return cursor.getString(0);
    }
    /*取得被照顧者ID*/
    public String getpatientinfo(){
        Cursor cursor = db.rawQuery("SELECT id FROM 'patient'", null);
        if(cursor.getCount() > 0){
            cursor.moveToFirst();
        }
        else {
            Toast.makeText(mCtx, "no user!", Toast.LENGTH_SHORT).show();
        }
        return cursor.getString(0); //回傳ID
    }
    /*取得被照顧者姓名*/
    public String getpatientname(){
        Cursor cursor = db.rawQuery("SELECT name FROM 'patient'", null);
        if(cursor.getCount() > 0){
            cursor.moveToFirst();
        }
        else {
            Toast.makeText(mCtx, "no user!", Toast.LENGTH_SHORT).show();
        }
        return cursor.getString(0); //回傳name
    }
    public void update_alert(String top, String bottom, String record_type){

        String sql = "UPDATE alarm SET top=" + top + ", bottom=" + bottom + " WHERE type='" + record_type + "'";
        try{
            db.execSQL(sql);
            if(record_type.equals("bloodsugar_after")
                    || record_type.equals("tight_pressure")
                    || record_type.equals("relax_pressure")){ //避免跳多次toast

            }
            else
                Toast.makeText(mCtx, mCtx.getString(R.string.settingsuccess), Toast.LENGTH_SHORT).show();
        }catch(Exception e){
            Toast.makeText(mCtx, e.toString(), Toast.LENGTH_SHORT).show();
        }
    }
    public Cursor get_alert(String record_type){
        String sql = "SELECT * FROM alarm WHERE type='" + record_type + "'";
        Cursor cursor = db.rawQuery(sql, null);
        if(cursor.getCount() > 0) {
            cursor.moveToFirst();
        }
        return cursor;
    }
}
