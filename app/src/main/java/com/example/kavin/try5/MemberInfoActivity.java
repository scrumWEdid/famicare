package com.example.kavin.try5;

import android.content.Intent;
import android.database.Cursor;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class MemberInfoActivity extends AppCompatActivity {

    private localDatabase localDB = null;
    private Cursor cursor = null;
    private EditText account, name, cellphone, email;
    private EditText pwd;
    private TextView pwdTv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_member_info);
        account = (EditText)findViewById(R.id.checkInfoAccount);
        name = (EditText)findViewById(R.id.checkInfoName);
        cellphone = (EditText)findViewById(R.id.checkInfoPhone);
        email = (EditText)findViewById(R.id.checkInfoEmail);
        pwd = (EditText)findViewById(R.id.checkInfoPassword);
        pwdTv = findViewById(R.id.editpasssword);

        final Button finishEditButton = (Button) findViewById(R.id.finishEditButton);
        finishEditButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(name.getText().toString().isEmpty())
                    name.setError(getString(R.string.noblank));
                else if(cellphone.getText().toString().isEmpty())
                    cellphone.setError(getString(R.string.noblank));
                else if(TextUtils.isEmpty(email.getText().toString())
                        || !android.util.Patterns.EMAIL_ADDRESS.matcher(email.getText().toString()).matches())
                    email.setError(getString(R.string.wrongformat));
                else
                    updateinfo(cursor.getInt(0), cursor.getString(1));
            }
        });
        pwdTv.setOnClickListener(new View.OnClickListener() { //應該是旁邊要多個修改密碼的textview?
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setClass(MemberInfoActivity.this, EditPasswordActivity.class);
                startActivity(intent);
            }
        });
        //localdatebase
        localDB = new localDatabase(this);
        localDB.open();
        cursor =  localDB.getinfo();
        if(cursor.getCount() >= 0){
            String user_acc = cursor.getString(1);
            Integer user_id = cursor.getInt(0);
            getuser(user_id, user_acc);
        }

        //-------------------------------------------
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                MemberInfoActivity.this.finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.game_menu, menu);
        //hide icon
        MenuItem userinfo = menu.findItem(R.id.user_info);
        userinfo.setIcon(android.R.color.transparent);
        return true;
    }

    //close db
    @Override
    protected void onDestroy() {
        super.onDestroy();
        localDB.close();
    }

    //----------------connect to mySQL----------------
    //fetch info from mySQL
    private void getuser(final Integer user_id, final String user_acc){
        String URL = "http://140.117.71.74/graduation/fetch_user.php";
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            String success =jsonObject.getString("success");
                            if (success.equals("1")){
                                JSONObject jsonData = jsonObject.getJSONObject("0");
//                                Log.d("測試訊息", String.valueOf(jsonData));
                                account.setText(jsonData.getString("account"));
                                name.setText(jsonData.getString("name"));
                                cellphone.setText(jsonData.getString("phone"));
                                email.setText(jsonData.getString("email"));
                            }
                            else{
                                Toast.makeText(MemberInfoActivity.this, getString(R.string.looknouser), Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.d(getString(R.string.wrongmessage),e.toString());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(MemberInfoActivity.this,"error"+error.toString(),Toast.LENGTH_SHORT).show();
                    }
                })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String>params = new HashMap<>();
                params.put("id",String.valueOf(user_id));
                params.put("account",user_acc);
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    private void updateinfo(final Integer user_id, final String user_acc){
        String URL = "http://140.117.71.74/graduation/update_user.php";
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            String success =jsonObject.getString("success");
                            if (success.equals("1")){
                                Toast.makeText(MemberInfoActivity.this, getString(R.string.editsuccess), Toast.LENGTH_SHORT).show();
                                finish();
                            }
                            else{
                                Toast.makeText(MemberInfoActivity.this, getString(R.string.editfail), Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.d("錯誤訊息",e.toString());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(MemberInfoActivity.this,"error"+error.toString(),Toast.LENGTH_SHORT).show();
                    }
                })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String>params = new HashMap<>();
                params.put("id", String.valueOf(user_id));
                params.put("account",user_acc);
                params.put("name", name.getText().toString());
                params.put("phone", cellphone.getText().toString());
                params.put("email", email.getText().toString());
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }
}
