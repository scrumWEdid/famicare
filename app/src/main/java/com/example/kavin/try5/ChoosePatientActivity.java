package com.example.kavin.try5;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.kavin.try5.Volley.Volleyconnect;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChoosePatientActivity extends AppCompatActivity {
    private localDatabase localDB = null;
    private Cursor cursor = null;
    private String user_id;
    private DrawerLayout mDrawerLayout;
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private final static String RECORD_TYPE = "choosepatient";
    private List<ArrayListUsers> users;
    private Adapter adapter;
    private Apilnterface apilnterface;
    private Boolean flag = false;
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_patient);//xml檔案記得要改
        progressBar = findViewById(R.id.progress);
        recyclerView = findViewById(R.id.recycler);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);
        users = new ArrayList<>();


        final Button createPatientButton = (Button) findViewById(R.id.createPatientButton);
        createPatientButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                bundle.putString("userid", getIntent().getStringExtra("userid"));
                bundle.putString("useracc", getIntent().getStringExtra("useracc"));

                Intent createPatientIntent = new Intent(ChoosePatientActivity.this,CreatePatientActivity.class);
                createPatientIntent.putExtras(bundle);
                ChoosePatientActivity.this.startActivityForResult(createPatientIntent, 1);
            }
        });
        final Button managePatientButton =(Button) findViewById(R.id.managePatientButton);
        managePatientButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (flag) {
                    adapter.updatevisibility(false);
                    flag =false;
                } else {
                    adapter.updatevisibility(true);
                    flag = true;
                }
            }
        });


        //local Database
        localDB = new localDatabase(this);
        if(localDB.open()) {
            cursor = localDB.getinfo();
            if(cursor.getCount() >= 0){
                user_id = String.valueOf(cursor.getInt(0));
            }
        }
        else
            user_id = getIntent().getStringExtra("userid");
        fetchUsers(user_id);

        //------------------------------------------------------

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp);

        if(getIntent().getStringExtra("fromWhere").equals("login")){
            actionbar.setDisplayHomeAsUpEnabled(false);
        }


    }

    /*下一頁修改完畢回傳更新資料*/
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == 1){
            if(resultCode == RESULT_OK){
                fetchUsers(user_id);
            }
        }
    }

    //--介面部分
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.game_menu, menu);
        //hide icon
        MenuItem userinfo = menu.findItem(R.id.user_info);
        userinfo.setIcon(android.R.color.transparent);
        return true;
    }

    //------------------------------------------------------

    //closeDB
    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(localDB.open()) {
            localDB.close();
        }
    }

    //-------------------------About recycler view------------------------
    public void onResume() {
        super.onResume();
        recyclerView.setAdapter(adapter);

    }
    public class Adapter extends RecyclerView.Adapter<Adapter.MyViewHolder> {
        private boolean isvisible = false;
        private List<ArrayListUsers> users;
        private Context context;

        public Adapter(List<ArrayListUsers> users, Context context) {
            this.users = users;
            this.context = context;
        }

        @Override
        public Adapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.choosepatient_recyclerview,parent,false);
            return new Adapter.MyViewHolder(view);
        }

        @Override
        public void onBindViewHolder(final Adapter.MyViewHolder holder, final int position) {
            final String name = users.get(position).getPatientname();
            final String permission = users.get(position).getPermission();
            final String patientID = users.get(position).getPatientID();
            holder.patient.setText(name + permission);

            holder.patient.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(permission.equals("(待審核)")){ //這個不用改成getResources()
                        Toast.makeText(ChoosePatientActivity.this, "審核中", Toast.LENGTH_SHORT).show();
                    }
                    else {
                        Bundle bundle = getIntent().getExtras();
                        if (bundle != null) {
                            String user_id = bundle.getString("userid");
                            String user_acc = bundle.getString("useracc");
                            localDB.login_success(user_id, user_acc);
                        }
                        int flag = localDB.update_patient(patientID, name);
                        if (flag == 1) {
                            Intent patientIntent = new Intent(ChoosePatientActivity.this, indexActivity.class);
                            ChoosePatientActivity.this.startActivity(patientIntent);
                            finishAffinity();
                        }
                    }
                }
            });
            if(isvisible){
                holder.delBtn.setVisibility(View.VISIBLE);
            }
            else{
                holder.delBtn.setVisibility(View.GONE);

            }
            holder.delBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    new AlertDialog.Builder(ChoosePatientActivity.this)
                            .setTitle(getString(R.string.suredelete))
                            .setMessage(getString(R.string.deletenorecovery))
                            .setNegativeButton(getString(R.string.sure), new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialoginterface, int i)
                                {
                                    String recordid =patientID;
                                    //delete record
                                    Volleyconnect volleyconnect = new Volleyconnect(getApplicationContext());
                                    volleyconnect.delete_record(RECORD_TYPE, recordid, new Volleyconnect.VolleyCallback() {
                                        @Override
                                        public void onSuccess(String result) {
                                            if(result.equals("1")){
                                                users.remove(position);
                                                notifyItemRemoved(position);
                                                notifyItemRangeChanged(position, users.size());



                                            }
                                        }
                                    });
                                }
                            })
                            .setPositiveButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialoginterface, int i)
                                {

                                }
                            })
                            .show();
                }
            });
        }

        @Override
        public int getItemCount() {
            if(users == null)
                return 0;
            return users.size();
        }
        public void updatevisibility(boolean value){
            isvisible = value;
            if(value == true) //有動畫
                notifyItemRangeChanged(0, users.size());
            else //無動畫
                notifyDataSetChanged();
        }
        public class MyViewHolder extends RecyclerView.ViewHolder{

            public  TextView patient;
            public ImageView  delBtn;
            public MyViewHolder( View itemView) {
                super(itemView);
                patient = itemView.findViewById(R.id.patient);
                delBtn = itemView.findViewById(R.id.recycler_delBtn);
            }
        }
    }

    //-----------------------Connect to mySQL------------------------
    public void fetchUsers(String key){

        apilnterface = ApiClient.getApiCient().create(Apilnterface.class);
        Call<List<ArrayListUsers>> call = apilnterface.savePost(key);

        call.enqueue(new Callback<List<ArrayListUsers>>() {
            @Override
            public void onResponse(Call<List<ArrayListUsers>> call, Response<List<ArrayListUsers>> response) {
                progressBar.setVisibility(View.GONE);
                users=response.body();
                adapter =new Adapter(users, ChoosePatientActivity.this);
                recyclerView.setAdapter(adapter);
                adapter.notifyDataSetChanged();
                Log.d("TAG","fetch");

            }

            @Override
            public void onFailure(Call<List<ArrayListUsers>> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
                Toast.makeText(ChoosePatientActivity.this,"Error on :" + t.toString(),Toast.LENGTH_SHORT).show();
            }
        });
    }
}
