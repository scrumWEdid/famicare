package com.example.kavin.try5.reminder_medicine;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.kavin.try5.R;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class medicine_weekday_adapter extends RecyclerView.Adapter<medicine_weekday_adapter.ViewHolder> {
    private List<medicine_weekday_Listitem> listItem;
    private Context context;
    boolean flag = false;
    private reminder_medicine_weekday main;
    int row_index = -1;
    public medicine_weekday_Listitem checked[];
    public String checkedname[];
    public String checkedtime[];
    private  boolean positions[];
    private JSONObject medicineInfo;
    public medicine_weekday_adapter(List<medicine_weekday_Listitem> listItem, Context context, reminder_medicine_weekday inder) {
        this.listItem = listItem;
        this.context = context;
        this.main= inder;
    }


    @Override
    public ViewHolder onCreateViewHolder( ViewGroup parent, int viewType) {
      View v = LayoutInflater.from(parent.getContext())
              .inflate(R.layout.reminder_medicine_weekday_list_item,parent,false);
      return new ViewHolder(v);
    }


    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, final int position) {
        try {
        checked = new  medicine_weekday_Listitem[listItem.size()];
        checkedname = new String[listItem.size()];
        checkedtime = new  String[listItem.size()];
        positions = new boolean[listItem.size()];
        final medicine_weekday_Listitem weekdayListitem = listItem.get(position);
        viewHolder.textView_reinder_time.setText(weekdayListitem.getReminder_medicine_time());
        viewHolder.textView_reinder_medicine_name.setText(weekdayListitem.getReminder_medicine_name());

        String s = weekdayListitem.getReminder_medicine_info().getString("taketiming")+"   "
                  +weekdayListitem.getReminder_medicine_info().getString("takeamount")
                  +weekdayListitem.getReminder_medicine_info().getString("takeunit");
        viewHolder.textView_reinder_medicine_timing.setText(s);
        String imgURL = "http://140.117.71.74/graduation/medicineimage/"+weekdayListitem.getReminder_medicine_info().getString("engname") + ".jpg";
        Picasso.get()
                .load(imgURL)
                .error(R.drawable.ic_drugs)
                .into(viewHolder.imageview_reinder_img);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (medicine_weekday_common.Sdate!=""){
            flag= true;
            Log.d("medicineitem", viewHolder.textView_reinder_medicine_name.getText().toString());
            Log.d("medicinetime",viewHolder.textView_reinder_time.getText().toString());

//            viewHolder.checkbox_reinder.setChecked(positions == null ? false : checkedState);
        }
        if (flag==true){
            viewHolder.checkbox_reinder.setVisibility(View.VISIBLE);
            medicine_weekday_common.currentItem=null;
            medicine_weekday_common.position=-1;
            main.ShowCancel(true);
            main.ShowDone(true);
            main.ShowView(false);

        }
        else{
            viewHolder.checkbox_reinder.setVisibility(View.INVISIBLE);
            main.ShowCancel(false);
        }
        viewHolder.checkbox_reinder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (viewHolder.checkbox_reinder.isChecked()){
                    checked[position]=listItem.get(position);
                    checkedname[position]=listItem.get(position).getReminder_medicine_name();
                    checkedtime[position]=listItem.get(position).getReminder_medicine_time();

                }else{
                    checked[position]=null;
                }
            }
        });
        if (checked[position]==null){
            viewHolder.checkbox_reinder.setChecked(false);
        }
//        Log.d("notficationtest",viewHolder.textView_reinder_time.getText().toString());
//        Log.d("notficationtests",medicine_weekday_common.Sdate +"asd");

        //根據推通知回來
        if (medicine_weekday_common.Sdate.equals(viewHolder.textView_reinder_time.getText().toString())){
            viewHolder.checkbox_reinder.setChecked(true);
            if (viewHolder.checkbox_reinder.isChecked()){
                checked[position]=listItem.get(position);
                checkedname[position]=listItem.get(position).getReminder_medicine_name();
                checkedtime[position]=listItem.get(position).getReminder_medicine_time();
            }
            if(position == getItemCount()-1)
                medicine_weekday_common.Sdate = "";
        }
//        if (viewHolder.checkbox_reinder.isChecked()==true){
//            checked[position]=listItem.get(position);
//        }
//        if (!viewHolder.checkbox_reinder.isChecked()){
//            checked[position]=null;
//
//        }
//        viewHolder.setItemClickListener(new medicine_weekday_ItemClickListener() {
//            @Override
//            public void onClick(View view, int position) {
//                row_index =position;
//                medicine_weekday_common.currentItem=listItem.get(position);
//                notifyDataSetChanged();
//                Log.d("asd","asd");
//            }
//        });
        viewHolder.viewForeground.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (flag==false){
                    notifyDataSetChanged();
                    row_index =position;
                    medicine_weekday_common.currentItem=listItem.get(position);
                    medicine_weekday_common.position=position;
                    notifyDataSetChanged();
                    main.ShowDone(true);
                    main.ShowView(true);
                }



//                Intent intent = new Intent();
//                intent.setClass(context, reminder_view_medicine.class);
//                Bundle bundle = new Bundle();
//                bundle.putString("medicine", weekdayListitem.getReinder_medicine_info().toString());
//                intent.putExtras(bundle);
//                ((Activity)context).startActivityForResult(intent, 1);
            }
        });
//        viewHolder.textView_reinder_medicine_name.setOnLongClickListener(new View.OnLongClickListener() {
//            @Override
//            public boolean onLongClick(View view) {
//                flag=true;
//                notifyDataSetChanged();
//                return true;
//            }
//        });

        if(row_index==position&&flag ==false){
            viewHolder.viewForeground.setBackgroundColor(Color.parseColor("#d5d5e2"));
        }
        else{
            viewHolder.viewForeground.setBackgroundColor(Color.parseColor("#FFFFFF"));
        }
    }
    public boolean remindertime(int position){
        if (position%2==0){
            return true;
        }
        else return false;
    }
    @Override
    public int getItemCount() {
        return listItem.size();
    }
    public void removeItem(int position) {
        listItem.remove(position);
        // notify the item removed by position
        // to perform recycler view delete animations
        // NOTE: don't call notifyDataSetChanged()
        notifyItemRemoved(position);
    }

    public void restoreItem(medicine_weekday_Listitem item, int position) {
        listItem.add(position, item);
        // notify item added by position
        notifyItemInserted(position);
    }



    public class ViewHolder extends RecyclerView.ViewHolder  {
            public TextView textView_reinder_time;
            public TextView textView_reinder_medicine_name;
            public TextView textView_reinder_medicine_timing;
            public CheckBox checkbox_reinder;
            public ImageView imageview_reinder_img;
//            medicine_weekday_ItemClickListener itemClickListener;
//            public void setItemClickListener(medicine_weekday_ItemClickListener itemClickListener){
//                this.itemClickListener = itemClickListener;
//            }
        public RelativeLayout viewBackground, viewForeground;
        public Button cancel ;
        public ViewHolder( View itemView) {
            super(itemView);
            textView_reinder_time = (TextView) itemView.findViewById(R.id.reminder_medicine_time);
            textView_reinder_medicine_name = (TextView)itemView.findViewById(R.id.reminder_medicine_name);
            checkbox_reinder= (CheckBox)itemView.findViewById(R.id.checkbox_reminder);
            textView_reinder_medicine_timing = (TextView) itemView.findViewById(R.id.reminder_medicine_timing);
            imageview_reinder_img =(ImageView)itemView.findViewById(R.id.reminder_medicine_img);
            viewBackground = itemView.findViewById(R.id.view_background);
            viewForeground = itemView.findViewById(R.id.view_foreground);
//            itemView.setOnClickListener(this);
            viewForeground.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    flag=true;
                    notifyDataSetChanged();
                    return true;
                }
            });


        }


//        @Override
//        public void onClick(View view) {
//            itemClickListener.onClick(view,getAdapterPosition());
//            Log.d("asd","asd");
//
//        }
    }


}
