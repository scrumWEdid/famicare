package com.example.kavin.try5;

import android.view.View;
import android.widget.TextView;

import com.thoughtbot.expandablerecyclerview.viewholders.ChildViewHolder;

public class TemperatureViewHolder extends ChildViewHolder {
    private TextView t_item_date;
    private TextView t_item_time;
    private TextView t_item_temperature;

    public TemperatureViewHolder(View itemView) {

        super(itemView);
        t_item_date = (TextView)itemView.findViewById(R.id.t_item_date);
        t_item_time = (TextView)itemView.findViewById(R.id.t_item_time);
        t_item_temperature = (TextView)itemView.findViewById(R.id.t_item_temperature);

    }

    public void setT_item_date(String date) {
        t_item_date.setText(date);
    }

    public void setT_item_time(String time) {
        t_item_time.setText(time);
    }

    public void setT_item_temperature(String temperature) {
        t_item_temperature.setText(temperature);
    }
}


