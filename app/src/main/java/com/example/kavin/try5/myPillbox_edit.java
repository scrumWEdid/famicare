package com.example.kavin.try5;

import android.app.DatePickerDialog;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

public class myPillbox_edit extends AppCompatActivity {

    private DrawerLayout mDrawerLayout;
    private localDatabase localDB = null;
    private Cursor cursor = null;
    private toolbar mytoolbar;

    private TextView medicine_name, startdatetv;
    private ImageView medicineImg;

    private Spinner methodSpn;
    private String[] methoditem_TWN = {"用法", "口服", "注射", "外用", "其他"};

    private Spinner takeday1Spn, takeday2Spn, taketimingSpn;
    private String[] takeday2item_TWN = {"0", "1天","2天", "3天", "4天", "5天"}; //選每隔X天的話

    private int takeday2pos = 0; //存takeday2位置 之後takeday2spn setadapter後再設定位置
    private String[] takeday2item2_TWN = {"0", "1次", "2次", "3次", "4次"}; //選每日的話
    private String[] taketimingitem_TWN = {"", "飯前","飯後","不限"};

    private Spinner takeunitSpn;
    private String[] takeunititem_TWN = {"片", "粒", "cc", "mg", "個", "滴", "劑", "單位"};

    private EditText takeamountEdt, stuckEdt;
    private EditText effect, sideeffect;
    private CheckBox checkBoxYes, checkBoxNo;
    private Button submitBtn;

    private DatePickerDialog.OnDateSetListener mDateSetListener;

    private JSONObject medicineInfo;
    private String id;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_pillbox_edit);

        localDB = new localDatabase(this);
        localDB.open();

        BottomNavigationView bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottom_navigation_view);
        bottomNavigationView.setSelectedItemId(R.id.drug);
        bottomNavigationView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);


        //--介面部分
        //setup toolbar
        mytoolbar = new toolbar(this);
        mDrawerLayout = findViewById(R.id.drawer_layout);
        //介面部分--
//----get string array from strings.xml
        String[] methoditem = getResources().getStringArray(R.array.usemethods);
        String[] takeday1item = getResources().getStringArray(R.array.time);
        final String[] takeday2item = getResources().getStringArray(R.array.days); //選每隔X天的話
        final String[] takeday2item2 = getResources().getStringArray(R.array.times); //選每日的話
        String[] taketimingitem = getResources().getStringArray(R.array.eattime);
        String[] takeunititem = getResources().getStringArray(R.array.units);
//get string array from strings.xml----

        medicine_name = findViewById(R.id.medicine_name);
        medicineImg = findViewById(R.id.medicineImg);

        methodSpn = findViewById(R.id.methodSpn);
        methodSpn.setAdapter(setupadapter(methoditem));

        takeday1Spn = findViewById(R.id.takeday1Spn);
        takeday2Spn = findViewById(R.id.takeday2Spn);
        takeday1Spn.setAdapter(setupadapter(takeday1item));
        takeday1Spn.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                switch (i){
                    case 0: //每日
                        takeday2Spn.setVisibility(View.VISIBLE);
                        takeday2Spn.setAdapter(setupadapter(takeday2item2));
                        takeday2Spn.setSelection(takeday2pos);
                        break;
                    case 1: //每隔X天
                        takeday2Spn.setVisibility(View.VISIBLE);
                        takeday2Spn.setAdapter(setupadapter(takeday2item));
                        takeday2Spn.setSelection(takeday2pos);
                        break;
                    case 2: //每周
                        takeday2Spn.setVisibility(View.GONE);
                        break;

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        taketimingSpn = findViewById(R.id.taketimingSpn);
        taketimingSpn.setAdapter(setupadapter(taketimingitem));

        takeunitSpn = findViewById(R.id.takeunitSpn);
        takeunitSpn.setAdapter(setupadapter(takeunititem));

        takeamountEdt = findViewById(R.id.takeamountEdt);
        stuckEdt = findViewById(R.id.stuckEdt);

        effect = findViewById(R.id.effectEdt);
        sideeffect = findViewById(R.id.sideeffectEdt);

        checkBoxYes = findViewById(R.id.checkBoxYes);
        checkBoxNo = findViewById(R.id.checkBoxNo);
        //一次只能選其中一個
        checkBoxYes.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b == true)
                    checkBoxNo.setChecked(false);
            }
        });
        checkBoxNo.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b == true)
                    checkBoxYes.setChecked(false);
            }
        });
        submitBtn = findViewById(R.id.submitBtn);
        submitBtn.setText(R.string.finishEdit);
        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                update_medicine();
            }
        });

        try {
            medicineInfo = new JSONObject(getIntent().getStringExtra("medicineInfo"));
            id = medicineInfo.getString("personalMedicineID");
            setup_view();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    //open datepickerDialog
    private void opendatepicker(){
        int year, month, day;

        if(startdatetv.getText().toString().equals(getResources().getString(R.string.selectstartdate))) {
            Calendar cal = Calendar.getInstance();
            year = cal.get(Calendar.YEAR);
            month = cal.get(Calendar.MONTH);
            day = cal.get(Calendar.DAY_OF_MONTH);
        }
        else{
            String[] date = startdatetv.getText().toString().split("/");
            year = Integer.valueOf(date[0]);
            month = Integer.valueOf(date[1])-1;
            day = Integer.valueOf(date[2]);
        }
        DatePickerDialog dialog = new DatePickerDialog(
                myPillbox_edit.this,
                android.R.style.Theme_Holo_Light_Dialog_MinWidth,
                mDateSetListener,
                year, month, day);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();

    }

    //spinner adapter
    ArrayAdapter<String> setupadapter(String[] item){
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_spinner_dropdown_item, item){
            @Override
            public boolean isEnabled(int position){
//                if(position == 0)
//                    return false;
//                else
                    return true;
            }
            @Override
            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView tv = (TextView) view;
                if(position == 0){
                    // Set the hint text color gray
//                    tv.setTextColor(Color.GRAY);
                    tv.setTextColor(Color.BLACK);
                }
                else {
                    tv.setTextColor(Color.BLACK);
                }
                return view;
            }
        };
        adapter.setDropDownViewResource(R.layout.spinner_item);
        return adapter;
    }

    private void setup_view() throws JSONException {
        medicine_name.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
        medicine_name.setText(medicineInfo.getString("name"));

        String imgURL = "http://140.117.71.74/graduation/medicineimage/"+medicineInfo.getString("engname") + ".jpg";
        Picasso.get()
                .load(imgURL)
                .error(R.drawable.ic_add_a_photo_black_24dp)
                .into(medicineImg);

        int position = Arrays.asList(methoditem_TWN).indexOf(medicineInfo.getString("method"));
        methodSpn.setSelection(position);

        int repeatday = Integer.valueOf(medicineInfo.getString("repeatday"));
        switch (repeatday){
            case 1: { //每日
                takeday1Spn.setSelection(0);
                position = Arrays.asList(takeday2item2_TWN).indexOf(medicineInfo.getString("taketime") + getResources().getString(R.string.times));
                takeday2pos = position;
                takeday2Spn.setSelection(position);
                break;
            }
            case 7: { //每周
                takeday1Spn.setSelection(2);
                break;
            }
            default: { //每隔X天
                takeday1Spn.setSelection(1);
                position = Arrays.asList(takeday2item_TWN).indexOf(medicineInfo.getString("repeatday") + getResources().getString(R.string.aday));
                takeday2pos = position;
            }
        }
        position = Arrays.asList(taketimingitem_TWN).indexOf(medicineInfo.getString("taketiming"));
        taketimingSpn.setSelection(position);

        takeamountEdt.setText(medicineInfo.getString("takeamount"));
        stuckEdt.setText(medicineInfo.getString("stuck"));

        position = Arrays.asList(takeunititem_TWN).indexOf(medicineInfo.getString("takeunit"));
        takeunitSpn.setSelection(position);

        if(medicineInfo.getString("prescription").equals("1"))
            checkBoxYes.setChecked(true);
        else
            checkBoxNo.setChecked(true);

        effect.setText(medicineInfo.getString("effect").replace(" ", "\n"));
        sideeffect.setText(medicineInfo.getString("sideeffect").replace(" ", "\n"));

    }
    //--------------connect to MySQL------------------
    private void update_medicine(){
        String URL = "http://140.117.71.74/graduation/update_medicine.php";
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            Log.d("書出訊息:", String.valueOf(jsonObject));
                            String success =jsonObject.getString("success");
                            String message = jsonObject.getString("message");
                            if (success.equals("1")){
                                Toast.makeText(getApplicationContext(), getString(R.string.editsuccess), Toast.LENGTH_SHORT).show();
                                setResult(RESULT_OK);
                                finish();
                            }
                            else{
                                Toast.makeText(getApplicationContext(), getString(R.string.editfail), Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.d("錯誤訊息",e.toString());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(),getResources().getString(R.string.searchfail)+error.toString(),Toast.LENGTH_SHORT).show();
                    }
                })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String>params = new HashMap<>();
                params.put("personalMedicineID", id);
                params.put("method", methoditem_TWN[methodSpn.getSelectedItemPosition()]);

                switch(takeday1Spn.getSelectedItemPosition()){
                    case 0: //每日
                        params.put("taketime", takeday2item2_TWN[takeday2Spn.getSelectedItemPosition()].substring(0,1));
                        params.put("repeatday", "1");
                        break;
                    case 1: //每隔X天
                        params.put("taketime", "1");
                        params.put("repeatday", takeday2item_TWN[takeday2Spn.getSelectedItemPosition()].substring(0,1));
                        break;
                    case 2: //每周
                        params.put("taketime", "1");
                        params.put("repeatday", "7");
                        break;
                }

                params.put("taketiming", taketimingitem_TWN[taketimingSpn.getSelectedItemPosition()]);
                params.put("takeamount", takeamountEdt.getText().toString());
                params.put("takeunit", takeunititem_TWN[takeunitSpn.getSelectedItemPosition()]);

                if(checkBoxYes.isChecked())
                    params.put("prescription", "1");
                else
                    params.put("prescription", "0");
                params.put("stuck", stuckEdt.getText().toString());

                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }
    //--介面部分
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.user_info:
                mDrawerLayout.openDrawer(Gravity.RIGHT);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.game_menu, menu);
        //set pattent name of menu
        MenuItem patientname = menu.findItem(R.id.patient_name);
        patientname.setTitle(localDB.getpatientname());
        return true;
    }
//    介面部分--

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected( MenuItem item) {
            BottomNavigation bn = new BottomNavigation(myPillbox_edit.this, item.getItemId());
            return true;
        }

    };

}
