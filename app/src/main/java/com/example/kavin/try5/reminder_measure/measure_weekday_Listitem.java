package com.example.kavin.try5.reminder_measure;

import org.json.JSONException;
import org.json.JSONObject;

public class measure_weekday_Listitem {

    private String reminder_measure_time;
    private String reminder_measure_name;
    private JSONObject reminder_measure_info;
    public measure_weekday_Listitem(String reminder_measure_time, String reminder_measure_name, JSONObject reminder_measure_info) {
        this.reminder_measure_time = reminder_measure_time;
        this.reminder_measure_name = reminder_measure_name;
        this.reminder_measure_info = reminder_measure_info;
    }

    public String getReminder_measure_time() {
        return reminder_measure_time;
    }

    public String getReminder_measure_name() {
        return reminder_measure_name;
    }

    public JSONObject getReminder_measure_info() {
        return reminder_measure_info;
    }

    public String getReminder_measure_id() throws JSONException {return reminder_measure_info.getString("recordReminderID");}
}