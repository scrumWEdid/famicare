package com.example.kavin.try5;

import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.kavin.try5.Volley.Volleyconnect;
import com.github.florent37.singledateandtimepicker.SingleDateAndTimePicker;
import com.github.florent37.singledateandtimepicker.dialog.SingleDateAndTimePickerDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import jp.wasabeef.recyclerview.adapters.AlphaInAnimationAdapter;
import jp.wasabeef.recyclerview.animators.SlideInUpAnimator;

public class other_main extends AppCompatActivity {
    private DrawerLayout mDrawerLayout;
    private localDatabase localDB = null;
    private Cursor cursor = null;
    private String user_id, user_acc, patient_id;
    private final static String RECORD_TYPE = "other";
    private EditText search_title;
    private TextView startdate_txt, enddate_txt;
    private Date start_date, end_date;
    private SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
    private SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy/M/dd HH:mm");
    private SimpleDateFormat sdf3 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private RecyclerView mRecyclerView;
    private MyAdapter mAdapter;
    private ArrayList<String> myDataset = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_other_main);

        BottomNavigationView bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottom_navigation_view);
        bottomNavigationView.setSelectedItemId(R.id.nav_description);
        bottomNavigationView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        //----open localDB
        localDB = new localDatabase(this);
        localDB.open();
//----介面部分
        toolbar mytoolbar = new toolbar(this);
        mDrawerLayout = findViewById(R.id.drawer_layout);
// 介面部分-----
//----讀取權限 設定是否要顯示的新增,編輯按鈕
        final Volleyconnect volleyconnect = new Volleyconnect(this);
        volleyconnect.fetch_permission(localDB.getuserid(), localDB.getpatientinfo(),
                new Volleyconnect.VolleyCallback() {
                    @Override
                    public void onSuccess(String result) {
                        if(result.equals("查看者")) {
                            volleyconnect.hideTextViewicon("addBtn");
                            volleyconnect.hideTextViewicon("editBtn");
                        }
                    }
                });
//讀取權限----

        TextView addBtn = findViewById(R.id.addimgBtn);
        TextView editBtn = findViewById(R.id.editimgBtn);

        startdate_txt = findViewById(R.id.start_date);
        enddate_txt = findViewById(R.id.end_date);

        search_title = findViewById(R.id.searchtitle);
        search_title.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                fetch_record(RECORD_TYPE);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        addBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setClass(other_main.this, other_add.class);
                startActivityForResult(intent, 1);
            }
        });
        editBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LinearLayout layout = findViewById(R.id.recyclerview_header);
                int column_count = layout.getChildCount();
                if(layout.getChildAt(column_count-1).getVisibility() == View.GONE){
                    layout.getChildAt(column_count-1).setVisibility(View.VISIBLE); //編輯
                    layout.getChildAt(column_count-2).setVisibility(View.VISIBLE); // 刪除
                    layout.getChildAt(column_count-3).setVisibility(View.GONE); //敘述
                    mAdapter.updatevisibility(true);
                }
                else{
                    layout.getChildAt(column_count-1).setVisibility(View.GONE);
                    layout.getChildAt(column_count-2).setVisibility(View.GONE);
                    layout.getChildAt(column_count-3).setVisibility(View.VISIBLE);
                    mAdapter.updatevisibility(false);
                }
            }
        });
        start_date = new Date();
        end_date = new Date();
        startdate_txt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                open_datetimepicker(1);
            }
        });
        enddate_txt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                open_datetimepicker(2);
            }
        });

        //local dasabase
        cursor = localDB.getinfo();
        if(cursor.getCount() > 0){
            user_id = String.valueOf(cursor.getInt(0));
            user_acc = cursor.getString(1);
        }
        patient_id = localDB.getpatientinfo();

        //初始化recycler view
        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerview);
        final LinearLayoutManager layoutManager = new LinearLayoutManager(other_main.this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mRecyclerView.setLayoutManager(layoutManager);
        //myDataset放道adapter中去建立recycler view
//        mAdapter = new MyAdapter(myDataset);
//        mRecyclerView.setAdapter(mAdapter);

        //取得體溫資料
        fetch_record(RECORD_TYPE);
    }
    @Override
    protected void onDestroy(){
        super.onDestroy();
        localDB.close(); // 關閉資料庫
    }

    private void open_datetimepicker(int id){
        if(id == 1) { //選開始時間
            new SingleDateAndTimePickerDialog.Builder(other_main.this)
                    .bottomSheet()
                    .displayMinutes(false)
                    .displayHours(false)
                    .displayDays(false)
                    .displayMonth(true)
                    .displayYears(true)
                    .displayDaysOfMonth(true)
                    .maxDateRange(end_date)
                    .displayListener(new SingleDateAndTimePickerDialog.DisplayListener() {
                        @Override
                        public void onDisplayed(SingleDateAndTimePicker picker) {
                            picker.setDefaultDate(start_date);
                        }
                    })
                    .title(getString(R.string.startdate))
                    .listener(new SingleDateAndTimePickerDialog.Listener() {
                        @Override
                        public void onDateSelected(Date date) {
                            start_date = date;
                            String select_date = sdf.format(date);
                            startdate_txt.setText(select_date);
                            fetch_record(RECORD_TYPE);
                        }
                    }).display();
        }
        else{ //選截止時間
            new SingleDateAndTimePickerDialog.Builder(other_main.this)
                    .bottomSheet()
                    .displayMinutes(false)
                    .displayHours(false)
                    .displayDays(false)
                    .displayMonth(true)
                    .displayYears(true)
                    .displayDaysOfMonth(true)
                    .minDateRange(start_date)
                    .maxDateRange(new Date())
                    .displayListener(new SingleDateAndTimePickerDialog.DisplayListener() {
                        @Override
                        public void onDisplayed(SingleDateAndTimePicker picker) {
                            picker.setDefaultDate(end_date);
                        }
                    })
                    .title(getString(R.string.stopdate))
                    .listener(new SingleDateAndTimePickerDialog.Listener() {
                        @Override
                        public void onDateSelected(Date date) {
                            end_date = date;
                            String select_date = sdf.format(date);
                            enddate_txt.setText(select_date);
                            fetch_record(RECORD_TYPE);
                        }
                    }).display();
        }
    }

    //取得體溫資料
    private void fetch_record(final String record_type){
        String URL = "http://140.117.71.74/graduation/fetch_record.php";
        myDataset.clear();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            Log.d("書出訊息:", String.valueOf(jsonObject));
                            String success =jsonObject.getString("success");
                            String message = jsonObject.getString("message");
                            JSONArray record = jsonObject.getJSONArray("record");
                            if (success.equals("1")){
                                //放進datalist中
                                for(int i = 0; i < record.length(); i++){
                                    JSONObject single_record = record.getJSONObject(i);
                                    String item = "";
                                    //處理輸出日期時間的格式
                                    String format_datetime = single_record.getString("datetime");
                                    try {
                                        Date dt = sdf3.parse(format_datetime);
                                        format_datetime = sdf2.format(dt);
                                    } catch (ParseException e) {
                                        e.printStackTrace();
                                    }
                                    item = format_datetime
                                            + " " + single_record.getString("title")
                                            + " " + single_record.getString("description")
                                            + " " + single_record.getString("otherID");
                                    myDataset.add(item);
                                }
                                //myDataset放道adapter中去建立recycler view
                                mAdapter = new MyAdapter(myDataset);
                                mRecyclerView.setItemAnimator(new SlideInUpAnimator());
                                mRecyclerView.getItemAnimator().setRemoveDuration(0);
                                mRecyclerView.getItemAnimator().setChangeDuration(500);
                                mRecyclerView.setAdapter(new AlphaInAnimationAdapter(mAdapter));

                                //Toast.makeText(other_main.this, message, Toast.LENGTH_SHORT).show();
                            }
                            else{
                                Toast.makeText(other_main.this, message, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.d("錯誤訊息",e.toString());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(other_main.this,getString(R.string.searchfail)+error.toString(),Toast.LENGTH_SHORT).show();
                    }
                })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String>params = new HashMap<>();
                params.put("record_type", record_type);
                params.put("patientid", patient_id);
                if(!startdate_txt.getText().toString().equals(getString(R.string.startdate)))
                    params.put("startdate", startdate_txt.getText().toString());
                if(!enddate_txt.getText().toString().equals(getString(R.string.enddate)))
                    params.put("enddate", enddate_txt.getText().toString());
                if(search_title.getText().length() != 0)
                    params.put("title", search_title.getText().toString());
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }
    private void update_record(final String record_type, final String title, final String recordID){
//        String URL = "http://140.117.71.74/graduation/update_record.php";
        String URL = "http://140.117.71.74/graduation/update_record.php";
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            Log.d("書出訊息:", String.valueOf(jsonObject));
                            String success =jsonObject.getString("success");
                            String message = jsonObject.getString("message");
                            if (success.equals("1")){
                                Toast.makeText(other_main.this, getString(R.string.updatesuccess), Toast.LENGTH_SHORT).show();
                            }
                            else{
                                Toast.makeText(other_main.this, getString(R.string.updatefail), Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.d("錯誤訊息",e.toString());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(other_main.this,getString(R.string.searchfail)+error.toString(),Toast.LENGTH_SHORT).show();
                    }
                })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String>params = new HashMap<>();
                params.put("record_type", record_type);
                params.put("caretakerID", user_id);
                params.put("title", title);
                params.put("recordID", recordID);
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }


    //以下皆為recyclerview有關的function
    public class MyAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
        private List<String> mData;
        private boolean isvisible = false;
        //要求權現的viewholder
        public class ViewHolder1 extends RecyclerView.ViewHolder {
            public TextView datetxt;
            public TextView titletxt, descriptiontxt;
            public ImageView editBtn, delBtn;
            public ViewHolder1(View v) {
                super(v);
                datetxt = (TextView) v.findViewById(R.id.recycler_date);
                titletxt = v.findViewById(R.id.recycler_time);
                descriptiontxt = v.findViewById(R.id.recycler_value);
                editBtn = v.findViewById(R.id.recycler_editBtn);
                delBtn = v.findViewById(R.id.recycler_delBtn);
            }

        }

        public MyAdapter(List<String> data) {
            mData = data;
        }
        @Override
        public int getItemViewType(int position) {
            return 0;
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v;
            v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.recycleview_temperature, parent, false);
            return new ViewHolder1(v);
        }

        @Override
        public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
            final ViewHolder1 viewholder1 = (ViewHolder1)holder;
            final String[] data = mData.get(position).split(" ");
            viewholder1.datetxt.setText(data[0].substring(5));
            viewholder1.titletxt.setText(data[2]);
            String text = data[3];
            if(text.length() > 5)
                text = text.substring(0, 5) + "...";
            viewholder1.descriptiontxt.setText(text);

            viewholder1.datetxt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent();
                    intent.setClass(other_main.this, other_view_and_edit.class);
                    Bundle bundle = new Bundle();
                    bundle.putStringArray("data", data);
                    intent.putExtras(bundle);
                    startActivityForResult(intent, 1);
                }
            });
            viewholder1.titletxt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent();
                    intent.setClass(other_main.this, other_view_and_edit.class);
                    Bundle bundle = new Bundle();
                    bundle.putStringArray("data", data);
                    intent.putExtras(bundle);
                    startActivityForResult(intent, 1);
                }
            });
            viewholder1.descriptiontxt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent();
                    intent.setClass(other_main.this, other_view_and_edit.class);
                    Bundle bundle = new Bundle();
                    bundle.putStringArray("data", data);
                    intent.putExtras(bundle);
                    startActivityForResult(intent, 1);
                }
            });

            //岸編輯按鈕時的反應
            if(isvisible){
                viewholder1.descriptiontxt.setVisibility(View.GONE);
                viewholder1.editBtn.setVisibility(View.VISIBLE);
                viewholder1.delBtn.setVisibility(View.VISIBLE);
            }
            else{
                viewholder1.descriptiontxt.setVisibility(View.VISIBLE);
                viewholder1.editBtn.setVisibility(View.GONE);
                viewholder1.delBtn.setVisibility(View.GONE);
            }

            viewholder1.editBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    final EditText input = new EditText(other_main.this);
                    input.setSingleLine(true);
                    input.setText(data[2]);
                    LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                            LinearLayout.LayoutParams.MATCH_PARENT,
                            LinearLayout.LayoutParams.MATCH_PARENT);
                    input.setLayoutParams(lp);

                    new AlertDialog.Builder(other_main.this)
                            .setView(input)
                            .setTitle(getString(R.string.edittitle))
                            .setMessage(getString(R.string.entertitlename))
                            .setNegativeButton(getString(R.string.sure), new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialoginterface, int i)
                                {
                                    String title = input.getText().toString();
                                    if(title.equals(""))
                                        Toast.makeText(other_main.this, getString(R.string.plsentertitle), Toast.LENGTH_SHORT).show();
                                    else {
                                        update_record(RECORD_TYPE, title, data[4]);
                                        viewholder1.titletxt.setText(title);
                                    }
                                }
                            })
                            .setPositiveButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialoginterface, int i)
                                {

                                }
                            })
                            .show();
                }
            });

            viewholder1.delBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    new AlertDialog.Builder(other_main.this)
                            .setTitle(getResources().getString(R.string.suredelete))
                            .setMessage(getResources().getString(R.string.deletenorecovery))
                            .setNegativeButton(getResources().getString(R.string.sure), new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialoginterface, int i)
                                {
                                    String recordid = data[4];
                                    //delete record
                                    Volleyconnect volleyconnect = new Volleyconnect(getApplicationContext());
                                    volleyconnect.delete_record(RECORD_TYPE, recordid, new Volleyconnect.VolleyCallback() {
                                        @Override
                                        public void onSuccess(String result) {
                                            if(result.equals("1")){
                                                myDataset.remove(position);
                                                notifyItemRemoved(position);
                                                notifyItemRangeChanged(position, myDataset.size());
                                            }
                                        }
                                    });
                                }
                            })
                            .setPositiveButton(getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialoginterface, int i)
                                {

                                }
                            })
                            .show();
                }
            });
        }

        @Override
        public int getItemCount() {
            return mData.size();
        }

        public void updatevisibility(boolean value){
            isvisible = value;
            if(value == true) //有動畫
                notifyItemRangeChanged(0, mData.size());
            else //無動畫
                notifyDataSetChanged();
        }
    }
    /*下一頁修改完畢回傳時刷新recyclerview*/
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == 1){
            if(resultCode == RESULT_OK){
                LinearLayout layout = findViewById(R.id.recyclerview_header);
                int column_count = layout.getChildCount();
                layout.getChildAt(column_count-1).setVisibility(View.GONE); //編輯
                layout.getChildAt(column_count-2).setVisibility(View.GONE); // 刪除
                layout.getChildAt(column_count-3).setVisibility(View.VISIBLE); // 敘述

                fetch_record(RECORD_TYPE);
            }
        }
    }
    //--介面部分
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
//                mDrawerLayout.openDrawer(Gravity.RIGHT);
                other_main.this.finish();
                return true;
            case R.id.user_info:
                mDrawerLayout.openDrawer(Gravity.RIGHT);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.game_menu, menu);
        //set pattent name of menu
        MenuItem patientname = menu.findItem(R.id.patient_name);
        patientname.setTitle(localDB.getpatientname());
        return true;
    }
//    介面部分--
private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
        = new BottomNavigationView.OnNavigationItemSelectedListener() {

    @Override
    public boolean onNavigationItemSelected( MenuItem item) {
        BottomNavigation bn = new BottomNavigation(other_main.this, item.getItemId());
        return true;
    }

};
}
