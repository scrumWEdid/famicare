package com.example.kavin.try5;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.w3c.dom.Text;

public class recordPageActivity2 extends Fragment {

    public View onCreateView(LayoutInflater inflater , ViewGroup container , Bundle savedInstanceState ){

        String title = getResources().getString(R.string.recordEveryday);
        getActivity().setTitle(title);

        View rootView = inflater.inflate(R.layout.activity_record_page2,container,false);
        TextView temperature = (TextView) rootView.findViewById(R.id.temperature);
        temperature.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), temperature_main.class);
                startActivity(intent);
            }
        });
        TextView heartbeatTxt = rootView.findViewById(R.id.heartbeat);
        heartbeatTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setClass(getActivity(), heartandpressure_main.class);
                startActivity(intent);
            }
        });
        TextView bloodsugarTxt = rootView.findViewById(R.id.bloodsugar);
        bloodsugarTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setClass(getActivity(), bloodsugar_main.class);
                startActivity(intent);
            }
        });
        ImageView selectpreviewTxt =(ImageView) rootView.findViewById(R.id.historyview);
        selectpreviewTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setClass(getActivity(), recordHistoryActivity.class);
                startActivity(intent);
            }
        });
        TextView otherTxt = rootView.findViewById(R.id.other);
        otherTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Fragment selectedFragement = null ;
                selectedFragement = new recordPageOtherActivity2();
                getFragmentManager().beginTransaction().replace(R.id.fragment_container,selectedFragement).commit();
            }
        });

        return rootView;
    }

    }




