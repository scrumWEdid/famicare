package com.example.kavin.try5;

import android.content.DialogInterface;
import android.graphics.Color;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import jp.wasabeef.recyclerview.animators.SlideInUpAnimator;

public class CareTakerManagePermissionActivity extends AppCompatActivity {

    private DrawerLayout mDrawerLayout;
    private localDatabase localDB = null;
    private MyAdapter mAdapter;
    private RecyclerView mRecyclerView;
    private int count_accept = 0; //要求權現的人數
    private String user_id, patientID, patientName;
    private ArrayList<String> myDataset = new ArrayList<>(); //照顧者資料(先放要求權限的照顧者 再放要改權現的照顧者)
    private ArrayList<String> all_permission = new ArrayList<>(); //所有權現資料

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_care_taker_manage_permission);


        //-------開啟localDB
        localDB = new localDatabase(this);
        localDB.open();
        patientID = localDB.getpatientinfo();
        patientName = localDB.getpatientname();
        TextView patientinfo = findViewById(R.id.permission_patientinfo);
        String info = patientName + "(ID:" + patientID + ")";
        patientinfo.setText(info);
        //抓取mySQL中該被照顧者之所有照顧者的權限
        fetch_permission();

        //----------------------------------------
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp);

        TextView addBtn = findViewById(R.id.addimgBtn);
        addBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final EditText input = new EditText(CareTakerManagePermissionActivity.this);
                input.setSingleLine(true);
                LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.MATCH_PARENT);
                input.setLayoutParams(lp);

                new AlertDialog.Builder(CareTakerManagePermissionActivity.this)
                        .setView(input)
                        .setTitle("請輸入要新增的照顧者帳號")
                        .setMessage("帳號可至會員資料中查看")
                        .setNegativeButton(getString(R.string.sure), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialoginterface, int i)
                            {
                                String addUserAcc = input.getText().toString();
                                if(addUserAcc.equals(""))
                                    Toast.makeText(getApplicationContext(), getString(R.string.noblank), Toast.LENGTH_SHORT).show();
                                else {
                                    addpatient(addUserAcc);
                                }
                            }
                        })
                        .setPositiveButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialoginterface, int i)
                            {

                            }
                        })
                        .show();
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                CareTakerManagePermissionActivity.this.finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.game_menu, menu);
        //hide icon
        MenuItem userinfo = menu.findItem(R.id.user_info);
        userinfo.setIcon(android.R.color.transparent);
        return true;
    }

    //close localDB
    @Override
    protected void onDestroy() {
        super.onDestroy();
        localDB.close();
    }

    //spinner adapter
    ArrayAdapter<String> setupadapter(String[] item){
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, item){
            @Override
            public boolean isEnabled(int position){
               return true;
            }
            @Override
            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView tv = (TextView) view;
                tv.setTextColor(Color.BLACK);
                return view;
            }
        };
        return adapter;
    }

    //--------------------------connect to mySQL---------------------------
    private void fetch_permission(){
        myDataset.clear();
        all_permission.clear();
        //從mySQL抓權現
        String URL = "http://140.117.71.74/graduation/fetch_permission.php";
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            Log.d("書出訊息:", String.valueOf(jsonObject));
                            String success =jsonObject.getString("success");
                            if (success.equals("1")){
                                count_accept = jsonObject.getInt("count");
                                String name = "", per = "";
                                for(int i = 0; i < count_accept; i++) { //加入待審核的照顧者
                                    name = jsonObject.getJSONArray("person1").getJSONObject(i).getString("name");
                                    name = name + ',' + jsonObject.getJSONArray("person1").getJSONObject(i).getString("account");
                                    myDataset.add(name);
                                    all_permission.add(getString(R.string.waitjudge));
                                }
                                for(int i = 0; i < jsonObject.getInt("count2"); i++){
                                    name = jsonObject.getJSONArray("person2").getJSONObject(i).getString("name");
                                    name = name + ',' + jsonObject.getJSONArray("person2").getJSONObject(i).getString("account");
                                    per = jsonObject.getJSONArray("person2").getJSONObject(i).getString("permission");
                                    myDataset.add(name);
                                    all_permission.add(per);
                                }

                                //myDataset放道adapter中去建立recycler view
                                mAdapter = new MyAdapter(myDataset);
                                mRecyclerView = (RecyclerView) findViewById(R.id.list_view);
                                final LinearLayoutManager layoutManager = new LinearLayoutManager(CareTakerManagePermissionActivity.this);
                                layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
                                mRecyclerView.setLayoutManager(layoutManager);
                                mRecyclerView.setAdapter(mAdapter);
                                mRecyclerView.setItemAnimator(new SlideInUpAnimator());
                                mRecyclerView.getItemAnimator().setRemoveDuration(500);
                                mRecyclerView.getItemAnimator().setAddDuration(500);
                            }
                            else{
                                Toast.makeText(CareTakerManagePermissionActivity.this, getString(R.string.wronghappen), Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.d("錯誤訊息",e.toString());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
//                        Toast.makeText(addPantient.this,"搜尋失敗"+error.toString(),Toast.LENGTH_SHORT).show();
                    }
                })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String>params = new HashMap<>();
                params.put("id", patientID);
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);

    }

    private void update_permission(final String previlege, final String acc){
        //更新mySQL permission資料
        String URL = "http://140.117.71.74/graduation/update_permission.php";
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            Log.d("書出訊息:", String.valueOf(jsonObject));
                            String success =jsonObject.getString("success");
                            if (success.equals("1")){
                                Toast.makeText(CareTakerManagePermissionActivity.this, getString(R.string.updatesuccess), Toast.LENGTH_SHORT).show();
                            }
                            else{
                                Toast.makeText(CareTakerManagePermissionActivity.this, getString(R.string.wronghappen
                                ), Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.d("錯誤訊息",e.toString());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
//                        Toast.makeText(addPantient.this,"搜尋失敗"+error.toString(),Toast.LENGTH_SHORT).show();
                    }
                })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String>params = new HashMap<>();
                params.put("id", patientID);
                params.put("caretaker_acc", acc);
                params.put("permission", previlege);
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    private void addpatient(final String addUserAcc){
        //進SQL新增資料
        String URL = "http://140.117.71.74/graduation/add_patient.php";
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            Log.d("書出訊息:", String.valueOf(jsonObject));
                            String success =jsonObject.getString("success");
                            if (success.equals("1")){
                                String addsuccesswait = getResources().getString(R.string.addsuccess);
                                Toast.makeText(getApplicationContext(), addsuccesswait, Toast.LENGTH_SHORT).show();
                                fetch_permission();
                            }
                            else{
                                String message = jsonObject.getString("message");
                                Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.d("錯誤訊息",e.toString());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
//                        Toast.makeText(addPantient.this,"搜尋失敗"+error.toString(),Toast.LENGTH_SHORT).show();
                    }
                })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String>params = new HashMap<>();
                params.put("patient_id", patientID);
                params.put("caretaker_acc", addUserAcc);
                params.put("permission", "查看者");
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    //----------------------------以下皆為recyclerview有關的function
    public class MyAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
        private List<String> mData;
        //要求權現的viewholder
        public class ViewHolder1 extends RecyclerView.ViewHolder {
            public TextView mTextView, accTextView;
            public TextView acceptBtn, declineBtn;
            public ViewHolder1(View v) {
                super(v);
                mTextView = (TextView) v.findViewById(R.id.caretaker_text);
                accTextView = v.findViewById(R.id.caretaker_acc);
                acceptBtn = v.findViewById(R.id.accept_btn);
                declineBtn = v.findViewById(R.id.decline_btn);
            }

        }
        //改權限區塊的viewholder
        public class ViewHolder2 extends RecyclerView.ViewHolder {
            public TextView mTextView, accTextView;
            public Spinner dropdown;
            public ViewHolder2(View v) {
                super(v);
                mTextView = (TextView) v.findViewById(R.id.caretaker_text);
                accTextView = v.findViewById(R.id.caretaker_acc);
                dropdown = v.findViewById(R.id.drop_down_menu);
                String[] items = new String[]{getString(R.string.owner), getString(R.string.editor) , getString(R.string.viewer)};
                dropdown.setAdapter(setupadapter(items));
            }
        }

        public MyAdapter(List<String> data) {
            mData = data;
        }
        @Override
        public int getItemViewType(int position) {
            if(position < count_accept && all_permission.get(position).equals(getString(R.string.waitjudge)))
                return 0;
            else{
                return 1;
            }
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v;
            if(viewType == 0) {
                v = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.accept_listview, parent, false);
                return new ViewHolder1(v);
            }
            else{
                v = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.permission_listview, parent, false);
                return new ViewHolder2(v);
            }
        }

        @Override
        public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
            if(holder.getItemViewType() == 0){
                ViewHolder1 viewholder1 = (ViewHolder1)holder;
                String[] info = mData.get(position).split(",");
                viewholder1.mTextView.setText(info[0]);
                viewholder1.accTextView.setText(info[1]);
                final String caretaker_acc = viewholder1.accTextView.getText().toString();
                viewholder1.acceptBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        update_permission(getString(R.string.viewer), caretaker_acc);
                        all_permission.set(position, getString(R.string.viewer));
                        count_accept--;
                        notifyItemChanged(position);
                    }
                });
                viewholder1.declineBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        update_permission(getString(R.string.dagreeButton), caretaker_acc);
                        myDataset.remove(position);
                        count_accept--;
                        notifyItemRemoved(position);
                    }
                });
            }
            else{
                ViewHolder2 viewholder2 = (ViewHolder2)holder;
                String[] info = mData.get(position).split(",");
                viewholder2.mTextView.setText(info[0]);
                viewholder2.accTextView.setText(info[1]);
                final String caretaker_acc = viewholder2.accTextView.getText().toString();
                final int index = position;
                String[] items = null;
                switch(all_permission.get(index)){
                    case "擁有者" :
                        items = new String[]{"擁有者"};
                        viewholder2.dropdown.setAdapter(setupadapter(items));
                        viewholder2.dropdown.setSelection(0);
                        break;
                    case "編輯者" :
                        items = new String[]{"編輯者" , "查看者"};
                        viewholder2.dropdown.setAdapter(setupadapter(items));
                        viewholder2.dropdown.setSelection(0);
                        break;
                    case "查看者" :
                        items = new String[]{"編輯者" , "查看者"};
                        viewholder2.dropdown.setAdapter(setupadapter(items));
                        viewholder2.dropdown.setSelection(1);
                        break;
                }
                viewholder2.mTextView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Toast.makeText(CareTakerManagePermissionActivity.this, String.valueOf(position), Toast.LENGTH_SHORT).show();
                    }
                });
                viewholder2.dropdown.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                        String permission = adapterView.getSelectedItem().toString();
                        if(!permission.equals(all_permission.get(index))) { //跟本來的不銅才去更新資料庫
                            update_permission(permission, caretaker_acc);
                            all_permission.set(index, permission);
                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {

                    }
                });
            }
        }

        @Override
        public int getItemCount() {
            return mData.size();
        }
    }
}
