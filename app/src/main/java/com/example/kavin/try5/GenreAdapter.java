package com.example.kavin.try5;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.thoughtbot.expandablerecyclerview.MultiTypeExpandableRecyclerViewAdapter;
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;
import com.thoughtbot.expandablerecyclerview.models.ExpandableListPosition;
import com.thoughtbot.expandablerecyclerview.viewholders.ChildViewHolder;

import java.util.List;

/**
 * Created by Brad on 12/18/2016.
 */
public class GenreAdapter extends MultiTypeExpandableRecyclerViewAdapter<GenreViewHolder, ChildViewHolder> {

    public static final int HEARTBEATPRESSURE_VIEW_TYPE = 3;
    public static final int TEMPERATURE_VIEW_TYPE = 4;
    public static final int BLOODSUGAR_VIEW_TYPE = 5;

    public GenreAdapter(List<? extends ExpandableGroup> groups) {
        super(groups);
    }

    @Override
    public GenreViewHolder onCreateGroupViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_genre, parent, false);
        return new GenreViewHolder(view);
    }

    //    @Override
//    public TypeViewHolder onCreateChildViewHolder(ViewGroup parent, int viewType) {
//        View view = LayoutInflater.from(parent.getContext())
//                .inflate(R.layout.list_item, parent, false);
//        return new TypeViewHolder(view);
//    }
    @Override
    public ChildViewHolder onCreateChildViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case HEARTBEATPRESSURE_VIEW_TYPE:
                View type = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item, parent, false);
                return new TypeViewHolder(type);
            case TEMPERATURE_VIEW_TYPE:
                View temperature =
                        LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_temperature, parent, false);
                return new TemperatureViewHolder(temperature);
            case BLOODSUGAR_VIEW_TYPE:
                View bloodsugar =
                        LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_bloodsugar, parent, false);
                return new TemperatureViewHolder(bloodsugar);
            default:
                throw new IllegalArgumentException("Invalid viewType");
        }
    }

    @Override
    public void onBindChildViewHolder(ChildViewHolder holder, int flatPosition, ExpandableGroup group,
                                      int childIndex) {
        int viewType = getItemViewType(flatPosition);
        Type type = (Type) group.getItems().get(childIndex);
        switch (viewType) {
            case HEARTBEATPRESSURE_VIEW_TYPE:
                ((TypeViewHolder) holder).setItem_date(type.getItem_date());
                ((TypeViewHolder) holder).setItem_time(type.getItem_time());
                ((TypeViewHolder) holder).setItem_data1(type.getItem_data1());
                ((TypeViewHolder) holder).setItem_data2(type.getItem_data2());
                ((TypeViewHolder) holder).setItem_data3(type.getItem_data3());
                break;
            case BLOODSUGAR_VIEW_TYPE:
                ((BloodsugarViewHolder) holder).setB_item_date(type.getItem_date());
                ((BloodsugarViewHolder) holder).setB_item_time(type.getItem_time());
                ((BloodsugarViewHolder) holder).setB_item_bloodsugar(type.getItem_data1());
                ((BloodsugarViewHolder) holder).setB_item_timing(type.getItem_data2());
                break;
            case TEMPERATURE_VIEW_TYPE:
                ((TemperatureViewHolder) holder).setT_item_date(type.getItem_date());
                ((TemperatureViewHolder) holder).setT_item_time(type.getItem_time());
                ((TemperatureViewHolder) holder).setT_item_temperature(type.getItem_data1());
                break;

        }
    }

    @Override
    public void onBindGroupViewHolder(GenreViewHolder holder, int flatPosition, ExpandableGroup group) {
        holder.setGenreName(group.getTitle());
    }

    @Override
    public int getChildViewType(int position, ExpandableGroup group, int childIndex) {
        Type type = (Type) group.getItems().get(childIndex);
        if (type.getFlag().equals("3")) {
            return HEARTBEATPRESSURE_VIEW_TYPE;
      } else if(type.getFlag().equals("4")){
            return TEMPERATURE_VIEW_TYPE;
        } else if(type.getFlag().equals("5")){
            return BLOODSUGAR_VIEW_TYPE;
        }
        else return 1;

    }

    @Override
    public boolean isGroup(int viewType) {
        return viewType == ExpandableListPosition.GROUP;
    }

    @Override
    public boolean isChild(int viewType) {
        return viewType == HEARTBEATPRESSURE_VIEW_TYPE || viewType == TEMPERATURE_VIEW_TYPE||viewType == BLOODSUGAR_VIEW_TYPE;
    }
}
//    @Override
//    public void onBindChildViewHolder(TypeViewHolder holder, int flatPosition, ExpandableGroup group, int childIndex) {
//        Type type = (Type) group.getItems().get(childIndex);
//        holder.setItem_date(type.getItem_date());
//        holder.setItem_time(type.getItem_time());
//        holder.setItem_data1(type.getItem_data1());
//        holder.setItem_data2(type.getItem_data2());
//        holder.setItem_data3(type.getItem_data3());
//    }

