package com.example.kavin.try5;

import android.content.Intent;
import android.database.Cursor;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class bloodsugar_edit extends AppCompatActivity {
    private DrawerLayout mDrawerLayout;
    private localDatabase localDB = null;
    private Cursor cursor = null;
    private String user_id, user_acc, patient_id;
    private static final String RECORD_TYPE = "bloodsugar";
    private SimpleDateFormat sdf = new SimpleDateFormat("yyyy/M/dd HH:mm");
    private SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy/MM/dd a hh:mm");
    private String recordID;
    private TextView datetime;
    private EditText value;
    private RadioButton before_eat, after_eat, before_sleep;
    private Button submitBtn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bloodsugar_edit);
        BottomNavigationView bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottom_navigation_view);
        bottomNavigationView.setSelectedItemId(R.id.nav_description);
        bottomNavigationView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        //local dasabase
        localDB = new localDatabase(this);
        localDB.open();

        //----介面部分
        toolbar mytoolbar = new toolbar(this);
        mDrawerLayout = findViewById(R.id.drawer_layout);
// 介面部分-----
        datetime = findViewById(R.id.datetime);
        value = findViewById(R.id.value);
        before_eat = findViewById(R.id.radioButton);
        after_eat = findViewById(R.id.radioButton2);
        before_sleep = findViewById(R.id.radioButton3);

        Bundle bundle = this.getIntent().getExtras();
        String[] data = bundle.getStringArray("data");

        String format_time = data[0] + " " + data[1];
        try {
            Date dt = sdf.parse(format_time);
            format_time = sdf2.format(dt);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        datetime.setText(format_time);

        value.setText(data[2]);
        if(data[3].equals("飯前"))
            before_eat.setChecked(true);
        else if(data[3].equals("飯後"))
            after_eat.setChecked(true);
        else
            before_sleep.setChecked(true);
        recordID = data[4];

        submitBtn = findViewById(R.id.submitBtn);
        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                update_record(RECORD_TYPE);
            }
        });


        cursor = localDB.getinfo();
        if(cursor.getCount() >= 0){
            user_id = String.valueOf(cursor.getInt(0));
            user_acc = cursor.getString(1);
        }
        patient_id = localDB.getpatientinfo();
    }
    @Override
    protected void onDestroy(){
        super.onDestroy();
        localDB.close(); // 關閉資料庫
    }
    //----------------connect to mySQL------------------
    private void update_record(final String record_type){
//        String URL = "http://140.117.71.74/graduation/update_record.php";
        String URL = "http://140.117.71.74/graduation/update_record.php";
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            Log.d("書出訊息:", String.valueOf(jsonObject));
                            String success =jsonObject.getString("success");
                            String message = jsonObject.getString("message");
                            if (success.equals("1")){
                                Toast.makeText(bloodsugar_edit.this, message, Toast.LENGTH_SHORT).show();
                                //為了解決回傳後mpandroidchart崩潰的問題才這樣寫
                                Intent intent = new Intent();
                                intent.setClass(getApplication(), bloodsugar_main.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(intent);
                            }
                            else{
                                Toast.makeText(bloodsugar_edit.this, message, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.d("錯誤訊息",e.toString());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(bloodsugar_edit.this,getString(R.string.searchfail)+error.toString(),Toast.LENGTH_SHORT).show();
                    }
                })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String>params = new HashMap<>();
                params.put("record_type", record_type);
                params.put("caretakerID", user_id);
                params.put("recordID", recordID);
                params.put("value", value.getText().toString());
                String status = "";
                if(before_eat.isChecked())
                    status = "飯前";
                else if(after_eat.isChecked())
                    status = "飯後";
                else
                    status = "睡前";
                params.put("timing", status);
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }
    //--介面部分
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
//                mDrawerLayout.openDrawer(Gravity.RIGHT);
                bloodsugar_edit.this.finish();
                return true;
            case R.id.user_info:
                mDrawerLayout.openDrawer(Gravity.RIGHT);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.game_menu, menu);
        //set pattent name of menu
        MenuItem patientname = menu.findItem(R.id.patient_name);
        patientname.setTitle(localDB.getpatientname());
        return true;
    }
//    介面部分--

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected( MenuItem item) {
            BottomNavigation bn = new BottomNavigation(bloodsugar_edit.this, item.getItemId());
            return true;
        }

    };
}
