package com.example.kavin.try5;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class recordHistoryActivity extends AppCompatActivity {

    private DrawerLayout mDrawerLayout;
    private localDatabase localDB = null;
    //variables
    private RecyclerView mRecyclerView;
    private GenreAdapter mAdapter;
    private List<Genre> genres;
    private String patientID;
    private Button select_preview;
    List<Type> heartbeat = new ArrayList<>();
    List<Type> temperature = new ArrayList<>();
    List<Type> bloodsugar = new ArrayList<>();
    List<Type> pain =new ArrayList<>();
    List<Type> bathroom =new ArrayList<>();
    List<Type> other =new ArrayList<>();
    private static String URL_HEARTBEAT_PRESSURE = "http://140.117.71.74/graduation/fetch_heartandpressure_record.php";
    private static String URL_TEMPERATURE = "http://140.117.71.74/graduation/fetch_temperature_record.php";
    private static String URL_BLOODSUGAR = "http://140.117.71.74/graduation/fetch_bloodsugar_record.php";
    private static String URL_PAIN = "http://140.117.71.74/graduation/fetch_pain_record.php";
    private static String URL_BATHROOM = "http://140.117.71.74/graduation/fetch_bathroom_record.php";
    private static String URL_OTHER = "http://140.117.71.74/graduation/fetch_other_record.php";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recordhistoryactivity);

        BottomNavigationView bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottom_navigation_view);
        bottomNavigationView.setSelectedItemId(R.id.nav_description);
        bottomNavigationView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        //----open localDB
        localDB = new localDatabase(this);
        localDB.open();
//----介面部分
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp);

        mDrawerLayout = findViewById(R.id.drawer_layout);

        NavigationView navigationView = findViewById(R.id.nav_view);

        //drawer nav header on click的部分
        View headerview = navigationView.getHeaderView(0);

        TextView header_PatientID = headerview.findViewById(R.id.nav_headerPatientIDTextView);
        header_PatientID.setText("ID : " + localDB.getpatientinfo());
        TextView header_PatientName = headerview.findViewById(R.id.nav_headerPatientNameTextView);
        header_PatientName.setText(localDB.getpatientname());

        LinearLayout header = (LinearLayout) headerview.findViewById(R.id.nav_view_layout);
        header.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent patientInfoIntent = new Intent(getApplicationContext(),patientInfoActivity.class);
                startActivity(patientInfoIntent);
                mDrawerLayout.closeDrawers();
            }
        });

        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        // set item as selected to persist highlight
                        menuItem.setChecked(true);
                        // close drawer when item is tapped
                        mDrawerLayout.closeDrawers();

                        // Add code here to update the UI based on the item selected
                        // For example, swap UI fragments here

                        switch(menuItem.getItemId()){
                            case R.id.nav_switch_patient:
                                Intent switchIntent = new Intent(getApplicationContext(),ChoosePatientActivity.class);
                                startActivityForResult(switchIntent, 1);
                                mDrawerLayout.closeDrawers();
                                return true;
                            case R.id.nav_manage_patient:
                                Intent managePatientIntent = new Intent(getApplicationContext(),CareTakerManagePermissionActivity.class);
                                startActivity(managePatientIntent);
                                mDrawerLayout.closeDrawers();
                                return true;
                            case R.id.nav_member_info:
                                Intent MemberInfoIntent = new Intent(getApplicationContext(),MemberInfoActivity.class);
                                startActivity(MemberInfoIntent);
                                mDrawerLayout.closeDrawers();
                                return true;
                            case R.id.nav_setting:
                                return true;
                        }

                        return true;
                    }
                });
// 介面部分-----
        patientID =localDB.getpatientinfo();
        FetchHeartandPressure(patientID);
        FetchTemperature(patientID);
        FetchBloodSugar(patientID);
        FetchPain(patientID);
        FetchBathroom(patientID);
        FetchOther(patientID);
        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        getGenres();
        mAdapter = new GenreAdapter(genres);
        select_preview = findViewById(R.id.select_preview);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(recordHistoryActivity.this));
        mRecyclerView.setAdapter(mAdapter);
        select_preview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent selectpreviewIntent = new Intent(recordHistoryActivity.this, SelectPreviewActivity.class);
                recordHistoryActivity.this.startActivity(selectpreviewIntent);
            }
        });

    }

    public void getGenres() {
        genres = new ArrayList<>();
        genres.add(new Genre(getResources().getString(R.string.heartpressure), heartbeat));
        genres.add(new Genre(getResources().getString(R.string.temperature), temperature));
        genres.add(new Genre(getResources().getString(R.string.bloodsugar), bloodsugar));
        genres.add(new Genre(getResources().getString(R.string.pain),pain));
        genres.add(new Genre(getResources().getString(R.string.bathrrom),bathroom));
        genres.add(new Genre(getResources().getString(R.string.other),other));
    }


    private void FetchHeartandPressure(final String patientID) {

        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL_HEARTBEAT_PRESSURE,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONArray jsonArray = new JSONArray(response);
                            heartbeat.add(new Type(getResources().getString(R.string.date), getString(R.string.time), getString(R.string.heartbeat), getString(R.string.sbp), getString(R.string.dbp), "3"));
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject object = jsonArray.getJSONObject(i);
                                String date = object.getString("date").trim();
                                String time = object.getString("time").trim();
                                String heartbeatvalue = object.getString("heartbeatvalue").trim();
                                String tightpressurevalue = object.getString("tightpressurevalue").trim();
                                String relaxpressurevalue = object.getString("relaxpressurevalue").trim();
                                heartbeat.add(new Type(date, time, heartbeatvalue, tightpressurevalue, relaxpressurevalue, "3"));
//
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.d("錯誤訊息",e.toString());

                        }
                    }
                },
                new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("錯誤訊息",error.toString());
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("patientID", patientID);
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    private void FetchTemperature(final String patientID) {

        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL_TEMPERATURE,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONArray jsonArray = new JSONArray(response);
                            temperature.add(new Type(getString(R.string.date), getString(R.string.time), getString(R.string.temperature), "", "", "3"));
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject object = jsonArray.getJSONObject(i);
                                String date = object.getString("date").trim();
                                String time = object.getString("time").trim();
                                String temperaturevalue = object.getString("temperaturevalue").trim();
                                temperature.add(new Type(date, time, temperaturevalue, "", "", "3"));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.d("錯誤訊息",e.toString());

                        }
                    }
                },
                new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("錯誤訊息",error.toString());
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("patientID", patientID);
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    private void FetchBloodSugar(final String patientID) {

        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL_BLOODSUGAR,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONArray jsonArray = new JSONArray(response);
                            bloodsugar.add(new Type(getString(R.string.date), getString(R.string.time), getString(R.string.bloodsugar), getString(R.string.timeperiod), "", "3"));
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject object = jsonArray.getJSONObject(i);
                                String date = object.getString("date").trim();
                                String time = object.getString("time").trim();
                                String value = object.getString("value").trim();
                                String timing = object.getString("timing").trim();
                                bloodsugar.add(new Type(date, time, value, timing, "", "3"));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.d("錯誤訊息",e.toString());
                        }
                    }
                },
                new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("錯誤訊息",error.toString());
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("patientID", patientID);
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    private void FetchPain(final String patientID) {

        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL_PAIN,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONArray jsonArray = new JSONArray(response);
                            pain.add(new Type(getString(R.string.date), getString(R.string.time), getString(R.string.part), getString(R.string.level), "", "3"));
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject object = jsonArray.getJSONObject(i);
                                String date = object.getString("date").trim();
                                String time = object.getString("time").trim();
                                String painpart = object.getString("painpart").trim();
                                String painlevel = object.getString("painlevel").trim();
                                pain.add(new Type(date, time, painpart, painlevel,"", "3"));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.d("錯誤訊息",e.toString());

                        }
                    }
                },
                new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("錯誤訊息",error.toString());
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("patientID", patientID);
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    private void FetchBathroom(final String patientID) {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL_BATHROOM,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONArray jsonArray = new JSONArray(response);
                            bathroom.add(new Type(getString(R.string.date), getString(R.string.time), getString(R.string.condition), "", "", "3"));
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject object = jsonArray.getJSONObject(i);
                                String date = object.getString("date").trim();
                                String time = object.getString("time").trim();
                                String status = object.getString("status").trim();
                                bathroom.add(new Type(date, time, status, "","", "3"));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.d("錯誤訊息",e.toString());
                        }
                    }
                },
                new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("錯誤訊息",error.toString());
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("patientID", patientID);
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }
    private void FetchOther(final String patientID) {

        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL_OTHER,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONArray jsonArray = new JSONArray(response);
                            other.add(new Type(getString(R.string.date), getString(R.string.topic), getString(R.string.description), "", "", "3"));
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject object = jsonArray.getJSONObject(i);
                                String date = object.getString("date").trim();
                                String title = object.getString("title").trim();
                                String description = object.getString("description").trim();
                                other.add(new Type(date, title, description, "","", "3"));
//                                    Intent loginButtonIntent = new Intent(LoginActivity.this,success.class);
//                                    loginButtonIntent.putExtra("name", name);
//                                    startActivity(loginButtonIntent);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.d("錯誤訊息",e.toString());

                        }
                    }
                },
                new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("錯誤訊息",error.toString());
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("patientID", patientID);
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }


    //--介面部分
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
//                mDrawerLayout.openDrawer(Gravity.RIGHT);
                finish();
                return true;
            case R.id.user_info:
                mDrawerLayout.openDrawer(Gravity.RIGHT);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.game_menu, menu);
        //set pattent name of menu
        MenuItem patientname = menu.findItem(R.id.patient_name);
        patientname.setTitle(localDB.getpatientname());
        return true;
    }
//    介面部分--

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected( MenuItem item) {
            BottomNavigation bn = new BottomNavigation(recordHistoryActivity.this, item.getItemId());
            return true;
        }

    };

}
