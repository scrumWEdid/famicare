package com.example.kavin.try5;

import android.content.Intent;
import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class EditPasswordActivity extends AppCompatActivity {

    private localDatabase localDB = null;
    private Cursor cursor = null;
    private EditText pwd1, pwd2;
    private String user_acc, user_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_password);
        pwd1 = findViewById(R.id.editPw1);
        pwd2 = findViewById(R.id.editText9);
        final Button finishEditPw = (Button) findViewById(R.id.sendEditPw);
        finishEditPw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changepwd();
            }
        });

        //localdatabase
        localDB = new localDatabase(this);
        localDB.open();
        cursor =  localDB.getinfo();
        if(cursor.getCount() >= 0){
            user_acc = cursor.getString(1);
            user_id = String.valueOf(cursor.getInt(0));
        }
    }

    //----------------connect to mySQL----------------
    private void changepwd(){
        String URL = "http://140.117.71.74/graduation/update_pwd.php";
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            String success =jsonObject.getString("success");
                            if (success.equals("1")){
                                Toast.makeText(EditPasswordActivity.this, getString(R.string.editfail), Toast.LENGTH_SHORT).show();
                                finish();
                            }
                            else{
                                Toast.makeText(EditPasswordActivity.this, getString(R.string.editfail ), Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.d("錯誤訊息",e.toString());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(EditPasswordActivity.this,getString(R.string.editfail)+error.toString(),Toast.LENGTH_SHORT).show();
                    }
                })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String>params = new HashMap<>();
                params.put("id", user_id);
                params.put("account",user_acc);
                params.put("password", pwd1.getText().toString());
                params.put("password2", pwd2.getText().toString());
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    //close localdb
    @Override
    protected void onDestroy() {
        super.onDestroy();
        localDB.close();
    }
}
