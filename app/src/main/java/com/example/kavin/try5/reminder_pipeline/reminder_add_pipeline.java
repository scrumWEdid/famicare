package com.example.kavin.try5.reminder_pipeline;

import android.app.Notification;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.NavigationView;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.kavin.try5.BottomNavigation;
import com.example.kavin.try5.CareTakerManagePermissionActivity;
import com.example.kavin.try5.ChoosePatientActivity;
import com.example.kavin.try5.MemberInfoActivity;
import com.example.kavin.try5.R;
import com.example.kavin.try5.localDatabase;
import com.example.kavin.try5.patientInfoActivity;
import com.example.kavin.try5.toolbar;
import com.github.florent37.singledateandtimepicker.SingleDateAndTimePicker;
import com.github.florent37.singledateandtimepicker.dialog.SingleDateAndTimePickerDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static com.example.kavin.try5.notification.CHANNEL_1_ID;

public class reminder_add_pipeline extends AppCompatActivity {
    private DrawerLayout mDrawerLayout;
    private localDatabase localDB = null;
    private Cursor cursor = null;
    private String REMINDER_TYPE = "pipeline";
    private String user_id, patient_id;
    private NotificationManagerCompat notificationManager;

    private Spinner type_spinner;
    private String[] typeitem_TWN = {"管路種類", "尿管", "B管", "C管"};

    private EditText usehourEdt, useminEdt;
    private TextView startdatetime, enddatetime;
    private Date start_date;
    private SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm");
    private Button continueBtn, confirmBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reminder_add_pipeline);
        //----open localDB
        localDB = new localDatabase(this);
        localDB.open();
        BottomNavigationView bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottom_navigation_view);
        bottomNavigationView.setSelectedItemId(R.id.alarm);
        bottomNavigationView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

//----介面部分
        toolbar mytoolbar = new toolbar(this);
        mDrawerLayout = findViewById(R.id.drawer_layout);
// 介面部分-----
//----get string array from strings.xml
        String[] typeitem = getResources().getStringArray(R.array.pipetypeitem);
//get string array from strings.xml----

        cursor = localDB.getinfo();
        if(cursor.getCount() >= 0){
            user_id = String.valueOf(cursor.getInt(0));
        }
        patient_id = localDB.getpatientinfo();

        notificationManager = NotificationManagerCompat.from(this);

        type_spinner = findViewById(R.id.type_spinner);
        type_spinner.setAdapter(setupadapter(typeitem));

        usehourEdt = findViewById(R.id.usehourEdt);
        //設定結束時間
        usehourEdt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                setenddate();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        useminEdt = findViewById(R.id.useminEdt);
        useminEdt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                setenddate();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });


        startdatetime = findViewById(R.id.startdatetime);
        startdatetime.setText(sdf.format(new Date()));
        enddatetime = findViewById(R.id.enddatetime);
        start_date = new Date();
        startdatetime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                open_datetimepicker();
            }
        });

        continueBtn = findViewById(R.id.continueBtn);
        continueBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                add_reminder(REMINDER_TYPE, new VolleyCallback() {
                    @Override
                    public void onSuccess(String result) {
                        if(result.equals("1")){
                            setResult(RESULT_OK);
                            type_spinner.setSelection(0,true);
                            usehourEdt.setText("");
                            useminEdt.setText("");
                            startdatetime.setText(sdf.format(new Date()));
                            enddatetime.setText("");
                        }
                    }
                });
            }
        });

        confirmBtn = findViewById(R.id.confirmBtn);
        confirmBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                add_reminder(REMINDER_TYPE, new VolleyCallback() {
                    @Override
                    public void onSuccess(String result) {
                        if(result.equals("1")){

                            setResult(RESULT_OK);
                            finish();
                        }
                    }
                });

            }
        });
    }
    @Override
    protected void onDestroy(){
        super.onDestroy();
        localDB.close(); // 關閉資料庫
    }

    //spinner adapter
    ArrayAdapter<String> setupadapter(String[] item){
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, item){
            @Override
            public boolean isEnabled(int position){
                if(position == 0)
                    return false;
                else
                    return true;
            }
            @Override
            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView tv = (TextView) view;
                if(position == 0){
                    // Set the hint text color gray
                    tv.setTextColor(Color.GRAY);
                }
                else {
                    tv.setTextColor(Color.BLACK);
                }
                return view;
            }
        };
        return adapter;
    }

    //open_datetimepicker
    private void open_datetimepicker() {
        new SingleDateAndTimePickerDialog.Builder(reminder_add_pipeline.this)
//                .bottomSheet()
                .displayAmPm(false)
                .minDateRange(new Date())
                .displayListener(new SingleDateAndTimePickerDialog.DisplayListener() {
                    @Override
                    public void onDisplayed(SingleDateAndTimePicker picker) {
                        picker.setDefaultDate(new Date());
                    }
                })
                .title("起始時間")
                .listener(new SingleDateAndTimePickerDialog.Listener() {
                    @Override
                    public void onDateSelected(Date date) {
                        start_date = date;
                        String select_date = sdf.format(date);
                        startdatetime.setText(select_date);
                        setenddate();
                    }
                }).display();
    }
    //set enddate
    private void setenddate(){
        Calendar cal = Calendar.getInstance();
        cal.setTime(start_date);
        if(!usehourEdt.getText().toString().isEmpty())
            cal.add(Calendar.HOUR, Integer.valueOf(usehourEdt.getText().toString()));
        if(!useminEdt.getText().toString().isEmpty())
            cal.add(Calendar.MINUTE, Integer.valueOf(useminEdt.getText().toString()));
        enddatetime.setText(sdf.format(cal.getTime()));
    }


    //send to channel to create notification (Undone)
    public void sendOnChannel(){
        Notification notification = new NotificationCompat.Builder(this, CHANNEL_1_ID)
               .setSmallIcon(R.drawable.ic_access_alarm_black_24dp)
               .setContentTitle("提醒標題")
               .setContentText("提醒內容")
               .setPriority(NotificationCompat.PRIORITY_HIGH)
               .setCategory(NotificationCompat.CATEGORY_MESSAGE)
               .build();

        notificationManager.notify(1, notification);
    }

    //--------connect to MySQL----------
    public interface VolleyCallback{
        void onSuccess(String result);
    }
    private void add_reminder(final String record_type, final VolleyCallback callback){
        String URL = "http://140.117.71.74/graduation/add_reminder.php";
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            Log.d("書出訊息:", String.valueOf(jsonObject));
                            String success =jsonObject.getString("success");
                            String message = jsonObject.getString("message");
                            if (success.equals("1")){
                                Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                                callback.onSuccess(success);
                            }
                            else{
                                Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.d("錯誤訊息",e.toString());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(),getString(R.string.searchfail)+error.toString(),Toast.LENGTH_SHORT).show();
                    }
                })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String>params = new HashMap<>();
                params.put("patientid", patient_id);
                params.put("caretakerid", user_id);
                params.put("record_type", record_type);
                params.put("startdatetime", startdatetime.getText().toString());
                params.put("enddatetime", enddatetime.getText().toString());
                params.put("type", typeitem_TWN[type_spinner.getSelectedItemPosition()]);

                int duration = 0;
                if(!usehourEdt.getText().toString().isEmpty())
                    duration += Integer.valueOf(usehourEdt.getText().toString()) * 60;
                if(!useminEdt.getText().toString().isEmpty())
                    duration += Integer.valueOf(useminEdt.getText().toString());
                params.put("duration", String.valueOf(duration));
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }
    //--介面部分
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
//                mDrawerLayout.openDrawer(Gravity.RIGHT);
                finish();
                return true;
            case R.id.user_info:
                mDrawerLayout.openDrawer(Gravity.RIGHT);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.game_menu, menu);
        //set pattent name of menu
        MenuItem patientname = menu.findItem(R.id.patient_name);
        patientname.setTitle(localDB.getpatientname());
        return true;
    }
//    介面部分--

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected( MenuItem item) {
            BottomNavigation bn = new BottomNavigation(reminder_add_pipeline.this, item.getItemId());
            return true;
        }

    };

}
