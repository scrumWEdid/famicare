package com.example.kavin.try5.reminder_pipeline;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.kavin.try5.R;

import org.json.JSONException;

import java.util.Arrays;
import java.util.List;

public class pipeline_weekday_adapter extends RecyclerView.Adapter<pipeline_weekday_adapter.ViewHolder> {
    private List<pipeline_weekday_Listitem> listItem;
    private Context context;
    boolean flag = false;
    private reminder_pipeline_weekday main;
    int row_index = -1;
    public pipeline_weekday_Listitem checked[];
    public String checkedid[];
    public String checkedtime[];

    public pipeline_weekday_adapter(List<pipeline_weekday_Listitem> listItem, Context context, reminder_pipeline_weekday inder) {
        this.listItem = listItem;
        this.context = context;
        this.main= inder;
    }


    @Override
    public ViewHolder onCreateViewHolder( ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.reminder_pipeline_weekday_list_item,parent,false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, final int position) {
        checked = new  pipeline_weekday_Listitem[listItem.size()];
        checkedid = new String[listItem.size()];
        checkedtime = new  String[listItem.size()];
        final pipeline_weekday_Listitem weekdayListitem = listItem.get(position);
        viewHolder.textView_reinder_time.setText(weekdayListitem.getReminder_pipeline_time());

        String[] typeitem_TWN = {"管路種類", "尿管", "B管", "C管"};
        int index = Arrays.asList(typeitem_TWN).indexOf(weekdayListitem.getReminder_pipeline_name());
        String s = context.getResources().getStringArray(R.array.pipetypeitem)[index];
        viewHolder.textView_reinder_pipeline_name.setText(s);

        if (flag==true){
            viewHolder.checkbox_reinder.setVisibility(View.VISIBLE);
            pipeline_weekday_common.currentItem=null;
            pipeline_weekday_common.position=-1;
            main.ShowCancel(true);
            main.ShowDone(true);
            main.ShowView(false);

        }
        else{
            viewHolder.checkbox_reinder.setVisibility(View.INVISIBLE);
            main.ShowCancel(false);


        }

        viewHolder.checkbox_reinder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (viewHolder.checkbox_reinder.isChecked()){
                    checked[position]=listItem.get(position);
                    try {
                        checkedid[position]=listItem.get(position).getReminder_pipeline_id();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    checkedtime[position]=listItem.get(position).getReminder_pipeline_time();

                }else{
                    checked[position]=null;
                }
            }
        });
        if (checked[position]==null){
            viewHolder.checkbox_reinder.setChecked(false);
        }
//        if (viewHolder.checkbox_reinder.isChecked()==true){
//            checked[position]=listItem.get(position);
//        }
//        if (!viewHolder.checkbox_reinder.isChecked()){
//            checked[position]=null;
//
//        }
//        viewHolder.setItemClickListener(new pipeline_weekday_ItemClickListener() {
//            @Override
//            public void onClick(View view, int position) {
//                row_index =position;
//                pipeline_weekday_common.currentItem=listItem.get(position);
//                notifyDataSetChanged();
//                Log.d("asd","asd");
//            }
//        });
        viewHolder.viewForeground.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (flag==false){
                    notifyDataSetChanged();
                    row_index =position;
                    pipeline_weekday_common.currentItem=listItem.get(position);
                    pipeline_weekday_common.position=position;
                    notifyDataSetChanged();
                    main.ShowDone(true);
                    main.ShowView(true);
                }


//                Intent intent = new Intent();
//                intent.setClass(context, reminder_view_pipeline.class);
//                Bundle bundle = new Bundle();
//                bundle.putString("pipeline", weekdayListitem.getReinder_pipeline_info().toString());
//                intent.putExtras(bundle);
//                ((Activity)context).startActivityForResult(intent, 1);
            }
        });
//        viewHolder.textView_reinder_pipeline_name.setOnLongClickListener(new View.OnLongClickListener() {
//            @Override
//            public boolean onLongClick(View view) {
//                flag=true;
//                notifyDataSetChanged();
//                return true;
//            }
//        });

        if(row_index==position&&flag ==false){
            viewHolder.viewForeground.setBackgroundColor(Color.parseColor("#d5d5e2"));
        }
        else{
            viewHolder.viewForeground.setBackgroundColor(Color.parseColor("#FFFFFF"));

        }
    }

    @Override
    public int getItemCount() {
        return listItem.size();
    }
    public void removeItem(int position) {
        listItem.remove(position);
        // notify the item removed by position
        // to perform recycler view delete animations
        // NOTE: don't call notifyDataSetChanged()
        notifyItemRemoved(position);
    }

    public void restoreItem(pipeline_weekday_Listitem item, int position) {
        listItem.add(position, item);
        // notify item added by position
        notifyItemInserted(position);
    }




    public class ViewHolder extends RecyclerView.ViewHolder  {
        public TextView textView_reinder_time;
        public TextView textView_reinder_pipeline_name;
        public CheckBox checkbox_reinder;
        //            pipeline_weekday_ItemClickListener itemClickListener;
//            public void setItemClickListener(pipeline_weekday_ItemClickListener itemClickListener){
//                this.itemClickListener = itemClickListener;
//            }
        public RelativeLayout viewBackground, viewForeground;
        public Button cancel ;
        public ViewHolder( View itemView) {
            super(itemView);
            textView_reinder_time = (TextView) itemView.findViewById(R.id.reminder_pipeline_time);
            textView_reinder_pipeline_name = (TextView)itemView.findViewById(R.id.reminder_pipeline_name);
            checkbox_reinder= (CheckBox)itemView.findViewById(R.id.checkbox_reminder);
            viewBackground = itemView.findViewById(R.id.view_background);
            viewForeground = itemView.findViewById(R.id.view_foreground);
//            itemView.setOnClickListener(this);
            viewForeground.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    flag=true;
                    notifyDataSetChanged();
                    return true;
                }
            });


        }


//        @Override
//        public void onClick(View view) {
//            itemClickListener.onClick(view,getAdapterPosition());
//            Log.d("asd","asd");
//
//        }
    }

}
