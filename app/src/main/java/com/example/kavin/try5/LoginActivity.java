package com.example.kavin.try5;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class LoginActivity extends AppCompatActivity {
    private localDatabase localDB = null;
    private EditText account, password;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
//        LoginActivity.this.deleteDatabase("local.db"); //功能都好之後再刪掉
        account = findViewById(R.id.accountEditText);
        password = findViewById(R.id.passwordEditText);
        final Button registerLink = (Button) findViewById(R.id.LoginRegisterButton);
        registerLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent registerIntent = new Intent(LoginActivity.this,agreementActivity.class);
                LoginActivity.this.startActivity(registerIntent);
            }
        });


        final Button forgetPasswordLink = (Button) findViewById(R.id.LoginForgotPasswordButton);
        forgetPasswordLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent forgetPasswordIntent = new Intent(LoginActivity.this,forgetPasswordActivity.class);
                LoginActivity.this.startActivity(forgetPasswordIntent);
            }
        });

        final Button loginButton = (Button) findViewById(R.id.loginButton);
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String Account = account.getText().toString().trim();
                ;
                String Password = password.getText().toString().trim();
                if (Account.isEmpty()) {
                    Toast toast = Toast.makeText(LoginActivity.this,
                            getString(R.string.pleaseenteraccount), Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER, 0, -100);
                    toast.show();
                } else if (Password.isEmpty()){
                    Toast toast = Toast.makeText(LoginActivity.this, getString(R.string.pleaseenterpassword), Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER, 0, -100);
                    toast.show();
                }
                else
                    Login(Account,Password);

            }
        });

        localDB = new localDatabase(this);
        if(localDB.open()){
            Log.d("訊息", localDB.getpatientinfo());
            Intent intent = new Intent();
            intent.setClass(LoginActivity.this, indexActivity.class);
            startActivity(intent);
            finish();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(localDB.open()) {
            localDB.close();
        }
    }

    private void Login(final String account, final String password) {
        final String URL_LOGIN = "http://140.117.71.74/graduation/login.php";
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL_LOGIN,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            String success =jsonObject.getString("success");
                            String message = jsonObject.getString("message");
                            Log.d("asd", String.valueOf(jsonObject));
                            if (success.equals("1")){
                                JSONArray jsonArray = jsonObject.getJSONArray("login");
                                for (int i = 0 ; i <jsonArray.length(); i++){
                                    JSONObject object = jsonArray.getJSONObject(i);
                                    String acc = object.getString("account").trim();
                                    String id = object.getString("caretakerID").trim();
                                    Toast.makeText(LoginActivity.this, message, Toast.LENGTH_SHORT).show();

                                    Bundle bundle = new Bundle();
                                    bundle.putString("userid", id);
                                    bundle.putString("useracc", acc);
                                    bundle.putString("fromWhere", "login");
                                    
                                    Intent loginIntent = new Intent(LoginActivity.this,ChoosePatientActivity.class);
                                    loginIntent.putExtras(bundle);
                                    LoginActivity.this.startActivity(loginIntent);
                                }
                            }
                            else{
                                Toast.makeText(LoginActivity.this, message, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(LoginActivity.this,"error"+e.toString(),Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(LoginActivity.this,"error"+error.toString(),Toast.LENGTH_SHORT).show();
                    }
                })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String>params = new HashMap<>();
                params.put("account",account);
                params.put("password",password);
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

}
