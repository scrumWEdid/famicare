package com.example.kavin.try5.reminder_measure;

import android.app.TimePickerDialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.kavin.try5.R;
import com.example.kavin.try5.localDatabase;
import com.example.kavin.try5.toolbar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class reminder_edit_measure extends AppCompatActivity {
    private DrawerLayout mDrawerLayout;
    private localDatabase localDB = null;
    private toolbar mytoolbar;
    private Cursor cursor = null;
    private String REMINDER_TYPE = "measure";
    private String user_id, patient_id;

    private TextView typeTxt;
    private TextView[] taketime = new TextView[4];
    private ImageView clockimg;
    private Spinner repeat_spinner, repeat_spinner2;

    private Spinner taketime_spinner;
    private Button confirmBtn;

    private String[] typeitem_TWN = {"測量項目", "體溫", "心跳血壓", "血糖"};
    private String[] repeatitem_TWN = {"每日", "每隔", "每週"};
    private String[] repeat2item_TWN = {"0","1天","2天","3天","4天","5天"};
    private String[] taketimeitem_TWN = {"0", "1次", "2次", "3次", "4次"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reminder_edit_measure);
        //----open localDB
        localDB = new localDatabase(this);
        localDB.open();
//----介面部分
        mytoolbar = new toolbar(this);
        mDrawerLayout = findViewById(R.id.drawer_layout);
// 介面部分-----
//----get string array from strings.xml
        String[] repeatitem = getResources().getStringArray(R.array.time);
        String[] repeat2item = getResources().getStringArray(R.array.days);
        String[] taketimeitem = getResources().getStringArray(R.array.times);
//get string array from strings.xml----
        cursor = localDB.getinfo();
        if(cursor.getCount() >= 0){
            user_id = String.valueOf(cursor.getInt(0));
        }
        patient_id = localDB.getpatientinfo();

        typeTxt = findViewById(R.id.typeTxt);
        clockimg = findViewById(R.id.clockimage);
        taketime[0] = findViewById(R.id.taketimetxt1);
        taketime[1] = findViewById(R.id.taketimetxt2);
        taketime[2] = findViewById(R.id.taketimetxt3);
        taketime[3] = findViewById(R.id.taketimetxt4);
        for(int i = 0; i < 4; i++){ //set timepicker when onclick
            final int finalI = i;
            taketime[i].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    opentimepicker(finalI);
                }
            });
        }

        taketime_spinner = findViewById(R.id.taketime_spinner);
        taketime_spinner.setAdapter(setupadapter(taketimeitem));
        taketime_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                if(position > 0){
                    clockimg.setVisibility(View.VISIBLE);
                    int i = 0;
                    for(i = 0; i < position; i++)
                        taketime[i].setVisibility(View.VISIBLE);
                    for(; i < 4; i++)
                        taketime[i].setVisibility(View.GONE);
                }
                else{
                    clockimg.setVisibility(View.GONE);
                    for(int i = 0; i < 4; i++)
                        taketime[i].setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        repeat_spinner = findViewById(R.id.repeat_spinner);
        repeat_spinner2 = findViewById(R.id.repeat_spinner2);

        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, repeatitem);
        adapter.setDropDownViewResource(R.layout.spinner_item);
        repeat_spinner.setAdapter(adapter);

        repeat_spinner2.setAdapter(setupadapter(repeat2item));
        repeat_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if(i == 1)
                    repeat_spinner2.setVisibility(View.VISIBLE);
                else
                    repeat_spinner2.setVisibility(View.GONE);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        confirmBtn = findViewById(R.id.confirmBtn);
        confirmBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                JSONObject medicineInfo = null;
                try {
                    medicineInfo = new JSONObject(getIntent().getStringExtra("reminder"));
                    String pipelineReminderID = medicineInfo.getString("recordReminderID");
                    update_reminder(pipelineReminderID, new VolleyCallback() {
                        @Override
                        public void onSuccess(String result) {
                            if(result.equals("1")){
                                set_return_data();
                            }
                        }
                    });
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });

        fetch_data(); //顯示資料
    }
    @Override
    protected void onDestroy(){
        super.onDestroy();
        localDB.close(); // 關閉資料庫
    }


    //spinner adapter
    ArrayAdapter<String> setupadapter(String[] item){
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, item){
            @Override
            public boolean isEnabled(int position){
                if(position == 0)
                    return false;
                else
                    return true;
            }
            @Override
            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView tv = (TextView) view;
                if(position == 0){
                    // Set the hint text color gray
                    tv.setTextColor(Color.GRAY);
                }
                else {
                    tv.setTextColor(Color.BLACK);
                }
                return view;
            }
        };
        adapter.setDropDownViewResource(R.layout.spinner_item);
        return adapter;
    }

    //open timepickerdialog
    private void opentimepicker(final int position){
        int hour, minute;
        String[] time= taketime[position].getText().toString().split(":");
        hour = Integer.valueOf(time[0]);
        minute = Integer.valueOf(time[1]);
//        Calendar cal = Calendar.getInstance();
//        int hour = cal.get(Calendar.HOUR_OF_DAY);
//        int minute = cal.get(Calendar.MINUTE);
        new TimePickerDialog(reminder_edit_measure.this, new TimePickerDialog.OnTimeSetListener(){
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                String h = String.valueOf(hourOfDay);
                String m = String.valueOf(minute);
                if(hourOfDay < 10)
                    h = "0" + h;
                if(minute < 10)
                    m = "0" + m;
                taketime[position].setText(h + ":" + m);
            }
        }, hour, minute, false).show();
    }


    //設定編輯畫面
    private void fetch_data(){
        try {
            int position;
            String s = "";
            JSONObject reminder =  new JSONObject(getIntent().getStringExtra("reminder"));
            position = Arrays.asList(typeitem_TWN).indexOf(reminder.getString("type"));
            s = getResources().getStringArray(R.array.typeitem)[position];
            typeTxt.setText(s);

            position = Arrays.asList(taketimeitem_TWN).indexOf(reminder.getString("recordtimes") + "次");
            taketime_spinner.setSelection(position);
            JSONArray remindertime = reminder.getJSONArray("remindertime");
            for(int i = 0; i < remindertime.length(); i++)
                taketime[i].setText(remindertime.getString(i).substring(0, 5));
            int repeat_day = Integer.valueOf(reminder.getString("repeatday"));
            if (repeat_day == 1)
                repeat_spinner.setSelection(0);
            else if (repeat_day == 7)
                repeat_spinner.setSelection(2);
            else {
                repeat_spinner.setSelection(1);
                position = Arrays.asList(repeat2item_TWN).indexOf(repeat_day + "天");
                repeat_spinner2.setSelection(position);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    //回傳更新的data
    private void set_return_data(){
        try {
            JSONObject reminder =  new JSONObject(getIntent().getStringExtra("reminder"));
            reminder.put("recordtimes", taketimeitem_TWN[taketime_spinner.getSelectedItemPosition()].substring(0,1));

            JSONArray remindertime = reminder.getJSONArray("remindertime");
            int times = Integer.valueOf(taketimeitem_TWN[taketime_spinner.getSelectedItemPosition()].substring(0,1));
            for(int i = 0; i < times; i++){
                remindertime.put(i, taketime[i].getText().toString());
            }

            String repeat = "";
            if(repeat_spinner.getSelectedItemPosition() == 0)
                repeat = "1";
            else if(repeat_spinner.getSelectedItemPosition() == 1)
                repeat = repeat2item_TWN[repeat_spinner2.getSelectedItemPosition()].substring(0,1);
            else
                repeat = "7";
            reminder.put("repeatday", repeat);

            //回傳
            Intent intent = new Intent();
            intent.putExtra("reminder", reminder.toString());
            setResult(RESULT_OK, intent);
            finish();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    //--------connect to MySQL----------
    public interface VolleyCallback{
        void onSuccess(String result);
    }
    private void update_reminder(final String reminderID, final VolleyCallback callback){
        String URL = "http://140.117.71.74/graduation/update_reminder.php";
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            Log.d("書出訊息:", String.valueOf(jsonObject));
                            String success =jsonObject.getString("success");
                            String message = jsonObject.getString("message");
                            if (success.equals("1")){
                                Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                                callback.onSuccess(success);
                            }
                            else{
                                Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.d("錯誤訊息",e.toString());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(),getResources().getString(R.string.searchfail)+error.toString(),Toast.LENGTH_SHORT).show();
                    }
                })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String>params = new HashMap<>();
                params.put("recordReminderID", reminderID);
                params.put("record_type", REMINDER_TYPE);
                params.put("recordtimes", taketimeitem_TWN[taketime_spinner.getSelectedItemPosition()].substring(0,1));
                String repeat = "";
                if(repeat_spinner.getSelectedItemPosition() == 1)
                    repeat = "1";
                else if(repeat_spinner.getSelectedItemPosition() == 2)
                    repeat = repeat2item_TWN[repeat_spinner2.getSelectedItemPosition()].substring(0,1);
                else
                    repeat = "7";
                params.put("repeatday", repeat);
                int times = Integer.valueOf(taketimeitem_TWN[taketime_spinner.getSelectedItemPosition()].substring(0,1));
                for(int i = 0; i < times; i++){
                    params.put("taketime"+i, taketime[i].getText().toString());
                }
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }
    //--介面部分
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
//                mDrawerLayout.openDrawer(Gravity.RIGHT);
                finish();
                return true;
            case R.id.user_info:
                mDrawerLayout.openDrawer(Gravity.RIGHT);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.game_menu, menu);
        //set pattent name of menu
        MenuItem patientname = menu.findItem(R.id.patient_name);
        patientname.setTitle(localDB.getpatientname());
        return true;
    }
//    介面部分--
}
