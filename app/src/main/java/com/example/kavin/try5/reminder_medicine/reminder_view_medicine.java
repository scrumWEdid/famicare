package com.example.kavin.try5.reminder_medicine;

import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.kavin.try5.R;
import com.example.kavin.try5.localDatabase;
import com.example.kavin.try5.toolbar;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class reminder_view_medicine extends AppCompatActivity {
    private DrawerLayout mDrawerLayout;
    private localDatabase localDB = null;
    private Cursor cursor = null;
    private toolbar mytoolbar;
    private String RECORD_TYPE = "medicine";
    private JSONObject record;
    private TextView name, method, taketype, taketime, takeamount, repeatday, remark;
    private TextView[] taketimetxt = new TextView[4];
    private TextView editBtn, delBtn;
    private ImageView medicineImg;
    private Button confirmBtn;

    private String[] taketimingitem_TWN = {"飯前","飯後","不限"};
    private String[] takeunititem_TWN = {"片","粒","cc","mg","個","滴","劑","單位"};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reminder_view_medicine);
        //----open localDB
        localDB = new localDatabase(this);
        localDB.open();
//----介面部分
        mytoolbar = new toolbar(this);
        mDrawerLayout = findViewById(R.id.drawer_layout);
// 介面部分-----
        name = findViewById(R.id.name);
        medicineImg = findViewById(R.id.medicineImg);
        method = findViewById(R.id.method);
        taketype = findViewById(R.id.taketype);
        taketime = findViewById(R.id.taketime);
        takeamount = findViewById(R.id.takeamount);
//        repeatday = findViewById(R.id.repeatday);
        remark = findViewById(R.id.remark);
        taketimetxt[0] = findViewById(R.id.taketimetxt1);
        taketimetxt[1] = findViewById(R.id.taketimetxt2);
        taketimetxt[2] = findViewById(R.id.taketimetxt3);
        taketimetxt[3] = findViewById(R.id.taketimetxt4);

        fetch_data(getIntent()); //顯示資料

        editBtn = findViewById(R.id.editimgBtn);
        editBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent();
                intent.setClass(reminder_view_medicine.this, reminder_edit_medicine.class);
                Bundle bundle = new Bundle();
                bundle.putString("medicine", record.toString());
                intent.putExtras(bundle);
                startActivityForResult(intent, 1);

            }
        });
        delBtn = findViewById(R.id.clearBtn);
        delBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new AlertDialog.Builder(reminder_view_medicine.this)
                        .setTitle(getString(R.string.suredelete))
                        .setMessage(getString(R.string.deletenorecovery))
                        .setNegativeButton(getString(R.string.sure), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialoginterface, int i)
                            {
                                delete_reminder(RECORD_TYPE, record);
                            }
                        })
                        .setPositiveButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialoginterface, int i)
                            {

                            }
                        })
                        .show();
            }
        });
        confirmBtn = findViewById(R.id.confirmBtn);
        confirmBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setResult(RESULT_OK);
                finish();
            }
        });

    }
    @Override
    protected void onDestroy(){
        super.onDestroy();
        localDB.close(); // 關閉資料庫
    }

    //設定顯示資料
    private void fetch_data(Intent intent){
        try {
            record = new JSONObject(intent.getStringExtra("medicine"));

            String imgURL = "http://140.117.71.74/graduation/medicineimage/"+record.getString("engname") + ".jpg";
            Picasso.get()
                    .load(imgURL)
                    .error(R.drawable.ic_add_a_photo_black_24dp)
                    .into(medicineImg);

            name.setText(record.getString("name"));
            method.setText(record.getString("method"));

            String s = "", temp = "";
            int index = 0;
            s = record.getString("takedatevalue");
            taketype.setText(s);

            index = Arrays.asList(taketimingitem_TWN).indexOf(record.getString("taketiming"));
            temp = getResources().getStringArray(R.array.eattime)[index];
            s = getString(R.string.oneday) + record.getString("taketime") + getString(R.string.times) + temp;
            taketime.setText(s);

            index = Arrays.asList(takeunititem_TWN).indexOf(record.getString("takeunit"));
            temp = getResources().getStringArray(R.array.units)[index];
            s = getString(R.string.onetime) + record.getString("takeamount") + " " + temp;
            takeamount.setText(s);

            remark.setText(record.getString("remark"));

//            if(record.getString("repeatday").equals("1"))
//                s = "每日";
//            else if(record.getString("repeatday").equals("7"))
//                s = "每周";
//            else
//                s = "每隔" + record.getString("repeatday") + "天";
//            repeatday.setText(s);

            JSONArray remindertime = record.getJSONArray("remindertime");
            for(int i = 0; i < 4; i++){ //因為可能會從編輯畫面回來 所以先重置成都沒顯示時間
                taketimetxt[i].setVisibility(View.INVISIBLE);
            }
            for(int i = 0; i < Integer.valueOf(record.getString("taketime")); i++) {
                taketimetxt[i].setText(remindertime.getString(i).substring(0, 5));
                taketimetxt[i].setVisibility(View.VISIBLE);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /*下一頁修改完畢回傳更新資料*/
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == 1){
            if(resultCode == RESULT_OK){
                fetch_data(data);
                setResult(RESULT_OK);
            }
        }
    }

    //-----------connect to MYSQL--------------
    private void delete_reminder(final String RECORD_TYPE, final JSONObject record){
        String URL = "http://140.117.71.74/graduation/delete_reminder.php";
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            Log.d("書出訊息:", String.valueOf(jsonObject));
                            String success =jsonObject.getString("success");
                            String message = jsonObject.getString("message");
                            if (success.equals("1")){
                                Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                                setResult(RESULT_OK);
                                finish();
                            }
                            else{
                                Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.d("錯誤訊息",e.toString());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(),getString(R.string.searchfail)+error.toString(),Toast.LENGTH_SHORT).show();
                    }
                })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String>params = new HashMap<>();
                params.put("record_type", RECORD_TYPE);
                try {
                    params.put("personalMedicineID", record.getString("personalMedicineID"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }
    //--介面部分
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
//                mDrawerLayout.openDrawer(Gravity.RIGHT);
                finish();
                return true;
            case R.id.user_info:
                mDrawerLayout.openDrawer(Gravity.RIGHT);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.game_menu, menu);
        //set pattent name of menu
        MenuItem patientname = menu.findItem(R.id.patient_name);
        patientname.setTitle(localDB.getpatientname());
        return true;
    }
//    介面部分--
}
