package com.example.kavin.try5;


import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.facebook.accountkit.Account;
import com.facebook.accountkit.AccountKit;
import com.facebook.accountkit.AccountKitCallback;
import com.facebook.accountkit.AccountKitError;
import com.facebook.accountkit.AccountKitLoginResult;
import com.facebook.accountkit.PhoneNumber;
import com.facebook.accountkit.ui.AccountKitActivity;
import com.facebook.accountkit.ui.AccountKitConfiguration;
import com.facebook.accountkit.ui.LoginType;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;

import static com.facebook.accountkit.ui.LoginType.EMAIL;
import static com.facebook.accountkit.ui.LoginType.PHONE;

public class forgetPasswordActivity extends AppCompatActivity {
    private static String URL_Validation = "http://140.117.71.74/graduation/forgetpassword.php";
    private static String phonenumber = "09123";
    private static String phoneNumberCountryCode = "+886";
    private EditText forgetPasswordAccountInput,forgetPasswordPhoneInput;
    private Button forgetPasswordSendButton;
    private final static int REQUEST_CODE =999;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget_password);
        forgetPasswordAccountInput = findViewById(R.id.forgetPasswordAccountInput);
        forgetPasswordPhoneInput = findViewById(R.id.forgetPasswordPhoneInput);
        forgetPasswordSendButton = findViewById(R.id.forgetPasswordSendButton);
        forgetPasswordSendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String account = forgetPasswordAccountInput.getText().toString().trim();
                String phone = forgetPasswordPhoneInput.getText().toString().trim();
                validation(account,phone);

            }
        });
    }
    private void startLoginButton(LoginType LoginType) {
        if (LoginType == EMAIL)
        {
            Intent intent =new Intent(this, AccountKitActivity.class);
            AccountKitConfiguration.AccountKitConfigurationBuilder configurationBuilder =
                    new AccountKitConfiguration.AccountKitConfigurationBuilder(LoginType.EMAIL,
                            AccountKitActivity.ResponseType.TOKEN) ; //use Token when 啟用用戶端存取權杖流程

            intent.putExtra(
                    AccountKitActivity.ACCOUNT_KIT_ACTIVITY_CONFIGURATION,
                    configurationBuilder.build());
            intent.putExtra(AccountKitActivity.ACCOUNT_KIT_ACTIVITY_CONFIGURATION,configurationBuilder.build());
            startActivityForResult(intent,REQUEST_CODE);
        }
        else if(LoginType == LoginType.PHONE)
        {
            Intent intent =new Intent(this, AccountKitActivity.class);
            AccountKitConfiguration.AccountKitConfigurationBuilder configurationBuilder =
                    new AccountKitConfiguration.AccountKitConfigurationBuilder(LoginType.PHONE,
                            AccountKitActivity.ResponseType.TOKEN);//use Token when 啟用用戶端存取權杖流程
            configurationBuilder.setInitialPhoneNumber( new PhoneNumber( phoneNumberCountryCode, phonenumber ) );
            intent.putExtra(AccountKitActivity.ACCOUNT_KIT_ACTIVITY_CONFIGURATION,configurationBuilder.build());
            startActivityForResult(intent,REQUEST_CODE);
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode ==REQUEST_CODE)
        {
            AccountKitLoginResult result =data.getParcelableExtra(AccountKitLoginResult.RESULT_KEY);
            if(result.getError() !=null)
            {
                Toast.makeText(this,""+result.getError().getErrorType().getMessage(),Toast.LENGTH_SHORT).show();
                return;
            }
            else if(result.wasCancelled())
            {
                Toast.makeText(this,"Cancel",Toast.LENGTH_SHORT).show();

                return;
            }
            else
            {
                if(result.getAccessToken()!=null){
                    //Toast.makeText(this,"success! %s"+result.getAuthorizationCode().substring(0,10),Toast.LENGTH_SHORT).show();


                    AccountKit.getCurrentAccount(new AccountKitCallback<Account>() {
                        @Override
                        public void onSuccess(Account account) {


                            //check phone
                            String userPhone = account.getPhoneNumber().toString();
                            userPhone = userPhone.substring(userPhone.length() - 9);
                            phonenumber =userPhone.substring(userPhone.length() - 9);

                            if(userPhone.equals(phonenumber)){
                                Intent it = new Intent(forgetPasswordActivity.this,indexActivity.class);
                                startActivity(it);
                            }
                            else{

                                Log.d("message",userPhone+"    " + phonenumber);
                                Toast.makeText(forgetPasswordActivity.this,"帳號以及帳號驗證錯誤",Toast.LENGTH_SHORT).show();
                            }

                        }

                        @Override
                        public void onError(AccountKitError accountKitError) {

                        }
                    });
                }

            }
        }
    }
    private void validation(final String account, final String phone) {


        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL_Validation,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {

                            JSONObject jsonObject = new JSONObject(response);
                            String success =jsonObject.getString("success");
                            JSONArray jsonArray = jsonObject.getJSONArray("validation");
                            Log.d("message", String.valueOf(jsonArray));
                            if (success.equals("1")){
                                for (int i = 0 ; i <jsonArray.length(); i++){
                                    JSONObject object = jsonArray.getJSONObject(i);
                                    String phone = object.getString("phone").trim();
                                    String account = object.getString("account").trim();
                                    phonenumber =phone ;
                                    startLoginButton(PHONE);
                                }

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(forgetPasswordActivity.this,getString(R.string.accountwrong),Toast.LENGTH_SHORT).show();
                            Log.d("message",e.toString());

                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(forgetPasswordActivity.this,getString(R.string.accountfail),Toast.LENGTH_SHORT).show();
                        Log.d("message",error.toString());
                    }
                })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String>params = new HashMap<>();
                params.put("account",account);
                params.put("phone",phone);
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }


    private void printKeyHash() {
        try{
            PackageInfo info = getPackageManager().getPackageInfo(
                    "com.example.mis108.careapplication",
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures){
                MessageDigest md =MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KEYHASH", Base64.encodeToString(md.digest(),Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }
}
