package com.example.kavin.try5;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class temperature_alarm extends AppCompatActivity {
    private DrawerLayout mDrawerLayout;
    private localDatabase localDB = null;
    private Cursor cursor = null;
    private final static  String RECORD_TYPE = "temperature";
    private EditText topvalue, bottomvalue;
    private Button submitBtn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_temperature_alarm);

        BottomNavigationView bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottom_navigation_view);
        bottomNavigationView.setSelectedItemId(R.id.nav_description);
        bottomNavigationView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        localDB = new localDatabase(this);
        localDB.open();

        //----介面部分
        toolbar mytoolbar = new toolbar(this);
        mDrawerLayout = findViewById(R.id.drawer_layout);
// 介面部分-----

        topvalue = findViewById(R.id.temperature_alarm_top);
        bottomvalue = findViewById(R.id.temperature_alarm_bottom);
        submitBtn = findViewById(R.id.temperature_alarm_submitBtn);
//        //返回按鈕
//        TextView backBtn = findViewById(R.id.backBtn);
//        backBtn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                setResult(RESULT_OK);
//                finish();
//            }
//        });
        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String top = topvalue.getText().toString();
                String bottom = bottomvalue.getText().toString();
                if(top.equals("")) {
                    top = "0";
                }
                if(bottom.equals("")){
                    bottom = "0";
                }
                if(Double.valueOf(top) < Double.valueOf(bottom)) {
                    Toast.makeText(temperature_alarm.this, getString(R.string.wronglimitsetting), Toast.LENGTH_SHORT).show();
                }
                else {
                    localDB.update_alert(top, bottom, RECORD_TYPE);
                    setResult(RESULT_OK);
                    finish();
                }
            }
        });

        cursor = localDB.get_alert(RECORD_TYPE);
        if(cursor.getCount() > 0) {
            if (cursor.getInt(1) != 0)
                topvalue.setText(String.valueOf(cursor.getDouble(1)));
            if (cursor.getInt(2) != 0)
                bottomvalue.setText(String.valueOf(cursor.getDouble(2)));
        }
    }

    //--介面部分
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
//                mDrawerLayout.openDrawer(Gravity.RIGHT);
                temperature_alarm.this.finish();
                return true;
            case R.id.user_info:
                mDrawerLayout.openDrawer(Gravity.RIGHT);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.game_menu, menu);
        //set pattent name of menu
        MenuItem patientname = menu.findItem(R.id.patient_name);
        patientname.setTitle(localDB.getpatientname());
        return true;
    }
//    介面部分--

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected( MenuItem item) {
            BottomNavigation bn = new BottomNavigation(temperature_alarm.this, item.getItemId());
            return true;
        }

    };
    //close DB
    @Override
    protected void onDestroy() {
        super.onDestroy();
        localDB.close();
    }
}
