package com.example.kavin.try5.reminder_pipeline;

import android.content.Intent;
import android.database.Cursor;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.kavin.try5.BottomNavigation;
import com.example.kavin.try5.R;
import com.example.kavin.try5.localDatabase;
import com.example.kavin.try5.toolbar;
import com.github.florent37.singledateandtimepicker.SingleDateAndTimePicker;
import com.github.florent37.singledateandtimepicker.dialog.SingleDateAndTimePickerDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class reminder_edit_pipeline extends AppCompatActivity {
    private DrawerLayout mDrawerLayout;
    private localDatabase localDB = null;
    private toolbar mytoolbar;
    private Cursor cursor = null;
    private String REMINDER_TYPE = "pipeline";
    private String user_id, patient_id;

    private TextView typeTxt;
    private EditText usehourEdt, useminEdt;
    private TextView startdatetime, enddatetime;
    private Date start_date;
    private SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm");
    private SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private Button confirmBtn;

    private String[] typeitem_TWN = {"管路種類", "尿管", "B管", "C管"};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reminder_edit_pipeline);
        //----open localDB
        localDB = new localDatabase(this);
        localDB.open();
        BottomNavigationView bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottom_navigation_view);
        bottomNavigationView.setSelectedItemId(R.id.alarm);
        bottomNavigationView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

//----介面部分
        mytoolbar = new toolbar(this);
        mDrawerLayout = findViewById(R.id.drawer_layout);
// 介面部分-----
        cursor = localDB.getinfo();
        if(cursor.getCount() >= 0){
            user_id = String.valueOf(cursor.getInt(0));
        }
        patient_id = localDB.getpatientinfo();
        

        typeTxt = findViewById(R.id.typeTxt);
        usehourEdt = findViewById(R.id.usehourEdt);
        //設定結束時間
        usehourEdt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                setenddate();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        useminEdt = findViewById(R.id.useminEdt);
        useminEdt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                setenddate();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });


        startdatetime = findViewById(R.id.startdatetime);
        startdatetime.setText(sdf.format(new Date()));
        enddatetime = findViewById(R.id.enddatetime);
        start_date = new Date();
        startdatetime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                open_datetimepicker();
            }
        });
        
        confirmBtn = findViewById(R.id.confirmBtn);
        confirmBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                JSONObject medicineInfo = null;
                try {
                    medicineInfo = new JSONObject(getIntent().getStringExtra("reminder"));
                    String pipelineReminderID = medicineInfo.getString("pipelineReminderID");
                    update_reminder(pipelineReminderID, new VolleyCallback() {
                        @Override
                        public void onSuccess(String result) {
                            if(result.equals("1")){
                                set_return_data();
                            }
                        }
                    });
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });

        fetch_data(); //顯示資料
    }
    @Override
    protected void onDestroy(){
        super.onDestroy();
        localDB.close(); // 關閉資料庫
    }
    

    //open_datetimepicker
    private void open_datetimepicker() {
        new SingleDateAndTimePickerDialog.Builder(reminder_edit_pipeline.this)
//                .bottomSheet()
                .displayAmPm(false)
//                .minDateRange(new Date())
                .displayListener(new SingleDateAndTimePickerDialog.DisplayListener() {
                    @Override
                    public void onDisplayed(SingleDateAndTimePicker picker) {
                        picker.setDefaultDate(start_date);
                    }
                })
                .title(getString(R.string.starttime))
                .listener(new SingleDateAndTimePickerDialog.Listener() {
                    @Override
                    public void onDateSelected(Date date) {
                        start_date = date;
                        String select_date = sdf.format(date);
                        startdatetime.setText(select_date);
                        setenddate();
                    }
                }).display();
    }
    //set enddate
    private void setenddate(){
        Calendar cal = Calendar.getInstance();
        cal.setTime(start_date);
        if(!usehourEdt.getText().toString().isEmpty())
            cal.add(Calendar.HOUR, Integer.valueOf(usehourEdt.getText().toString()));
        if(!useminEdt.getText().toString().isEmpty())
            cal.add(Calendar.MINUTE, Integer.valueOf(useminEdt.getText().toString()));
        enddatetime.setText(sdf.format(cal.getTime()));
    }


    //設定編輯畫面
    private void fetch_data(){
        try {
            JSONObject reminder =  new JSONObject(getIntent().getStringExtra("reminder"));

            int index = Arrays.asList(typeitem_TWN).indexOf(reminder.getString("type"));
            String s = getResources().getStringArray(R.array.pipetypeitem)[index];
            typeTxt.setText(s);

            start_date = sdf2.parse(reminder.getString("startdatetime"));
            startdatetime.setText(sdf.format(start_date));
            int duration = Integer.valueOf(reminder.getString("duration"));
            usehourEdt.setText(String.valueOf(duration / 60));
            useminEdt.setText(String.valueOf(duration % 60));
            setenddate();
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    //回傳更新的data
    private void set_return_data(){
        try {
            JSONObject reminder =  new JSONObject(getIntent().getStringExtra("reminder"));

            reminder.put("startdatetime", sdf2.format(sdf.parse(startdatetime.getText().toString())));
            reminder.put("enddatetime", sdf2.format(sdf.parse(enddatetime.getText().toString())));

            int duration = 0;
            if(!usehourEdt.getText().toString().isEmpty())
                duration += Integer.valueOf(usehourEdt.getText().toString()) * 60;
            if(!useminEdt.getText().toString().isEmpty())
                duration += Integer.valueOf(useminEdt.getText().toString());
            reminder.put("duration", String.valueOf(duration));

            //回傳
            Intent intent = new Intent();
            intent.putExtra("reminder", reminder.toString());
            setResult(RESULT_OK, intent);
            finish();
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    //--------connect to MySQL----------
    public interface VolleyCallback{
        void onSuccess(String result);
    }
    private void update_reminder(final String reminderID, final VolleyCallback callback){
        String URL = "http://140.117.71.74/graduation/update_reminder.php";
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            Log.d("書出訊息:", String.valueOf(jsonObject));
                            String success =jsonObject.getString("success");
                            String message = jsonObject.getString("message");
                            if (success.equals("1")){
                                Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                                callback.onSuccess(success);
                            }
                            else{
                                Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.d("錯誤訊息",e.toString());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(),getString(R.string.searchfail)+error.toString(),Toast.LENGTH_SHORT).show();
                    }
                })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String>params = new HashMap<>();
                params.put("pipelineReminderID", reminderID);
                params.put("record_type", REMINDER_TYPE);
                params.put("startdatetime", startdatetime.getText().toString());
                params.put("enddatetime", enddatetime.getText().toString());
                params.put("type", typeTxt.getText().toString());
                int duration = 0;
                if(!usehourEdt.getText().toString().isEmpty())
                    duration += Integer.valueOf(usehourEdt.getText().toString()) * 60;
                if(!useminEdt.getText().toString().isEmpty())
                    duration += Integer.valueOf(useminEdt.getText().toString());
                params.put("duration", String.valueOf(duration));
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }
    //--介面部分
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
//                mDrawerLayout.openDrawer(Gravity.RIGHT);
                finish();
                return true;
            case R.id.user_info:
                mDrawerLayout.openDrawer(Gravity.RIGHT);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.game_menu, menu);
        //set pattent name of menu
        MenuItem patientname = menu.findItem(R.id.patient_name);
        patientname.setTitle(localDB.getpatientname());
        return true;
    }
//    介面部分--

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected( MenuItem item) {
            BottomNavigation bn = new BottomNavigation(reminder_edit_pipeline.this, item.getItemId());
            return true;
        }

    };
}
