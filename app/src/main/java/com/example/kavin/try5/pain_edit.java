package com.example.kavin.try5;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class pain_edit extends AppCompatActivity {
    private DrawerLayout mDrawerLayout;
    private localDatabase localDB = null;
    private Cursor cursor = null;
    private String user_id, user_acc, patient_id;
    private static final String RECORD_TYPE = "pain";
    private SimpleDateFormat sdf = new SimpleDateFormat("yyyy/M/dd HH:mm");
    private SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy/MM/dd a hh:mm");
    private String recordID;
    private Spinner part, locate, locate2, level;
    private EditText hour, minute;
    private TextView datetime;
    private CheckBox[] painstatusCheckbox = new CheckBox[6];
    private Button submitBtn;
    private String[] data;

    private String[][] pain_locate_TWN = {
            {},
            {"位置", "額頭到頭頂之間(額葉)", "頭頂到後腦勺之間(頂葉)", "後腦勺(枕葉)", "臉頰後之腦部(顳葉)", "後腦勺到脖子之間(腦幹)"},
            {"位置", "耳朵", "鼻子", "牙齒", "眼睛", "臉頰", "額頭"},
            {"位置", "手指", "手掌", "手腕", "小手臂(前臂)", "大手臂(上臂)"},
            {"位置", "肩膀", "頸部"},
            {"位置", "大腿", "膝蓋", "小腿", "腳踝", "腳掌", "腳趾"},
            {"位置", "胸部", "肺", "心臟", "腹部", "大小腸", "胃", "肝"},
            {"位置", "上背部", "下背部", "脊椎", "腎臟"},
            {"位置", "子宮", "卵巢", "膀胱", "尿道"}
    };
    private String[] pain_part_TWN = {"部位", "頭部", "臉部", "手", "肩頸", "腳", "胸腹", "背部", "其他"};
    private String[] pain_locate2_TWN = {"左右邊", "左邊", "右邊"};
    private String[] pain_level_TWN = {"選取等級", "1","2", "3", "4", "5"};
    private String[] painCheckboxText_TWN = {"紅", "腫", "癢", "神經痛", "肌肉痛", "表皮挫傷"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pain_edit);
        BottomNavigationView bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottom_navigation_view);
        bottomNavigationView.setSelectedItemId(R.id.nav_description);
        bottomNavigationView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        //local dasabase
        localDB = new localDatabase(this);
        localDB.open();
        cursor = localDB.getinfo();
        if(cursor.getCount() >= 0){
            user_id = String.valueOf(cursor.getInt(0));
            user_acc = cursor.getString(1);
        }
        patient_id = localDB.getpatientinfo();

//----介面部分
        toolbar mytoolbar = new toolbar(this);
        mDrawerLayout = findViewById(R.id.drawer_layout);
// 介面部分-----

        datetime = findViewById(R.id.datetime);
        hour = findViewById(R.id.hourEdt);
        minute = findViewById(R.id.minuteEdt);

        part = findViewById(R.id.spinner_part);
        String[] pain_part = getResources().getStringArray(R.array.painpart);
        ArrayAdapter<String> adapter = setupadapter(pain_part);
        part.setAdapter(adapter);

        locate = findViewById(R.id.spinner_locate);
        final String[][] pain_locate = {
                {},
                getResources().getStringArray(R.array.painlocatearray1),
                getResources().getStringArray(R.array.painlocatearray2),
                getResources().getStringArray(R.array.painlocatearray3),
                getResources().getStringArray(R.array.painlocatearray4),
                getResources().getStringArray(R.array.painlocatearray5),
                getResources().getStringArray(R.array.painlocatearray6),
                getResources().getStringArray(R.array.painlocatearray7),
                getResources().getStringArray(R.array.painlocatearray8)
        };

        ArrayAdapter<String> adapter2 = setupadapter(pain_locate[0]);
        locate.setAdapter(adapter2);

        locate2 = findViewById(R.id.spinner_locate2);
        String[] pain_locate2 =  getResources().getStringArray(R.array.painlocate2);;
        ArrayAdapter<String> adapter4 = setupadapter(pain_locate2);
        locate2.setAdapter(adapter4);

        level = findViewById(R.id.spinner_level);
        String[] pain_level =  getResources().getStringArray(R.array.painlevel);;
        ArrayAdapter<String> adapter3 = setupadapter(pain_level);
        level.setAdapter(adapter3);

        part.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if(i == 0){
                    locate.setVisibility(View.INVISIBLE);
                    locate2.setVisibility(View.INVISIBLE);
                }
                else{
                    locate.setVisibility(View.VISIBLE);
                    locate2.setVisibility(View.VISIBLE);
                    locate.setAdapter(setupadapter(pain_locate[i]));
                    int position = Arrays.asList(pain_locate_TWN[i]).indexOf(data[3]);
                    locate.setSelection(position);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        painstatusCheckbox[0] = findViewById(R.id.checkBox1);
        painstatusCheckbox[1] = findViewById(R.id.checkBox2);
        painstatusCheckbox[2] = findViewById(R.id.checkBox3);
        painstatusCheckbox[3] = findViewById(R.id.checkBox4);
        painstatusCheckbox[4] = findViewById(R.id.checkBox5);
        painstatusCheckbox[5] = findViewById(R.id.checkBox6);

        Bundle bundle = this.getIntent().getExtras();
        data = bundle.getStringArray("data");
        String format_time = data[0] + " " + data[1];
        try {
            Date dt = sdf.parse(format_time);
            format_time = sdf2.format(dt);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        datetime.setText(format_time);

        part.setSelection(Arrays.asList(pain_part_TWN).indexOf(data[2]));
        locate2.setSelection(Arrays.asList(pain_locate2_TWN).indexOf(data[4]));
        level.setSelection(Arrays.asList(pain_level_TWN).indexOf(data[6]));

        hour.setText(String.valueOf( Integer.valueOf(data[5])/60) );
        minute.setText(String.valueOf( Integer.valueOf(data[5])%60) );

        String[] painstatus_data = data[7].split(",");
        for(int i = 0, j = 0; i < 6; i++){
            if(painCheckboxText_TWN[i].equals(painstatus_data[j])){
                painstatusCheckbox[i].setChecked(true);
                j++;
                if(j == painstatus_data.length)
                    break;
            }
        }

        recordID = data[8];
        submitBtn = findViewById(R.id.submitBtn);
        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                update_record(RECORD_TYPE);
            }
        });

    }

    @Override
    protected void onDestroy(){
        super.onDestroy();
        localDB.close(); // 關閉資料庫
    }

    //spinner adapter
    ArrayAdapter<String> setupadapter(String[] item){
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, item){
            @Override
            public boolean isEnabled(int position){
                if(position == 0)
                    return false;
                else
                    return true;
            }
            @Override
            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView tv = (TextView) view;
                if(position == 0){
                    // Set the hint text color gray
                    tv.setTextColor(Color.GRAY);
                }
                else {
                    tv.setTextColor(Color.BLACK);
                }
                return view;
            }
        };
        adapter.setDropDownViewResource(R.layout.spinner_item);
        return adapter;
    }

    //回傳更新的data
    private void set_return_data(){

        data[2] = pain_part_TWN[part.getSelectedItemPosition()];
        data[3] = pain_locate_TWN[part.getSelectedItemPosition()][locate.getSelectedItemPosition()];
        data[4] = pain_locate2_TWN[locate2.getSelectedItemPosition()];
        data[6] = pain_level_TWN[level.getSelectedItemPosition()];

        int duration = Integer.valueOf(hour.getText().toString()) * 60;
        duration = duration + Integer.valueOf(minute.getText().toString());
        data[5] = String.valueOf(duration);

        String painstatus = "";
        for(int i = 0; i < 6; i++) {
            if (painstatusCheckbox[i].isChecked())
                painstatus += painCheckboxText_TWN[i] + ",";
        }
        data[7] = painstatus;

        Intent intent = new Intent();
        Bundle bundle = new Bundle();
        bundle.putStringArray("data", data);
        intent.putExtras(bundle);
        setResult(RESULT_OK, intent);
        finish();
    }
    private void update_record(final String record_type){
//        String URL = "http://140.117.71.74/graduation/update_record.php";
        String URL = "http://140.117.71.74/graduation/update_record.php";
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            Log.d("書出訊息:", String.valueOf(jsonObject));
                            String success =jsonObject.getString("success");
                            String message = jsonObject.getString("message");
                            if (success.equals("1")){
                                Toast.makeText(pain_edit.this, message, Toast.LENGTH_SHORT).show();
                                set_return_data();
                            }
                            else{
                                Toast.makeText(pain_edit.this, message, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.d("錯誤訊息",e.toString());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(pain_edit.this,getString(R.string.searchfail)+error.toString(),Toast.LENGTH_SHORT).show();
                    }
                })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String>params = new HashMap<>();
                params.put("record_type", record_type);
                params.put("caretakerID", user_id);
                params.put("recordID", recordID);
                params.put("painpart", pain_part_TWN[part.getSelectedItemPosition()]);
                params.put("painlocate", pain_locate_TWN[part.getSelectedItemPosition()][locate.getSelectedItemPosition()]);
                params.put("painlocate2", pain_locate2_TWN[locate2.getSelectedItemPosition()]);
                params.put("painlevel", pain_level_TWN[level.getSelectedItemPosition()]);

                int duration = Integer.valueOf(hour.getText().toString()) * 60;
                duration = duration + Integer.valueOf(minute.getText().toString());
                params.put("duration", String.valueOf(duration));

                String painstatus = "";
                for(int i = 0; i < 6; i++) {
                    if (painstatusCheckbox[i].isChecked())
                        painstatus += painCheckboxText_TWN[i] + ",";
                }
                params.put("painstatus", painstatus);

                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    //--介面部分
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
//                mDrawerLayout.openDrawer(Gravity.RIGHT);
                pain_edit.this.finish();
                return true;
            case R.id.user_info:
                mDrawerLayout.openDrawer(Gravity.RIGHT);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.game_menu, menu);
        //set pattent name of menu
        MenuItem patientname = menu.findItem(R.id.patient_name);
        patientname.setTitle(localDB.getpatientname());
        return true;
    }
//    介面部分--

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected( MenuItem item) {
            BottomNavigation bn = new BottomNavigation(pain_edit.this, item.getItemId());
            return true;
        }

    };
}
