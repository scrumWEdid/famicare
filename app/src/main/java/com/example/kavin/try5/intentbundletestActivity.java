package com.example.kavin.try5;

import android.content.Intent;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class intentbundletestActivity extends AppCompatActivity {
    private DrawerLayout mDrawerLayout;
    private localDatabase localDB = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intentbundletest);


        BottomNavigationView bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottom_navigation_view);
        bottomNavigationView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        String type;
        Bundle extra = getIntent().getExtras();
        if(extra!=null){
           type = extra.getString("type");
           switch (type){
               case "record":
                   this.setTitle(R.string.recordEveryday);
                   bottomNavigationView.setSelectedItemId(R.id.nav_description);
                   break;
               case "reminder":
                   this.setTitle(R.string.reminder);
                   bottomNavigationView.setSelectedItemId(R.id.alarm);
                   break;
               case "mypillbox":
                   this.setTitle(R.string.mypillbox);
                   bottomNavigationView.setSelectedItemId(R.id.drug);
                   break;
               case "video":
                   this.setTitle(R.string.video);
                   bottomNavigationView.setSelectedItemId(R.id.videoPage);
                   break;
               default:
                   return;
           }

        }

        //local dasabase
        localDB = new localDatabase(this);
        localDB.open();

        //----介面部分
        toolbar mytoolbar = new toolbar(this);
        mDrawerLayout = findViewById(R.id.drawer_layout);
// 介面部分-----
    }

    //--介面部分
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Fragment f = getSupportFragmentManager().findFragmentById(R.id.fragment_container);
                if(f instanceof recordPageOtherActivity2) {
                    Fragment selectedFragement = new recordPageActivity2();
                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, selectedFragement).commit();
                }
                else
                    intentbundletestActivity.this.finish();
                return true;
            case R.id.user_info:
                mDrawerLayout.openDrawer(Gravity.RIGHT);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.game_menu, menu);
        //set pattent name of menu
        MenuItem patientname = menu.findItem(R.id.patient_name);
        patientname.setTitle(localDB.getpatientname());
        return true;
    }
//    介面部分--

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected( MenuItem item) {
            Fragment selectedFragement = null ;
            switch (item.getItemId()){
                case R.id.nav_home:
                    intentbundletestActivity.this.finish();
                     break;
                case R.id.alarm:
                    intentbundletestActivity.this.setTitle(R.string.reminder);
                    selectedFragement = new reminder_mainPage2();
                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,selectedFragement).commit();
                    break;
                case R.id.drug:
                    intentbundletestActivity.this.setTitle(R.string.mypillbox);
                    selectedFragement = new myPillbox_main2();
                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,selectedFragement).commit();
                    break;
                case R.id.nav_description:
                    intentbundletestActivity.this.setTitle(R.string.recordEveryday);
                    selectedFragement = new recordPageActivity2();
                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,selectedFragement).commit();
                    break;
                case R.id.videoPage:
                    intentbundletestActivity.this.setTitle(R.string.video);
                    selectedFragement = new videoMainPage2();
                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,selectedFragement).commit();
                    break;
            }





            return true;
        }

    };

}
