package com.example.kavin.try5.reminder_medicine;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.kavin.try5.BottomNavigation;
import com.example.kavin.try5.CareTakerManagePermissionActivity;
import com.example.kavin.try5.ChoosePatientActivity;
import com.example.kavin.try5.MemberInfoActivity;
import com.example.kavin.try5.R;
import com.example.kavin.try5.localDatabase;
import com.example.kavin.try5.patientInfoActivity;
import com.example.kavin.try5.toolbar;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

public class reminder_edit_medicine extends AppCompatActivity {
    private DrawerLayout mDrawerLayout;
    private localDatabase localDB = null;
    private Cursor cursor = null;

    private JSONObject record;
    private String REMINDER_TYPE = "medicine";
    private String user_id, patient_id;
    private TextView medicine_name;
    private ImageView medicineImg;
    private TextView[] taketime = new TextView[4];
    private ImageView clockimg;

    private Spinner takemethod_spinner;
    private String[] takemethoditem_TWN = {"用法", "口服", "注射", "外用", "其他"};

    private Spinner takeday1Spn, takeday2Spn, taketimingSpn;
    private String[] takeday1item_TWN = {"每日", "每隔", "每週"};
    private String[] takeday2item_TWN = {"0", "1天","2天", "3天", "4天", "5天"}; //選每隔X天的話
    private String[] takeday2item2_TWN = {"0", "1次", "2次", "3次", "4次"}; //選每日的話
    private int takeday2pos = 0; //存takeday2位置 之後takeday2spn setadapter後再設定位置
    private String[] taketimingitem_TWN = { "飯前","飯後","不限"};

    private Spinner taketiming_spinner;
    private Spinner takeunit_spinner;
    private EditText takeamountEdt, takedayEdt;
    private String[] takeunititem_TWN = {"片", "粒", "cc", "mg", "個", "滴", "劑", "單位"};
    private EditText remark;
    private Button submitBtn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reminder_edit_medicine);

        //----open localDB
        localDB = new localDatabase(this);
        localDB.open();

        BottomNavigationView bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottom_navigation_view);
        bottomNavigationView.setSelectedItemId(R.id.nav_description);
        bottomNavigationView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

//----介面部分
        toolbar mytoolbar = new toolbar(this);
        mDrawerLayout = findViewById(R.id.drawer_layout);
//----介面部分
//----get string array from strings.xml
        String[] takemethoditem=getResources().getStringArray(R.array.usemethods);
        String[] takeday1item=getResources().getStringArray(R.array.time);
        final String[] takeday2item= getResources().getStringArray(R.array.days);
        final String[] takeday2item2=getResources().getStringArray(R.array.times);
        String[] taketimingitem=getResources().getStringArray(R.array.eattime);
        String[] takeunititem=getResources().getStringArray(R.array.units);
//get string array from strings.xml----
        cursor = localDB.getinfo();
        if(cursor.getCount() >= 0){
            user_id = String.valueOf(cursor.getInt(0));
        }
        patient_id = localDB.getpatientinfo();

        medicine_name = findViewById(R.id.medicine_name);
        medicineImg = findViewById(R.id.medicineImg);
        remark = findViewById(R.id.remark);
        clockimg = findViewById(R.id.clockimage);
        taketime[0] = findViewById(R.id.taketimetxt1);
        taketime[1] = findViewById(R.id.taketimetxt2);
        taketime[2] = findViewById(R.id.taketimetxt3);
        taketime[3] = findViewById(R.id.taketimetxt4);
        for(int i = 0; i < 4; i++){ //set timepicker when onclick
            final int finalI = i;
            taketime[i].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    opentimepicker(finalI);
                }
            });
        }

        takemethod_spinner = findViewById(R.id.takemethod_spinner);
        takemethod_spinner.setAdapter(setupadapter(takemethoditem));

        takeday1Spn = findViewById(R.id.takeday1Spn);
        takeday2Spn = findViewById(R.id.takeday2Spn);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, takeday1item);
        adapter.setDropDownViewResource(R.layout.spinner_item);
        takeday1Spn.setAdapter(adapter);
        takeday1Spn.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                switch (position){
                    case 0: //每日
                        takeday2Spn.setVisibility(View.VISIBLE);
                        takeday2Spn.setAdapter(setupadapter(takeday2item2));
                        takeday2Spn.setSelection(takeday2pos);
                        break;
                    case 1: //每隔X天
                        takeday2Spn.setVisibility(View.VISIBLE);
                        takeday2Spn.setAdapter(setupadapter(takeday2item));
                        takeday2Spn.setSelection(takeday2pos);
                        break;
                    case 2: //每周
                        takeday2Spn.setVisibility(View.GONE);

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        takeday2Spn.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                if(takeday1Spn.getSelectedItemPosition() == 0) { //服用時間選每日
                    if (position > 0) {
                        clockimg.setVisibility(View.VISIBLE);
                        int i = 0;
                        for (i = 0; i < position; i++)
                            taketime[i].setVisibility(View.VISIBLE);
                        for (; i < 4; i++)
                            taketime[i].setVisibility(View.GONE);
                    } else {
                        clockimg.setVisibility(View.GONE);
                        for (int i = 0; i < 4; i++)
                            taketime[i].setVisibility(View.GONE);
                    }
                }
                else{ //服用時間選每周或每隔幾天 提醒一定只有一次
                    clockimg.setVisibility(View.VISIBLE);
                    taketime[0].setVisibility(View.VISIBLE);
                    for (int i = 1; i < 4; i++)
                        taketime[i].setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        taketiming_spinner = findViewById(R.id.taketiming_spinner);
        adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, taketimingitem);
        adapter.setDropDownViewResource(R.layout.spinner_item);
        taketiming_spinner.setAdapter(adapter);

        takeunit_spinner = findViewById(R.id.takeunit_spinner);
        adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, takeunititem);
        adapter.setDropDownViewResource(R.layout.spinner_item);
        takeunit_spinner.setAdapter(adapter);

        takeamountEdt = findViewById(R.id.takeamountEdt);
        takedayEdt = findViewById(R.id.takedayEdt);


        submitBtn = findViewById(R.id.submitBtn);
        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    JSONObject medicineInfo =  new JSONObject(getIntent().getStringExtra("medicine"));
                    String personalmedicinaID = medicineInfo.getString("personalMedicineID");
                    update_medicine(personalmedicinaID);
//                update_reminder(REMINDER_TYPE); //Undone
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

        fetch_data();
    }
    @Override
    protected void onDestroy(){
        super.onDestroy();
        localDB.close(); // 關閉資料庫
    }

    //spinner adapter
    ArrayAdapter<String> setupadapter(String[] item){
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, item){
            @Override
            public boolean isEnabled(int position){
                if(position == 0)
                    return false;
                else
                    return true;
            }
            @Override
            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView tv = (TextView) view;
                if(position == 0){
                    // Set the hint text color gray
                    tv.setTextColor(Color.GRAY);
                }
                else {
                    tv.setTextColor(Color.BLACK);
                }
                return view;
            }
        };
        adapter.setDropDownViewResource(R.layout.spinner_item);
        return adapter;
    }
    //open timepickerdialog
    private void opentimepicker(final int position){
        int hour, minute;
        String[] time= taketime[position].getText().toString().split(":");
        hour = Integer.valueOf(time[0]);
        minute = Integer.valueOf(time[1]);
//        Calendar cal = Calendar.getInstance();
//        int hour = cal.get(Calendar.HOUR_OF_DAY);
//        int minute = cal.get(Calendar.MINUTE);
        new TimePickerDialog(reminder_edit_medicine.this, new TimePickerDialog.OnTimeSetListener(){
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                String h = String.valueOf(hourOfDay);
                String m = String.valueOf(minute);
                if(hourOfDay < 10)
                    h = "0" + h;
                if(minute < 10)
                    m = "0" + m;
                taketime[position].setText(h + ":" + m);
            }
        }, hour, minute, false).show();
    }



    //設定編輯畫面
    private void fetch_data(){
        try {
            JSONObject medicineInfo =  new JSONObject(getIntent().getStringExtra("medicine"));
            medicine_name.setText(medicineInfo.getString("name"));
            String imgURL = "http://140.117.71.74/graduation/medicineimage/"+medicineInfo.getString("engname") + ".jpg";
            Picasso.get()
                    .load(imgURL)
                    .error(R.drawable.ic_add_a_photo_black_24dp)
                    .into(medicineImg);

            int position = Arrays.asList(takemethoditem_TWN).indexOf(medicineInfo.getString("method"));
            takemethod_spinner.setSelection(position);
            takedayEdt.setText(medicineInfo.getString("takedatevalue"));

            int repeatday = Integer.valueOf(medicineInfo.getString("repeatday"));
            switch (repeatday){
                case 1: { //每日
                    takeday1Spn.setSelection(0);
                    position = Arrays.asList(takeday2item2_TWN).indexOf(medicineInfo.getString("taketime") + "次");
                    takeday2pos = position;
                    takeday2Spn.setSelection(position);
                    break;
                }
                case 7: { //每周
                    takeday1Spn.setSelection(2);
                    break;
                }
                default: { //每隔X天
                    takeday1Spn.setSelection(1);
                    position = Arrays.asList(takeday2item_TWN).indexOf(medicineInfo.getString("repeatday") + "天");
                    takeday2pos = position;
                }
            }

            JSONArray remindertime = medicineInfo.getJSONArray("remindertime");
            for(int i = 0; i < remindertime.length(); i++)
                taketime[i].setText(remindertime.getString(i).substring(0, 5));
            position = Arrays.asList(taketimingitem_TWN).indexOf(medicineInfo.getString("taketiming"));
            taketiming_spinner.setSelection(position);

            takeamountEdt.setText(medicineInfo.getString("takeamount"));
            position = Arrays.asList(takeunititem_TWN).indexOf(medicineInfo.getString("takeunit"));
            takeunit_spinner.setSelection(position);
            remark.setText(medicineInfo.getString("remark"));

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    //回傳更新的data
    private void set_return_data(){

        try {
            JSONObject medicineInfo =  new JSONObject(getIntent().getStringExtra("medicine"));
            medicineInfo.put("takedatetype", "");
            medicineInfo.put("takedatevalue", takedayEdt.getText().toString());

            String times;
            if(takeday1Spn.getSelectedItemPosition() == 0) //每日
                times = takeday2item2_TWN[takeday2Spn.getSelectedItemPosition()].substring(0,1);
            else
                times = "1";
            medicineInfo.put("taketime", times);

            JSONArray remindertime = medicineInfo.getJSONArray("remindertime");
            for(int i = 0; i < Integer.valueOf(times); i++){
                remindertime.put(i, taketime[i].getText().toString());
            }
            medicineInfo.put("taketiming", taketimingitem_TWN[taketiming_spinner.getSelectedItemPosition()]);
            medicineInfo.put("takeamount", takeamountEdt.getText().toString());
            medicineInfo.put("takeunit", takeunititem_TWN[takeunit_spinner.getSelectedItemPosition()]);
            medicineInfo.put("remark", remark.getText().toString());

            String repeat = "";
            if(takeday1Spn.getSelectedItemPosition() == 0) //每日
                repeat = "1";
            else if(takeday1Spn.getSelectedItemPosition() == 1) { //每隔
                repeat = takeday1item_TWN[takeday2Spn.getSelectedItemPosition()].substring(0,1);
            }
            else   //每周
                repeat = "7";
            medicineInfo.put("repeatday", repeat);

            //回傳
            Intent intent = new Intent();
            intent.putExtra("medicine", medicineInfo.toString());
            setResult(RESULT_OK, intent);
            finish();
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
    //--------connect to MySQL----------
    private void update_medicine(final String personalmedicineID){
        String URL = "http://140.117.71.74/graduation/update_reminder.php";
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            Log.d("書出訊息:", String.valueOf(jsonObject));
                            String success =jsonObject.getString("success");
                            String message = jsonObject.getString("message");
                            if (success.equals("1")){
                                Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                                set_return_data(); //回傳更新的資料
                            }
                            else{
                                Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.d("錯誤訊息",e.toString());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(),"搜尋失敗"+error.toString(),Toast.LENGTH_SHORT).show();
                    }
                })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String>params = new HashMap<>();
                params.put("personalMedicineID", personalmedicineID);
                params.put("record_type", REMINDER_TYPE);
                params.put("takedatetype", "");
                params.put("takedatevalue", takedayEdt.getText().toString());

                String times;
                if(takeday1Spn.getSelectedItemPosition() == 0) //每日
                    times = takeday2item2_TWN[takeday2Spn.getSelectedItemPosition()].substring(0,1);
                else
                    times = "1";
                params.put("taketime", times);
                for(int i = 0; i < Integer.valueOf(times); i++){
                    params.put("taketime"+i, taketime[i].getText().toString());
                }
                params.put("taketiming", taketimingitem_TWN[taketiming_spinner.getSelectedItemPosition()]);
                params.put("takeamount", takeamountEdt.getText().toString());
                params.put("takeunit", takeunititem_TWN[takeunit_spinner.getSelectedItemPosition()]);
                params.put("remark", remark.getText().toString());

                String repeat = "";
                if(takeday1Spn.getSelectedItemPosition() == 0) //每日
                    repeat = "1";
                else if(takeday1Spn.getSelectedItemPosition() == 1) { //每隔
                    repeat = takeday1item_TWN[takeday2Spn.getSelectedItemPosition()].substring(0,1);
                }
                else   //每周
                    repeat = "7";
                params.put("repeatday", repeat);
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    private void update_reminder(String reminder_type){

    }


    //--介面部分
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
//                mDrawerLayout.openDrawer(Gravity.RIGHT);
                finish();
                return true;
            case R.id.user_info:
                mDrawerLayout.openDrawer(Gravity.RIGHT);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.game_menu, menu);
        //set pattent name of menu
        MenuItem patientname = menu.findItem(R.id.patient_name);
        patientname.setTitle(localDB.getpatientname());
        return true;
    }
//    介面部分--

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected( MenuItem item) {
            BottomNavigation bn = new BottomNavigation(reminder_edit_medicine.this, item.getItemId());
            return true;
        }

    };


}
