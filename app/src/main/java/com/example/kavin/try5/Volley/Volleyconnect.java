package com.example.kavin.try5.Volley;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class Volleyconnect {
    private Context context;
    public Volleyconnect(Context ctx){
        this.context = ctx;
    }

    public interface VolleyCallback{
        void onSuccess(String result);
    }
    public void delete_record(final String record_type, final String id, final VolleyCallback callback){
        String URL = "http://140.117.71.74/graduation/delete_record.php";
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            Log.d("書出訊息:", String.valueOf(jsonObject));
                            String success =jsonObject.getString("success");
                            String message = jsonObject.getString("message");
                            callback.onSuccess(success);
                            if (success.equals("1")){
                                Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                            }
                            else{
                                Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.d("錯誤訊息",e.toString());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(context,"搜尋失敗"+error.toString(),Toast.LENGTH_SHORT).show();
                    }
                })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String>params = new HashMap<>();
                params.put("record_type", record_type);
                params.put("recordID", id);
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        requestQueue.add(stringRequest);
    }

    //讀取權限
    public void fetch_permission(final String caretakerid, final String patientid,final VolleyCallback callback){
        String URL = "http://140.117.71.74/graduation/fetch_single_permission.php";
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            Log.d("書出訊息:", String.valueOf(jsonObject));
                            String success =jsonObject.getString("success");
                            String message = jsonObject.getString("message");
                            String permission = jsonObject.getString("permission");
                            callback.onSuccess(permission); //回傳權限
                            if (success.equals("1")){
//                                Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                            }
                            else{
                                Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.d("錯誤訊息",e.toString());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(context,"搜尋失敗"+error.toString(),Toast.LENGTH_SHORT).show();
                    }
                })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String>params = new HashMap<>();
                params.put("caretakerID", caretakerid);
                params.put("patientID", patientid);
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        requestQueue.add(stringRequest);
    }

    //因為每日紀錄中有種頁面(數據/圖表) 所以也要傳view的名稱
    public void hideTextViewicon(String iconname, String viewname){
        int viewid = context.getResources().getIdentifier(viewname, "id", context.getPackageName());
        int imgid = context.getResources().getIdentifier(iconname, "id", context.getPackageName());

        View view = ((Activity)context).findViewById(viewid);
        TextView icon = view.findViewById(imgid);
        icon.setVisibility(View.INVISIBLE);
    }
    //每日紀錄中只有一種頁面的情況
    public void hideTextViewicon(String iconname){
        int imgid = context.getResources().getIdentifier(iconname, "id", context.getPackageName());
        TextView icon = ((Activity)context).findViewById(imgid);
        icon.setVisibility(View.INVISIBLE);
    }

    public void hideImageViewicon(String iconname){
        int id = context.getResources().getIdentifier(iconname, "id", context.getPackageName());
        ImageView icon = ((Activity)context).findViewById(id);
        icon.setVisibility(View.INVISIBLE);
    }

    //查看是否有用藥互斥
    public void checkeffect(final String patientID, final String medname, final VolleyCallback callback){
        String URL = "http://140.117.71.74/graduation/fetch_medicineeffect.php";
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            Log.d("書出訊息:", String.valueOf(jsonObject));
                            String success =jsonObject.getString("success");
                            String message = jsonObject.getString("message");
                            callback.onSuccess(response);
                            if (success.equals("1")){
//                                Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                            }
                            else{
                                Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.d("錯誤訊息",e.toString());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(context,"搜尋失敗"+error.toString(),Toast.LENGTH_SHORT).show();
                    }
                })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String>params = new HashMap<>();
                params.put("patientID", patientID);
                params.put("medicinename", medname);
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        requestQueue.add(stringRequest);
    }
}

