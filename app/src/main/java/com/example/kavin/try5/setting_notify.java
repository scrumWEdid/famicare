package com.example.kavin.try5;

import android.content.Intent;
import android.database.Cursor;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

public class setting_notify extends AppCompatActivity {
    private DrawerLayout mDrawerLayout;
    private localDatabase localDB = null;
    private Cursor cursor = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting_notify);

        BottomNavigationView bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottom_navigation_view);
        bottomNavigationView.setSelectedItemId(R.id.nav_description);
        bottomNavigationView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        localDB = new localDatabase(this);
        localDB.open();
        //----介面部分
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp);

        mDrawerLayout = findViewById(R.id.drawer_layout);

        NavigationView navigationView = findViewById(R.id.nav_view);

        //drawer nav header on click的部分
        View headerview = navigationView.getHeaderView(0);

        TextView header_PatientID = headerview.findViewById(R.id.nav_headerPatientIDTextView);
        header_PatientID.setText("ID : " + localDB.getpatientinfo());
        TextView header_PatientName = headerview.findViewById(R.id.nav_headerPatientNameTextView);
        header_PatientName.setText(localDB.getpatientname());

        LinearLayout header = (LinearLayout) headerview.findViewById(R.id.nav_view_layout);
        header.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent patientInfoIntent = new Intent(setting_notify.this,patientInfoActivity.class);
                setting_notify.this.startActivity(patientInfoIntent);
                mDrawerLayout.closeDrawers();
            }
        });

        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        // set item as selected to persist highlight
                        menuItem.setChecked(true);
                        // close drawer when item is tapped
                        mDrawerLayout.closeDrawers();

                        // Add code here to update the UI based on the item selected
                        // For example, swap UI fragments here

                        switch(menuItem.getItemId()){
                            case R.id.nav_switch_patient:
                                Intent switchIntent = new Intent(setting_notify.this,ChoosePatientActivity.class);
                                setting_notify.this.startActivityForResult(switchIntent, 1);
                                mDrawerLayout.closeDrawers();
                                return true;
                            case R.id.nav_manage_patient:
                                Intent managePatientIntent = new Intent(setting_notify.this,CareTakerManagePermissionActivity.class);
                                setting_notify.this.startActivity(managePatientIntent);
                                mDrawerLayout.closeDrawers();
                                return true;
                            case R.id.nav_member_info:
                                Intent MemberInfoIntent = new Intent(setting_notify.this,MemberInfoActivity.class);
                                setting_notify.this.startActivity(MemberInfoIntent);
                                mDrawerLayout.closeDrawers();
                                return true;
                            case R.id.nav_setting:
                                return true;
                        }

                        return true;
                    }
                });
// 介面部分-----


    }

    //--介面部分
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
//                mDrawerLayout.openDrawer(Gravity.RIGHT);
                setting_notify.this.finish();
                return true;
            case R.id.user_info:
                mDrawerLayout.openDrawer(Gravity.RIGHT);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.game_menu, menu);
        //set pattent name of menu
        MenuItem patientname = menu.findItem(R.id.patient_name);
        patientname.setTitle(localDB.getpatientname());
        return true;
    }
    //    介面部分--
    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected( MenuItem item) {
            BottomNavigation bn = new BottomNavigation(setting_notify.this, item.getItemId());
            return true;
        }

    };
}
